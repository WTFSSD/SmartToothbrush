//
//  APIHelper+User.h
//  Base
//
//  Created by admin on 2017/1/17.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "APIHelper.h"

@interface APIHelper (User)

- (void)regist:(NSString *)account
      password:(NSString *)pw
          code:(NSString *)code
      complete:(ApiRequestCompleteBlock)complete;

- (void)fetchCode:(NSString *)account
             type:(NSInteger)type
                complete:(ApiRequestCompleteBlock)complete;

- (void)login:(NSString *)account
     password:(NSString *)pw
     complete:(ApiRequestCompleteBlock)complete;

- (void)logout;

- (void)changePassword:(NSString*)newPassword
              complete:(ApiRequestCompleteBlock)complete;

- (void)updateUserInfo:(NSString *)nickname
                  face:(NSString *)face
                   sex:(NSInteger)sex
              birthday:(NSString *)birthday
                  area:(NSString *)areaID
              complete:(ApiRequestCompleteBlock)complete;

@end
