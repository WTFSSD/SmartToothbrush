//
//  APIHelper+Mine.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/29.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "APIHelper+Mine.h"

@implementation APIHelper (Mine)

- (void)mineBrushComplete:(ApiRequestCompleteBlock)complete {
    [APIHELPER postWithURL:@"Member/my_toothbrush" param:nil complete:complete];
}

- (void)mineBindBrushListWithP:(NSInteger)p
                         limit:(NSInteger)limit
                      complete:(ApiRequestCompleteBlock)complete {
    NSMutableDictionary* param = [NSMutableDictionary dictionary];
    [param safe_setValue:@(p) forKey:@"p"];
    [param safe_setValue:@(limit) forKey:@"limit"];
    [APIHELPER postWithURL:@"Member/get_bind_toothbrush" param:param complete:complete];
}

- (void)unBindBrushWithId:(NSInteger)tbId
                 complete:(ApiRequestCompleteBlock)complete {
    NSMutableDictionary* param = [NSMutableDictionary dictionary];
    [param safe_setValue:@(tbId) forKey:@"id"];
    [APIHELPER postWithURL:@"Member/unbind_toothbrush" param:param complete:complete];
}

- (void)bindBrushWithNo:(NSString*)tbNo
               nickname:(NSString*)nickname
                 isMine:(NSInteger)isMine
               complete:(ApiRequestCompleteBlock)complete {
    NSMutableDictionary* param = [NSMutableDictionary dictionary];
    [param safe_setValue:tbNo forKey:@"tb_no"];
    [param safe_setValue:nickname forKey:@"nickname"];
    [param safe_setValue:@(isMine) forKey:@"is_my"];
    [APIHELPER postWithURL:@"Member/bind_toothbrush" param:param complete:complete];
}

- (void)healthQuestionComplete:(ApiRequestCompleteBlock)complete {
    [APIHELPER postWithURL:@"Health/questions" param:nil complete:complete];
}

- (void)healthSubmitAnswer:(NSDictionary*)answer
                  complete:(ApiRequestCompleteBlock)complete {
    NSMutableDictionary* param = [NSMutableDictionary dictionary];
    [param safe_setValue:[answer yy_modelToJSONString] forKey:@"reply"];
    [APIHELPER postWithURL:@"Health/submit" param:param complete:complete];
}

- (void)healthResultComplete:(ApiRequestCompleteBlock)complete {
    [APIHELPER postWithURL:@"Health/result" param:nil complete:complete];
}


/**
 获取刷牙方案

 @param complete <#complete description#>
 */
- (void)brushSolutionComplete:(ApiRequestCompleteBlock)complete {
    [APIHELPER postWithURL:@"Solution/get" param:nil complete:complete];
}

- (void)brushCustomSolutionComplete:(ApiRequestCompleteBlock)complete {
    [APIHELPER postWithURL:@"Solution/get_custom" param:nil complete:complete];
}

- (void)brushSolutionSubmitWithTbNo:(NSString*)tbNo
                             modeId:(NSInteger)modeId
                               time:(NSInteger)time
                              press:(NSInteger)press
                          fitStatus:(NSInteger)fitStatus
                            impArea:(NSString*)impArea
                           complete:(ApiRequestCompleteBlock)complete {
    NSMutableDictionary* param = [NSMutableDictionary dictionary];
    [param safe_setValue:tbNo forKey:@"tb_no"];
    [param safe_setValue:@(modeId) forKey:@"mode_id"];
    if (time>=0) {
        [param safe_setValue:@(time) forKey:@"hours"];
    }
    if (press>=0) {
        [param safe_setValue:@(press) forKey:@"press"];
    }
    if (fitStatus>=0) {
        [param safe_setValue:@(fitStatus) forKey:@"fit_status"];
    }
    [param safe_setValue:impArea forKey:@"imp_area"];
    [APIHELPER postWithURL:@"Solution/set" param:param complete:complete];
}

- (void)messageListPage:(NSInteger)p
                  limit:(NSInteger)limit
               complete:(ApiRequestCompleteBlock)complete {
    NSMutableDictionary* param = [NSMutableDictionary dictionary];
    [param safe_setValue:@(p) forKey:@"p"];
    [param safe_setValue:@(limit) forKey:@"limit"];
    [APIHELPER postWithURL:@"Member/message" param:param complete:complete];
}

- (void)fetchCommonProblemsComplete:(ApiRequestCompleteBlock)complete {
    [APIHELPER postWithURL:@"Problem/get" param:nil complete:complete];
}

- (void)fetchUsingIntroductionComplete:(ApiRequestCompleteBlock)complete {
    [APIHELPER postWithURL:@"Public/introduction" param:nil complete:complete];
}

@end
