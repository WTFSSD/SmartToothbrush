//
//  APIHelper+Info.m
//  SmartToothbrush
//
//  Created by admin on 2018/6/8.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "APIHelper+Info.h"

@implementation APIHelper (Info)

- (void)fetchInfoListWithP:(NSInteger)p
                     limit:(NSInteger)limit
                  complete:(ApiRequestCompleteBlock)complete {
    NSMutableDictionary* param = [NSMutableDictionary dictionary];
    [param safe_setValue:@(p) forKey:@"p"];
    [param safe_setValue:@(limit) forKey:@"limit"];
    [APIHELPER postWithURL:@"Wiki/lists" param:param complete:complete];
}

- (void)infoDetailWithId:(NSInteger)Id
                complete:(ApiRequestCompleteBlock)complete {
    NSMutableDictionary* param = [NSMutableDictionary dictionary];
    [param safe_setValue:@(Id) forKey:@"id"];
    [APIHELPER postWithURL:@"Wiki/detail" param:param complete:complete];
}

@end
