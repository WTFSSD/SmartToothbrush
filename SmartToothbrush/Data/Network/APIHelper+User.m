//
//  APIHelper+User.m
//  Base
//
//  Created by admin on 2017/1/17.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "APIHelper+User.h"
#import "ShareSDKThirdLoginHelper.h"
#import "NSString+HYUtilities.h"
#import "JPUSHService.h"

@implementation APIHelper (User)

- (void)regist:(NSString *)account
      password:(NSString *)pw
          code:(NSString *)code
      complete:(ApiRequestCompleteBlock)complete{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param safe_setValue:account forKey:@"account"];
    [param safe_setValue:pw forKey:@"password"];
    [param safe_setValue:code forKey:@"code"];
    
    [APIHELPER postWithURL:@"User/register" param:param complete:complete];
}

- (void)fetchCode:(NSString *)account
             type:(NSInteger)type
         complete:(ApiRequestCompleteBlock)complete {
    
    NSMutableDictionary* param = [NSMutableDictionary dictionary];
    [param safe_setValue:account forKey:@"account"];
    [param safe_setValue:@(type) forKey:@"type"];
    [APIHELPER postWithURL:@"User/verify_code" param:param complete:complete];
}

- (void)login:(NSString *)account
     password:(NSString *)pw
     complete:(ApiRequestCompleteBlock)complete{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param safe_setValue:account forKey:@"account"];
    [param safe_setValue:pw forKey:@"password"];
    [APIHELPER postWithURL:@"User/login" param:param complete:complete];
}

- (void)logout{
    //退出登陆，清空UserAuth和userInfo
    [Global setUserAuth:nil];
    self.userInfo = nil;
    [ShareSDKThirdLoginHelper logout];
    kCleanPassword;
}

- (void)changePassword:(NSString*)newPassword
              complete:(ApiRequestCompleteBlock)complete {
    
}

- (void)updateUserInfo:(NSString *)nickname
                  face:(NSString *)face
                   sex:(NSInteger)sex
              birthday:(NSString *)birthday
                  area:(NSString *)areaID
              complete:(ApiRequestCompleteBlock)complete {
    
}

@end
