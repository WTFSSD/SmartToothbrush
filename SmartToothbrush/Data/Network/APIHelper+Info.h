//
//  APIHelper+Info.h
//  SmartToothbrush
//
//  Created by admin on 2018/6/8.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "APIHelper.h"

@interface APIHelper (Info)

- (void)fetchInfoListWithP:(NSInteger)p
                     limit:(NSInteger)limit
                  complete:(ApiRequestCompleteBlock)complete;

- (void)infoDetailWithId:(NSInteger)Id
                complete:(ApiRequestCompleteBlock)complete;


@end
