//
//  HttpHelper.m
//  Base
//
//  Created by admin on 2017/1/17.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "HttpHelper.h"
#import "HYServer.h"
#import "NSError+HYError.h"
#import "NSDictionary+Safety.h"
#import "NSString+json.h"

#define SHOWNETDEBUG 0
@interface HttpHelper ()

@end

@implementation HttpHelper

+(HttpHelper *)shareInstance {
    static HttpHelper* helper = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        if (helper == nil) {
            helper = [[HttpHelper alloc] init];
            helper.requestSerializer = [AFHTTPRequestSerializer serializer];
            helper.responseSerializer = [AFJSONResponseSerializer serializer];
            helper.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        }
    });
    return helper;
}

- (instancetype)init
{
    self = [super initWithBaseURL:[[HYServer defaultServer] APIEndpoint]];
    if (self) {
        
        self.requestSerializer.timeoutInterval = 10.0f;
        
        [self.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            switch (status) {
                case AFNetworkReachabilityStatusNotReachable:
                    break;
                case AFNetworkReachabilityStatusUnknown:
                {
                    DLog(@"网络有问题了");
                    [kNotificationCenter postNotificationName:kNetNotReachabilityNotification object:nil];
                }
                    break;
                case AFNetworkReachabilityStatusReachableViaWiFi:
                {
                    DLog(@"wifi");
                }
                    break;
                case AFNetworkReachabilityStatusReachableViaWWAN:
                {
                    DLog(@"2G 3G");
                }
                    break;
                default:
                    break;
            }
        }];
        [self.reachabilityManager startMonitoring];
    }
    return self;
}

-(NSURLSessionDataTask *)getWithURL:(NSString *)relativeurl param:(NSDictionary *)parameters complete:(ApiRequestCompleteBlock)complete {
    NSMutableDictionary* param = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if ([Global userAuth]) {
        [param safe_setValue:[Global userAuth] forKey:@"key"];
    }
    return [self GET:relativeurl parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#ifdef SHOWNETDEBUG
#if 1
        NSString *queryLog = [[NSString alloc] initWithData:task.originalRequest.HTTPBody encoding:NSUTF8StringEncoding];
        NSString *urlLog = [task.originalRequest.URL.absoluteString stringByAppendingString: [NSString stringWithFormat:task.originalRequest.URL.query ? @"&%@" : @"?%@", queryLog]];
        DLog(@"http request success ---  GET---:\n%@\n", urlLog);
#endif
#endif
        
        NSDictionary *responseDict = responseObject;
        NSInteger code = [[responseDict objectForKey:@"code"] integerValue];
        if (code == 1) {
            if (complete) {
                //NSDictionary *dataDict = [responseDict objectForKey:@"data"];
                complete(YES, responseObject, nil);
            }
        } else {
            if (complete) {
                NSString *msg = [responseDict objectForKey:@"msg"];
                complete(NO, nil, [NSError errorWithServerErrorCode:code message:msg]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
#ifdef SHOWNETDEBUG
#if 1
        NSString *queryLog = [[NSString alloc] initWithData:task.originalRequest.HTTPBody encoding:NSUTF8StringEncoding];
        NSString *urlLog = [task.originalRequest.URL.absoluteString stringByAppendingString: [NSString stringWithFormat:task.originalRequest.URL.query ? @"&%@" : @"?%@", queryLog]];
        DLog(@"http request failure ---  GET---:\n%@\n", urlLog);
#endif
#endif
        if(complete){
            complete(NO, nil, error);}
    }];
}

-(NSURLSessionDataTask *)postWithURL:(NSString *)relativeurl param:(NSDictionary *)parameters complete:(ApiRequestCompleteBlock)complete {
    NSMutableDictionary* param = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if ([UserInfoModel share].key) {
        [param safe_setValue:[UserInfoModel share].key forKey:@"key"];
    }
    //打印POST的JSON数据
    NSString* jsonStr = [NSString jsonStringWithDictionary:param];
    DLog(@"%@",jsonStr);
    return [self POST:relativeurl parameters:param progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#ifdef SHOWNETDEBUG
#if 1
        NSString *queryLog = [[NSString alloc] initWithData:task.originalRequest.HTTPBody encoding:NSUTF8StringEncoding];
        NSString *urlLog = [task.originalRequest.URL.absoluteString stringByAppendingString: [NSString stringWithFormat:task.originalRequest.URL.query ? @"&%@" : @"?%@", queryLog]];
        DLog(@"http request success ---  POST---:\n%@\n", urlLog);
#endif
#endif
        NSDictionary *responseDict = responseObject;
        NSInteger code = [[responseDict objectForKey:@"code"] integerValue];
        if (code == 1) {
            if (complete) {
                complete(YES, responseObject, nil);
            }
        } else {
            if (complete) {
                NSString *msg = [responseDict objectForKey:@"msg"];
                complete(NO, nil, [NSError errorWithServerErrorCode:code message:msg]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
#ifdef SHOWNETDEBUG
#if 1
        NSString *queryLog = [[NSString alloc] initWithData:task.originalRequest.HTTPBody encoding:NSUTF8StringEncoding];
        NSString *urlLog = [task.originalRequest.URL.absoluteString stringByAppendingString: [NSString stringWithFormat:task.originalRequest.URL.query ? @"&%@" : @"?%@", queryLog]];
        DLog(@"http request failure ---  POST---:\n%@\n", urlLog);
#endif
#endif
        if (complete) {
            complete(NO, nil, error);
        }
    }];
}


- (NSURLSessionDataTask*)formDataPostWithURL:(NSString*)relativeurl param:(NSDictionary*)parameters complete:(ApiRequestCompleteBlock)complete {
    NSMutableDictionary* param = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if ([Global userAuth]) {
        [param safe_setValue:[Global userAuth] forKey:@"key"];
    }
    return [self POST:relativeurl parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#ifdef SHOWNETDEBUG
#if 1
        NSString *queryLog = [[NSString alloc] initWithData:task.originalRequest.HTTPBody encoding:NSUTF8StringEncoding];
        NSString *urlLog = [task.originalRequest.URL.absoluteString stringByAppendingString: [NSString stringWithFormat:task.originalRequest.URL.query ? @"&%@" : @"?%@", queryLog]];
        DLog(@"http request success ---  POST---:\n%@\n", urlLog);
#endif
#endif
        NSDictionary *responseDict = responseObject;
        NSInteger code = [[responseDict objectForKey:@"code"] integerValue];
        if (code == 1) {
            if (complete) {
                complete(YES, responseObject, nil);
            }
        } else {
            if (complete) {
                NSString *msg = [responseDict objectForKey:@"msg"];
                complete(NO, nil, [NSError errorWithServerErrorCode:code message:msg]);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
#ifdef SHOWNETDEBUG
#if 1
        NSString *queryLog = [[NSString alloc] initWithData:task.originalRequest.HTTPBody encoding:NSUTF8StringEncoding];
        NSString *urlLog = [task.originalRequest.URL.absoluteString stringByAppendingString: [NSString stringWithFormat:task.originalRequest.URL.query ? @"&%@" : @"?%@", queryLog]];
        DLog(@"http request failure ---  POST---:\n%@\n", urlLog);
#endif
#endif
        if (complete) {
            complete(NO, nil, error);
        }
    }];
}

-(NSURLSessionDataTask *)uploadFileToServerByURL:(NSString *)relativeurl param:(NSDictionary *)parameters file:(NSData *)filedata fileURL:(NSURL *)fileURL filename:(NSString *)filename mimeType:(NSString *)mimeType progress:(void (^)(NSProgress *))uploadProgress complete:(ApiRequestCompleteBlock)complete {
    
    NSMutableDictionary* param = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if ([UserInfoModel share].key) {
        [param safe_setValue:[UserInfoModel share].key forKey:@"key"];
    }
    return [self POST:relativeurl parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if (filedata) {
            [formData appendPartWithFileData:filedata name:@"upfile" fileName:filename mimeType:mimeType];
        }else if(fileURL){
            [formData appendPartWithFileURL:fileURL name:@"upfile" fileName:filename mimeType:mimeType error:nil];
        }
    } progress:uploadProgress
              success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                  NSDictionary *responseDict = responseObject;
                  NSInteger code = [[responseDict objectForKey:@"code"] integerValue];
                  if (code == 1) {
                      if (complete) {
                          //NSDictionary *dataDict = [responseDict objectForKey:@"data"];
                          complete(YES, responseObject, nil);
                      }
                  } else {
                      if (complete) {
                          NSString *msg = [responseDict objectForKey:@"msg"];
                          complete(NO, nil, [NSError errorWithServerErrorCode:code message:msg]);
                      }
                  }
              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
#ifdef SHOWNETDEBUG
#if 1
                  NSString *queryLog = [[NSString alloc] initWithData:task.originalRequest.HTTPBody encoding:NSUTF8StringEncoding];
                  NSString *urlLog = [task.originalRequest.URL.absoluteString stringByAppendingString: [NSString stringWithFormat:task.originalRequest.URL.query ? @"&%@" : @"?%@", queryLog]];
                  DLog(@"http request failure ---  UPLOAD---:\n%@\n", urlLog);
#endif
#endif
                  
                  if(complete){
                      complete(NO, nil, error);}
              }];
}

@end
