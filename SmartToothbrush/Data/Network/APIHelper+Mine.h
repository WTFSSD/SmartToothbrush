//
//  APIHelper+Mine.h
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/29.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "APIHelper.h"

@interface APIHelper (Mine)

- (void)mineBrushComplete:(ApiRequestCompleteBlock)complete;

- (void)mineBindBrushListWithP:(NSInteger)p
                         limit:(NSInteger)limit
                      complete:(ApiRequestCompleteBlock)complete;

- (void)unBindBrushWithId:(NSInteger)tbId
                 complete:(ApiRequestCompleteBlock)complete;

- (void)bindBrushWithNo:(NSString*)tbNo
               nickname:(NSString*)nickname
                 isMine:(NSInteger)isMine
               complete:(ApiRequestCompleteBlock)complete;

- (void)healthQuestionComplete:(ApiRequestCompleteBlock)complete;

- (void)healthSubmitAnswer:(NSDictionary*)answer
                  complete:(ApiRequestCompleteBlock)complete;

- (void)healthResultComplete:(ApiRequestCompleteBlock)complete;

- (void)brushSolutionComplete:(ApiRequestCompleteBlock)complete;

- (void)brushCustomSolutionComplete:(ApiRequestCompleteBlock)complete;

- (void)brushSolutionSubmitWithTbNo:(NSString*)tbNo
                             modeId:(NSInteger)modeId
                              time:(NSInteger)time
                              press:(NSInteger)press
                          fitStatus:(NSInteger)fitStatus
                            impArea:(NSString*)impArea
                           complete:(ApiRequestCompleteBlock)complete;

- (void)messageListPage:(NSInteger)p
                  limit:(NSInteger)limit
               complete:(ApiRequestCompleteBlock)complete;

- (void)fetchCommonProblemsComplete:(ApiRequestCompleteBlock)complete;

- (void)fetchUsingIntroductionComplete:(ApiRequestCompleteBlock)complete;

@end
