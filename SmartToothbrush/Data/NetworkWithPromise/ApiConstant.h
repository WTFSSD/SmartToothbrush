//
//  ApiConstant.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/6/9.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#ifndef ApiConstant_h
#define ApiConstant_h



#define SERVER_URL @"http://udo.lemobit.com/index.php"
///请求路径
#define REQUEST_URL(x)  [NSString stringWithFormat:@"%@/%@",SERVER_URL,x]

///引导页
#define API_GUIDE @"Api/Public/launch"
///登录
#define API_LOGIN @"Api/User/login"
///注册
#define API_REGEISTER @"Api/User/register"
///验证码
#define API_VERIFY_CODE @"Api/User/verify_code"

///问卷调查
#define API_MEMBER_SURVER @"Api/Member/survey"

///首页
#define API_MEMBER_INDEX @"Api/Member/index"

///我的牙刷
#define API_MEMBER_MY_TOOTHBRUSH @"Api/Member/my_toothbrush"
///意见反馈
#define API_FEEDBACK @"Api/Member/feedback"

///健康等级状态
#define API_GRADE_STATE @"Api/Health/result"

///刷牙数据
#define API_RECORD  @"Api/Member/record"

///绑定牙刷
#define  API_MEMBER_BIND_TOOTHBRUSH @"Api/Member/bind_toothbrush"



///刷牙记录(日)
#define API_MEMBER_HISTORY_DAY @"Api/Member/history_day"
///刷牙记录(月)
#define API_MEMBER_HISTORY_MOUTH @"Api/Member/history_month"

///刷牙记录(季)
#define API_MEMBER_HISTORY_QUARTER @"Api/Member/history_quarte"

///刷牙记录(年)
#define API_MEMBER_HISTORY_YEAR @"Api/Member/history_year"

///牙医预约
#define API_APPOINTMENT @"Api/Member/dentist_order"

///个人资料
#define API_PERSONAL_INFOMARION @"Api/Member/get_profile"
#define API_CHANGE_PERSONAL_INFOMATION @"Api/Member/set_profile"

///微信登录
#define API_USER_WXLOGIN @"Api/User/wxlogin"

///facebook登录 (web登录)
#define API_USER_FBLOGIN @"https://udo.udosmart.cn/Api/User/fblogin.html"



///切换刷牙模式
#define API_BRUSH_TOGGLE_MODE @"Api/ToothBrush/toggle_mode"

///获取私人定制
#define  API_BRUSH_GTE_CUSTOM @"Api/Solution/get_custom"

///刷牙实时数据
#define API_BRUSH_BRUSHING @"Api/Member/brushing"

///问卷详情
#define API_MEMBER_SERVER @"Api/Member/get_survey"


//登录访问地址：https://udo.udosmart.cn/Api/User/fblogin.html
//登录成功跳转地址：https://udo.udosmart.cn/Api/User/fb_login_success.html
//取消登录跳转地址：https://udo.udosmart.cn/Api/User/cancel_callback.html
#endif /* ApiConstant_h */
