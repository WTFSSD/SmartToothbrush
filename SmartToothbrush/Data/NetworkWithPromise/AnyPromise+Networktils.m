//
//  AnyPromise+Networktils.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/6/9.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "AnyPromise+Networktils.h"

@implementation AnyPromise (Networktils)

-(RACSignal *)signal{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        self.then(^(id res){
            if(res){
                [subscriber sendNext:res];
                [subscriber sendCompleted];
            }
            return nil;
        });
        return nil;
    }];
    return signal;
}

@end
