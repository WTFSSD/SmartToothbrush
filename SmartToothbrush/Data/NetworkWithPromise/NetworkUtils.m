//
//  DemoUtils.m
//  Base
//
//  Created by WTFSSD on 2018/5/2.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "NetworkUtils.h"
#import <AFNetworking/AFNetworking.h>

#import "AppDelegate.h"
@interface NetworkUtils(){
    
}

@property(nonatomic,strong)AFHTTPSessionManager * manager;

+(instancetype)share;
@end;
static NetworkUtils* _instance = nil;
@implementation NetworkUtils


+(instancetype)share{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
        _instance.manager = [AFHTTPSessionManager manager];
        _instance.manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        _instance.manager.responseSerializer = [AFJSONResponseSerializer serializer];
        _instance.manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    });
    return _instance;
}





#pragma -mark 共有




+(AnyPromise *)requestWith:(HTTPMethod)method
                       url:(NSString *)url
                    params:(NSDictionary<NSString *,id> *)params{
    return [self requestWith:method url:url params:params headers:nil progress:nil obj:nil];
}

+(AnyPromise *)requestWith:(HTTPMethod)method
                       url:(NSString *)url
                    params:(NSDictionary<NSString *,id> *)params
                   headers:(NSDictionary *)headers{
    return [self requestWith:method url:url params:params headers:headers progress:nil obj:nil];
}

+(AnyPromise *)requestWith:(HTTPMethod)method
                       url:(NSString *)url
                    params:(NSDictionary<NSString *,id> *)params
                   headers:(NSDictionary *)headers
                  progress:(void (^)(NSProgress *))progress{
       return [self requestWith:method url:url params:params headers:headers progress:progress obj:nil];
}

+(AnyPromise *)requestWith:(HTTPMethod)method
                       url:(NSString *)url
                    params:(NSDictionary<NSString *,id> *)params
                   headers:(NSDictionary *)headers
                  progress:(void (^)(NSProgress *))progress
                       obj:(__unsafe_unretained Class)cls{
    
    AnyPromise * promise ;
    switch (method) {
        case HTTPMethodGET:{
            return [self GET:url params:params headers:headers progress:progress cls:cls];
            
        } break;
        case HTTPMethodPOST:{
            return [self POST:url params:params headers:headers progress:progress cls:cls];
            
        } break;
        case HTTPMethodPATCH:{
           return [self PATCH:url params:params headers:headers progress:progress cls:cls];
            
        } break;
        case HTTPMethodPUT:{
            return [self PUT:url params:params headers:headers progress:progress cls:cls];
            
        } break;
            
        case HTTPMethodDELETE:{
          return [self DELETE:url params:params headers:headers progress:progress cls:cls];
            
        } break;
        default:
            break;
    }
    
    return promise;
    
}










#pragma -mark 私有
+(AnyPromise *)GET:(NSString *)url
            params:(NSDictionary<NSString *,id> *)params
           headers:(NSDictionary *)headers
          progress:(void (^)(NSProgress *))progress
               cls:(Class)cls{
    
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        NetworkUtils * share = [self share];
        AnyPromise * promise = [AnyPromise promiseWithResolverBlock:^(PMKResolver res) {
            [share.manager GET:url parameters:params progress:progress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                
                NSLog(@"\n-------->get:%@\n-------->response:%@\n",url,responseObject);
                
                res(responseObject);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 [self showError:error];
            }];
        }];
    
   return promise.then(^id(id response){
        return [self chekWithResponse:response];
    }).then(^id(id response){
        return [self translateWith:response cls:cls];
    }).catch(^(NSError * error){
        [self showError:error];
    });
    
};


+(AnyPromise *)POST:(NSString *)url
             params:(NSDictionary<NSString *,id> *)params
            headers:(NSDictionary *)headers
           progress:(void (^)(NSProgress *))progress
                cls:(Class)cls{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSMutableDictionary* mutableParam = [NSMutableDictionary dictionaryWithDictionary:params];
    if ([UserInfoModel share].key) {
        [mutableParam safe_setValue:[UserInfoModel share].key forKey:@"key"];
    }
    
    NSLog(@"\npost:%@\nparams:%@",url,mutableParam);
    NetworkUtils * share = [self share];
    AnyPromise * promise = [AnyPromise promiseWithResolverBlock:^(PMKResolver res) {
        [share.manager POST:url parameters:mutableParam progress:progress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             NSLog(@"\n-------->post:%@\n-------->response:%@\n",url,responseObject);
            res(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             [self showError:error];
            
        }];
    }];
    
  return  promise.then(^id(id response){
        return [self chekWithResponse:response];
    }).then(^id(id response){
        return [self translateWith:response cls:cls];
    }).catch(^(NSError * error){
        [self showError:error];
    });
    
};

+(AnyPromise *)PATCH:(NSString *)url
              params:(NSDictionary<NSString *,id> *)params
             headers:(NSDictionary *)headers
            progress:(void (^)(NSProgress *))progress
                 cls:(Class)cls{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NetworkUtils * share = [self share];
    AnyPromise * promise = [AnyPromise promiseWithResolverBlock:^(PMKResolver res) {
        [share.manager PATCH:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             NSLog(@"\n-------->patch:%@\n-------->response:%@\n",url,responseObject);
             res(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self showError:error];
        }];
    }];
    
   return  promise.then(^id(id response){
        return [self chekWithResponse:response];
    }).then(^id(id response){
        return [self translateWith:response cls:cls];
    }).catch(^(NSError * error){
        [self showError:error];
    });
    
  
};


+(AnyPromise *)PUT:(NSString *)url
            params:(NSDictionary<NSString *,id> *)params
           headers:(NSDictionary *)headers
          progress:(void (^)(NSProgress *))progress
               cls:(Class)cls{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NetworkUtils * share = [self share];
    AnyPromise * promise = [AnyPromise promiseWithResolverBlock:^(PMKResolver res) {
        [share.manager PUT:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"\n-------->put:%@\n-------->response:%@\n",url,responseObject);
             res(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              [self showError:error];
        }];
    }];
    
   return promise.then(^id(id response){
        return [self chekWithResponse:response];
    }).then(^id(id response){
        return [self translateWith:response cls:cls];
    }).catch(^(NSError * error){
        [self showError:error];
    });
    

};

+(AnyPromise *)DELETE:(NSString *)url
               params:(NSDictionary<NSString *,id> *)params
              headers:(NSDictionary *)headers
             progress:(void (^)(NSProgress *))progress
                  cls:(Class)cls{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NetworkUtils * share = [self share];
    AnyPromise * promise = [AnyPromise promiseWithResolverBlock:^(PMKResolver res) {
        [share.manager DELETE:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"\n-------->delete:%@\n-------->response:%@\n",url,responseObject);
            res(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             [self showError:error];
        }];
    }];
    
   return promise.then(^id(id response){
        return [self chekWithResponse:response];
    }).then(^id(id response){
        return [self translateWith:response cls:cls];
    }).catch(^(NSError * error){
        [self showError:error];
    });
    
};

+(NSString*)httpMethodWith:(HTTPMethod)method{
    switch (method) {
        case HTTPMethodGET: return @"GET";
        case HTTPMethodPOST:return @"POST";
        case HTTPMethodPUT:return @"PUT";
        case HTTPMethodPATCH: return @"PATCH";
        case HTTPMethodDELETE:return @"DELETE";
        default:return @"GET";
           
    }
}



+(id)translateWith:(id)response cls:(Class)cls{
    if(!cls)return response;
    if(!response) return nil;
    id obj = [[cls alloc] init];
    [obj yy_modelSetWithDictionary:response[@"data"]];
    return obj;
}


+(id)chekWithResponse:(id)response{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    if(!response){
        @throw [NSError errorWithDomain:SERVER_URL code:-1 userInfo:@{NSLocalizedDescriptionKey:@"请求错误!!!"}];
    }
    if([response[@"code"] longValue] == 1){
        return response;
    }else{
        @throw [NSError errorWithDomain:SERVER_URL code:-1 userInfo:@{NSLocalizedDescriptionKey:response[@"msg"]}];
    }
};


+(void)showError:(NSError*)error{
     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    NSLog(@"请求错误------>\n:%@",error);
    AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [MBProgressHUD hy_showMessage:error.userInfo[NSLocalizedDescriptionKey] inView:delegate.window];
}


@end
