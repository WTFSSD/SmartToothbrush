//
//  UtilsDemoJsonModel.h
//  Base
//
//  Created by WTFSSD on 2018/5/2.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UtilsDemoJsonModel <NSObject>




@required
+(instancetype)instanceFrom:(NSDictionary*)jsonDict;




@end
