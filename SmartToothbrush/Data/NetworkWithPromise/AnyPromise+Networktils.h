//
//  AnyPromise+Networktils.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/6/9.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <PromiseKit/PromiseKit.h>

#import <ReactiveCocoa/ReactiveCocoa.h>
@interface AnyPromise (Networktils)

-(RACSignal*)signal;

@end
