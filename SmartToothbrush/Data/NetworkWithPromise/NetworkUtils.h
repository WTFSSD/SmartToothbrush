//
//  DemoUtils.h
//  Base
//
//  Created by WTFSSD on 2018/5/2.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AnyPromise+Networktils.h"
typedef NS_ENUM(NSUInteger, HTTPMethod) {
    HTTPMethodGET = 0,
    HTTPMethodPOST,
    HTTPMethodPUT,
    HTTPMethodPATCH,
    HTTPMethodDELETE,
};


@interface NetworkUtils : NSObject


/**
 *  url params
 */
+(AnyPromise*)requestWith:(HTTPMethod)method
                      url:(NSString*)url
                   params:(NSDictionary<NSString*,id>*)params;

/**
 *  url params headers
 */
+(AnyPromise*)requestWith:(HTTPMethod)method
                      url:(NSString*)url
                   params:(NSDictionary<NSString*,id>*)params
                  headers:(NSDictionary *)headers;

/**
 *  url params headers progress
 */
+(AnyPromise*)requestWith:(HTTPMethod)method
                      url:(NSString*)url
                   params:(NSDictionary<NSString*,id>*)params
                  headers:(NSDictionary *)headers
                 progress:(void (^)(NSProgress * progress))progress;


/**
 *  url params headers progress cls
 */
+(AnyPromise*)requestWith:(HTTPMethod)method
                      url:(NSString*)url
                   params:(NSDictionary<NSString*,id>*)params
                  headers:(NSDictionary *)headers
                  progress:(void (^)(NSProgress * progress))progress
                      obj:(Class)cls;




@end
