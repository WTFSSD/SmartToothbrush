//
//  BrushSettingControl.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/8/12.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>



///刷牙模式设置
typedef NS_ENUM(NSUInteger, BrushSettingModel) {
    ///美白模式
    BrushSettingModelWhite = 2,
    
    ///清洁模式
    BrushSettingModelClean = 1,
    
    ///敏感模式
    BrushSettingModelSensitive = 3,
    
    ///私人定制
    BrushSettingModelCustom = 6,
};


@interface BrushSettingControl : NSObject



/**
 *  切换刷牙模式
 *  @params model BrushSettingModel
 *  @return AnyPromise
 */
+(AnyPromise*)toggleBrushModelWith:(BrushSettingModel)model;


///获取刷牙实时数据
+(RACSignal*)getBurshingData;

///停止获取刷牙实时数据
+(void)stopGetBurshingData;

@end
