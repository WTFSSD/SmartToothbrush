//
//  BrushModel.m
//  SmartToothbrush
//
//  Created by admin on 2018/6/8.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BrushModel.h"

@implementation BrushModel : NSObject

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper{
    return @{
             @"tbId":@"id",
             @"tbVersion":@"tb_version",
             @"tbNo":@"tb_no",
             @"firmwareVersion":@"firmware_version",
             @"enableTime":@"enable_time",
             @"lifeTime":@"lifetime"
             };
}

@end
