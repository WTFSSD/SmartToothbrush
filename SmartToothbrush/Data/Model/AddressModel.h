//
//  AddressModel.h
//  Base
//
//  Created by admin on 2017/5/27.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "BaseModel.h"

@interface AddressModel : BaseModel

@property (nonatomic,strong)NSNumber* Id;
@property (nonatomic,strong)NSString* name;
@property (nonatomic,strong)NSString* phone;
@property (nonatomic,strong)NSString* addressName;

@end
