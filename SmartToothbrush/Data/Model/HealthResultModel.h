//
//  HealthResultModel.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/7/15.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HealthResultModel : NSObject

///评估状态 0 未填写 1已填写
@property(nonatomic,assign)NSInteger status;


///健康等级
@property(nonatomic,copy)NSString * grade;

///建议
@property(nonatomic,copy)NSString * advice;
@end
