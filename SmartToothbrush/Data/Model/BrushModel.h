//
//  BrushModel.h
//  SmartToothbrush
//
//  Created by admin on 2018/6/8.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BaseModel.h"
#import <Foundation/Foundation.h>
@interface BrushModel : NSObject

/*
 - id    整型    ID    -
 - tb_version    字符串    牙刷型号    -
 - tb_no    字符串    牙刷编号    -
 - firmware_version    字符串    固件版本    如：v2.1
 - enable_time    整型    启用时间    时间戳
 - lifetime     字符串 剩余使用寿命 如：00/90
 */

@property(nonatomic,assign) NSInteger tbId;

@property(nonatomic,copy) NSString* tbVersion;
@property(nonatomic,copy) NSString* tbNo;
@property(nonatomic,copy) NSString* firmwareVersion;
@property(nonatomic,assign) NSInteger enableTime;
@property(nonatomic,copy) NSString* lifeTime;

@end
