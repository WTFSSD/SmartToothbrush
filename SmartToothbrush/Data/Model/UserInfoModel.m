//
//  UserInfoModel.m
//  Base
//
//  Created by admin on 2017/3/2.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "UserInfoModel.h"


static UserInfoModel * _instance = nil;
@implementation UserInfoModel

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper{
    return @{
             @"userID":@"id",
             @"username":@"username",
             @"avatar":@"avatar",
             @"nickName":@"nickname",
             @"regTime":@"reg_time",
             };
}

+(instancetype)share{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[UserInfoModel alloc] init];
        _instance.key = @"";
        _instance.userID = @"";
        _instance.username = @"";
        _instance.avatar = @"";
        _instance.nickName = @"";
        _instance.regTime = @"";
        _instance.isLogin = NO;
    });
    return _instance;
}

+(void)store{
    NSString * jsonString = [[self share] yy_modelToJSONString];
    
    [[NSUserDefaults standardUserDefaults] setValue:jsonString forKey:@"UserInfoModel"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(instancetype)restore{
    
    NSString * jsonString =  [[NSUserDefaults standardUserDefaults] valueForKey:@"UserInfoModel"];
    if(!jsonString){
        return [self share];
    }
   

    [[self share] yy_modelSetWithJSON:jsonString];
     UserInfoModel * mode = [self share];
    [self store];
    return [self share];
}

+(void)updateWithData:(NSDictionary *)data {
    [[self share] yy_modelSetWithDictionary:data];
    [self store];
}

+(void)logout{
    [self share].isLogin = NO;
    [self share].key = nil;
    [self share].userID = nil;
    [self share].username = nil;
    [self share].avatar = nil;
    [self share].nickName = nil;
    [self share].regTime = nil;
    
//    [[self share] yy_modelSetWithDictionary:@{}];
    
    [self store];
    NSLog(@"share:%@",[self share]);
    
}

-(NSString *)key{
    if(!_key){
        return @"";
    }
    return _key;
}


@end
