//
//  UserInfoModel.h
//  Base
//
//  Created by admin on 2017/3/2.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "BaseModel.h"

@interface UserInfoModel : BaseModel

@property(nonatomic, strong) NSString *userID;
@property(nonatomic, strong) NSString *username;
@property(nonatomic, strong) NSString *avatar;
@property(nonatomic, strong) NSString *nickName;
@property(nonatomic, strong) NSString *regTime;      //注册时间
///用户标识
@property(nonatomic,strong)NSString * key;
///自己添加的属性
@property(nonatomic,assign)BOOL isLogin;




+(UserInfoModel*)share;

///本地恢复
+(UserInfoModel*)restore;
//本地存储
+(void)store;
///更新用户信息,返回新的用户信息
+(void)updateWithData:(NSDictionary*)data;

///登出
+(void)logout;
@end
