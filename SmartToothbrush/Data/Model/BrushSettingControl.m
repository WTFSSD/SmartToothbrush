//
//  BrushSettingControl.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/8/12.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BrushSettingControl.h"
static BrushSettingControl * _instance = nil;

@interface BrushSettingControl()

///获取刷牙实时数据的轮询定时器
@property(nonatomic,strong)NSTimer * getBrushingDataTimer;


@property(nonatomic)id <RACSubscriber>  getBrushingDataSubscriber;
@end

@implementation BrushSettingControl


+(BrushSettingControl*)share{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    return _instance;
}


+(AnyPromise *)toggleBrushModelWith:(BrushSettingModel)model{

    return [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_BRUSH_TOGGLE_MODE) params:@{@"mode_id":@(model)}];
};


+(RACSignal *)getBurshingData{
    RACSignal*signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        [self share].getBrushingDataSubscriber = subscriber;
        [self share].getBrushingDataTimer  =   [NSTimer bk_timerWithTimeInterval:1 block:^(NSTimer *timer) {
            [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_BRUSH_BRUSHING) params:@{}].then(^(id res){
                 [subscriber sendNext:res];
            });
        } repeats:YES];
        
        
        
        [[NSRunLoop mainRunLoop] addTimer:[self share].getBrushingDataTimer forMode:NSRunLoopCommonModes];
        return nil;
    }];
    
    return signal;
}

+(void)stopGetBurshingData{
    
    if([self share].getBrushingDataTimer){
        [[self share].getBrushingDataTimer invalidate];
    
    }
    [[self share].getBrushingDataSubscriber sendCompleted];
}

@end
