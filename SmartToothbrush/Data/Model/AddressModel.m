

//
//  AddressModel.m
//  Base
//
//  Created by admin on 2017/5/27.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "AddressModel.h"

@implementation AddressModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.Id = @0;
        self.name = @"";
        self.phone = @"";
        self.addressName = @"";
    }
    return self;
}

@end
