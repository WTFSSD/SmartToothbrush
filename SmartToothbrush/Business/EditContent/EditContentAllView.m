//
//  EditContentAllView.m
//  EditContentDemo
//
//  Created by Eleven on 17/2/22.
//  Copyright © 2017年 Hawk. All rights reserved.
//

#import "EditContentAllView.h"
#import "EditContentModel.h"
#import "UIImageView+WebCache.h"
#import <AVFoundation/AVFoundation.h>

#define HexColorInt32_t(rgbValue) \
[UIColor colorWithRed:((float)((0x##rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((0x##rgbValue & 0x00FF00) >> 8))/255.0 blue:((float)(0x##rgbValue & 0x0000FF))/255.0  alpha:1]

#define BLOCK_SAFE_RUN(block, ...) block ? block(__VA_ARGS__) : nil;


@implementation EditContentTableHeader

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)_dismissKeyBoard {
    [_field resignFirstResponder];
}

@end


#pragma mark - imgVCell
@implementation EditContentImgViewCell
{
    __weak IBOutlet UIImageView *_imgView;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString * ID = @"EditContentImgViewCell";
    EditContentImgViewCell * cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"EditContentAllView" owner:nil options:nil] objectAtIndex:1];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark event response
- (IBAction)_deleteImg:(id)sender {
    if (self.deleteImgBlock) {
        self.deleteImgBlock();
    }
}


- (void)setModel:(EditContentModel *)model {
    _model = model;
    [_imgView sd_setImageWithURL:[NSURL URLWithString:model.imageUrl] placeholderImage:model.img];
    _imgView.contentMode = UIViewContentModeScaleAspectFill;
    _imgView.clipsToBounds = YES;
}

@end


#pragma mark - textCell
@implementation EditContentTextViewCell
{
    __weak IBOutlet NSLayoutConstraint *textViewHeight;
}

#pragma mark  life cycle
- (void)awakeFromNib {
    [super awakeFromNib];
    
    @weakify(self);
    [_inputTextView.rac_textSignal subscribeNext:^(id x) {
        @strongify(self);
        self.model.inputStr = x;
        
        CGRect bounds = _inputTextView.bounds;
        CGSize maxSize = CGSizeMake(bounds.size.width, CGFLOAT_MAX);
        CGSize newSize = [_inputTextView sizeThatFits:maxSize];
        bounds.size = newSize;
        _inputTextView.bounds = bounds;
        
        // 让 table view 重新计算高度
        UITableView *tableView = [self tableView];
        [tableView beginUpdates];
        [tableView endUpdates];
    }];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString * ID = @"EditContentTextViewCell";
    EditContentTextViewCell * cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"EditContentAllView" owner:nil options:nil] objectAtIndex:2];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark  private methods
- (UITableView *)tableView {
    UIView *tableView = self.superview;
    
    while (![tableView isKindOfClass:[UITableView class]] && tableView) {
        tableView = tableView.superview;
    }
    return (UITableView *)tableView;
}

- (void)_dismissKeyBoard {
    [_inputTextView resignFirstResponder];
}

#pragma mark  setter
- (void)setModel:(EditContentModel *)model {
    _model = model;
    _inputTextView.text = model.inputStr;
}

@end


#pragma mark - videoCell
@implementation EditContentVideoCell
{
    __weak IBOutlet UIImageView *_imgV;
    __weak IBOutlet UIButton *_deleteBtn;
    __weak IBOutlet UILabel *_timeLbl;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [_deleteBtn bk_whenTapped:^{
        if (self.deleteBtnClick) {
            self.deleteBtnClick();
        }
    }];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString * ID = @"EditContentTextVideoCell";
    EditContentVideoCell * cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"EditContentAllView" owner:nil options:nil] objectAtIndex:3];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)setModel:(EditContentModel *)model {
    _model = model;
    _imgV.image = model.img;
    
    AVURLAsset * asset = [AVURLAsset assetWithURL:model.videoUrl];
    CMTime time = [asset duration];
    int seconds = ceil(time.value/time.timescale);
    _timeLbl.text = [NSString stringWithFormat:@"%02d:%02d",seconds/60,seconds%60];
}

@end

