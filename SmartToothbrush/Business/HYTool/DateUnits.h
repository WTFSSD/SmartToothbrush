//
//  DateUnits.h
//  ToastDemo
//
//  Created by seed on 2016/11/24.
//  Copyright © 2016年 WTFSSD. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#define Today [NSDate date]
#define trandination
#ifdef trandination
#define ChineseMonths @[@"正月", @"二月", @"三月", @"四月", @"五月", @"六月", @"七月", @"八月",@"九月", @"十月", @"冬月", @"腊月"]
#else
#define ChineseMonths @[@"一月", @"二月", @"三月", @"四月", @"五月", @"六月", @"七月", @"八月",@"九月", @"十月", @"十一", @"十二"]
#endif
#define ChineseDays @[@"初一", @"初二", @"初三", @"初四", @"初五", @"初六", @"初七", @"初八", @"初九", @"初十",@"十一", @"十二", @"十三", @"十四", @"十五", @"十六", @"十七", @"十八", @"十九", @"二十", @"廿一", @"廿二", @"廿三", @"廿四", @"廿五", @"廿六", @"廿七", @"廿八", @"廿九", @"三十"]

#define WeekKeys @[@"周日",@"周一",@"周二",@"周三",@"周四",@"周无",@"周六"]

typedef NSString* TimeFormatStyle;

@interface DateUnits : NSObject

+(instancetype)shareUnits;


+(instancetype)makeUnits:(void(^)(DateUnits * units))block;

/**当前月有多少天*/
-(DateUnits*(^)(NSInteger * ))daysOfCurrentMonth;

/**当前月第一天*/
-(DateUnits*((^)(NSDate** )))firstDayOfCurrentMonth;

/**当前月第一天 是周几*/
-(DateUnits*((^)(NSInteger * )))firstWeekOfCurrentMonth;

/**给定日期的那一月，有多少天*/
-(DateUnits*(^)(NSInteger*,NSDate*))daysOfMonthInDate;

/**给定日期的那一月，第一天是周几*/
-(DateUnits*(^)(NSInteger*,NSDate * date))weekdayOfFirstDayInDate;

/**给定日期的农历*/
-(DateUnits*(^)(NSString**,NSString **,NSDate*))chinessDateInDate;

/**给定日期的那一月，给定阳历天的农历天*/
-(DateUnits*(^)(NSString**,NSString **,NSDate*,NSInteger))chinessDateInDateWithDay;

/**给定日期的上一个月的日期*/
-(DateUnits*(^)(NSDate**,NSDate*))preMonthDataInDate;

/**给定日期的下一个月日期*/
-(DateUnits*(^)(NSDate**,NSDate*))nextMonthDataInDate;


/**给定日期的详细信息 年，月，日，时，分，秒(注:不需要计算则传NULL)*/
-(DateUnits*(^)(NSDate*,NSInteger*,NSInteger*,NSInteger*,NSInteger*,NSInteger*,NSInteger*))dateInfoInDate;

-(DateUnits*(^)(NSTimeInterval time,TimeFormatStyle formatter,NSString **res))formatTimestamp;

-(DateUnits*(^)(NSDate * date,TimeFormatStyle formatter,NSString **res))formatData;


+(NSString*)formatTimestamp:(NSTimeInterval)time withTimeFormatStyle:(TimeFormatStyle)style;

+(NSDate*)dateFromString:(NSString*)str TimeFormatStyle:(TimeFormatStyle)style;
@end

/**yyyy年MM月dd日*/
UIKIT_EXTERN TimeFormatStyle const TimeFormatStyle1 ;

/**yyyy年MM月*/
UIKIT_EXTERN TimeFormatStyle const TimeFormatStyle2 ;

/**HH时mm分ss秒*/
UIKIT_EXTERN TimeFormatStyle const TimeFormatStyle3 ;

/**HH时mm分*/
UIKIT_EXTERN TimeFormatStyle const TimeFormatStyle4 ;

/**yyyy年MM月dd日 HH时mm分ss秒*/
UIKIT_EXTERN TimeFormatStyle const TimeFormatStyle5 ;

/**yyyy.MM.dd*/
UIKIT_EXTERN TimeFormatStyle const TimeFormatStyle6 ;

/**yyyy.MM*/
UIKIT_EXTERN TimeFormatStyle const TimeFormatStyle7 ;

/**HH:mm:ss秒*/
UIKIT_EXTERN TimeFormatStyle const TimeFormatStyle8 ;

/**HH:mm*/
UIKIT_EXTERN TimeFormatStyle const TimeFormatStyle9 ;

/**yyyy.MM.dd HH:mm:ss*/
UIKIT_EXTERN TimeFormatStyle const TimeFormatStyle10;

/**yyyy-MM-dd HH:mm:ss*/
UIKIT_EXTERN TimeFormatStyle const TimeFormatStyle11;
