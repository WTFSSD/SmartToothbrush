//
//  DateUnits.m
//  ToastDemo
//
//  Created by seed on 2016/11/24.
//  Copyright © 2016年 WTFSSD. All rights reserved.
//

#import "DateUnits.h"


@interface DateUnits()
@property(nonnull,strong)NSCalendar * calendar;
@end

@implementation DateUnits


-(instancetype)init{
    if(self = [super init]){
        self.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierChinese];
    }
    return self;
}



+(instancetype)shareUnits{
    static dispatch_once_t onceToken;
    static DateUnits * units = nil;
    dispatch_once(&onceToken, ^{
        units = [[self alloc] init];
    });
    
    return units;
}

+(instancetype)makeUnits:(void(^)(DateUnits * units))block{
    if(block){
        DateUnits * unit = [DateUnits shareUnits];
        block(unit);
        return unit;
    }
    return nil;
}


-(DateUnits*(^)(NSInteger * daysOfCurrentMonth))daysOfCurrentMonth{
    return  ^(NSInteger * days){
        self.daysOfMonthInDate(days,[NSDate date]);
        return self;
        
    };
}

-(DateUnits *((^)(NSDate *__autoreleasing *)))firstDayOfCurrentMonth{
    return ^(NSDate ** day){
        NSDate * d = [NSDate date];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        [calendar setFirstWeekday:1];
        NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:d];
        [components setDay:1];
        
        *day = [calendar dateFromComponents:components];
        NSLog(@"%@",*day);
        return self;
    };
}

-(DateUnits *((^)(NSInteger *)))firstWeekOfCurrentMonth{
    return ^(NSInteger * week){
        self.weekdayOfFirstDayInDate(week,[NSDate date]);
        return self;
    };
}


/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////

-(DateUnits *(^)(NSInteger *, NSDate *))weekdayOfFirstDayInDate{
    return  ^(NSInteger * week, NSDate * date){
        NSCalendar *calendar = [NSCalendar currentCalendar];
        [calendar setFirstWeekday:1];
        NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
        [components setDay:1];
        NSDate *firstDate = [calendar dateFromComponents:components];
        NSDateComponents *firstComponents = [calendar components:NSCalendarUnitWeekday fromDate:firstDate];
        *week = (NSInteger)(firstComponents.weekday - 1);
        return self;
    };
}

-(DateUnits *(^)(NSInteger *, NSDate *))daysOfMonthInDate{
    return  ^(NSInteger * days,NSDate * date){
        *days =  [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date].length;
        return self;
    };
}


-(DateUnits *(^)(NSString *__autoreleasing *, NSString *__autoreleasing *, NSDate *))chinessDateInDate{
    return ^(NSString **month,NSString ** day,NSDate * date){
        NSCalendar *chineseCalendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierChinese];
        NSDateComponents *components = [chineseCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
        *month = ChineseMonths[components.month - 1];
        *day = ChineseDays[components.day - 1];
        return self;
    };
}

-(DateUnits*(^)(NSString**,NSString **,NSDate*,NSInteger))chinessDateInDateWithDay{
    return  ^(NSString **c_month,NSString ** c_day,NSDate * date,NSInteger day){
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
        [components setDay:day-1];
        NSDate *dateOfDay = [calendar dateFromComponents:components];
        self.chinessDateInDate(c_month,c_day,dateOfDay);
        return self;
    };
}


-(DateUnits *(^)(NSDate *__autoreleasing *, NSDate *))preMonthDataInDate{
    return  ^(NSDate ** preDate,NSDate * date){
        NSDateComponents *components = [[NSDateComponents alloc] init];
        components.month = -1;
        *preDate = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:date options:NSCalendarMatchStrictly];
        return self;
    };
}

-(DateUnits *(^)(NSDate *__autoreleasing *, NSDate *))nextMonthDataInDate{
    return  ^(NSDate ** nextDate,NSDate * date){
        NSDateComponents *components = [[NSDateComponents alloc] init];
        components.month = 1;
        *nextDate = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:date options:NSCalendarMatchStrictly];
        return self;
    };
}

-(DateUnits*(^)(NSDate*,NSInteger*,NSInteger*,NSInteger*,NSInteger*,NSInteger*,NSInteger*))dateInfoInDate{
    return ^(NSDate* date,NSInteger*year,NSInteger*month,NSInteger*day,NSInteger*hour,NSInteger*minute,NSInteger*second){
        NSCalendar * calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:date];
        if(year)    *year   = components.year;
        if(month)   *month  = components.month;
        if(day)     *day    = components.day;
        if(hour)    *hour   = components.hour;
        if(minute)  *minute = components.minute;
        if(second)  *second = components.second;
        return self;
    };
}


-(DateUnits *(^)(NSTimeInterval, TimeFormatStyle, NSString *__autoreleasing *))formatTimestamp{
    return  ^(NSTimeInterval timestamp, TimeFormatStyle formatter, NSString **res){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.timeZone = [NSTimeZone systemTimeZone];
        [dateFormatter setDateFormat:formatter];
        NSDate *theday = [NSDate dateWithTimeIntervalSince1970:timestamp];
        *res = [dateFormatter stringFromDate:theday];
        return self;
    };
}

-(DateUnits*(^)(NSDate * date,TimeFormatStyle formatter,NSString **res))formatData{
    return ^(NSDate * date,TimeFormatStyle formatter,NSString **res){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.timeZone = [NSTimeZone systemTimeZone];
        [dateFormatter setDateFormat:formatter];
        *res = [dateFormatter stringFromDate:date];
        return self;
    };
}

+(NSString *)formatTimestamp:(NSTimeInterval)time withTimeFormatStyle:(TimeFormatStyle)style{
    __block NSString * ss = nil;
    [self makeUnits:^(DateUnits *units) {
        units.formatTimestamp(time,style,&ss);
    }];
    return ss;
}

+(NSDate*)dateFromString:(NSString*)str TimeFormatStyle:(TimeFormatStyle)style
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:style];
    return [formatter dateFromString:str];
}
@end

TimeFormatStyle const TimeFormatStyle1 = @"yyyy年MM月dd日";
TimeFormatStyle const TimeFormatStyle2 = @"yyyy年MM月";
TimeFormatStyle const TimeFormatStyle3 = @"HH时mm分ss秒";
TimeFormatStyle const TimeFormatStyle4 = @"HH时mm分";
TimeFormatStyle const TimeFormatStyle5 = @"yyyy年MM月dd日 HH时mm分ss秒";

TimeFormatStyle const TimeFormatStyle6 = @"yyyy.MM.dd";
TimeFormatStyle const TimeFormatStyle7 = @"yyyy.MM";
TimeFormatStyle const TimeFormatStyle8 = @"HH:mm:ss秒";
TimeFormatStyle const TimeFormatStyle9 = @"HH:mm";
TimeFormatStyle const TimeFormatStyle10 = @"yyyy.MM.dd HH:mm:ss";

TimeFormatStyle const TimeFormatStyle11 = @"yyyy-MM-dd HH:mm:ss";

/*
 G:         公元时代，例如AD公元
 yy:     年的后2位
 yyyy:     完整年
 MM:     月，显示为1-12,带前置0
 MMM:     月，显示为英文月份简写,如 Jan
 MMMM:     月，显示为英文月份全称，如 Janualy
 dd:     日，2位数表示，如02
 d:         日，1-2位显示，如2，无前置0
 EEE:     简写星期几，如Sun
 EEEE:     全写星期几，如Sunday
 aa:     上下午，AM/PM
 H:         时，24小时制，0-23
 HH:     时，24小时制，带前置0
 h:         时，12小时制，无前置0
 hh:     时，12小时制，带前置0
 m:         分，1-2位
 mm:     分，2位，带前置0
 s:         秒，1-2位
 ss:     秒，2位，带前置0
 S:         毫秒
 Z：        GMT（时区）
 */
