//
//  UIButton+Make.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ActionBlock)(UIButton * button);
@interface UIButton (Make)

+(instancetype)makeWith:(void (^)(UIButton * button))block;

-(UIButton*(^)(CGRect frame))setFrame;

-(UIButton*(^)(id target,SEL action,UIControlEvents events))addTarget;

-(UIButton*(^)(NSString*title,UIControlState state))setTitle;

-(UIButton*(^)(UIColor*color,UIControlState state))setTitleColor;

-(UIButton*(^)(UIFont * font))setTitleFont;

-(UIButton*(^)(UIEdgeInsets edgs))setTitleEdgs;

-(UIButton*(^)(UIEdgeInsets edgs))setImageEdgs;

-(UIButton*(^)(UIImage * image,UIControlState state))setBackgroundImage;

-(UIButton*(^)(UIImage * image,UIControlState state))setImage;


-(UIButton*(^)(ActionBlock block,UIControlEvents events))setActionBlock;

-(void)addActionBlock:(ActionBlock)block events:(UIControlEvents)events;
@end
