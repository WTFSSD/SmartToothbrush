//
//  UILabel+Make.m
//  Base
//
//  Created by WTFSSD on 2018/4/28.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "UILabel+Make.h"

@implementation UILabel (Make)


+(instancetype)makeWith:(void (^)(UILabel *))block{
    UILabel * label = [[self alloc] init];
    label.textColor = [UIColor colorWithRed:51/255.f green:51/255.f blue:51/255.f alpha:1];
    label.font = [UIFont systemFontOfSize:16];
    label.adjustsFontForContentSizeCategory = false;
    if(block){
        block(label);
    }
    return label;
}
@end
