//
//  UIColor+HYColors.m
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "UIColor+HYColors.h"

@implementation UIColor (HYColors)

+ (UIColor *)hyBarTintColor {
    return [UIColor whiteColor];
}

+(UIColor *)hyBarSelectedColor {
    return [UIColor colorWithString:@"#027AFE"];
}

+(UIColor *)hyBarUnselectedColor {
    return [UIColor colorWithString:@"#98B2CE"];
}

+ (UIColor *)hyRedColor {
    return [UIColor colorWithString:@"#FF7995"];
}

+ (UIColor *)hyBlackTextColor {
    return [UIColor colorWithString:@"#555555"];
}

+ (UIColor *)hyHighlightedGrayColor {
    return [UIColor colorWithString:@"#8a8a8a"];
}

+ (UIColor *)hyGrayTextColor {
    return [UIColor colorWithString:@"#888888"];
}

+ (UIColor *)hyLightGrayColor {
    return [UIColor colorWithString:@"#c2c2c2"];
}

+ (UIColor*)hyBlueTextColor {
    return [UIColor colorWithString:@"#52c9da"];
}

+ (UIColor *)hySeparatorColor {
    return [UIColor colorWithString:@"#e6e6e6"];
}

+ (UIColor *)hyViewBackgroundColor {
    return [UIColor colorWithString:@"#f5f5f5"];
}

+ (UIColor *)hyYellowColor {
    return [UIColor colorWithString:@"#fe9159"];
}

+ (UIColor *)hyGreenColor {
    return [UIColor colorWithString:@"#47ca65"];
}

+ (UIColor *)hyBlueColor {
    return [UIColor colorWithString:@"027afe"];
}

+ (UIColor *)hyColorWithString:(NSString *) string{
    if (!string.length) return nil;
    return [UIColor colorWithString:string];
}

+(UIColor*)hyDefaultBackColor {
    return [UIColor colorWithString:@"f1f3f8"];
}

@end
