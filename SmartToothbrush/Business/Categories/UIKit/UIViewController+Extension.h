//
//  UIViewController+Extension.h
//  Base
//
//  Created by admin on 2017/3/2.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Extension)

- (void)addBackgroundImage:(NSString*)imageName frame:(CGRect)frame;

- (void)addBackgroundImageWithFrame:(CGRect)frame;


/**
 添加“消息” navigationItem
 */
- (void)configMessage;

/**
 添加两个navigationItems

 @param imageNames items‘ imageNames
 @param block1 first Item's block
 @param block2 second Items's block
 */
- (void)addDoubleNavigationItemsWithImages:(NSArray*)imageNames firstBlock:(void(^)())block1 secondBlock:(void(^)())block2;


/**
 弹出actionSheet选择已安装地图导航
 
 @param addressString 目的地址
 */
- (void)geocoderClick:(NSString *)addressString;



/**
 弹出Alert

 @param title <#title description#>
 @param message <#message description#>
 @param title1 确定按钮的title,为nil则只有一个btn
 @param title2 取消按钮的title
 @param handler 确定按钮的handler
 */
- (void)presentAlertWithTitle:(NSString*)title message:(NSString*)message destructiveBtnTitle:(NSString*)title1 cancleBtnTitle:(NSString*)title2 handler:(void(^)())handler;

@end
