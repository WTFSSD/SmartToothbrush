//
//  UIStoryboard+HYStoryboard.h
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (HYStoryboard)
+ (UIStoryboard *)mainStoryboard;
+ (UIStoryboard *)dataStoryboard;
+ (UIStoryboard *)infoStoryboard;
+ (UIStoryboard *)mineStoryboard;
+ (UIStoryboard *)loginStoryboard;
@end
