//
//  OpenRoutesManager.m
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "OpenRoutesManager.h"

// vc标识
NSString* const kHomeViewController = @"HomeViewController";
NSString* const kDataHomeController = @"DataHomeController";
NSString* const kInfoHomeController = @"InfoHomeController";
NSString* const kMineHomeViewController = @"MineHomeViewController";

//Main

//Data

//Info

//Mine
NSString* const kUserSettlementController = @"UserSettlementController";
NSString* const kVoiceSetController = @"VoiceSetController";
NSString* const kHealthGradeController = @"HealthGradeController";
NSString* const kHealthWaitController = @"HealthWaitController";
NSString* const kHealthResultController = @"HealthResultController";
NSString* const kUserInfoViewController = @"UserInfoViewController";
NSString* const kMineBrushController = @"MineBrushController";
NSString* const kAddressManageController = @"AddressManageController";
NSString* const kAddressAddController = @"AddressAddController";
NSString* const kBrushSchemeController = @"BrushSchemeController";
NSString* const kPersonalMadeController = @"PersonalMadeController";
NSString* const kWKWebViewController = @"WKWebViewController";
NSString* const kCommonProblemsController = @"CommonProblemsController";
NSString* const kUsingIntroductionController = @"UsingIntroductionController";
NSString* const kMessageListController = @"MessageListController";
NSString* const kMessageSetController = @"MessageSetController";
NSString* const kAboutUsViewController = @"AboutUsViewController";
NSString* const kFeedbackController = @"FeedbackController";
NSString* const kContactViewController = @"ContactViewController";
NSString* const kCodeScanController = @"CodeScanController";
NSString* const kRegistViewController = @"RegistViewController";
NSString* const kLoginViewController = @"LoginViewController";
NSString* const kForgotViewController = @"ForgotController";
NSString* const kChangePasswordController = @"ChangePasswordController";
NSString* const kCheckCodeController = @"CheckCodeController";
NSString* const kAddressController = @"HYAddressController";
NSString* const kConectToothBrush = @"ConectStep1ViewController";
NSString* const kAppointmentController = @"AppointmentViewController";


@implementation OpenRoutesManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

+(id)shareInstance {
    static OpenRoutesManager* manager = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        if (manager == nil) {
            manager = [[OpenRoutesManager alloc] init];
        }
    });
    return manager;
}

-(NSArray*)routeConfigs {
    NSArray *routes = @[
                        @{@"cls":kHomeViewController,@"sb":@"Main",@"method":@"push"},
                        @{@"cls":kDataHomeController,@"sb":@"Data",@"method":@"push"},
                        @{@"cls":kInfoHomeController,@"sb":@"Info",@"method":@"push"},
                        @{@"cls":kMineHomeViewController,@"sb":@"Mine",@"method":@"push"},

                        @{@"cls":kUserSettlementController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kVoiceSetController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kHealthGradeController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kHealthWaitController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kHealthResultController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kUserInfoViewController,@"sb":@"",@"method":@"push"},
                        @{@"cls":kMineBrushController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kAddressManageController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kAddressAddController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kBrushSchemeController,@"sb":@"",@"method":@"push"},
                        @{@"cls":kWKWebViewController,@"sb":@"",@"method":@"push"},
                        @{@"cls":kPersonalMadeController,@"sb":@"",@"method":@"push"},
                        @{@"cls":kCommonProblemsController,@"sb":@"",@"method":@"push"},
                        @{@"cls":kUsingIntroductionController,@"sb":@"",@"method":@"push"},
                        @{@"cls":kMessageListController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kMessageSetController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kAboutUsViewController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":@"ValidateMethodsViewController",@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kFeedbackController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kContactViewController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kAppointmentController,@"sb":@"Mine",@"method":@"push"},
                        @{@"cls":kCodeScanController,@"sb":@"Mine",@"method":@"push"},

                        @{@"cls":kRegistViewController,@"sb":@"Login",@"method":@"push"},
                        @{@"cls":kLoginViewController,@"sb":@"Login",@"method":@"push"},
                        @{@"cls":kForgotViewController,@"sb":@"Login",@"method":@"push"},
                        @{@"cls":kChangePasswordController,@"sb":@"Login",@"method":@"push"},
                        @{@"cls":kCheckCodeController,@"sb":@"Login",@"method":@"push"},
                        
                        @{@"cls":kAddressController,@"sb":@"Login",@"method":@"push"},
                        @{@"cls":kConectToothBrush,@"sb":@"",@"method":@"present"},
                        ];
    return routes;
}



-(void)registSchema {
    NSArray * arr = [self routeConfigs];
    for (NSDictionary *item in arr) {
        NSString* cls = [item objectForKey:@"cls"];
        NSString* sb = [item objectForKey:@"sb"];
        NSString* method = [item objectForKey:@"method"];
        if ([method isEqualToString:@"push"]) {
            [JLRoutes addRoute:cls handler:^BOOL(NSDictionary<NSString *,id> * _Nonnull parameters) {
                [self navigationToViewControler:cls argu:parameters storyboard:sb];
                return YES;
            }];
        }else{
            [JLRoutes addRoute:cls handler:^BOOL(NSDictionary<NSString *,id> * _Nonnull parameters) {
                [self presentToViewControler:cls argu:parameters storyboard:sb];
                return NO;
            }];
        }
    }
}

-(void)routeSchemaByURL:(NSURL*)url {
    if ([url.scheme isEqualToString:@"http"]) {
        // to webview
        return;
    }
    [JLRoutes routeURL:url];
}

-(void)routeSchemaByString:(NSString*)urlstr {
    NSString *encodingString = [urlstr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [self routeSchemaByURL:[NSURL URLWithString:encodingString]];
}

-(void)routeByStoryboardID:(NSString*)sid {
    [self routeSchemaByString:[NSString stringWithFormat:@"Base://%@",sid]];
}

-(void)routeByStoryboardID:(NSString*)sid withParam:(NSDictionary*)param {
    //参数序列号
    NSMutableString *str = [[NSMutableString alloc] init];
    for (NSString *key in param.allKeys) {
        NSString *args = [NSString stringWithFormat:@"%@=%@&",key,[param objectForKey:key]];
        [str appendString:args];
    }
//    NSString *p = [NSString stringWithFormat:@"%@?%@",sid,str];
    NSString *p = [NSString stringWithFormat:@"%@%@",sid,str];
    [self routeByStoryboardID:p];
}

-(UIViewController *)viewControllerForStoryboardID:(NSString*)storyboardID {
    NSArray *arr = [self routeConfigs];
    for (NSDictionary *item in arr) {
        NSString *cls = [item objectForKey:@"cls"];
        NSString *sb = [item objectForKey:@"sb"];
        if ([storyboardID isEqualToString:cls]) {
            UIStoryboard *m = [UIStoryboard storyboardWithName:sb bundle:nil];
            UIViewController *tarvc = [m instantiateViewControllerWithIdentifier:storyboardID];
            return tarvc;
        }
    }
    return nil;
}


#pragma mark - help
-(UIViewController*)getCurrentRootViewController {
    
    UIViewController *result;
    UIWindow *topWindow = [[[UIApplication sharedApplication] windows] firstObject];
    
    if (topWindow.windowLevel != UIWindowLevelNormal){
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(topWindow in windows){
            if (topWindow.windowLevel == UIWindowLevelNormal)
                break;
        }
    }
    
    UIView *rootView = [[topWindow subviews] objectAtIndex:0];
    id nextResponder = [rootView nextResponder];
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else if ([topWindow respondsToSelector:@selector(rootViewController)] && topWindow.rootViewController != nil)
        result = topWindow.rootViewController;
    else
        NSAssert(NO, @"ShareKit: Could not find a root view controller.  You can assign one manually by calling [[SHK currentHelper] setRootViewController:YOURROOTVIEWCONTROLLER].");
    return result;
}

-(void)presentToViewControler:(NSString*)vcIdentifier argu:(NSDictionary*)argu storyboard:(NSString*)sb
{
    UITabBarController *tab = (UITabBarController*)[self getCurrentRootViewController];
    UINavigationController *nav = tab.viewControllers[tab.selectedIndex];
    UIViewController *tarvc = nil;
    if (sb && sb.length) {
        UIStoryboard *m = [UIStoryboard storyboardWithName:sb bundle:[NSBundle mainBundle]];
        tarvc = [m instantiateViewControllerWithIdentifier:vcIdentifier];
    }else{
        tarvc = [NSClassFromString(vcIdentifier) new];
    }
    UINavigationController *navtar = [[UINavigationController alloc] initWithRootViewController:tarvc];
    tarvc.schemaArgu = argu;
    [tarvc setHidesBottomBarWhenPushed:YES];
    UIViewController *vis = [nav visibleViewController];
    if (vis) {
        [vis presentViewController:navtar animated:YES completion:nil];
    }else{
        [nav presentViewController:navtar animated:YES completion:nil];
    }
}

-(void)navigationToViewControler:(NSString*)vcIdentifier argu:(NSDictionary*)argu storyboard:(NSString*)sb
{
    
    
    
    
    
    id currentVc = [self getCurrentRootViewController];
    
    UIViewController *tarvc = nil;
    if (sb && sb.length) {
        UIStoryboard *m = [UIStoryboard storyboardWithName:sb bundle:[NSBundle mainBundle]];
        tarvc = [m instantiateViewControllerWithIdentifier:vcIdentifier];
    }else{
        tarvc = [NSClassFromString(vcIdentifier) new];
    }
    tarvc.schemaArgu = argu;
    if([currentVc isKindOfClass:[UINavigationController class]]){
        //导航视图控制器
        [currentVc pushViewController:tarvc animated:YES];
    }else if([currentVc isKindOfClass:[UITabBarController class]]){
        UITabBarController * tbvc = (UITabBarController*)currentVc;
        id sel = tbvc.selectedViewController;
        if([sel isKindOfClass:[UINavigationController class]]){
            //导航视图控制器
            [sel pushViewController:tarvc animated:YES];
        }else if ([sel isKindOfClass:[UIViewController class]]){
            //普通 的视图控制器
        }
    }else{
        //普通 的视图控制器
    }
    
    
    
    
    
    
    
    
    
    /// getCurrentRootViewController 这个可能不是 UITabBarController
    
    /*
    
    
    UITabBarController *tab = (UITabBarController*)[self getCurrentRootViewController];
    UINavigationController *nav = tab.viewControllers[tab.selectedIndex];
    UIViewController *tarvc = nil;
    if (sb && sb.length) {
        UIStoryboard *m = [UIStoryboard storyboardWithName:sb bundle:[NSBundle mainBundle]];
        tarvc = [m instantiateViewControllerWithIdentifier:vcIdentifier];
    }else{
        tarvc = [NSClassFromString(vcIdentifier) new];
    }
    tarvc.schemaArgu = argu;
    [tarvc setHidesBottomBarWhenPushed:YES];
    UIViewController *vis = [nav visibleViewController];
    if (vis) {
        [vis.navigationController pushViewController:tarvc animated:YES];
    }else{
        [nav pushViewController:tarvc animated:YES];
    }
    if ([nav respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        nav.interactivePopGestureRecognizer.delegate = nil;
    }
     
     */
}

@end
