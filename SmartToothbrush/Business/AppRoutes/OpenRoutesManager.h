//
//  OpenRoutesManager.h
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// vc标识
extern NSString* const kHomeViewController;
extern NSString* const kDataHomeController;
extern NSString* const kInfoHomeController;
extern NSString* const kMineHomeViewController;

//Main




//data
extern NSString* const kRecordPublishController;
extern NSString* const kDailyPublishController;
extern NSString* const kGraphAddController;


//info

//Mine
extern NSString* const kUserSettlementController;
extern NSString* const kVoiceSetController;
extern NSString* const kHealthGradeController;
extern NSString* const kHealthWaitController;
extern NSString* const kHealthResultController;
extern NSString* const kUserInfoViewController;
extern NSString* const kMineBrushController;
extern NSString* const kAddressManageController;
extern NSString* const kAddressAddController;
extern NSString* const kBrushSchemeController;
extern NSString* const kPersonalMadeController;
extern NSString* const kWKWebViewController;
extern NSString* const kCommonProblemsController;
extern NSString* const kUsingIntroductionController;
extern NSString* const kMessageListController;
extern NSString* const kMessageSetController;
extern NSString* const kAboutUsViewController;
extern NSString* const kFeedbackController;
extern NSString* const kContactViewController;
extern NSString* const kAppointmentController;
extern NSString* const kCodeScanController;


extern NSString* const kRegistViewController;
extern NSString* const kLoginViewController;
extern NSString* const kForgotViewController;
extern NSString* const kChangePasswordController;
extern NSString* const kCheckCodeController;

extern NSString* const kAddressController;


extern NSString* const kConectToothBrush;






#define APPROUTE(storyboardID) [[OpenRoutesManager shareInstance] routeByStoryboardID:(storyboardID)];
#define VIEWCONTROLLER(storyboardID) [[OpenRoutesManager shareInstance] viewControllerForStoryboardID:storyboardID];
#define ROUTER ([OpenRoutesManager shareInstance])

@interface OpenRoutesManager : NSObject

+(id)shareInstance;

-(void)registSchema;
-(void)routeSchemaByURL:(NSURL*)url;
-(void)routeSchemaByString:(NSString*)urlstr;
-(void)routeByStoryboardID:(NSString*)sid;    //如果需要传参数(单个参数) 则: storyboardID?user_id=2&sex=1
-(void)routeByStoryboardID:(NSString*)sid withParam:(NSDictionary*)param; //如果需要传参数(字典) 则: storyboardID?  param = @{@"user_id":@"2",@"sex":@(1)};
-(UIViewController*)viewControllerForStoryboardID:(NSString*)storyboardID;

@end
