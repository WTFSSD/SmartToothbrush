//
//  Global.m
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "Global.h"
#import "LTKeyChain.h"

@implementation Global

+(long long)cacheSize {
    SDImageCache * sdCache = [SDImageCache sharedImageCache];
    long long size = sdCache.getSize;
    SDWebImageManager * manager = [SDWebImageManager sharedManager];
    size += manager.imageCache.getSize;
    return size;
}

+ (void)clearCacheWithCompletionHandler:(void (^)(void))completionHandler {

    SDImageCache* sdCache = [SDImageCache sharedImageCache];
    [sdCache clearMemory];
    [sdCache cleanDisk];
    
    //SDWebImageManager图片清除
    SDWebImageManager * manager = [SDWebImageManager sharedManager];
    [manager.imageCache clearMemory];
    [manager.imageCache clearDiskOnCompletion:completionHandler];
}

#define kUDIDKeyChain   @"identifierVendorKeyChainKey"
+ (NSString *)IDFV
{
    //ios 7 禁用网卡地址,所以改为UDID ＋ keychain
    NSString *passWord =  [LTKeyChain load:kUDIDKeyChain];
    
    if (passWord && ![passWord isEqualToString:@"(null)"] && ![passWord isEqualToString:@"-1"]) {
        return passWord;
    }
    else{
        NSString *identifierVendor = [[UIDevice currentDevice] identifierForVendor].UUIDString;
        [LTKeyChain save:kUDIDKeyChain data:identifierVendor];
        return identifierVendor;
    }
}

+(NSString *)userAuth {
    return kGetObjectFromUserDefaults(@"userAuth");
}

+(void)setUserAuth:(NSString *)auth {
    kSaveObjectToUserDefaults(@"userAuth", auth);
}


+(AnyPromise *)timeOutWith:(NSTimeInterval)Interval{
   __block  NSTimeInterval count = 0;
    return [AnyPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        [NSTimer bk_scheduledTimerWithTimeInterval:1/1000.f block:^(NSTimer *timer) {
            if(count ++ >Interval){
                count = 0;
                resolve(nil);
                [timer invalidate];
            }
        } repeats:YES];
    }];
    
}


+(RACSignal *)timeIntervalWith:(NSTimeInterval)interval {
    
    
    RACSignal* signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [NSTimer bk_scheduledTimerWithTimeInterval:interval block:^(NSTimer *timer) {
            [subscriber sendNext:timer];
        } repeats:YES];
        return nil;
    }];
    return signal;
}

+(RACSignal *)timeOut:(NSTimeInterval)interval total:(NSTimeInterval)total{
    __block  NSTimeInterval count = 0;
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [NSTimer bk_scheduledTimerWithTimeInterval:interval block:^(NSTimer *timer) {
            if(count ++ >total){
                count = 0;
                [subscriber sendCompleted];
                [timer invalidate];
            }else{
                [subscriber sendNext:@{@"timer":timer,@"time":@(count)}];
            }
        } repeats:YES];
        return nil;
    }];
}
@end
