//
//  Global.h
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PromiseKit/PromiseKit.h>

#import <ReactiveCocoa/ReactiveCocoa.h>

@interface Global : NSObject

+ (long long)cacheSize;

+ (void)clearCacheWithCompletionHandler:(void(^)(void))completionHandler;

+ (NSString*)IDFV;

+ (NSString*)userAuth;
+ (void)setUserAuth:(NSString *)auth;


/**
 * 延时 类似 js里面 setTimeout
 * @params Interval NSTimeInterval
 * @return AnyPromise*
 */
+ (AnyPromise*)timeOutWith:(NSTimeInterval)Interval;


+(RACSignal*)timeIntervalWith:(NSTimeInterval)interval;
+(RACSignal*)timeOut:(NSTimeInterval)interval total:(NSTimeInterval)total;

@end
