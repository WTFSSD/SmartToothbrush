//
//  HYIconInfo.h
//  Base
//
//  Created by admin on 2018/4/20.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface HYIconInfo : NSObject

@property (nonatomic,strong) NSString *text;
@property (nonatomic,assign) NSInteger size;
@property (nonatomic,strong) UIColor *color;

- (instancetype)initWithText:(NSString *)text size:(NSInteger)size color:(UIColor *)color;
+ (instancetype)iconInfoWithText:(NSString *)text size:(NSInteger)size color:(UIColor *)color;

@end
