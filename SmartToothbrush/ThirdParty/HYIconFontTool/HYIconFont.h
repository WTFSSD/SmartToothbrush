//
//  HYIconFont.h
//  Base
//
//  Created by admin on 2018/4/20.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "UIImage+HYIconFont.h"
#import "HYIconInfo.h"

#define HYIconInfoMake(text, imageSize, imageColor) [HYIconInfo iconInfoWithText:text size:imageSize color:imageColor]


@interface HYIconFont : NSObject

+ (UIFont *)fontWithSize:(CGFloat)size;
+ (void)setFontName:(NSString *)fontName;

@end
