//
//  HYIconInfo.m
//  Base
//
//  Created by admin on 2018/4/20.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "HYIconInfo.h"

@implementation HYIconInfo

- (instancetype)initWithText:(NSString *)text size:(NSInteger)size color:(UIColor *)color {
    if (self = [super init]) {
        self.text = text;
        self.size = size;
        self.color = color;
    }
    return self;
}

+ (instancetype)iconInfoWithText:(NSString *)text size:(NSInteger)size color:(UIColor *)color {
    return [[HYIconInfo alloc] initWithText:text size:size color:color];
}
@end
