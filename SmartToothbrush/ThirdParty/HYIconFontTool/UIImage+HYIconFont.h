//
//  UIImage+HYIconFont.h
//  Base
//
//  Created by admin on 2018/4/20.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HYIconInfo.h"

@interface UIImage (HYIconFont)

+ (UIImage *)iconWithInfo:(HYIconInfo *)info;

@end
