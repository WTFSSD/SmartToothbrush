//
//  CodeScanController.m
//  Base
//
//  Created by admin on 2017/5/8.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "CodeScanController.h"
#import "LBXScanView.h"
#import "LBXScanViewStyle.h"
#import "LBXScanNative.h"
#import "ZXingWrapper.h"
#import "HYAlertView.h"

@interface CodeScanController ()

@property (strong,nonatomic) ZXingWrapper* zxingObj;
@property (strong,nonatomic) LBXScanView* qRScanView;
@property (strong,nonatomic) LBXScanViewStyle* style;

@end

@implementation CodeScanController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationBarTransparent = YES;
    [self captureInit];
    [self subviewStyle];
    [self subviewBind];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];

    [self.zxingObj start];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    [self.qRScanView startScanAnimation];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    [_qRScanView stopScanAnimation];
    [self stopScan];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -实现类继承该方法，作出对应处理

- (void)scanResultWithArray:(NSArray<LBXScanResult*>*)array
{
    if (!array ||  array.count < 1)
    {
        [self popAlertMsgWithScanResult:nil];
        return;
    }
    
    LBXScanResult *scanResult = array[0];
    
    NSString*strResult = scanResult.strScanned;
    
    
    if (!strResult) {
        
        [self popAlertMsgWithScanResult:nil];
        
        return;
    }
    
    [self handleResult:(NSString*)strResult];
}

- (void)popAlertMsgWithScanResult:(NSString*)strResult
{
    if (!strResult) {
        
        strResult = @"识别失败";
    }
    
    __weak __typeof(self) weakSelf = self;
    HYAlertView* alert = [HYAlertView sharedInstance];
    [alert showAlertView:@"扫码内容" message:strResult subBottonTitle:@"知道了" handler:^(AlertViewClickBottonType bottonType) {
        [weakSelf reStartDevice];
    }];
}

- (void)reStartDevice {
    
    [self.zxingObj start];
}


#pragma mark - private methods
- (LBXScanViewStyle *)style {
    if (!_style) {
        _style = [[LBXScanViewStyle alloc] init];
        _style.centerUpOffset = 20;
        _style.notRecoginitonArea = RGB(40, 40, 40, 0.5);
        _style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_On;
        _style.isNeedShowRetangle = NO;
        _style.photoframeAngleH = 16;
        _style.photoframeAngleW = 16;
        _style.photoframeLineW = 3;
        _style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
        _style.colorAngle = [UIColor hyRedColor];
        UIImage* image = [UIImage imageNamed:@"扫码效果"];
        _style.animationImage = image;
    }
    return _style;
}

- (void)captureInit {
    //绘制扫描区域
    if (!_qRScanView)
    {
        CGRect rect = [[UIScreen mainScreen] bounds];
        self.qRScanView = [[LBXScanView alloc]initWithFrame:rect style:self.style];
        [self.view addSubview:_qRScanView];
        [_qRScanView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(-64, 0, 0, 0)];
    }
    [_qRScanView startDeviceReadyingWithText:@"相机启动中"];
    
    [self startScan];
}

- (void)startScan {
    
    [_qRScanView stopDeviceReadying];
    UIView *videoView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    videoView.backgroundColor = [UIColor clearColor];
    [self.view insertSubview:videoView atIndex:0];
    __weak __typeof(self) weakSelf = self;
    
    if (!_zxingObj) {
        self.zxingObj = [[ZXingWrapper alloc]initWithPreView:videoView block:^(ZXBarcodeFormat barcodeFormat, NSString *str, UIImage *scanImg) {
            
            LBXScanResult *result = [[LBXScanResult alloc]init];
            result.strScanned = str;
            result.imgScanned = scanImg;
            result.strBarCodeType = AVMetadataObjectTypeQRCode;
            
            [weakSelf scanResultWithArray:@[result]];
            
        }];
    }

    CGRect cropRect = [LBXScanView getZXingScanRectWithPreView:videoView style:_style];
    [_zxingObj setScanRect:cropRect];
}

- (void)stopScan {
    
    [self.zxingObj stop];
}

- (void)subviewStyle {

}

- (void)subviewBind {
    
}

- (void)input:(UIButton*)btn {
    
}

//MARK: - ScanResultDelegate
- (void)actionSucceedAfterScan {
    
}

//MARK: - 扫描得到结果
- (void)handleResult:(NSString*)strResult {
    

}


- (void)showResultViewWithData:(NSDictionary*)data {
    
    [self stopScan];

}

- (void)hideResultView {
    
    
    [self.zxingObj start];
}

@end
