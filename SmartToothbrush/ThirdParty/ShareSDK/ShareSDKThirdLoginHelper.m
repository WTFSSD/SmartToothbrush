//
//  ShareSDKThirdLoginHelper.m
//  ZhiMaDi
//
//  Created by haijie on 16/7/4.
//  Copyright © 2016年 ZhiMaDi. All rights reserved.
//

#import "ShareSDKThirdLoginHelper.h"
#import <ShareSDKExtension/SSEThirdPartyLoginHelper.h>

@interface ShareSDKThirdLoginHelper ()


@property(nonatomic,assign)SSDKPlatformType loginPlatform;

@end

@implementation ShareSDKThirdLoginHelper : NSObject

+ (ShareSDKThirdLoginHelper*)shareThirdLoginHelper
{
    static ShareSDKThirdLoginHelper* instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

-(void)loginWithIndex:(NSInteger)index {
    NSInteger platform = 0;
    switch (index) {
        case 0:
            self.loginPlatform = SSDKPlatformTypeWechat;
            platform = 2;
            break;
        case 1:
            self.loginPlatform = SSDKPlatformTypeWechat;
            platform = 1;
            break;
        default:
            self.loginPlatform = SSDKPlatformTypeSinaWeibo;
            platform = 3;
            break;
    }
    [SSEThirdPartyLoginHelper loginByPlatform:self.loginPlatform
                                   onUserSync:^(SSDKUser *user, SSEUserAssociateHandler associateHandler) {
                                       
                                       //在此回调中可以将社交平台用户信息与自身用户系统进行绑定，最后使用一个唯一用户标识来关联此用户信息。
                                       //在此示例中没有跟用户系统关联，则使用一个社交用户对应一个系统用户的方式。将社交用户的uid作为关联ID传入associateHandler。
                                       associateHandler (user.uid, user, user);
                                       NSString* openId = @"";
                                       if (self.loginPlatform == SSDKPlatformTypeWechat) {
                                           openId = user.rawData[@"unionid"];
                                       }else{
                                           openId = user.uid;
                                       }
                                       [self.delegate thirdLogin:openId platform:platform];
                                   }
                                onLoginResult:^(SSDKResponseState state, SSEBaseUser *user, NSError *error) {
                                    if (state == SSDKResponseStateSuccess)
                                    {
                                        NSLog(@"successed");
                                    }
                                    
                                }];
    
}

-(void)onResp:(BaseResp *)resp{
    if([resp isKindOfClass:[SendAuthResp class]]){
        if(self.delegate){
            if(resp.errCode == 0){
                SendAuthResp * r = (SendAuthResp*)resp;
                [self.delegate thirdLogin:r.code platform:0];
            }
        }
    }
}

+(void)logout {
    SSEBaseUser* user = [SSEThirdPartyLoginHelper currentUser];
    if (user != nil) {
        [SSEThirdPartyLoginHelper logout:user];
    }
    kCleanOpenId;
    kCleanPlatform;
}

@end

