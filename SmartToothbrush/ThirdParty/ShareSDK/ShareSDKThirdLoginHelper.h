//
//  ShareSDKThirdLoginHelper.h
//  ZhiMaDi
//
//  Created by haijie on 16/7/4.
//  Copyright © 2016年 ZhiMaDi. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "WXApi.h"
@protocol ShareSDKThirdLoginHelperDelegate <NSObject>

-(void)thirdLogin:(NSString*)openId platform:(NSInteger)platform;

@end

@interface ShareSDKThirdLoginHelper : NSObject<WXApiDelegate>

@property(nonatomic,assign)id<ShareSDKThirdLoginHelperDelegate>delegate;

+(ShareSDKThirdLoginHelper*)shareThirdLoginHelper;
-(void)loginWithIndex:(NSInteger)index;
+(void)logout;


@end
