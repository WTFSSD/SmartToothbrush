//
//  AppDelegate+Extension.h
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Extension) <UINavigationControllerDelegate>

- (void)initIconFont;

- (void)configTabbar;

/**
 * 设置tabBar
 * @param test BOOL 是否是测试
 */
- (void)configTabbar:(BOOL)test;

- (void)configUIAppearance;

- (void)verifyLogin;

- (void)configIQKeyBoardManager;

- (void)configPaySDK;

- (void)configShareSDK;

///引导页
-(void)configGuide;
///问卷页
-(void)configQuestion;

///测试连接牙刷页面
-(void)configConnect;

///连接牙刷页面
-(void)configConnectWith:(NSDictionary*)params;


///登录页
-(void)configLogin;

@end
