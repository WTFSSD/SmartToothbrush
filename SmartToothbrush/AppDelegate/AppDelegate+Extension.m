//
//  AppDelegate+Extension.m
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "AppDelegate+Extension.h"
#import "UIStoryboard+HYStoryboard.h"
#import "BaseNavigationController.h"
#import "BaseViewController.h"
#import "UIImage+HYImages.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKConnector/ShareSDKConnector.h>
//腾讯开放平台（对应QQ和QQ空间）SDK头文件
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>

//微信SDK头文件
#import "WXApi.h"

//新浪微博SDK头文件
#import "WeiboSDK.h"

#import "JPUSHService.h"
#import "HYIconFont.h"


#import "GuideViewController.h"

#import "Questionnaire2ViewController.h"

#import "LoginViewController.h"

//测试控制器
#import "TestViewController.h"


#import "ConectStep1ViewController.h"

NS_ENUM(NSUInteger, TabType) {
    TabTypeHome = 0,
    TabTypeSecond,
    TabTypeThird,
    TabTypeForth
};


@implementation AppDelegate (Extension)

-(void)verifyLogin {

    if (kPassword && kAccount) {
        [APIHELPER login:kAccount password:kPassword complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            if (isSuccess) {
                [Global setUserAuth:responseObject[@"data"][@"key"]];
                APIHELPER.userInfo = [UserInfoModel yy_modelWithJSON:responseObject[@"data"]];
                [[UserInfoModel share] yy_modelSetWithJSON:[responseObject[@"data"] yy_modelToJSONString]];
                [UserInfoModel store];
                NSString* pushAccount = [NSString stringWithFormat:@"%@",responseObject[@"data"][@"push_account"]];
                [JPUSHService setAlias:pushAccount callbackSelector:nil object:nil];
            }else{
                [Global setUserAuth:nil];
                APIHELPER.userInfo = nil;
            }
        }];
    }
}

- (void)initIconFont {
    [HYIconFont setFontName:@"iconFont"];
}

-(void)configTabbar {
    UIViewController* main = [[UIStoryboard mainStoryboard] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    main.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"首页" image:[ImageNamed(@"tab_index") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[ImageNamed(@"tab_index_in") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    UIViewController* data = [[UIStoryboard dataStoryboard] instantiateViewControllerWithIdentifier:@"DataHomeController"];
    data.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"刷牙数据" image:[ImageNamed(@"tab_data") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[ImageNamed(@"tab_data_in") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    UIViewController* info = [[UIStoryboard infoStoryboard] instantiateViewControllerWithIdentifier:@"InfoHomeController"];
    info.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"牙齿百科" image:[ImageNamed(@"tab_index") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[ImageNamed(@"tab_index_in") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    UIViewController* mine = [[UIStoryboard mineStoryboard] instantiateViewControllerWithIdentifier:@"MineHomeViewController"];
    mine.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"个人中心" image:[ImageNamed(@"tab_mine") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[ImageNamed(@"tab_mine_in") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    UITabBarController* tabController = [[UITabBarController alloc] init];
    tabController.tabBar.backgroundColor = [UIColor clearColor];
    NSMutableArray* vcs = [NSMutableArray arrayWithCapacity:4];
    vcs[TabTypeHome] = [[BaseNavigationController alloc] initWithRootViewController:main];
    vcs[TabTypeSecond] = [[BaseNavigationController alloc] initWithRootViewController:data];
    vcs[TabTypeThird] = [[BaseNavigationController alloc] initWithRootViewController:info];
    vcs[TabTypeForth] = [[BaseNavigationController alloc] initWithRootViewController:mine];
    tabController.viewControllers = vcs;
    
    for (BaseNavigationController* navC in vcs) {
        navC.delegate = self;
    }
    self.window.rootViewController = tabController;
    [self.window makeKeyAndVisible];
}

-(void)configUIAppearance {
    [[UINavigationBar appearance] setTranslucent:NO];
//    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
//    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName: [UIFont systemFontOfSize:17.0f]}];
    
    //隐藏导航条带的“返回”标题
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor hyBlackTextColor]} forState:UIControlStateNormal];
    
    // 字体颜色 选中
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:11.0F], NSForegroundColorAttributeName : [UIColor hyBarSelectedColor]} forState:UIControlStateSelected];
    
    // 字体颜色 未选中
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:11.0F],  NSForegroundColorAttributeName:[UIColor hyBarUnselectedColor]} forState:UIControlStateNormal];
    
    [[UITabBar appearance] setTranslucent:NO];
    [[UITabBar appearance] setBackgroundColor:[UIColor whiteColor]];
    
    [UITableView appearance].separatorColor = [UIColor hySeparatorColor];
    [UITableView appearance].separatorInset = UIEdgeInsetsMake(0, 15, 0, 15);
}


// 如果vc.navigationBarHidden=YES,则会隐藏导航栏
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if ([(BaseViewController *)viewController respondsToSelector:@selector(navigationBarHidden)]) {
        [navigationController setNavigationBarHidden:[(BaseViewController *)viewController navigationBarHidden] animated:YES];
    }
//    
//// 设置蓝色背景navigationBar
//    if (((BaseViewController*)viewController).navigationBarBlue == YES) {
//        [navigationController.navigationBar setBackgroundImage:ImageNamed(@"bg_index") forBarMetrics:UIBarMetricsDefault];
//    }else{
//        [navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
//    }
}


-(void)configIQKeyBoardManager  {
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}


- (void)configShareSDK{
    [ShareSDK registerApp:kShareSDK_APPID
          activePlatforms:@[
                            @(SSDKPlatformTypeSinaWeibo),
                            @(SSDKPlatformSubTypeWechatSession),
                            @(SSDKPlatformSubTypeWechatTimeline),
                            @(SSDKPlatformTypeQQ)]
                 onImport:^(SSDKPlatformType platformType)
     {
         switch (platformType)
         {
             case SSDKPlatformTypeWechat:
                 [ShareSDKConnector connectWeChat:[WXApi class]];
                 break;
             case SSDKPlatformTypeQQ:
                 [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
                 break;
             case SSDKPlatformTypeSinaWeibo:
                 [ShareSDKConnector connectWeibo:[WeiboSDK class]];
                 break;
             default:
                 break;
         }
     }
          onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo)
     {
         
         switch (platformType)
         {
             case SSDKPlatformTypeSinaWeibo:
                 //设置新浪微博应用信息,其中authType设置为使用SSO＋Web形式授权
                 [appInfo SSDKSetupSinaWeiboByAppKey:kSINA_APPID
                                           appSecret:kSINA_KEY
                                         redirectUri:kSINA_REDIRECTURL
                                            authType:SSDKAuthTypeBoth];
                 break;
             case SSDKPlatformTypeWechat:
                 [appInfo SSDKSetupWeChatByAppId:kWX_APPID
                                       appSecret:kWX_KEY];
                 break;
             case SSDKPlatformTypeQQ:
                 [appInfo SSDKSetupQQByAppId:kQQ_APPID
                                      appKey:kQQ_KEY
                                    authType:SSDKAuthTypeBoth];
                 break;
             default:
                 break;
         }
     }];
}



-(void)configTabbar:(BOOL)test{
    if(!test){
        [self configTabbar];
    }else{
        TestViewController * v = [[TestViewController alloc] init];
        self.window.rootViewController =  [[BaseNavigationController alloc] initWithRootViewController:v];
        [self.window makeKeyAndVisible];
        
    }
}
-(void)configPaySDK {
    
    [WXApi registerApp:kWX_APPID withDescription:@"demo"];
}

-(void)configGuide{
    GuideViewController * guide = [[GuideViewController alloc] init];
   
    self.window.rootViewController =  guide;
    [self.window makeKeyAndVisible];
}

-(void)configQuestion{
    Questionnaire2ViewController * vc = [[Questionnaire2ViewController alloc] init];
//    BaseNavigationController * nvc = [BaseNavigationController instanceWith:vc];
    self.window.rootViewController =  vc;
    [self.window makeKeyAndVisible];
}

-(void)configConnect{
    ConectStep1ViewController * vc = [ConectStep1ViewController instance];
    self.window.rootViewController =  vc;
    [self.window makeKeyAndVisible];
}


///连接牙刷页面
-(void)configConnectWith:(NSDictionary*)params{
    ConectStep1ViewController * vc = [ConectStep1ViewController instance];
    vc.params = params;
    if(params && params[@"from"] && [params[@"from"]  isKindOfClass:[UIViewController class]]){
        UIViewController * from = params[@"from"];
        [from presentViewController:vc animated:YES completion:nil];
    }else{
         [self configConnect];
    }
}

-(void)configLogin{
    AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //这个是引导页跳转到登录页...
    LoginViewController * vc = [[UIStoryboard storyboardWithName:@"Login" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"LoginViewController"];
    
    BaseNavigationController * nvc = [[BaseNavigationController alloc] initWithRootViewController:vc];
    delegate.window.rootViewController = nvc;
    [delegate.window makeKeyAndVisible];
}
@end
