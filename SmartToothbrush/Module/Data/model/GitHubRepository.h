//
//  GitHubRepository.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/4.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GitHubUser.h"
@interface GitHubRepository : NSObject<UtilsDemoJsonModel>


@property(nonatomic,strong)NSString * name;
@property(nonatomic,strong)NSString * full_name;
@property(nonatomic,strong)NSString * desc;
@property(nonatomic,assign)NSInteger ID;

@property(nonatomic,strong)GitHubUser * user;
+(instancetype)instanceFrom:(NSDictionary *)jsonDict;
@end
