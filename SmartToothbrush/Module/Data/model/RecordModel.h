//
//  RecordModel.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/6/21.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeDataModel.h"
#import <YYModel/YYModel.h>
@interface RecordModel : NSObject

@property(nonatomic,strong)NSString * ID;

@property(nonatomic,strong)NSString *score;

@property(nonatomic,strong)NSString *nickname;


@property(nonatomic,strong)NSString * tb_no;
@property(nonatomic,strong)NSArray<ScoreModel*>*week;

@property(nonatomic,strong)NSArray<PressModel*>*press;

@property(nonatomic,strong)NSArray<PressModel*>*clear;

@end
