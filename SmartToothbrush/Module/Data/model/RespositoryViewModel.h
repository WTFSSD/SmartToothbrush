//
//  RespositoryViewModel.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/4.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GitHubRepository.h"
@interface RespositoryViewModel : NSObject


+(instancetype)instance;

@property(nonatomic,readonly)NSArray   * respositories;


-(void)search:(NSString*)keyword;
@end
