//
//  GitHubRepository.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/4.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "GitHubRepository.h"

@implementation GitHubRepository
+(instancetype)instanceFrom:(NSDictionary *)jsonDict{
    return [self yy_modelWithJSON:jsonDict];
}

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper{
    return @{@"description":@"desc",@"id":@"ID"};
}
@end
