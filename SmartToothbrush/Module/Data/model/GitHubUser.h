//
//  GitHubUser.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/4.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GitHubUser : NSObject<UtilsDemoJsonModel>

@property(nonatomic,strong)NSString * login;
@property(nonatomic,assign)NSInteger ID;
@property(nonatomic,strong)NSString *avatar_url;
+(instancetype)instanceFrom:(NSDictionary *)jsonDict;
@end
