//
//  TodayBrushScoreView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "TodayBrushScoreView.h"

@interface TodayBrushScoreView(){
    UILabel * titleLabel;
    CycleScoreView * cycleView;
}
@property(nonatomic,strong)UIButton * historyScore;
@end


@implementation TodayBrushScoreView



+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}


-(CycleScoreView *)scoreView{
    return cycleView;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

-(void)setUp{
   
    
    self.backgroundColor = [UIColor clearColor];
    titleLabel=[UILabel makeWith:^(UILabel *label) {
        label.textColor = UIColorFromHEX(0x70879e);
        label.font = [UIFont systemFontOfSize:16 weight:400];
        label.text = @"今天刷牙得分";
        
    }];
//    _title = @"今天刷牙得分";
    [self addSubview:titleLabel];
    [titleLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [titleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:15];
    
    
    _historyScore = [UIButton makeWith:^(UIButton *button) {
        button.setTitle(@"历史刷牙得分",UIControlStateNormal)
        .setTitleFont([UIFont systemFontOfSize:15])
        .setTitleColor(UIColorFromHEX(0x70879e),UIControlStateNormal);
        button.layer.cornerRadius = 5;
        button.layer.borderColor = UIColorFromHEX(0xeef2f6).CGColor;
        button.layer.borderWidth = 0.5;
    }];
    
    
    
    [self addSubview:_historyScore];
    [_historyScore autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [_historyScore autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:_historyScore withMultiplier:28/95.f];
    [_historyScore autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self withOffset:-15];
    
    
    cycleView = [CycleScoreView instance];
    [self addSubview:cycleView];
    [cycleView autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [cycleView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:titleLabel withOffset:5];
    [cycleView autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:_historyScore withOffset:-5];
    
    [cycleView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionHeight ofView:cycleView withMultiplier:1];
    
    cycleView.score = 66.f;
//    RAC(titleLabel,text) = [self rac_valuesForKeyPath:@"title" observer:nil];
}



@end
