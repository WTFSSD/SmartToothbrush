//
//  ScoreWithOtherView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/6/2.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "ScoreWithOtherView.h"

@interface ScoreWithOtherView (){
    UILabel * titleLabel;
}
@property(nonatomic,strong,readwrite)ChartView * chartView;

@end;
@implementation ScoreWithOtherView

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}
+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self setUp];
    }
    return self;
}

-(void)setUp{
    
    
    titleLabel = [UILabel makeWith:^(UILabel *label) {
        label.textColor = UIColorFromHEX(0x70879E);
        label.font = [UIFont systemFontOfSize:17];
    }];
    [self addSubview:titleLabel];
    [titleLabel autoAlignAxis:ALAxisVertical toSameAxisOfView:self];
    [titleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:zoom(40)];
    
    _chartView = [ChartView instance];
    
    [self addSubview:_chartView];
    
    
    [_chartView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:titleLabel withOffset:zoom(20)];
    [_chartView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self];
    [_chartView autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self];
    [_chartView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:_chartView withMultiplier:100/274.f];
    
    RAC(titleLabel,text) = [self rac_valuesForKeyPath:@"title" observer:self];
}

@end
