//
//  CalendarView.m
//  ToastDemo
//
//  Created by seed on 2016/11/25.
//  Copyright © 2016年 WTFSSD. All rights reserved.
//

#import "CalendarView.h"

/********************************************************/
/********                                        ********/
/********            单片日历组件Cell              ********/
/********                                        ********/
/********                  私有                   ********/
/********************************************************/
#pragma mark - 日历单元格 private 私有类
/**日历单元格 private 私有类*/

NSString * const _CalendarItemCellID = @"_CalendarItemCell";
@interface _CalendarItemCell : UICollectionViewCell

/**阴历时间标签*/
@property(nonatomic,strong)UILabel * lunarDayLable;
/**阳历历时间标签*/
@property(nonatomic,strong)UILabel * solarDayLable;

@property(nonnull,strong)NSDate * date;
-(_CalendarItemCell*(^)(NSInteger solarDay,NSString * chinessDay))setInfo;
-(_CalendarItemCell*(^)(NSData*)) setDate;
@end

@implementation _CalendarItemCell
-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        self.solarDayLable = [[UILabel alloc] initWithFrame:(CGRect){0,0,frame.size.width,frame.size.height}];
        self.solarDayLable.textColor = [UIColor blackColor];
        self.solarDayLable.font = [UIFont systemFontOfSize:12];
        self.solarDayLable.textAlignment = NSTextAlignmentCenter;
        
        self.lunarDayLable = [[UILabel alloc] initWithFrame:(CGRect){0,frame.size.height * 0.5,frame.size.width,frame.size.height * 0.5}];

        self.lunarDayLable.font = [UIFont systemFontOfSize:12];
        self.lunarDayLable.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.solarDayLable];
        [self.contentView addSubview:self.lunarDayLable];
        self.backgroundColor = [UIColor colorWithRed:0xf1/255.f green:0xf3/255.f blue:0xf8/255.f alpha:1];
        self.contentView.backgroundColor = [UIColor colorWithRed:0xf1/255.f green:0xf3/255.f blue:0xf8/255.f alpha:1];
        self.lunarDayLable.hidden = YES;
    }
    return self;
}

-(_CalendarItemCell *(^)(NSInteger, NSString *))setInfo{
    _CalendarItemCell *(^block)() = ^(NSInteger solarDay, NSString *lunarDay){
        self.solarDayLable.text = solarDay!=0?[NSString stringWithFormat:@"%ld",solarDay]:@"";
        self.lunarDayLable.text = lunarDay;
        return self;
    };
    return block;
}


-(_CalendarItemCell *(^)(NSData *))setDate{
    _CalendarItemCell *(^block)() = ^(NSDate * date){
        self.date = date;
        return self;
    };
    return block;
}


@end




/********************************************************/
/********                                        ********/
/********              单片日历组件                ********/
/********                                        ********/
/********                  私有                   ********/
/********************************************************/
#pragma mark - 单片日历组件 private 私有类
/**单片日历组件*/
@interface _CalendarItem:UICollectionView<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property(nonatomic,strong)NSDate * __today;
-(_CalendarItem*(^)(NSDate*))setCurrentDate;
@property(nonatomic,assign)CalendarViewDisplayType dType;
@end
@implementation _CalendarItem

-(instancetype)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout{
    if(self = [super initWithFrame:frame collectionViewLayout:layout]){
        self.delegate = self;
        self.dataSource = self;
        self.showsVerticalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        self.backgroundColor = [UIColor clearColor];
        [self registerClass:[_CalendarItemCell class] forCellWithReuseIdentifier:_CalendarItemCellID];
        self.dType = CalendarViewDisplayType2;
    }
    return self;
}

-(void)setDType:(CalendarViewDisplayType)dType{
    _dType = dType;
    [self reloadData];
}

-(void)layoutSubviews{
    [super layoutSubviews];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 35;
}




-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    _CalendarItemCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:_CalendarItemCellID forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor colorWithRed:0xf1/255.f green:0xf3/255.f blue:0xf8/255.f alpha:1];
    cell.contentView.backgroundColor = [UIColor colorWithRed:0xf1/255.f green:0xf3/255.f blue:0xf8/255.f alpha:1];
    cell.backgroundColor = [UIColor clearColor];
    cell.layer.cornerRadius = 0;
    cell.setInfo(0,@"");
    //月份的第一周
    NSInteger   firstWeekday            = 0;
    
    //月份总共天数
    NSInteger   totalDaysOfMonth        = 0;
    
    //上一个月总共天数
    NSInteger   totalDaysOfLastMonth    = 0;
    //日
    NSInteger   itemDay                 = 0;
    NSDate   *  itemDate                = nil;
    NSString *  chinessMonth            = nil;
    
    //农历日
    NSString *  chinessDay              = nil;
    //上一个月
    NSDate   *  dateOfPreMonth          = nil;
    //下一个月
    NSDate   *  dateOfNextMonth         = nil;
    [DateUnits shareUnits]
    .weekdayOfFirstDayInDate(&firstWeekday,         self.__today)
    .daysOfMonthInDate      (&totalDaysOfMonth,     self.__today)
    .preMonthDataInDate     (&dateOfPreMonth,       self.__today)
    .nextMonthDataInDate    (&dateOfNextMonth,      self.__today)
    .daysOfMonthInDate      (&totalDaysOfLastMonth, dateOfPreMonth);
    
    if(indexPath.item<firstWeekday){
        //上月
        itemDay = totalDaysOfLastMonth - firstWeekday + indexPath.item + 1;
        itemDate = dateOfPreMonth;
    }else if (indexPath.item >= totalDaysOfMonth + firstWeekday){
        //下月
        itemDay = indexPath.item - totalDaysOfMonth - firstWeekday + 1;
        itemDate = dateOfNextMonth;
    }else{
        //本月
        itemDay = indexPath.item - firstWeekday + 1;
        itemDate = self.__today;
        
        //MARK:高亮与今天同一天的日期
        
        NSInteger y1 = -1,m1 = -2,d1 = -3;
        NSInteger y2 = -10,m2 = -20,d2 = -30;

         [DateUnits shareUnits].dateInfoInDate(itemDate,&y1,&m1,&d1,NULL,NULL,NULL);
         [DateUnits shareUnits].dateInfoInDate(Today,&y2,&m2,&d2,NULL,NULL,NULL);

        if(y1 == y2 && m1 == m2 && itemDay == d2){
//            cell.backgroundColor = [UIColor redColor];
//            cell.layer.cornerRadius = cell.frame.size.height / 2;
            
            cell.backgroundColor = [UIColor colorWithRed:0x12/255.f green:0xc6/255.f blue:0xfc/255.f alpha:1];
            cell.contentView.backgroundColor = [UIColor colorWithRed:0x12/255.f green:0xc6/255.f blue:0xfc/255.f alpha:1];
        }
//        if(self.dType == CalendarViewDisplayType1){
//            [DateUnits shareUnits].chinessDateInDateWithDay(&chinessMonth,&chinessDay,itemDate,itemDay);
//            if([chinessDay isEqualToString:@"初一"]) {chinessDay = chinessMonth;}
//            cell.setInfo(itemDay,chinessDay);
//        }
//
    }
//    if(self.dType == CalendarViewDisplayType2){
//        [DateUnits shareUnits].chinessDateInDateWithDay(&chinessMonth,&chinessDay,itemDate,itemDay);
//        if([chinessDay isEqualToString:@"初一"]) {chinessDay = chinessMonth;}
//        cell.setInfo(itemDay,chinessDay);
//    }
    [DateUnits shareUnits].chinessDateInDateWithDay(&chinessMonth,&chinessDay,itemDate,itemDay);
    if([chinessDay isEqualToString:@"初一"]) {chinessDay = chinessMonth;}
    cell.setInfo(itemDay,chinessDay);
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //月份的第一周
    NSInteger   firstWeekday            = 0;
    
    //月份总共天数
    NSInteger   totalDaysOfMonth        = 0;
    
    //上一个月总共天数
    NSInteger   totalDaysOfLastMonth    = 0;
    //日
    NSInteger   itemDay                 = 0;
    NSDate   *  itemDate                = nil;
    //上一个月
    NSDate   *  dateOfPreMonth          = nil;
    //下一个月
    NSDate   *  dateOfNextMonth         = nil;
    [DateUnits shareUnits]
    .weekdayOfFirstDayInDate(&firstWeekday,         self.__today)
    .daysOfMonthInDate      (&totalDaysOfMonth,     self.__today)
    .preMonthDataInDate     (&dateOfPreMonth,       self.__today)
    .nextMonthDataInDate    (&dateOfNextMonth,      self.__today)
    .daysOfMonthInDate      (&totalDaysOfLastMonth, dateOfPreMonth);
    
    if(indexPath.item<firstWeekday){
        //上月
        itemDay = totalDaysOfLastMonth - firstWeekday + indexPath.item + 1;
        itemDate = dateOfPreMonth;
    }else if (indexPath.item >= totalDaysOfMonth + firstWeekday){
        //下月
        itemDay = indexPath.item - totalDaysOfMonth - firstWeekday + 1;
        itemDate = dateOfNextMonth;
    }else{
        //本月
        itemDay = indexPath.item - firstWeekday + 1;
        itemDate = self.__today;
    }
    
    NSInteger y1 = -1,m1 = -2,d1 = itemDay;
    
    [DateUnits shareUnits].dateInfoInDate(itemDate,&y1,&m1,NULL,NULL,NULL,NULL);
    
    NSDate * date = [[NSCalendar currentCalendar] dateWithEra:0 year:y1 month:m1 day:d1 hour:0 minute:0 second:0 nanosecond:0];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"_CalendarItemDidSelectDate" object:nil userInfo:@{@"value":date}];
    
}

-(_CalendarItem*(^)(NSDate*))setCurrentDate{
    _CalendarItem*(^block)() = ^(NSDate*date){
        self.__today = date;
        [self reloadData];
        return self;
    };
    return block;
}
@end




/********************************************************/
/********                                        ********/
/********            日历组件顶部工具栏             ********/
/********                                        ********/
/********                  私有                   ********/
/********************************************************/

@interface _CalendarViewToolBar : UIView{
    UIView * lineTop;
    UIView * lineBottom;
}

@property(nonatomic,strong)UILabel * titleLabel;

@property(nonatomic,strong)UIButton * nextButton;
@property(nonatomic,strong)UIButton * preButton;

-(_CalendarViewToolBar*(^)(NSString * mounth))setTitle;
@end

@implementation _CalendarViewToolBar
-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        self.titleLabel = [[UILabel alloc] initWithFrame:(CGRect){frame.size.width * 0.25,0,frame.size.width * 0.5,frame.size.height}];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:24];
        self.titleLabel.textColor = [UIColor colorWithRed:0x4a/255.f green:0x5a/255.f blue:0x6a/255.f alpha:1];
        [self addSubview:self.titleLabel];
        
        self.nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.nextButton setImage:[UIImage imageNamed:@"calendar_next.png"] forState:UIControlStateNormal];
        
        self.preButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.preButton setImage:[UIImage imageNamed:@"calendar_prev.png"] forState:UIControlStateNormal];
        
        
        [self addSubview:self.nextButton];
        [self addSubview:self.preButton];
        
        lineTop = [[UIView alloc] initWithFrame:CGRectZero];
        lineTop.backgroundColor = [UIColor colorWithRed:0xee/255.f green:0xee/255.f blue:0xee/255.f alpha:1];
        lineBottom = [[UIView alloc] initWithFrame:CGRectZero];
        lineBottom.backgroundColor = [UIColor colorWithRed:0xee/255.f green:0xee/255.f blue:0xee/255.f alpha:1];
        
        [self addSubview:lineBottom];
        [self addSubview:lineTop];
        
    }
    return self;
}


-(void)layoutSubviews{
    [super layoutSubviews];
    
    self.preButton.frame = CGRectMake(self.frame.size.width * 0.25 - 20 - 20, (self.frame.size.height - 20) * 0.5, 20, 20);
    self.nextButton.frame = CGRectMake(self.frame.size.width * 0.75 + 20 +20, (self.frame.size.height - 20) * 0.5, 20, 20);
    
    lineTop.frame = CGRectMake(0, 0, self.frame.size.width, 1);
    lineBottom.frame = CGRectMake(0, self.frame.size.height - 1, self.frame.size.width, 1);
    
    
}
-(_CalendarViewToolBar *(^)(NSString *))setTitle{
    _CalendarViewToolBar *(^block)() = ^(NSString * msg){
        self.titleLabel.text = msg;
        return self;
    };
    return block;
}

@end




/********************************************************/
/********                                        ********/
/********              日历组件主体                ********/
/********                                        ********/
/********                  私有                   ********/
/********************************************************/





#pragma mark - 日历组件 public 公共类

@interface CalendarView()<UIScrollViewDelegate>

/**左侧单片日历组件 私有*/
@property(nonatomic,strong)_CalendarItem * leftCalendar;
/**中间单片日历组件 私有*/
@property(nonatomic,strong)_CalendarItem * centerCalendar;
/**右侧单片日历组件 私有*/
@property(nonatomic,strong)_CalendarItem * rightCalendar;


@property(nonatomic,strong,readwrite)NSDate * today;

@property(nonatomic,strong)NSDate * __today;

@property(nonatomic,strong)UIScrollView * scrollView;

@property(nonatomic,strong)_CalendarViewToolBar * toolBar;
@property(nonatomic,strong)NSDate * selectedDate;

@end

@implementation CalendarView


#pragma mark - 以下为公共方法 可以由外部调用或主动调用
-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:CGRectMake(frame.origin.x, 0, frame.size.width, 5*(frame.size.width - CollectionViewHorizonMargin*2)/6)]){
        self.backgroundColor = [UIColor whiteColor];
        self.__today = Today;
        [self setUpWeekTitle];
        [self setUpToolBar];
        [self setUpCalendars];
        [self updateCalendars];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNotice:) name:@"_CalendarItemDidSelectDate" object:nil];
        self.selectedDate = self.today;
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
    return self;
}


-(void)onNotice:(NSNotification * )notice{
    if([notice.name isEqualToString:@"_CalendarItemDidSelectDate"]){
        self.selectedDate = notice.userInfo[@"value"];
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"_CalendarItemDidSelectDate" object:nil];
}

+(instancetype)calendarView{
    
    return [[CalendarView alloc] initWithFrame:(CGRect){0,0,ScreenWidth,}].setCurrentDate(Today).setDisplayType(CalendarViewDisplayTypeDefault);
}

#pragma mark - 更新日历
/**更新日历*/
-(void)updateCalendars{
    CGPoint offset = self.scrollView.contentOffset;
    NSDate * preDate = nil;
    NSDate * nextDate = nil;
    NSInteger year = 0;
    NSInteger month = 0;
    [DateUnits shareUnits].preMonthDataInDate(&preDate,self.__today).nextMonthDataInDate(&nextDate,self.__today)
    ;
    if(offset.x>self.frame.size.width){
        //TODO:处理下个月数据
        self.setCurrentDate(nextDate);
    }else if(offset.x<self.frame.size.width){
        //TODO:处理上个月数据
        self.setCurrentDate(preDate);
    }
    [DateUnits shareUnits].dateInfoInDate(self.__today,&year,&month,NULL,NULL,NULL,NULL);
    self.toolBar.setTitle([NSString stringWithFormat:@"%ld年%ld月",year,month]);
    if(offset.x == self.frame.size.width) return;
    self.scrollView.contentOffset = (CGPoint){self.frame.size.width,0};
}

#pragma mark - 设置日历的"今天"
/**设置日历的"今天"*/
-(CalendarView *(^)(NSDate *))setCurrentDate{
    CalendarView*(^block)() = ^(NSDate * date){
        NSDate * preDate = nil;
        NSDate * nextDate = nil;
        self.__today = date;
        [DateUnits shareUnits].preMonthDataInDate(&preDate,date).nextMonthDataInDate(&nextDate,date);
        self.centerCalendar.setCurrentDate(date);
        self.leftCalendar.setCurrentDate(preDate);
        self.rightCalendar.setCurrentDate(nextDate);
        return self;
    };
    return block;
}

-(NSDate *)today{
    if(!_today){
        _today = Today;
    }
    return _today;
}

-(CalendarView *(^)(CalendarViewDisplayType))setDisplayType{
    CalendarView *(^block)() = ^(CalendarViewDisplayType atype){
        self.leftCalendar.dType = atype;
        self.centerCalendar.dType = atype;
        self.rightCalendar.dType = atype;
        return self;
    };
    return block;
}


#pragma mark - 以下为私有方法 不能被外部调用或主动调用
//MARK:设置3个单片日历组件
-(void)setUpCalendars{
    self.scrollView = [[UIScrollView alloc] initWithFrame:(CGRect){0,2*CalendarViewToolBarHeight,self.frame.size.width,self.frame.size.height - CalendarViewToolBarHeight}];
    [self addSubview:self.scrollView];
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.delegate = self;
    CGFloat itemWidth = (self.frame.size.width - CollectionViewHorizonMargin * 2) / 7;
    CGFloat itemHeight = itemWidth;
    
    UICollectionViewFlowLayout *flowLayot = [[UICollectionViewFlowLayout alloc] init];
    flowLayot.sectionInset = UIEdgeInsetsZero;
    flowLayot.itemSize = CGSizeMake(itemWidth, itemHeight);
    flowLayot.minimumLineSpacing = 2;
    flowLayot.minimumInteritemSpacing = 0;
    self.leftCalendar = [[_CalendarItem alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayot];
    self.centerCalendar = [[_CalendarItem alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayot];
    self.rightCalendar = [[_CalendarItem alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayot];
    

    [self.scrollView addSubview:self.leftCalendar];
    [self.scrollView addSubview:self.centerCalendar];
    [self.scrollView addSubview:self.rightCalendar];
    
    
    CGRect rect1 = self.frame;
    CGRect rect2 = self.frame;
    CGRect rect3 = self.frame;
    
    rect1.origin.x = 0;
    rect2.origin.x = rect1.size.width;
    rect3.origin.x = 2*rect1.size.width;
    self.leftCalendar.frame = rect1;
    self.centerCalendar.frame = rect2;
    self.rightCalendar.frame = rect3;
    
    
    self.scrollView.contentSize = (CGSize){3*rect1.size.width,0};
    self.scrollView.contentOffset = (CGPoint){rect1.size.width,0};
}


//MARK:设置顶部工具栏
-(void)setUpToolBar{
    self.toolBar = [[_CalendarViewToolBar alloc] initWithFrame:(CGRect){0,0,self.frame.size.width,CalendarViewToolBarHeight}];
    [self.toolBar.nextButton addTarget:self action:@selector(onNextClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.toolBar.preButton addTarget:self action:@selector(onPreClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.toolBar];
}

//MARK:设置星期显示
-(void)setUpWeekTitle{
    UIView * contentView = [[UIView alloc] initWithFrame:(CGRect){0,CalendarViewToolBarHeight,self.frame.size.width,CalendarViewToolBarHeight}];
    contentView.backgroundColor = [UIColor clearColor];
    [self addSubview:contentView];
    NSArray * arr = @[@"周日",@"周一",@"周二",@"周三",@"周四",@"周五",@"周六"];
    CGFloat itemWidth = (self.frame.size.width - CollectionViewHorizonMargin * 2) / 7;
    for (int i = 0; i<7; i++) {
        UILabel * label = [[UILabel alloc] initWithFrame:(CGRect){i*(itemWidth+CollectionViewHorizonMargin/3.f),0,itemWidth,CalendarViewToolBarHeight}];
        label.textColor = [UIColor blackColor];
        label.font = [UIFont systemFontOfSize:12];
        label.textColor = i == 0?[UIColor colorWithRed:0x99/255.f green:0x99/255.f blue:0x99/255.f alpha:1]:[UIColor colorWithRed:0x33/255.f green:0x33/255.f blue:0x33/255.f alpha:1];
        label.text = arr[i];
        label.textAlignment = NSTextAlignmentCenter;
        [contentView addSubview:label];
    }
    
}


-(void)onPreClick:(id)sender{
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x - self.frame.size.width, 0) animated:YES];
}

-(void)onNextClick:(id)sender{
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x + self.frame.size.width, 0) animated:YES];
}

-(void)layoutSubviews{
    [super layoutSubviews];

    
    self.scrollView.frame = CGRectMake(0,2*CalendarViewToolBarHeight , self.frame.size.width, self.frame.size.height - 2*CalendarViewToolBarHeight);

    CGRect rect1 = self.scrollView.frame;
    CGRect rect2 = self.scrollView.frame;
    CGRect rect3 = self.scrollView.frame;
    rect1.origin.y = 0;
    rect2.origin.y = 0;
    rect3.origin.y = 0;
    rect1.origin.x = 0;
    rect2.origin.x = rect1.size.width;
    rect3.origin.x = 2*rect1.size.width;
    
    rect1.size.height = self.frame.size.height - 2*CalendarViewToolBarHeight;
    rect2.size.height = self.frame.size.height - 2*CalendarViewToolBarHeight;
    rect3.size.height = self.frame.size.height - 2*CalendarViewToolBarHeight;
    self.leftCalendar.frame = rect1;
    self.centerCalendar.frame = rect2;
    self.rightCalendar.frame = rect3;
}



#pragma mark - ScrollViewDelegate scrollview代理方法 停止减速
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self updateCalendars];
}
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [self updateCalendars];
}
@end
