//
//  TodayBrushScoreView.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CycleScoreView.h"
@interface TodayBrushScoreView : UIView




@property(nonatomic,copy)NSString * title;


@property(nonatomic,strong,readonly)UIButton * historyScore;


@property(nonatomic,strong)CycleScoreView * scoreView;
+(instancetype)instance;




@end
