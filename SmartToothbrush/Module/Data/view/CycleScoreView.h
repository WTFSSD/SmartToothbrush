//
//  CycleScoreView.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CycleScoreView : UIView

/**得分0.f~100.f default = 90.f */
@property(nonatomic,assign)CGFloat score;

+(instancetype)instance;
@end
