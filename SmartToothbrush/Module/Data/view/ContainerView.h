//
//  ContainerView.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContainerView : UIView

+(instancetype)instance;


-(void)drawRoundRect:(CGFloat)lt rt:(CGFloat)rt lb:(CGFloat)lb rb:(CGFloat)rb;

@end
