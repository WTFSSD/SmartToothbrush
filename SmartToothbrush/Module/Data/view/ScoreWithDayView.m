//
//  ScoreWithDayView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/6/2.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "ScoreWithDayView.h"
#import "CalendarView.h"
#import "CycleScoreView.h"

@interface ScoreWithDayView(){
        UILabel * titleLabel;
}
@end;

@implementation ScoreWithDayView


-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}
+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self setUp];
    }
    return self;
}

-(void)setUp{
    CalendarView * v = [CalendarView calendarView];
        [self addSubview:v];
        [v autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self];
        [v autoSetDimension:ALDimensionHeight toSize:CalendarViewToolBarHeight*2 +  5*(ScreenWidth- CollectionViewHorizonMargin*2 - 4 * 6)/6];
        [v autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self];
        [v autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self];

        titleLabel = [UILabel makeWith:^(UILabel *label) {
            label.textColor = UIColorFromHEX(0x70879E);
            label.font = [UIFont systemFontOfSize:17];
            NSString * str = nil;
            [DateUnits shareUnits].formatData(Today,TimeFormatStyle1,&str);
            label.text = [NSString stringWithFormat:@"%@刷牙得分",str];
        }];
        [self addSubview:titleLabel];
        [titleLabel autoAlignAxis:ALAxisVertical toSameAxisOfView:self];
        [titleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:v withOffset:15];
    
        CycleScoreView * score = [CycleScoreView instance];
        [self addSubview:score];
        [score autoAlignAxis:ALAxisVertical toSameAxisOfView:self];
        [score autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:titleLabel withOffset:15];
        [score autoSetDimensionsToSize:CGSizeMake(zoom(75), zoom(75))];
    
        RAC(titleLabel,text) = [[v rac_signalForControlEvents:UIControlEventValueChanged] map:^id(id value) {
            CalendarView * cv = (CalendarView *)value;
            NSString * str = nil;
            [DateUnits shareUnits].formatData(cv.selectedDate,TimeFormatStyle1,&str);
            return [NSString stringWithFormat:@"%@刷牙得分",str];
        }];
}

@end
