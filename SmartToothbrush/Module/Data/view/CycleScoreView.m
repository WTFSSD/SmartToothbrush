//
//  CycleScoreView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "CycleScoreView.h"

@interface CycleScoreView(){
    UILabel * textLabel;
    
    
    CAShapeLayer * layer;
}
@end

@implementation CycleScoreView

+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

-(void)setUp{
    
    self.backgroundColor = [UIColor clearColor];
    textLabel = [UILabel makeWith:^(UILabel *label) {
        label.textColor = UIColorFromHEX(0x708794);
    }];
    [self addSubview:textLabel];
    [textLabel autoCenterInSuperview];
    RAC(textLabel,attributedText) = [[self rac_valuesForKeyPath:@"score" observer:self] map:^id(id value) {
        CGFloat f = [value floatValue];
        
        NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%02.0f分",f]];
        CGFloat l = str.length;
        
        [str addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:32 weight:400]} range:NSMakeRange(0, l-1)];
        [str addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12 weight:400]} range:NSMakeRange(l-1, 1)];
        return str;
    }];
}


-(void)drawRect:(CGRect)rect{
    
    CGPoint center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidX(rect));
    
    CGFloat lineWidth = 3;
    
//    if(layer){
//        [layer removeFromSuperlayer];
//    }else{
//        layer = [CAShapeLayer layer];
//        layer.bounds = rect;
//    }

    
    UIBezierPath * path = [UIBezierPath bezierPath];
    [UIColorFromHEX(0xeaeef4) set];
    path.lineWidth = lineWidth;
    [path addArcWithCenter:center radius:rect.size.width/2.f-lineWidth/2.f startAngle:0 endAngle:M_PI * 2 clockwise:YES];
    [path stroke];

    CGFloat endAngle =   M_PI * 2.f * (1-_score/100.f) - M_PI/2.f ;
    
    UIBezierPath * path1 = [UIBezierPath bezierPath];
    [UIColorFromHEX(0x12c6fc) set];
    path1.lineWidth = lineWidth;
    [path1 addArcWithCenter:center radius:rect.size.width/2.f-lineWidth/2.f startAngle:-M_PI/2.f  endAngle:endAngle clockwise:NO];
    [path1 stroke];
    
//    CGContextRef contex = UIGraphicsGetCurrentContext();
//
//    CGContextSetStrokeColorWithColor(contex, UIColorFromHEX(0xeaeef4).CGColor);
//    CGContextSetFillColorWithColor(contex, [UIColor clearColor].CGColor);
//    CGContextSetLineWidth(contex, 2);
//    CGContextAddArc(contex, center.x, center.y, rect.size.width/2, 0, M_PI * 2, YES);
//    CGContextStrokePath(contex);
//    CGContextFillPath(contex);
    
    
    
    
}
@end
