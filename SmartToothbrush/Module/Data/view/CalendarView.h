//
//  CalendarView.h
//  ToastDemo
//
//  Created by seed on 2016/11/25.
//  Copyright © 2016年 WTFSSD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DateUnits.h"

#ifndef ScreenWidth
#define ScreenWidth [UIScreen mainScreen].bounds.size.width
#endif

#ifndef ScreenHeight
#define ScreenHeight [UIScreen mainScreen].bounds.size.height
#endif
#define CalendarViewToolBarHeight  64.0f
#define CollectionViewHorizonMargin 5
#define CollectionViewVerticalMargin 5

typedef NS_ENUM(NSUInteger, CalendarViewDisplayType) {
    CalendarViewDisplayType1        = 0,
    CalendarViewDisplayType2        = 1,
    CalendarViewDisplayTypeDefault  = CalendarViewDisplayType1,
};
/**
 *   日历组件
 */
@interface CalendarView : UIControl


@property(nonatomic,strong,readonly)NSDate * today;

@property(nonatomic,strong,readonly)NSDate * selectedDate;


+(instancetype)calendarView;



/**设置日历当前的日期*/
-(CalendarView*(^)(NSDate*))setCurrentDate;

-(CalendarView*(^)(CalendarViewDisplayType))setDisplayType;

@end
