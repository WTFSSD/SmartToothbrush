//
//  ScoreWithOtherView.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/6/2.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChartView.h"
@interface ScoreWithOtherView : UIView

@property(nonatomic,copy)NSString * title;

@property(nonatomic,strong,readonly)ChartView * chartView;
+(instancetype)instance;
@end
