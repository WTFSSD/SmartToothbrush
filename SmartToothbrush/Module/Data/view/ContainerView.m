//
//  ContainerView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "ContainerView.h"

@interface ContainerView(){
    CGFloat lt,lb,rt,rb;
}
@end
@implementation ContainerView
+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}


-(void)setUp{
    lt=0;lb=0;rt=0;rb=0;
    self.backgroundColor = [UIColor clearColor];
    self.userInteractionEnabled = YES;
    
}


-(void)drawRect:(CGRect)rect{
    CGContextRef contex =UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(contex, [UIColor whiteColor].CGColor);
    [self drawRoundRect:contex rect:rect r1:lt r2:rt r3:rb r4:lb];
    CGContextFillPath(contex);
}

-(void)drawRoundRect:(CGFloat)r1 rt:(CGFloat)r2 lb:(CGFloat)r3 rb:(CGFloat)r4{
    lt = r1;rt = r2;rb= r3;lb = r4;
    [self setNeedsDisplay];
}

//顺时钟绘画 左上角 圆角矩形
-(void)drawRoundRect:(CGContextRef)context rect:(CGRect)rect r1:(CGFloat)r1 r2:(CGFloat)r2 r3:(CGFloat)r3 r4:(CGFloat)r4{
    
    CGContextMoveToPoint(context, rect.origin.x, rect.origin.y+r1);
    
    
    CGContextAddArc(context, CGRectGetMinX(rect)+r1, rect.origin.y+r1, r1, M_PI, -M_PI/2, NO);
    
    CGContextAddLineToPoint(context, CGRectGetMaxX(rect)-r2, CGRectGetMinY(rect));
    
    
    CGContextAddArc(context, CGRectGetMaxX(rect)-r2, CGRectGetMinY(rect)+r2, r2, -M_PI/2, 0, NO);
    
    CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMaxY(rect)-r3);
    
    
    CGContextAddArc(context, CGRectGetMaxX(rect)-r3, CGRectGetMaxY(rect)-r3, r3, 0 , M_PI/2, NO);
    
    CGContextAddLineToPoint(context, CGRectGetMinX(rect)+r4, CGRectGetMaxY(rect));
    
    
    CGContextAddArc(context, CGRectGetMinX(rect)+r4, CGRectGetMaxY(rect)-r4, r4, M_PI/2, M_PI ,NO);
    
    CGContextAddLineToPoint(context, CGRectGetMinX(rect), CGRectGetMinY(rect) + r1);
    
    //     CGContextAddRect(context, rect);
}







@end
