//
//  DataHomeSectionHeader.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//


typedef NS_ENUM(NSUInteger, DataHomeSectionHeaderArrow) {
    DataHomeSectionHeaderArrowNone = 1,
    DataHomeSectionHeaderArrowDown,
    DataHomeSectionHeaderArrowUp,
};
#import <UIKit/UIKit.h>


@interface DataHomeSectionHeader : UITableViewHeaderFooterView

@property(nonatomic,assign)DataHomeSectionHeaderArrow arrowType;

@property(nonatomic,assign)NSUInteger section;


@property(nonatomic,strong)NSString * title;

@property(nonatomic)void(^onArrowClick)(NSUInteger section);


@end
