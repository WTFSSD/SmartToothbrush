//
//  DataHomeSectionHeader.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "DataHomeSectionHeader.h"
#import "ContainerView.h"
@interface DataHomeSectionHeader(){
    UILabel * textLabel;
    ContainerView * container;
    
    UIButton * arrowButton;
    
}

@end

@implementation DataHomeSectionHeader


-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}
-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithReuseIdentifier:reuseIdentifier]){
        
        [self setUp];
    }
    return  self;
}


-(void)setUp{
    self.contentView.backgroundColor = [UIColor clearColor];
    container = [ContainerView instance];
    [self.contentView addSubview:container];
    [container autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.contentView];
    [container autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.contentView];
    [container autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.contentView withOffset:15];
    [container autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.contentView withOffset:-15];
    textLabel = [UILabel makeWith:^(UILabel *label) {
        label.text = @"我的牙刷";
        label.textColor = UIColorFromHEX(0x70879e);
    }];
    [container addSubview:textLabel];
    [textLabel autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [textLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:container withOffset:15];
    
    
    arrowButton = [[UIButton alloc] initWithFrame:CGRectZero];
    [container addSubview:arrowButton];
    [arrowButton autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [arrowButton autoSetDimensionsToSize:CGSizeMake(zoom(15), zoom(7.7))];
    [arrowButton autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:container withOffset:-15];
    
    
    
    
    
    RACSignal * s = [self rac_valuesForKeyPath:@"arrowType" observer:self];
    
    RAC(arrowButton,hidden) = [s map:^id(id value) {
        DataHomeSectionHeaderArrow type = [value integerValue];
        return @(type == DataHomeSectionHeaderArrowNone);
    }];
    
    [[s map:^id(id value) {
        DataHomeSectionHeaderArrow type = [value integerValue];
        switch (type) {
            case DataHomeSectionHeaderArrowUp:return ImageNamed(@"arrow_top");
            case DataHomeSectionHeaderArrowDown:return ImageNamed(@"arrow_down");
            default:return nil;
        }
        return nil;
    }] subscribeNext:^(id x) {
        [arrowButton setImage:x forState:UIControlStateNormal];
    }] ;
    
    _arrowType = DataHomeSectionHeaderArrowNone;
    
    
    @weakify(self);
    [[arrowButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        if(self.onArrowClick){
            self.onArrowClick(self.section);
        }
    }];
    
    
    
    RAC(textLabel,text) = [self rac_valuesForKeyPath:@"title" observer:self];
}
-(void)layoutSubviews{
    [super layoutSubviews];
    if(self.arrowType == DataHomeSectionHeaderArrowNone){
        [container drawRoundRect:10 rt:10 lb:0 rb:0];
    }
    if(self.arrowType == DataHomeSectionHeaderArrowUp){
         [container drawRoundRect:10 rt:10 lb:0 rb:0];
    }
    if(self.arrowType == DataHomeSectionHeaderArrowDown){
        [container drawRoundRect:10 rt:10 lb:10 rb:10];
    }
}
@end
