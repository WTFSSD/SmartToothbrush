//
//  HomeDataInfoCell.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "HomeDataInfoCell.h"


@interface HomeDataInfoCell(){
    TodayBrushScoreView * scoreView;
    
    ToothScoreView * toothChartView;
    
    
    ContainerView * container;
    ToothBrushingData * dataView;
}
@end

@implementation HomeDataInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
     [self setUp];
}

-(TodayBrushScoreView *)todayScoreView{
    return scoreView;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self setUp];
    }
    return self;
}


-(void)setUp{
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    container = [ContainerView instance];
    [self.contentView addSubview:container];
    [container autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.contentView];
    [container autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.contentView];
    [container autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.contentView withOffset:15];
    [container autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.contentView withOffset:-15];
    
    scoreView = [TodayBrushScoreView instance];
    @weakify(self);
    [scoreView.historyScore bk_whenTapped:^{
        @strongify(self);
        if (self.onHistoryScoreClick) {
            
            self.onHistoryScoreClick(self.model);
        }
    }];
    [container addSubview:scoreView];
    [scoreView autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [scoreView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:container withMultiplier:1];
    [scoreView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:scoreView withMultiplier:223/351.f];
    [scoreView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:container];
    
    
    
    toothChartView  = [ToothScoreView instance];
    toothChartView.title = @"本周刷牙得分";
    [container addSubview:toothChartView];
    [toothChartView autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [toothChartView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:container withMultiplier:1];
    [toothChartView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:toothChartView withMultiplier:211/300.f];
    [toothChartView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:scoreView];
    
    
    
    
    
    dataView = [ToothBrushingData instance];
    [container addSubview:dataView];
    [dataView autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [dataView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:toothChartView];
    [dataView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:container withMultiplier:1];
    [dataView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:dataView withMultiplier:327/351.f];
    
    
    [[self rac_valuesForKeyPath:@"model" observer:self] subscribeNext:^(id x) {
        if(!x) return;
        RecordModel * model = x;
        scoreView.scoreView.score = [model.score floatValue];
        [toothChartView.chart setDatas:[model.week bk_map:^id(id obj) {
            ScoreModel * m = obj;
            ChartViewData * data = [[ChartViewData alloc] init];
            
            data.score = [m.score floatValue];
            data.time = m.date;
            return data;
        }]];
        
    }];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [container drawRoundRect:0 rt:0 lb:10 rb:10];
}
@end
