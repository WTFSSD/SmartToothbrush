//
//  HomeDataInfoCell.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TodayBrushScoreView.h"
#import "ToothScoreView.h"
#import "ContainerView.h"
#import "ToothBrushingData.h"

#import "RecordModel.h"
@interface HomeDataInfoCell : UITableViewCell

@property(nonatomic)void(^onHistoryScoreClick)(RecordModel * model);
  @property(nonatomic,strong) TodayBrushScoreView * todayScoreView;




@property(nonatomic,strong)RecordModel * model;
@end
