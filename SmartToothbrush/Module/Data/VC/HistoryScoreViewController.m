//
//  HistoryScoreViewController.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/12.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "HistoryScoreViewController.h"
#import "CalendarView.h"
#import "CycleScoreView.h"
#import "BaseNavigationController.h"

#import "ScoreWithDayView.h"
#import "ScoreWithOtherView.h"

#import "HomeDataModel.h"
@interface HistoryScoreViewController ()<BaseNavgationControllerDelegate>{

    UISegmentedControl * segment;
    ScoreWithDayView * scoreViewDay;
    ScoreWithOtherView * scoreViewOther;
    
    NSMutableArray<ScoreModel * > * dataSource;
}
 


@end

@implementation HistoryScoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    self.navigationBarHidden = NO;
    self.navigationBarTransparent = NO;
    self.navigationItem.title = @"历史刷牙得分";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:UIColorFromHEX(0x333333)};
    
    [self getData];
}


-(void)baseSetBackgroundImage{}

-(BackButtonType)BaseNavgationControllerBackButtonTypeFor:(UINavigationController *)nvc{
    return BackButtonTypeBlack;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)getData{
    NSDictionary * params = @{@"key":[UserInfoModel share].key,@"month":[self stringFrom:nil],@"tb_no":self.tbNo};
    [NetworkUtils requestWith:HTTPMethodGET url:REQUEST_URL(API_MEMBER_HISTORY_DAY) params:params].then(^(id res){
        if(res && [res[@"code"] integerValue] == 1){
             [dataSource removeAllObjects];
            NSArray * temp  =[res[@"data"] bk_map:^id(id obj) {
                ScoreModel * m = [[ScoreModel alloc] init];
                [m yy_modelSetWithDictionary:obj];
                return m;
            }];
            [dataSource addObjectsFromArray:temp];
        }
    });
}


-(NSString *)stringFrom:(NSDate*)date{
    NSDate * d = date;
    if(!date){
        d = [NSDate date];
    }
    NSDateFormatter * dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyymm"];
    return [dateformatter stringFromDate:d];
}


-(void)setUp{
    
    dataSource = [NSMutableArray array];
    segment = [[UISegmentedControl alloc] initWithItems:@[@"日",@"月",@"季",@"年",]];
    segment.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:segment];
    segment.selectedSegmentIndex = 0;
    [segment autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [segment autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view withOffset:10];
    [segment autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view withOffset:-zoom(35)];
    [segment autoSetDimension:ALDimensionHeight toSize:zoom(32)];
    segment.tintColor = UIColorFromHEX(0x12C6FC);
    
    
    scoreViewDay = [ScoreWithDayView instance];
    scoreViewDay.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:scoreViewDay];
    [scoreViewDay autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:segment withOffset:2];
    [scoreViewDay autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.view];
    [scoreViewDay autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view];
    [scoreViewDay autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.view];
    
    scoreViewOther = [ScoreWithOtherView instance];
    
    [self.view addSubview:scoreViewOther];
    [scoreViewOther autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:segment withOffset:2];
    [scoreViewOther autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.view];
    [scoreViewOther autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view];
    [scoreViewOther autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.view];
    scoreViewOther.chartView.testData = NO;
    
    RACSignal * signal = [segment rac_valuesForKeyPath:@"selectedSegmentIndex" observer:self];
    
    RAC(scoreViewDay,hidden) = [signal map:^id(id value) {
        return @([value integerValue] != 0);
    }];
    RAC(scoreViewOther,hidden) = [signal map:^id(id value) {
        return @([value integerValue] == 0);
    }];
      [signal subscribeNext:^(id value) {
            switch ([value integerValue]) {
                case 1:{
                    scoreViewOther.title = @"2018年5月刷牙得分";
                    [scoreViewOther.chartView setDatas:@[
                                                         [ChartViewData instanceWith:@"2018-5-1" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-5" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-12" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-19" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-26" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-31" score:arc4random()%100],
                                                         ]];
                }break;
                case 2:{
                    scoreViewOther.title = @"2018年4-6月刷牙得分";
                    [scoreViewOther.chartView setDatas:@[
                                                         [ChartViewData instanceWith:@"2018-5-1" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-5" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-12" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-19" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-26" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-31" score:arc4random()%100],
                                                         ]];
                }break;
                case 3:{
                    scoreViewOther.title = @"2018年年度刷牙得分";
                    [scoreViewOther.chartView setDatas:@[
                                                         [ChartViewData instanceWith:@"2018-5-1" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-5" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-12" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-19" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-26" score:arc4random()%100],
                                                         [ChartViewData instanceWith:@"2018-5-31" score:arc4random()%100],
                                                         ]];
                }break;
                default: break;
                    
            }
        }];
    
}


@end
