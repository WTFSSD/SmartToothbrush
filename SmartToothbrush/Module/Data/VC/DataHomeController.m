//
//  DataHomeController.m
//  Base
//
//  Created by admin on 2018/4/26.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "DataHomeController.h"

#import "RespositoryViewModel.h"

#import "TodayBrushScoreView.h"

#import "DataHomeSectionHeader.h"

#import "HistoryScoreViewController.h"

#import "HomeDataInfoCell.h"

#import "RecordModel.h"

@class CycleScoreView;
@interface DataHomeController (){
    NSInteger index;
}
@property(nonatomic,strong)RespositoryViewModel * viewModel;


@property(nonatomic,strong)NSMutableArray<RecordModel*> * dataSource;


@property(nonatomic,assign)NSUInteger openSection;


@end




@implementation DataHomeController


-(RespositoryViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [RespositoryViewModel instance];
    }
    return _viewModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"刷牙数据";
    _openSection = -1;
    self.navigationBarBlue = YES;
    self.backItemHidden = YES;
    [self baseSetupTableView:UITableViewStyleGrouped InSets:UIEdgeInsetsMake(15, 0, 0, 0)];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self.tableView registerClass:[HomeDataInfoCell class] forCellReuseIdentifier:@"HomeDataInfoCell"];
    [self.tableView registerClass:[DataHomeSectionHeader class] forHeaderFooterViewReuseIdentifier:@"DataHomeSectionHeader"];
    
    
    UIImageView * bg = [[UIImageView alloc] initWithImage:ImageNamed(@"bg_index")];
    [self.view insertSubview:bg atIndex:0];
    [bg autoCenterInSuperview];
    [bg autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:self.view];
    [bg autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view];
   
//    [RACObserve(self.viewModel, respositories) subscribeNext:^(id x) {
//        [self.tableView reloadData];
//    }];
//
    
    
    
   
    
    self.dataSource = [NSMutableArray array];
   
   
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    if(![UserInfoModel share].isLogin){
        [MBProgressHUD hy_showMessage:@"暂未登录!" inView:self.view];
        return;
    }
    
    [NetworkUtils requestWith:HTTPMethodGET url:REQUEST_URL(API_RECORD) params:@{@"key":[UserInfoModel share].key} headers:nil progress:nil obj:nil].then(^(NSDictionary* res){
        if(!res || [res[@"code"] integerValue] != 1) return ;
        [self.dataSource removeAllObjects];
        [self.dataSource addObjectsFromArray:[res[@"data"] bk_map:^id(NSDictionary* obj) {
            
            RecordModel * mode = [[RecordModel alloc] init];
            [mode yy_modelSetWithDictionary:obj];
            return mode;
        }]];
        
        [self.tableView reloadData];
    });
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    if(!self.viewModel.respositories)return 0;
//    return self.viewModel.respositories.count;
    
    return self.dataSource.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return 1;
    }
    if(section == self.openSection) return 1;
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RecordModel * model  = self.dataSource[indexPath.section];
    
    
    HomeDataInfoCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HomeDataInfoCell"];
    cell.model = model;

    cell.onHistoryScoreClick = ^(RecordModel *m) {
        HistoryScoreViewController * vc =[[HistoryScoreViewController alloc] init];
        vc.tbNo = m.tb_no;
        [self.navigationController pushViewController:vc animated:YES];
    };

    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    DataHomeSectionHeader * header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"DataHomeSectionHeader"];
    header.arrowType = section == 0?DataHomeSectionHeaderArrowNone:(section == self.openSection?DataHomeSectionHeaderArrowUp:DataHomeSectionHeaderArrowDown);
    header.section = section;

    RecordModel * model  = self.dataSource[section];
    
    header.title = model.nickname;
    
    @weakify(self);
    header.onArrowClick = ^(NSUInteger sec) {
        @strongify(self);
        if(section == self.openSection){
            self.openSection = -1;
        }else{
            self.openSection = section;
        }
    
        [self.tableView reloadData];
        dispatch_async(dispatch_get_main_queue(), ^{
            if(self.openSection == sec){
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
        });
        
    };
    return header;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 781.f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}


@end
