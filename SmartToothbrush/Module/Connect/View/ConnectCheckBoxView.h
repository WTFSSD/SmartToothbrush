//
//  ConnectCheckBoxView.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/8/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConnectCheckBoxView : UIControl


@property(nonatomic,copy)NSString * text;

@property(nonatomic,assign)BOOL checked;



+(instancetype)instance;
@end
