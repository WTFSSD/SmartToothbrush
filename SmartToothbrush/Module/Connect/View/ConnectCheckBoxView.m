//
//  ConnectCheckBoxView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/8/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "ConnectCheckBoxView.h"

@interface ConnectCheckBoxView(){
    UILabel * textLabel;
    UIImageView * image;
}

@end

@implementation ConnectCheckBoxView



+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setUp];
    
    }
    return self;
}


-(void)setUp{
    textLabel = [UILabel makeWith:^(UILabel *label) {
        label.textColor = UIColorFromHEX(0x70879E);
        label.font = [UIFont systemFontOfSize:15];
        label.userInteractionEnabled = YES;
        
    }];
    [self addSubview:textLabel];
    
    image = [[UIImageView alloc] initWithImage:ImageNamed(@"checked")];
    image.userInteractionEnabled = YES;
    [self addSubview:image];
    [image autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self];
    [image autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self];
    
    [textLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:image withOffset:5];
    [textLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self];
    
    RAC(image,image) = [[self rac_valuesForKeyPath:@"checked" observer:self] map:^id(id value) {
        if([value boolValue]){
            return ImageNamed(@"checked_sin");
        }
        return ImageNamed(@"checked");
    }];
    
    RAC(textLabel,text) = [[self rac_valuesForKeyPath:@"text" observer:self] map:^id(id value) {
        return value;
    }];
    
    [textLabel sizeToFit];
    
    self.userInteractionEnabled = YES;
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClick)]];

}



-(void)onClick{
    [self sendActionsForControlEvents:UIControlEventTouchUpInside];
}
@end
