//
//  ConectStep3ViewController.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/23.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "ConectStep3ViewController.h"
#import "ConectStep4ViewController.h"
#import "ContainerView.h"
#import "SearchBrushModalView.h"
#import "UDPProtocol.h"
#import "CommonProblemModal.h"

#import "ConnectCheckBoxView.h"
@interface ConectStep3ViewController (){
    UIImageView * wifiImage;
    UILabel * label1;
    UILabel * label2;
    UILabel * label3;

    ContainerView * container;
    
    
    UITextField * ssid;
    UITextField * pwd;
    UITextField * nickName;
    
    UILabel * label4;
    ConnectCheckBoxView * checkBox1;
    
    ConnectCheckBoxView * checkBox2;
    
    SearchBrushModalView * modal;

}

@end

@implementation ConectStep3ViewController

-(void)setUp{
    [super setUp];

    
    wifiImage = [[UIImageView alloc] initWithImage:ImageNamed(@"ico_wifi.png")];
    [self.view addSubview:wifiImage];
    [wifiImage autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [wifiImage autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.closeButton withOffset:zoom(0)];
    
    label1 = [UILabel makeWith:^(UILabel *label) {
        label.text = @"牙刷连接网络";
        label.textColor = UIColorFromHEX(0xffffff);
        label.font = [UIFont systemFontOfSize:20 weight:400];
    }];
    [self.view addSubview:label1];
    [label1 autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [label1 autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:wifiImage withOffset:zoom(10)];
    
    
    label2 = [UILabel makeWith:^(UILabel *label) {
        label.text = @"请输入WIFI账号和密码";
        label.textColor = UIColorFromHEX(0xffffff);
        label.font = [UIFont systemFontOfSize:14 ];
    }];
    [self.view addSubview:label2];
    [label2 autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [label2 autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:label1 withOffset:zoom(10)];
    
    
    
    
    container = [ContainerView instance];
    [self.view addSubview:container];
    [container autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [container autoSetDimension:ALDimensionWidth toSize:zoom(351)];
    [container autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:container withMultiplier:250/351.f];
    
    [container autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:label2 withOffset:zoom(20)];
    [container drawRoundRect:5 rt:5 lb:5 rb:5];
    
    
    label3 = [UILabel makeWith:^(UILabel *label) {
        label.text = @"手机需连接路由器2.4GH WiFi，在5GHz\nWiFi环境下无法配置牙刷";
        label.textColor = [UIColor colorWithWhite:1 alpha:0.7];
        label.font = [UIFont systemFontOfSize:14 ];
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentCenter;
    }];
    [self.view addSubview:label3];
    [label3 autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [label3 autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:container withOffset:zoom(10)];
    
    
    
    ssid = [[UITextField alloc] init];
    ssid.borderStyle = UITextBorderStyleRoundedRect;
    ssid.placeholder = @"请输入WiFi名称";
    ssid.backgroundColor = UIColorFromHEX(0xf1f3f8);
    [container addSubview:ssid];
    [ssid autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:container withOffset:zoom(20)];
    [ssid autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:container withOffset:-zoom(20)];
    [ssid autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:container withOffset:zoom(40)];
    [ssid autoSetDimension:ALDimensionHeight toSize:zoom(40)];
    
 
    pwd = [[UITextField alloc] init];
    pwd.borderStyle = UITextBorderStyleRoundedRect;
    pwd.placeholder = @"请输入WiFi密码";
    pwd.secureTextEntry = YES;
    pwd.text = [UDPProtocol getWifiPwd];
    pwd.backgroundColor = UIColorFromHEX(0xf1f3f8);
    [container addSubview:pwd];
    [pwd autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:container withOffset:zoom(20)];
    [pwd autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:container withOffset:-zoom(20)];
    [pwd autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:ssid withOffset:zoom(20)];
    [pwd autoSetDimension:ALDimensionHeight toSize:zoom(40)];
    
    
    
    label4 = [UILabel makeWith:^(UILabel *label) {
        label.text = @"的牙刷";
        label.textColor = UIColorFromHEX(0x4A5A6A);
        label.font = [UIFont systemFontOfSize:zoom(16)];
    }];
    [container addSubview:label4];
    [label4 autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:container withOffset:-zoom(22.5)];

    [label4 autoSetDimension:ALDimensionHeight toSize:zoom(48)];
    
    
  
    
    
    
    
    
    
    nickName = [[UITextField alloc] init];
    nickName.borderStyle = UITextBorderStyleRoundedRect;
    nickName.placeholder = @"请输入您的昵称";
    nickName.backgroundColor = UIColorFromHEX(0xf1f3f8);
    [container addSubview:nickName];
    [nickName autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:container withOffset:zoom(20)];
    [nickName autoPinEdge:ALEdgeRight toEdge:ALEdgeLeft ofView:label4 withOffset:-zoom(17)];
    [nickName autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:pwd withOffset:zoom(20)];
    [nickName autoSetDimension:ALDimensionHeight toSize:zoom(40)];
    
    
   
    
    
    
    
    
    
    
    [label4 autoAlignAxis:ALAxisHorizontal toSameAxisOfView:nickName];
    
    
    [[nickName rac_textSignal] subscribeNext:^(id x) {
        [UDPProtocol share].nickName = x;
    }];
    
    
    
    
    
    checkBox1 = [ConnectCheckBoxView instance];
    checkBox1.text = @"我的牙刷";
    checkBox1.checked = YES;
    [container addSubview:checkBox1];
    checkBox2 = [ConnectCheckBoxView instance];
    checkBox2.text = @"其他人的牙刷";
    checkBox2.checked = NO;
    [container addSubview:checkBox2];
    [checkBox1 autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:container withOffset:zoom(20)];
    [checkBox1 autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:nickName withOffset:zoom(20)];
    
    
    [checkBox2 autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:label4];
    [checkBox2 autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:checkBox1];
    
    [checkBox1 autoSetDimension:ALDimensionWidth toSize:zoom(115)];
     [checkBox1 autoSetDimension:ALDimensionHeight toSize:zoom(24)];
     [checkBox2 autoSetDimension:ALDimensionWidth toSize:zoom(115)];
    [checkBox2 autoSetDimension:ALDimensionHeight toSize:zoom(24)];
    [[checkBox1 rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        checkBox1.checked = YES;
        checkBox2.checked = NO;
    }];
    [[checkBox2 rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        checkBox2.checked = YES;
        checkBox1.checked = NO;
    }];
    
    
    
    
    
    @weakify(self);
    modal = [SearchBrushModalView instanceWith:CGSizeMake(zoom(300), zoom(250))];
    modal.onRetry = ^(id x) {
        @strongify(self);
        [self connect:YES];
    };
    
    [[self.nextButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self connect:NO];
    }];
    [[self.backButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [self getWifiName];
    [[self.closeButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self close];
    }];
    
}

-(void)connect:(BOOL)retry{
   
    
    if(!ssid.text||ssid.text.length<=0){
        [MBProgressHUD hy_showMessage:@"请输入wifi名称" inView:self.view];
        return;
    }

    if(!pwd.text || pwd.text.length<=0){
        [MBProgressHUD hy_showMessage:@"请输入wifi密码" inView:self.view];
        return;
    }
  
    if(!retry){
        [self showModalWith:modal];
    }
    modal.loading = YES;
    [UDPProtocol stop];
    @weakify(self)
    RACSignal * s = [UDPProtocol connectWifiWith:[UDPProtocol getWifiName] pwd:pwd.text one:YES];
    [s subscribeNext:^(id x) {
        NSLog(@"subscribeNext:%@",x);
    } error:^(NSError *error) {
        modal.loading = NO;
 
    } completed:^{
        modal.loading = NO;
        @strongify(self)
        [modal.backButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        ConectStep4ViewController * vc = [ConectStep4ViewController instance];
        vc.params = self.params;
        [self presentViewController:vc animated:YES completion:nil];

    }];
    
 
}


-(void)getWifiName{
    
    
   NSString * str =  [UDPProtocol getWifiName];
    ssid.text = str;

}


-(void)dealloc{
    [UDPProtocol stop];
}


@end
