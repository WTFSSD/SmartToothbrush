//
//  ConectStep4ViewController.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/23.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "ConectStep4ViewController.h"
#import "FoundBrushTableViewCell.h"
#import "UDPProtocol.h"
@interface ConectStep4ViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UILabel * label1;
    
    UITableView * tableView;

    NSMutableArray * selecteds;
    NSArray * dataSource;
    
    
}

@end

@implementation ConectStep4ViewController
-(void)setUp{
    [super setUp];
    
    if([UDPProtocol share].deviceInfo){
        dataSource = @[[UDPProtocol share].deviceInfo];
    }else{
        dataSource = @[];
    }
    
    
    selecteds = [NSMutableArray array];
    label1 = [UILabel makeWith:^(UILabel *label) {
        label.text = @"发现以下牙刷";
        label.textColor = UIColorFromHEX(0xffffff);
        label.font = [UIFont systemFontOfSize:16 weight:400];
    }];
    [self.view addSubview:label1];
    [label1 autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view withOffset:zoom(100)];
     [label1 autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    
    
    tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.sectionHeaderHeight = 0.f;
    tableView.sectionFooterHeight = 20.f;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    [tableView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view withOffset:zoom(20)];
    [tableView autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.view withOffset:-zoom(20)];
    [tableView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:label1 withOffset:zoom(70)];
     [tableView autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:self.backButton withOffset:-zoom(60)];
    
    
    [tableView  registerClass:[FoundBrushTableViewCell class] forCellReuseIdentifier:@"FoundBrushTableViewCell"];
    
    
    
    self.nextButton.title = @"完成";
    [[self.backButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    
    
    @weakify(self)
    [[self.nextButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
        @strongify(self);
       
        AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        
        

    
        if([UserInfoModel share].isLogin){
            
            if(selecteds && selecteds.count >0){
                
                 HFSmartLinkDeviceInfo * info =dataSource[[selecteds[0] integerValue]];
                NSDictionary * params = @{
                                          @"key":[UserInfoModel share].key,
                                          @"tb_no":info.mac,
                                          @"nickname":[UDPProtocol share].nickName,
                                          @"is_my":@(1)
                                          };
               
                [NetworkUtils requestWith:HTTPMethodPOST
                                      url:REQUEST_URL(API_MEMBER_BIND_TOOTHBRUSH)
                                   params:params].then(^(id res){
                    if(res && [res[@"code"] integerValue] == 1){
                        [MBProgressHUD hy_showMessage:@"绑定牙刷成功" inView:self.view completionBlock:^{
                            [delegate configTabbar];
                        }];
                    }
                });
                
            }else{
                [MBProgressHUD hy_showMessage:@"请选择一只牙刷" inView:self.view];
            }
        }else{
            [delegate configGuide];
        }
    
      
    }];
    
    
    
    [[self.closeButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self close];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return dataSource.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FoundBrushTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"FoundBrushTableViewCell"];
    cell.selected = [selecteds indexOfObject:@(indexPath.section)] != NSNotFound;
    cell.deviceInfo = dataSource[indexPath.section];
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10.f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return zoom(60);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([selecteds indexOfObject:@(indexPath.section)] == NSNotFound) {
        [selecteds addObject:@(indexPath.section)];
    }else{
        [selecteds removeObject:@(indexPath.section)];
    }
    [tableView reloadData];
}
@end
