//
//  UDPProtocol.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/6/13.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HFSmartLinkDeviceInfo.h"
#include "HFSmartLink.h"

///刷牙模式
typedef enum : NSUInteger {
    ///清洁模式
    BrushModelClear=0x01,
    ///美白模式
    BrushModelWhite = 0x02,
    ///敏感模式
    BrushModelSensitive = 0x03,
    ///私人定制
    BrushModelCustom = 0x06,
} UdpBrushModel;


///刷牙力度
typedef NS_ENUM(NSUInteger, BrushIntensity) {
     BrushIntensityLevel1 = 0x01,
     BrushIntensityLevel2 = 0x02,
     BrushIntensityLevel3 = 0x03,
     BrushIntensityLevel4 = 0x04,
     BrushIntensityLevel5 = 0x05,
    
    
};

///刷牙时间
typedef enum : NSUInteger {
    
    
    BrushTimeNone = 0x00,
    
    ///1分00秒
    BrushTime1 = 0x01,
    ///1分45秒
    BrushTime2 = 0x02,
    ///2分00秒
    BrushTime3 = 0x03,
    ///2分30秒
    BrushTime4 = 0x04,
    ///3分00秒
    BrushTime5 = 0x05,
} BrushTime;


///防溅设置
typedef enum : NSUInteger {
    ///开启防溅设置
    SputteringModelOn = 0x00,
    ///关闭防溅设置
    SputteringModelOff = 0x01,
} SputteringModel;
@interface UDPProtocol : NSObject




/**
 * 广播牙刷 的mac地址 以及 wifi名称和密码
 * @param mac NSString* 牙刷 mac地址
 * @param name NSString* wifi名称
 * @param password NSString* wifi密码
 */
//+(void)broadcastWith:(NSString*)mac name:(NSString*)name password:(NSString*)password;


@property(nonatomic,strong)HFSmartLinkDeviceInfo * deviceInfo;

@property(nonatomic,copy)NSString * nickName;


+(instancetype)share;

+(NSString*)getWifiName;


+(NSString*)getWifiPwd;

+(void)saveWifiPwd:(NSString*)pwd;

+(RACSignal*)connectWifiWith:(NSString*)ssid pwd:(NSString*)pwd one:(BOOL)one;


+(void)stop;

@end
