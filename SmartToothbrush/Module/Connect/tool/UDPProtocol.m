//
//  UDPProtocol.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/6/13.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "UDPProtocol.h"
#import <CocoaAsyncSocket/GCDAsyncUdpSocket.h>

#import <SystemConfiguration/CaptiveNetwork.h>

#define udpPort 9527

@interface UDPProtocol()<GCDAsyncUdpSocketDelegate>
@property(nonatomic)GCDAsyncUdpSocket *client;
@end



@implementation UDPProtocol
static UDPProtocol * _instance = nil;

+(instancetype)share{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    return _instance;
}

- (instancetype)init
{
    
    if(_instance) return _instance;
    self = [super init];
    if (self) {
        
        self.client =  [[GCDAsyncUdpSocket alloc] init];
        
        [self.client setDelegate:self];
        [self.client setDelegateQueue:dispatch_get_main_queue()];
        NSError * err = nil;
        [self.client enableBroadcast:YES error:&err];
        
        NSLog(@"error:%@",err);
        
        
        self.client.delegate = self;
//        __block long tag = 0;
//        [NSTimer bk_scheduledTimerWithTimeInterval:1 block:^(NSTimer *timer) {
//            [self.client sendData:data toHost:@"255.255.255.255" port:7788 withTimeout:2000 tag:tag ++ ];
//        } repeats:YES];

    }
    return self;
}

-(void)sendWithOrder:(NSInteger)order cmd:(NSInteger)cmd len:(NSInteger)len mac:(NSInteger)mac data:(NSInteger)data hValidate:(NSInteger)hValidate lValidate:(NSInteger)lValidate{
    
}



-(NSArray*)coverStringToAscii:(NSString*)string{
    NSInteger len = string.length;
    long * bytes = malloc(sizeof(long) * string.length);
    NSMutableArray * arr = [NSMutableArray array];
    for (NSInteger i=0;i<len; i++) {
        unichar ch = [string characterAtIndex:i];
        [arr addObject:@(ch)];
        
        bytes[i] = (long)ch;
    }
    return [NSArray arrayWithArray:arr];
}

-(NSArray*)megre:(NSArray*)origin array:(NSArray*)array{
    NSMutableArray * arr = [NSMutableArray array];
    
    [origin enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [arr addObject:obj];
    }];
    
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [arr addObject:obj];
    }];

    return [NSArray arrayWithArray:arr];
}


-(int)toCRC16:(NSArray*)bufData buflen:(NSInteger)buflen pcrc:(NSMutableArray*)pcrc{
    int ret = 0;
    int CRC = 0x0000ffff;
    int POLYNOMIAL = 0x0000a001;
    int i, j;
    
    if (buflen == 0) {
        return ret;
    }
    for (i = 0; i < buflen; i++) {
        
        unichar ch = (unichar)[bufData[i] integerValue];
        
        CRC ^= ch & 0x000000ff;
        
        for (j = 0; j < 8; j++) {
            if ((CRC & 0x00000001) != 0) {
                CRC >>= 1;
                CRC ^= POLYNOMIAL;
            } else {
                CRC >>= 1;
            }
        }
    }
    pcrc[0] = @(CRC & 0x00ff);
    pcrc[1] = @(CRC >> 8);
     return ret;
}

-(NSArray*)protocol:(NSArray*)data{
  
    NSArray * all ;
    NSArray * code = @[@(0xaa),@(0x01),@(0x01)];
    
    all = [self megre:code array:@[@(18+data.count)]];
    all = [self megre:all array:data];
    NSMutableArray * crc = [NSMutableArray arrayWithCapacity:2];
    [self toCRC16:all buflen:all.count pcrc:crc];
    all = [self megre:all array:crc];
    
    
    return all;
}


-(NSData *)coverToData:(NSArray*)array{
    NSMutableData * data = [NSMutableData data];

    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        unichar ch = (unichar)[obj integerValue];
        
        uint intValue;
        
        NSScanner * scanner = [NSScanner scannerWithString:[NSString stringWithFormat:@"%04x",ch]];
        [scanner scanHexInt:&intValue];
        

        [data appendBytes:&intValue length:1];
    }];
    
    return data;
}

-(void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag{
    
    NSLog(@"发送数据:%ld",tag);
}

-(void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error{
    NSLog(@"发送数据失败:%@",error);
}


+(void)broadcastWith:(NSString *)mac name:(NSString *)name password:(NSString *)password{
    UDPProtocol * protocol = [self share];
    NSArray * all = [protocol coverStringToAscii:mac];
    all = [protocol megre:all array:[protocol coverStringToAscii:name]];
    all = [protocol megre:all array:[protocol coverStringToAscii:password]];
    all = [protocol protocol:all];
    NSData * data = [[self share] coverToData:all];
    [protocol.client sendData:data toHost:@"255.255.255.255" port:7788 withTimeout:2000 tag:0];
}



+(NSString*)getWifiPwd{
    NSString * wifiName = [self getWifiName];
    if(!wifiName) return nil;
    return [[NSUserDefaults standardUserDefaults] stringForKey:wifiName];
}

+(void)saveWifiPwd:(NSString*)pwd{
    NSString * wifiName = [self getWifiName];
    if(!wifiName) return;
    if(!pwd) return;
    if(pwd.length<=0) return;
    [[NSUserDefaults standardUserDefaults] setValue:pwd forKey:wifiName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSString*)getWifiName{
    id info = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        NSString *str = info[@"SSID"];
        return str;
    }
    return nil;
}

+(RACSignal*)connectWifiWith:(NSString*)ssid pwd:(NSString*)pwd one:(BOOL)one;{
    HFSmartLink * smtlk = [HFSmartLink shareInstence];
    smtlk.isConfigOneDevice = one;
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [smtlk startWithSSID:ssid Key:pwd withV3x:YES
                processblock: ^(NSInteger pro) {
                    [subscriber sendNext:@(pro)];
                } successBlock:^(HFSmartLinkDeviceInfo *dev) {
            
                    [UDPProtocol share].deviceInfo = dev;
                    [subscriber sendCompleted];
                } failBlock:^(NSString *failmsg) {
                    [subscriber sendError:[NSError errorWithDomain:@"com.brush.udo" code:-1 userInfo:@{NSLocalizedDescriptionKey:failmsg}]];
                    
                } endBlock:^(NSDictionary *deviceDic) {
                    NSLog(@"end:%@",deviceDic);
                }];
        return nil;
    }];
    return signal;
}

+(void)stop{
    HFSmartLink * smtlk = [HFSmartLink shareInstence];
    [smtlk stopWithBlock:^(NSString *stopMsg, BOOL isOk) {
        
    }];
}
//7gbvdbn7

@end
