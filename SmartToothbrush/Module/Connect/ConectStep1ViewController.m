//
//  ConectStep1ViewController.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/23.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "ConectStep1ViewController.h"
#import "ConectStep2ViewController.h"
#import "CommonProblemModal.h"
@interface ConectStep1ViewController (){
    UIImageView * imageView;
    UILabel * label1;
    UILabel * label2;
}

@end

@implementation ConectStep1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
-(void)setUp{
    [super setUp];
    
    label1 = [UILabel makeWith:^(UILabel *label) {
        label.text = @"连接牙刷";
        label.textColor = UIColorFromHEX(0xffffff);
        label.font = [UIFont systemFontOfSize:16 weight:400];
    }];
    [self.view addSubview:label1];
    [label1 autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view withOffset:zoom(88)];
    [label1 autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view withOffset:zoom(42.5)];
    
    label2 = [UILabel makeWith:^(UILabel *label) {
        label.text = @"轻按电源，启动电动牙刷";
        label.textColor = UIColorFromHEX(0xffffff);
        label.font = [UIFont systemFontOfSize:15];
    }];
    [self.view addSubview:label2];
    [label2 autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:label1 withOffset:zoom(10)];
    [label2 autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:label1 withOffset:zoom(0)];
    
    imageView = [[UIImageView alloc] initWithImage:ImageNamed(@"pic_link1.png")];
    
    [self.view addSubview:imageView];
    [imageView autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [imageView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:label2 withOffset:zoom(20)];
    [imageView autoSetDimension:ALDimensionWidth toSize:zoom(262)];
    [imageView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:imageView withMultiplier:325/262.f];
    [[self.nextButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        ConectStep2ViewController * vc = [ConectStep2ViewController instance];
        vc.params = self.params;
        [self presentViewController:vc animated:YES completion:nil];
    }];

    [[self.closeButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self close];
    }];
    self.backButton.hidden = YES;
    
    
}



@end
