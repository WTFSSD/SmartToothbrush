//
//  ConectStep2ViewController.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/23.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "ConectStep2ViewController.h"
#import "ConectStep3ViewController.h"
#import "SearchBrushModalView.h"
#import "CommonProblemModal.h"
@interface ConectStep2ViewController (){
    UIImageView * imageView;
    UILabel * label1;
    UILabel * label2;
    UILabel * label3;
}

@end

@implementation ConectStep2ViewController

-(void)setUp{
    [super setUp];
    
    label1 = [UILabel makeWith:^(UILabel *label) {
        label.text = @"连接牙刷";
        label.textColor = UIColorFromHEX(0xffffff);
        label.font = [UIFont systemFontOfSize:16 weight:400];
    }];
    [self.view addSubview:label1];
    [label1 autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view withOffset:zoom(78)];
    [label1 autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view withOffset:zoom(42.5)];
    
    label2 = [UILabel makeWith:^(UILabel *label) {
        label.text = @"请长按强度调节键。牙刷WIFI指示灯开始闪\n烁，牙刷开始连接，请点击下一步按钮。";
        label.textColor = UIColorFromHEX(0xffffff);
        label.font = [UIFont systemFontOfSize:15];
        label.numberOfLines = 0;
    }];
    [self.view addSubview:label2];
    [label2 autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:label1 withOffset:zoom(10)];
    [label2 autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:label1 withOffset:zoom(0)];
    
    imageView = [[UIImageView alloc] initWithImage:ImageNamed(@"pic_link2.png")];
    
    [self.view addSubview:imageView];
    [imageView autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [imageView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:label2 withOffset:zoom(20)];
    [imageView autoSetDimension:ALDimensionWidth toSize:zoom(262)];
    [imageView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:imageView withMultiplier:325/262.f];
    
    
    
    label3 = [UILabel makeWith:^(UILabel *label) {
        NSDictionary *attribtDic = @{NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        label.attributedText = [[NSMutableAttributedString alloc]initWithString:@"未看到指示灯闪烁？" attributes:attribtDic];
        label.textColor = UIColorFromHEX(0xffffff);
        label.font = [UIFont systemFontOfSize:16 weight:400];
        label.userInteractionEnabled = YES;
    }];
    [self.view addSubview:label3];
    
    [label3 autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:imageView withOffset:zoom(20)];
    [label3 autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:imageView withOffset:zoom(0)];
    
    [[self.nextButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        ConectStep3ViewController * vc = [ConectStep3ViewController instance];
        vc.params = self.params;
        [self presentViewController:vc animated:YES completion:nil];
    }];
    [[self.backButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    
    [label3 bk_whenTapped:^{
    [self showModalWith:[CommonProblemModal instanceWith:(CGSize)CGSizeMake(zoom(300), zoom(250))]];
    }];
    
    
    
    
    [[self.closeButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self close];
    }];
    
    
}




@end
