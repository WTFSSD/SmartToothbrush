//
//  BaseConectViewController.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/23.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BaseConectViewController.h"



#import "BaseModalView.h"
@interface _ModalContainerViewController:UIViewController

@property(nonatomic,strong)UIView * contentView;
+(instancetype)instanceWith:(UIView*)view;
@end

@implementation _ModalContainerViewController


+(instancetype)instanceWith:(UIView *)view{
    _ModalContainerViewController * vc = [[self alloc] init];
    vc.contentView = view;
    return vc;
}


-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.view.backgroundColor = RGB(0, 0, 0, 0.6);
    if(self.contentView){
        [self.view addSubview:self.contentView];
        self.contentView.backgroundColor = [UIColor whiteColor];
       
    
        if ([self.contentView isKindOfClass:[BaseModalView class]]) {
            BaseModalView * modal = (BaseModalView*)self.contentView;
            [[modal.backButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
        }
    }
    
}


-(void)dismissModal{
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(CGSize)preferredContentSize{
    return self.contentView.frame.size;
}
@end































@interface BaseConectViewController ()<UIPopoverPresentationControllerDelegate>
@property(nonatomic,strong,readwrite) UIButton * closeButton;

@property(nonatomic,strong,readwrite) GradientButton * nextButton;

@property(nonatomic,strong,readwrite) UIButton * backButton;

@end

@implementation BaseConectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
}

-(void)setUp{
    _closeButton = [UIButton makeWith:^(UIButton *button) {
        [button setImage:ImageNamed(@"ico_close_white") forState:UIControlStateNormal];
    }];
    [self.view addSubview:_closeButton];
    [_closeButton autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view withOffset:zoom(14)];
    [_closeButton autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view withOffset:zoom(14) + 20];
  
    _nextButton = [GradientButton instanceWith:@"下一步" colors:@[UIColorFromHEX(0xffffff),UIColorFromHEX(0xffffff)]];
    _nextButton.titleColor = UIColorFromHEX(0x027afe);
    [self.view addSubview:_nextButton];
    
    [_nextButton autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [_nextButton autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.view withOffset:-zoom(40.5)];
    [_nextButton autoSetDimensionsToSize:CGSizeMake(zoom(200), zoom(40))];
    
    _backButton = [UIButton makeWith:^(UIButton *button) {
        [button setTitle:@"上一步" forState:UIControlStateNormal];
        [button setTitleColor:UIColorFromHEX(0xffffff) forState:UIControlStateNormal];
    }];
    [self.view addSubview:_backButton];
    [_backButton autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [_backButton autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:_nextButton withOffset:-zoom(15)];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)showModalWith:(UIView*)view{
    
    _ModalContainerViewController * contentVC = [_ModalContainerViewController instanceWith:view];
    contentVC.modalPresentationStyle = UIModalPresentationPopover;
    contentVC.popoverPresentationController.sourceView = self.view;
    contentVC.popoverPresentationController.permittedArrowDirections = 0;
    contentVC.popoverPresentationController.sourceRect = CGRectMake(CGRectGetMidX(self.view.frame), CGRectGetMidY(self.view.frame), 0, 0);
    contentVC.popoverPresentationController.delegate = self;
    [self presentViewController:contentVC animated:YES completion:nil];
    
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    return YES;
}




-(void)close{
    AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;

    UIViewController *vc = self;
    while (vc.presentingViewController) {
        vc = vc.presentingViewController;
    }
    
    if(vc.navigationController){
        [vc.navigationController popViewControllerAnimated:YES];
    }else{
        if(self.params && [self.params[@"from"] isKindOfClass:NSClassFromString(@"LoginViewController")]){
            [vc dismissViewControllerAnimated:YES completion:^(){
                if(self.params && [self.params[@"from"] isKindOfClass:NSClassFromString(@"LoginViewController")]){
                    if([UserInfoModel share].isLogin){
                        [delegate configTabbar];
                    }
                }
            }];
        }else{
             [delegate configTabbar];
        }
    }
   
}
@end
