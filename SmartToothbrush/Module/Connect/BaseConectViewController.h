//
//  BaseConectViewController.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/23.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BaseViewController.h"
#import "GradientButton.h"
@interface BaseConectViewController : BaseViewController

@property(nonatomic,strong,readonly) UIButton * closeButton;

@property(nonatomic,strong,readonly) GradientButton * nextButton;

@property(nonatomic,strong,readonly) UIButton * backButton;


@property(nonatomic,strong)NSDictionary * params;

-(void)setUp;


-(void)showModalWith:(UIView*)view;

-(void)dismissModal;


-(void)close;


@end
