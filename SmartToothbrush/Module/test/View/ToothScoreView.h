//
//  ToothScoreView.h
//  Base
//
//  Created by WTFSSD on 2018/4/29.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "ChartView.h"
@interface ToothScoreView : UIView

@property(nonatomic,strong,readonly)ChartView * chart;

@property(nonatomic,copy)NSString * title;

+(instancetype)instance;

@end
