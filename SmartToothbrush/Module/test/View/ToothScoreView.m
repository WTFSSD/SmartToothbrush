//
//  ToothScoreView.m
//  Base
//
//  Created by WTFSSD on 2018/4/29.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "ToothScoreView.h"

@interface ToothScoreView(){
    UILabel * titleLabel;
}

@property(nonatomic,strong,readwrite)ChartView * chart;


@end

@implementation ToothScoreView

+(instancetype)instance{
    ToothScoreView * v = [[self alloc] initWithFrame:CGRectZero];
    return v;
}


-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
    self.title = @"本周刷牙得分";
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}


-(void)setUp{
    _chart = [ChartView instance];
    [self addSubview:_chart];
    titleLabel = [UILabel makeWith:^(UILabel *label) {
        label.textColor = UIColorFromHEX(0x70879E);
        label.font = [UIFont systemFontOfSize:16];
        label.backgroundColor = [UIColor clearColor];
        
    }];
    [self addSubview:titleLabel];
    
    [titleLabel autoAlignAxis:ALAxisVertical toSameAxisOfView:self];
    [titleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:15];
    [_chart autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:titleLabel withOffset:5];
    [_chart autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:0];
    [_chart autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self withOffset:0];
    [_chart autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self withOffset:0];
//
    RAC(titleLabel,text) = [[self rac_valuesForKeyPath:@"title" observer:self] map:^id(id value) {
        NSLog(@"ddddd:%@",value);
        return value;
        
    }];
}

@end
