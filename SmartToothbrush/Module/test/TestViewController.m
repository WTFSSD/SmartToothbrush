//
//  TestViewController.m
//  Base
//
//  Created by WTFSSD on 2018/4/29.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "TestViewController.h"

#import "ToothScoreView.h"

#import "ToothStatusView.h"



@interface TestViewController (){
    ToothStatusView * status;
}




@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    status = [ToothStatusView instance];
    [self.view addSubview:status];
    [status autoCenterInSuperview];
    [status autoSetDimensionsToSize:CGSizeMake(zoom(221), zoom(340))];
    
    
    [status setStatusWithT:a_no|c_yellow
                           B:a_side1|c_red
                          LT:a_side1|c_red
                          LB:a_side1|c_red
                          RT:a_side1|c_red
                          RB:a_side1|c_red];
    
    UIBarButtonItem * refresh = [[UIBarButtonItem alloc] initWithTitle:@"刷新" style:UIBarButtonItemStylePlain target:self action:@selector(refresh)];
    
    self.navigationItem.rightBarButtonItem = refresh;
    
  
}

-(void)refresh{
   
}


@end
