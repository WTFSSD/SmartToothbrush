//
//  BaseModalView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/24.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BaseModalView.h"

@interface BaseModalView(){
    
}
@property(nonatomic,strong,readwrite) GradientButton * backButton;
@end

@implementation BaseModalView

+(instancetype)instanceWith:(CGSize)size{
    return [[self alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        _backButton = [GradientButton instanceWith:@"返回"];
        _backButton.titleColor = [UIColor whiteColor];
        _backButton.titleColor = UIColorFromHEX(0xffffff);
        [self addSubview:_backButton];
        
        [_backButton autoAlignAxis:ALAxisVertical toSameAxisOfView:self];
        [_backButton autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self withOffset:-zoom(20)];
        [_backButton autoSetDimensionsToSize:CGSizeMake(zoom(200), zoom(40))];
    }
    return self;
}

@end
