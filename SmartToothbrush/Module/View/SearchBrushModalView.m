//
//  SearchBrushModalView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/24.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "SearchBrushModalView.h"

@interface SearchBrushModalView(){
    UIImageView * icon;
    
    UILabel * tip;
    
    NSLayoutConstraint * tipConstraint;
    
    
}
@end;

@implementation SearchBrushModalView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        icon = [[UIImageView alloc] initWithImage:ImageNamed(@"tip_search@png")];
        [self addSubview:icon];
        
        [icon autoAlignAxis:ALAxisVertical toSameAxisOfView:self];
        [icon autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:zoom(44)];
        
        tip= [UILabel makeWith:^(UILabel *label) {
            label.text = @"牙刷搜索中...";
            label.textColor = UIColorFromHEX(0x70879E);
            label.font = [UIFont systemFontOfSize:16 weight:400];
            label.userInteractionEnabled = YES;
        }];
        [self addSubview:tip];
        [tip autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:icon withOffset:zoom(18)];
        [tip autoAlignAxis:ALAxisVertical toSameAxisOfView:icon];
        
    
        
        
        
        [[self rac_valuesForKeyPath:@"loading" observer:self] subscribeNext:^(id x) {
            BOOL l = [x boolValue];
            if(l){
                icon.image = ImageNamed(@"tip_search.png");
                tip.attributedText = [self getTipWith:l];
                
            }else{
                icon.image = ImageNamed(@"tip_refresh.png");
                tip.attributedText = [self getTipWith:l];
            }
        }];
        
        [tip bk_whenTapped:^{
            if (!self.loading) {
                if(self.onRetry){
                    self.onRetry(nil);
                }
            }
        }];
        
    }
    return self;
}


-(NSMutableAttributedString*)getTipWith:(BOOL)x{
    if (x) {
        NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:@"牙刷搜索中..."];
        [str addAttributes:@{NSForegroundColorAttributeName:UIColorFromHEX(0x70879E)} range:NSMakeRange(0, str.length)];
        return str;
    }else{
        NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:@"未找到任何牙刷硬件，请重新连接"];
        [str addAttributes:@{NSForegroundColorAttributeName:UIColorFromHEX(0x70879E)} range:NSMakeRange(0, 10)];
        [str addAttributes:@{NSForegroundColorAttributeName:UIColorFromHEX(0x1A6CE3)} range:NSMakeRange(10, str.length - 10)];
        return str;
    }
}
@end
