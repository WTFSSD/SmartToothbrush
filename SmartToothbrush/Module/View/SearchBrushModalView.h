//
//  SearchBrushModalView.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/24.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BaseModalView.h"

@interface SearchBrushModalView : BaseModalView

@property(nonatomic,assign)BOOL loading;

@property(nonatomic,strong)void(^onRetry)(id x);

@end
