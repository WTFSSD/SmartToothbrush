//
//  CommonProblemModal.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/24.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "CommonProblemModal.h"

@interface CommonProblemModal(){
    UILabel * title;
    UILabel * brief;
    
    UILabel * problem;
}

@end

@implementation CommonProblemModal

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        title = [UILabel makeWith:^(UILabel *label) {
            label.text = @"指示灯未闪烁";
            label.textColor = UIColorFromHEX(0x333333);
            label.font = [UIFont systemFontOfSize:16 weight:500];
        }];
        [self addSubview:title];
        
        [title autoAlignAxis:ALAxisVertical toSameAxisOfView:self];
        [title autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:zoom(20)];
        
        brief = [UILabel makeWith:^(UILabel *label) {
            label.text = @"指示灯未闪烁";
            label.textColor = UIColorFromHEX(0x70879E);
            label.font = [UIFont systemFontOfSize:15];
        }];
        [self addSubview:brief];
        [brief autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:zoom(20)];
        [brief autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:title withOffset:zoom(20)];
        
        
        problem = [UILabel makeWith:^(UILabel *label) {
            label.text = @"1、将牙刷重新开机，长按“强度调节”按键，尝试重新连接\n2、牙刷电量不足，WiFi灯不会闪烁，可在充电后再次尝试绑定。\n3、其他情况请联系客服。";
            label.textColor = UIColorFromHEX(0x4a5a6a);
            label.font = [UIFont systemFontOfSize:15];
            label.numberOfLines = 0;
        }];
        [self addSubview:problem];
        [problem autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:zoom(20)];
        [problem autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self withOffset:-zoom(20)];
        [problem autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:brief withOffset:zoom(17)];

    }
    return self;
}

@end
