//
//  FoundBrushTableViewCell.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/24.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "FoundBrushTableViewCell.h"
#import "ContainerView.h"
@interface FoundBrushTableViewCell(){
    ContainerView * container;
    
    UIImageView * icon;
    UILabel * text;
    
    
}
@end;

@implementation FoundBrushTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setUp];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self setUp];
    }
    return self;
}

-(void)setUp{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    container = [ContainerView instance];
    [self.contentView addSubview:container];
    [container autoPinEdgesToSuperviewEdges];
    [container drawRoundRect:5 rt:5 lb:5 rb:5];
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    
    icon = [[UIImageView alloc] initWithImage:ImageNamed(@"checked.png")];
    [container addSubview:icon];
    [icon autoAlignAxis:ALAxisHorizontal toSameAxisOfView:container];
    [icon autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:container withOffset:zoom(17)];
    
    
    text = [UILabel makeWith:^(UILabel *label) {
        label.text = @"";
        label.textColor = UIColorFromHEX(0x70879e);
        label.font = [UIFont systemFontOfSize:15];
    }];
    
    [container addSubview:text];
    [text autoAlignAxis:ALAxisHorizontal toSameAxisOfView:container];
    [text autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:icon withOffset:zoom(17)];

    
   RAC(icon,image) =  [[self rac_valuesForKeyPath:@"selected" observer:self] map:^id(id value) {
        if([value boolValue]){
            return ImageNamed(@"checked_in.png");
        }
        return ImageNamed(@"checked.png");
    }];
    
    RAC(text,text) = [[self rac_valuesForKeyPath:@"deviceInfo" observer:self] map:^id(id value) {
        if(value) {
            return [value valueForKey:@"mac"];
        }
        return @"";
    }];

}

@end
