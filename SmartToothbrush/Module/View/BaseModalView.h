//
//  BaseModalView.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/24.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GradientButton.h"
@interface BaseModalView : UIView

@property(nonatomic,strong,readonly) GradientButton * backButton;

///请务必使用该方法初始化并设置大小
+(instancetype)instanceWith:(CGSize)size;

@end
