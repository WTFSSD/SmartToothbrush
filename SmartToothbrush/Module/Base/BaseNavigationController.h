//
//  BaseNavigationController.h
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSUInteger, BackButtonType) {
    BackButtonTypeBlack = 0,
    BackButtonTypeWhite = 1,
    BackButtonTypeDefault = BackButtonTypeBlack
};

@protocol BaseNavgationControllerDelegate <NSObject>

@optional


///基类导航控制器能否返回
-(BOOL)BaseNavgationControllerCanBack:(UINavigationController*)nvc;

///当BaseNavgationControllerCanBack 返回NO时调用本方法
-(void)BaseNavgationController:(UINavigationController*)nvc didClickBackButton:(UIButton*)button;

-(BackButtonType)BaseNavgationControllerBackButtonTypeFor:(UINavigationController*)nvc;

@end

@interface BaseNavigationController : UINavigationController

+(instancetype)instanceWith:(UIViewController*)rootViewController;

@end
