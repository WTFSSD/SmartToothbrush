//
//  BaseTableViewController.m
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)baseSetupTableView:(UITableViewStyle)style InSets:(UIEdgeInsets)edge {
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:style];
    [self.view addSubview:self.tableView];
    [self.tableView autoPinEdgesToSuperviewEdgesWithInsets:edge];
    self.tableView.backgroundColor = [UIColor hyDefaultBackColor];
    self.tableView.backgroundView = nil;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
}

-(void)addHeaderRefresh:(void(^)())block {
    MJRefreshNormalHeader* header = [MJRefreshNormalHeader headerWithRefreshingBlock:block];
    header.lastUpdatedTimeLabel.hidden = YES;
    [header setTitle:@"继续下拉刷新" forState:MJRefreshStateIdle];
    [header setTitle:@"松手加载" forState:MJRefreshStatePulling];
    [header setTitle:@"加载中..." forState:MJRefreshStateRefreshing];
    
    self.tableView.mj_header = header;
}

-(void)addFooterRefresh:(void(^)())block {
    if (self.tableView.mj_footer) {
        return;
    }
    MJRefreshAutoNormalFooter* footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:block];
    [footer setTitle:@"继续上拉加载" forState:MJRefreshStateIdle];
    [footer setTitle:@"松手加载" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载中..." forState:MJRefreshStateRefreshing];
    
    self.tableView.mj_footer = footer;
}

-(void)removeFooterRefresh {
    self.tableView.mj_footer = nil;
    
    if (self.haveTableFooter) {
        UIView* foot = LOADNIB(@"HomeUseView", 1);
        self.tableView.tableFooterView = foot;
    }
}

-(BOOL)hasFooterRefresh {
    return (self.tableView.mj_footer != nil);
}

-(void)endRefreshing {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - tableView dataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

@end
