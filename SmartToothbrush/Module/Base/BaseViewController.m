//
//  BaseViewController.m
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "BaseViewController.h"
#import "MBProgressHUD+hyHUD.h"
#import "LoginViewController.h"
#import "RegistViewController.h"
#import "HYAlertView.h"

@interface BaseViewController (){
    NSLayoutConstraint * altop;
    NSLayoutConstraint * albottom;
}

@property(nonatomic,strong) UIView* navBackView;
@property(nonatomic,strong) UIView* navLine;
@property(nonatomic,strong)UIImageView * backgroundImage;

@property(nonatomic,strong)NSArray< NSLayoutConstraint *>* constraints;
@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.needLogEvent = YES;
    self.navigationBarHidden = NO;
    self.navigationBarTransparent = YES;
    self.navigationLineHidden = NO;
    self.navigationBarBlue = YES;
    //在iOS 7中，苹果引入了一个新的属性，叫做[UIViewController setEdgesForExtendedLayout:]，它的默认值为UIRectEdgeAll。当你的容器是navigation controller时，默认的布局将从navigation bar的顶部开始。这就是为什么所有的UI元素都往上漂移了44pt。
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self baseSetBackgroundImage];
}


+(instancetype)instance{
    return [[self alloc] init];
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self setNeedsStatusBarAppearanceUpdate];
    if (self.needLogEvent) {
        //        [MobClick beginLogPageView:NSStringFromClass([self Class])];
    }
    if (!self.backItemHidden && !self.navigationItem.leftBarButtonItems) {
        UIButton* leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        leftButton.frame = CGRectMake(0, 0, 32, 32);
        leftButton.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
        leftButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
        leftButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        NSString* backImgName = self.navigationBarBlue == YES ? @"arrow_back_white" : @"arrow_back";
        [leftButton setImage:[[UIImage imageNamed:backImgName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    }
    if (self.navigationBarTransparent) {
        [self hideBackView:self.navigationController.navigationBar];
    }
    if (self.navigationBarBlue) {
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:17]};
    }else{{
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor hyBlackTextColor],NSFontAttributeName:[UIFont systemFontOfSize:17]};
    }}
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    if (self.navigationLineHidden) {
        self.navLine.hidden = YES;
    }
    if (self.navigationBarTransparent) {
        UIView * barBackground = self.navigationController.navigationBar.subviews.firstObject;
        if (@available(iOS 11.0, *))
        {
            barBackground.alpha = 0;
            [barBackground.subviews setValue:@(0) forKeyPath:@"alpha"];
        } else {
            barBackground.alpha = 0;
        }
    }
    if(self.backgroundImage){
        
        [albottom autoRemove];
        [altop autoRemove];
        BOOL hasTab = self.tabBarController!=nil;
        BOOL hasNav = self.navigationController!=nil;
//        [self.constraints autoRemoveConstraints];
        CGFloat top = hasNav?-64:0;
        CGFloat bottom = hasTab?53:53;
//        self.constraints = [self.backgroundImage autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(top, 0, bottom, 0)];
        albottom = [self.backgroundImage autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.view withOffset:bottom];
         altop = [self.backgroundImage autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view withOffset:top];

    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    if (self.needLogEvent) {
        //        [MobClick endLogPageView:NSStringFromClass([self class])];
    }
    if (self.navigationBarTransparent == YES) {
        [self showBackView:self.navigationController.navigationBar];
        UIView * barBackground = self.navigationController.navigationBar.subviews.firstObject;
        if (@available(iOS 11.0, *))
        {
            barBackground.alpha = 1;
            [barBackground.subviews setValue:@(1) forKeyPath:@"alpha"];
        } else {
            barBackground.alpha = 1;
            
        }
    }
    if (self.navigationLineHidden) {
        self.navLine.hidden = NO;
    }
    if (self.navigationBarBlue) {
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor hyBlackTextColor],NSFontAttributeName:[UIFont systemFontOfSize:17]};
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - navLine(导航栏横线)
-(UIView *)navLine {
    if (self.navigationController) {
        UIView *backgroundView = [self.navigationController.navigationBar subviews].firstObject;
        _navLine = backgroundView.subviews.lastObject;
        return _navLine;
    }else{
        return nil;
    }
}

//返回方法
- (void)backAction {
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.udo.nav.backButtonClick" object:nil];
    if (self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)showLoadingAnimation {
        [MBProgressHUD hy_showLoadingHUDAddedTo:self.view animated:YES];
}

- (void)hideLoadingAnimation {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)showMessage:(NSString *)message {
        [MBProgressHUD hy_showMessage:message inView:self.view];
}

- (void)showMessage:(NSString *)message complete:(void(^)())complate {
    [MBProgressHUD hy_showMessage:message inView:self.view completionBlock:complate];
}

#pragma mark - 横竖屏

-(BOOL)shouldAutorotate {
    return NO;
}
-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}


#pragma mark - 导航栏透明相关

/**
 设置导航栏透明

 @param superView navigationBar
 */
- (void)hideBackView:(UIView *) superView{
    NSString* backString = SYSTEM_VERSION_FLOAT >= 10.0 ? @"_UIBarBackground" : @"_UINavigationBarBackground";
    if ([superView isKindOfClass:NSClassFromString(backString)]) {
        for (UIView* view in superView.subviews) {
            //影藏分割线
            if ([view isKindOfClass:[UIImageView class]]) {
                self.navLine = view;
                self.navLine.hidden = YES;
            }
        }
        self.navBackView = superView;
        self.navBackView.alpha = 0;
    }else if ([superView isKindOfClass:NSClassFromString(@"_UIBackdropView")]) {
        superView.hidden = YES;
    }
    for (UIView* view in superView.subviews) {
        [self hideBackView:view];
    }
}

/**
 设置导航栏不透明

 @param superView navigationBar
 */
- (void)showBackView:(UIView*)superView {
    NSString* backString = SYSTEM_VERSION_FLOAT >= 10.0 ? @"_UIBarBackground" : @"_UINavigationBarBackground";
    if ([superView isKindOfClass:NSClassFromString(backString)]) {
        for (UIView* view in superView.subviews) {
            //显示分割线
            if ([view isKindOfClass:[UIImageView class]]) {
                self.navLine = view;
                self.navLine.hidden = NO;
            }
        }
        self.navBackView = superView;
        self.navBackView.alpha = 1.0;
    }else if ([superView isKindOfClass:NSClassFromString(@"_UIBackdropView")]) {
        superView.hidden = NO;
    }
    for (UIView* view in superView.subviews) {
        [self showBackView:view];
    }
}


#pragma mark - 验证是否登陆
-(BOOL)checkUserLogined {
    if ([UserInfoModel share].key==nil || [[UserInfoModel share].key isEqualToString:@""]) {
        HYAlertView* alert = [HYAlertView sharedInstance];
        [alert setSubBottonBackgroundColor:[UIColor whiteColor]];
        [alert setSubBottonTitleColor:[UIColor hyRedColor]];
        [alert setSubBottonBorderColor:[UIColor hyRedColor]];
        [alert setCancelButtonBackgroundColor:[UIColor hyRedColor]];
        [alert setCancelButtonBorderColor:[UIColor whiteColor]];
        [alert setCancelButtonTitleColor:[UIColor whiteColor]];
        [alert showAlertView:@"此操作需要登陆!" message:@"是否立即登陆?" subBottonTitle:@"取消" cancelButtonTitle:@"确定" handler:^(AlertViewClickBottonType bottonType) {
            switch (bottonType) {
                case AlertViewClickBottonTypeSubBotton:
                    break;
                default:
                    APPROUTE(kLoginViewController);
                    break;
            }
        }];
        return NO;
    }
    return YES;
}

-(void)baseSetBackgroundImage {
    self.backgroundImage = [[UIImageView alloc] initWithImage:ImageNamed(@"bg_index")];
    [self.view insertSubview:self.backgroundImage atIndex:0];
    [self.backgroundImage autoSetDimensionsToSize:CGSizeMake(kScreen_Width, kScreen_Height)];
    altop =  [self.backgroundImage autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view];
    albottom = [self.backgroundImage autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.view];
//    self.constraints = [self.backgroundImage autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(-64, 0, -49, 0)];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return (self.navigationBarTransparent == YES || self.navigationBarBlue == YES) ? UIStatusBarStyleLightContent : UIStatusBarStyleDefault;
}

@end
