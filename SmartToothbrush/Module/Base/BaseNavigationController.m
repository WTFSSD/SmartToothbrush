//
//  BaseNavigationController.m
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "BaseNavigationController.h"

@interface BaseNavigationController ()<UIGestureRecognizerDelegate>

@end

@implementation BaseNavigationController




+(instancetype)instanceWith:(UIViewController*)rootViewController{
    return [[self alloc] initWithRootViewController:rootViewController];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 横竖屏

- (BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}
- (UIViewController *)childViewControllerForStatusBarStyle {
    return self.topViewController;
}

- (void)navigationController:(UINavigationController *)navigationController
       didShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
    if ([navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.interactivePopGestureRecognizer.delegate = self;

    
}



- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.childViewControllers.count > 0) {
        UIButton * backButton = [[UIButton alloc] initWithFrame:CGRectZero];
        backButton.frame = CGRectMake(0, 0, 32, 32);
        backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
        backButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
        backButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [backButton addTarget:self action:@selector(backButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        BackButtonType type = BackButtonTypeDefault;
        UIViewController * vc = self.visibleViewController;
     
        if([viewController respondsToSelector:@selector(BaseNavgationControllerBackButtonTypeFor:)]){
            
            UIViewController<BaseNavgationControllerDelegate>* vc = viewController;
            
            type = [vc BaseNavgationControllerBackButtonTypeFor:self];
            
        }
        NSString * backImage = type == BackButtonTypeBlack?@"arrow_back.png":@"arrow_back_white.png";
        [backButton setImage:[UIImage imageNamed:backImage] forState:UIControlStateNormal];
//        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    }
    if (self.childViewControllers.count==1) {
        viewController.hidesBottomBarWhenPushed = YES; //viewController是将要被push的控制器
    }
    [super pushViewController:viewController animated:animated];
}



-(void)backButtonClick:(id)sender{
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.udo.nav.backButtonClick" object:nil];
    
    if([self.visibleViewController respondsToSelector:@selector(BaseNavgationControllerCanBack:)]){
    UIViewController<BaseNavgationControllerDelegate>* canBackDel = self.visibleViewController;
        if([canBackDel BaseNavgationControllerCanBack:self]){
            [self popViewControllerAnimated:true];
        }else{
            if([self.visibleViewController respondsToSelector:@selector(BaseNavgationController:didClickBackButton:)]){
                UIViewController<BaseNavgationControllerDelegate>* backDel = self.visibleViewController;
                [backDel BaseNavgationController:self didClickBackButton:sender];
            }
        }
    }else{
         [self popViewControllerAnimated:true];
    }
}

-(UIViewController *)popViewControllerAnimated:(BOOL)animated{
    return [super popViewControllerAnimated:animated];
}


@end
