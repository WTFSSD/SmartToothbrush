//
//  InfoDetailController.m
//  SmartToothbrush
//
//  Created by admin on 2018/6/8.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "InfoDetailController.h"

@interface InfoDetailController ()<UIWebViewDelegate>

@property (nonatomic,strong)UIWebView* webView;

@end

@implementation InfoDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self subviewStyle];
    [self fetchData];
}

-(void)subviewStyle {
    self.title = @"牙齿百科";
    self.webView = [[UIWebView alloc] init];
    self.webView.delegate = self;
    self.webView.scrollView.showsVerticalScrollIndicator = NO;
    self.webView.scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.webView];
    
    [self.webView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
}

- (void)fetchData {
    [APIHELPER infoDetailWithId:self.infoId complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
        if (isSuccess) {
            NSString* title = responseObject[@"data"][@"title"];
            NSString* htmlStr = responseObject[@"data"][@"content"];
            
            NSString * create_time =responseObject[@"data"][@"create_time"];
            
            //1530071681
            
            // iOS 生成的时间戳是10位
            NSTimeInterval interval    =[create_time doubleValue];
            NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *dateString       = [formatter stringFromDate: date];
            
        
            htmlStr =  [NSString stringWithFormat:@"<html><head><meta charset=\"utf-8\"><meta charset=\"utf-8\"/><meta content=\"width=device-width,minimum-scale=1,maximum-scale=1\" name=\"viewport\"/><meta content=\"telephone=no\" name=\"format-detection\"/><meta content=\"yes\" name=\"apple-mobile-web-app-capable\"/><meta content=\"yes\" name=\"apple-touch-fullscreen\"/><title>%@</title><style>* {padding: 0;margin: 0;}.container {padding: 10px;color: #4A5A6A;font-size: 18;}.container p {color: #98B2CE;font-size: 16px;margin: 3px 0;}.container img {width: 100%%;}.container .time {color: #98B2CE;font-size: 12px;margin: 5px 0;}</style></meta></head><body><div class=\"container\"><h2>%@</h2><div class=\"time\">%@</div>%@</div></body></html>",title,title,dateString,htmlStr];
//            self.title = title;
            [self.webView loadHTMLString:htmlStr baseURL:nil];
        }else{
            [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }
    }];
}

@end
