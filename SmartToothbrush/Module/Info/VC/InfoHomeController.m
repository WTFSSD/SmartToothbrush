//
//  InfoHomeController.m
//  Base
//
//  Created by admin on 2018/4/26.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "InfoHomeController.h"
#import "InfoDetailController.h"
#import "ToothWikiCell.h"

@interface InfoHomeController (){
   
}

@property(nonatomic,strong)NSMutableArray* dataArray;
@property(nonatomic,assign)NSInteger p;

@end

@implementation InfoHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"牙齿百科";
    [self subviewStyle];
    [self headerViewInit];
    [self fetchData];
}

#pragma mark - tableView delegate and dataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ToothWikiCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ToothWikiCell"];
    NSDictionary* data = self.dataArray[indexPath.row];
    [cell configCellWithData:data];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* data = self.dataArray[indexPath.row];
    InfoDetailController* vc = [[InfoDetailController alloc] init];
    vc.infoId = [data[@"id"] integerValue];
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - private methods
- (void)subviewStyle {
    self.backItemHidden = YES;
    [self baseSetupTableView:UITableViewStylePlain InSets:UIEdgeInsetsZero];
    [self.tableView registerClass:[ToothWikiCell class] forCellReuseIdentifier:@"ToothWikiCell"];
    self.tableView.estimatedRowHeight = 100;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)fetchData {
    [self showLoadingAnimation];
    [APIHELPER fetchInfoListWithP:self.p limit:10 complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
        [self hideLoadingAnimation];
        if (isSuccess) {
            NSArray* data = responseObject[@"data"];
            [self.dataArray addObjectsFromArray:data];
            [self.tableView reloadData];
            self.haveNext = data.count == 10;
            self.p += 1;
            if (self.haveNext) {
                [self appendFooterView];
            }else{
                [self removeFooterRefresh];
            }
        }else{
            [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }
    }];
}

-(void)headerViewInit {
    self.p = 0;
    @weakify(self);
    [self addHeaderRefresh:^{
        @strongify(self);
        [self showLoadingAnimation];
        [APIHELPER fetchInfoListWithP:self.p limit:10 complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            [self hideLoadingAnimation];
            
            [self.dataArray removeAllObjects];
            [self.tableView reloadData];
            if (isSuccess) {
                NSArray* data = responseObject[@"data"];
                [self.dataArray addObjectsFromArray:data];
                [self.tableView reloadData];
                self.haveNext = data.count == 10;
                self.p += 1;
                if (self.haveNext) {
                    [self appendFooterView];
                }else{
                    [self removeFooterRefresh];
                }
            }else{
                [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
            }
            [self endRefreshing];
        }];
    }];
}

-(void)appendFooterView {
    @weakify(self);
    [self addFooterRefresh:^{
        @strongify(self);
        [self showLoadingAnimation];
        [APIHELPER fetchInfoListWithP:self.p limit:10 complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            [self hideLoadingAnimation];
            
            if (isSuccess) {
                NSArray* data = responseObject[@"data"];
                [self.dataArray addObjectsFromArray:data];
                [self.tableView reloadData];
                self.haveNext = data.count == 10;
                self.p += 1;
                if (self.haveNext) {
                    [self appendFooterView];
                }else{
                    [self removeFooterRefresh];
                }
            }else{
                [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
            }
            [self endRefreshing];
        }];
    }];
}
@end
