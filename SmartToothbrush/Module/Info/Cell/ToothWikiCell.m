//
//  ToothWikiCell.m
//  Base
//
//  Created by WTFSSD on 2018/4/28.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "ToothWikiCell.h"


@interface ToothWikiCell(){
    UILabel * titleLabel;
    
    UILabel * contentLabel;
    
    UILabel * timeLabel;
    
    UIImageView * pic;
}

@end

@implementation ToothWikiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setUp];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self setUp];
    }
    return self;
}

-(void)setUp {

    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    titleLabel = [UILabel makeWith:^(UILabel *label) {
        label.text = @"牙齿矫正如何做？";
        label.textColor = UIColorFromHEX(0x4A5A6A);
        label.font = [UIFont systemFontOfSize:17 weight:300];
    }];
    
    contentLabel = [UILabel makeWith:^(UILabel *label) {
        label.text = @"口腔正畸就是采用科学的方法将畸形的牙颌进行矫正，通过正畸的治疗，达到美观的外貌家里的食醋(陈醋，白醋都可以，但是不可以是醋精)含在嘴里1分钟到3分钟，然后吐掉，刷牙。效果非常的好！";
        label.textColor = UIColorFromHEX(0x70879E);
        label.font = [UIFont systemFontOfSize:13 ];
        label.numberOfLines = 0;
        
    }];
    
    timeLabel = [UILabel makeWith:^(UILabel *label) {
        label.text = @"2018-4-28 21:16";
        label.textColor = UIColorFromHEX(0x98B2CE);
        label.font = [UIFont systemFontOfSize:12];
        
    }];
    
    pic = [[UIImageView alloc] initWithFrame:CGRectZero];
    
    [self.contentView addSubview:titleLabel];
    [self.contentView addSubview:contentLabel];
    [self.contentView addSubview:timeLabel];
    [self.contentView addSubview:pic];
    [pic autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.contentView withOffset:13];
    [pic autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.contentView withOffset:-15];
    [pic autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.contentView withOffset:-13];
    [pic autoSetDimensionsToSize:CGSizeMake(120, 90)];
    
    [timeLabel autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:pic];
    [timeLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.contentView withOffset:15];
    
    [titleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:pic withOffset:0];
    [titleLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.contentView withOffset:15];
    
    [contentLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:titleLabel withOffset:0];
    [contentLabel autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:timeLabel withOffset:0];
    [contentLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.contentView withOffset:15];
    [contentLabel autoPinEdge:ALEdgeRight toEdge:ALEdgeLeft ofView:pic withOffset:-12];
    timeLabel.hidden = YES;
}

- (void)configCellWithData:(NSDictionary*)data {

    titleLabel.text = data[@"title"];
    contentLabel.text = data[@"desc"];
    timeLabel.text = [HYTool dateStringWithTimestamp:[data[@"create_time"] integerValue] andFormat:@"yyyy-MM-dd HH:mm"] ;
    [pic sd_setImageWithURL:[NSURL URLWithString:data[@"cover"]]];
}

@end
