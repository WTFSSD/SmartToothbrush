//
//  ToothWikiCell.h
//  Base
//
//  Created by WTFSSD on 2018/4/28.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToothWikiCell : UITableViewCell

- (void)configCellWithData:(NSDictionary*)data;

@end
