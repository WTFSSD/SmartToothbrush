//
//  QuestionInputView.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/12.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionInputView : UIControl


@property(nonatomic,copy)NSString * title;

@property(nonatomic,copy)NSString * placeholder;

@property(nonatomic,strong)UIView * inputView;


@property(nonatomic,copy)NSString * value;


+(instancetype)instance;
@end
