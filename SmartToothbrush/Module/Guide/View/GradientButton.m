//
//  GradientButton.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/11.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "GradientButton.h"

@interface GradientButton(){
    UILabel * titleLabel;
    ///渐变色 layer
    CAGradientLayer *gradientLayer;
}

@end

@implementation GradientButton


+(instancetype)instance{
    return [self instanceWith:@""];
}

+(instancetype)instanceWith:(NSString *)title{
    return [self instanceWith:title colors:@[UIColorFromHEX(0x11CEFE),UIColorFromHEX(0x1A6CE3)]];
}

+(instancetype)instanceWith:(NSString *)title colors:(NSArray<UIColor *> *)colors{
    GradientButton * btn = [[GradientButton alloc] initWithFrame:CGRectZero];
    btn.title = title;
    btn.colors = colors;
    return btn;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self setUp];
    }
    return self;
}


-(void)setUp{
    titleLabel = [UILabel makeWith:^(UILabel *label) {
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont systemFontOfSize:16];
    }];
    [self addSubview:titleLabel];
    [titleLabel autoCenterInSuperview];
    
    RAC(titleLabel,text) = [self rac_valuesForKeyPath:@"title" observer:self];
    
    
    RAC(titleLabel,textColor) = [self rac_valuesForKeyPath:@"titleColor" observer:self];
    
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClick)];
    [self addGestureRecognizer:gesture];
    
}


-(void)layoutSubviews{
    [super layoutSubviews];
    self.layer.cornerRadius = self.frame.size.height/2.f;
    
    if(gradientLayer){
        [gradientLayer removeFromSuperlayer];
    }
    
    self.layer.masksToBounds = YES;
    
    
    gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.bounds;  // 设置显示的frame
    
    
//    gradientLayer.colors = @;  // 设置渐变颜色
    if(self.colors){
       gradientLayer.colors = [self.colors bk_map:^id(id obj) {
            return (id)[obj CGColor];
        }];
    }
    gradientLayer.startPoint = CGPointMake(0, 0.5);   //渐变色起点 在frame中的位置占比
    gradientLayer.endPoint = CGPointMake(1, 0.5);     //渐变色终点点 在frame中的位置占比
    
    //添加layer 防止覆盖subviews
    [self.layer insertSublayer:gradientLayer atIndex:0];
}

-(void)onClick{
    [self sendActionsForControlEvents:UIControlEventTouchUpInside];
}
@end
