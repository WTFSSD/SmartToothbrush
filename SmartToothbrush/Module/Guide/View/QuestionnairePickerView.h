//
//  QuestionnairePickerView.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/12.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PickerData:NSObject

@property(nonatomic,copy)NSString * label;

@property(nonatomic,strong)id value;

@property(nonatomic,strong)NSArray<PickerData*>* children;


+(instancetype)instanceWith:(NSString*)label value:(id)value children:(NSArray<PickerData*>*)children;
@end


@interface QuestionnairePickerView : UIControl

+(instancetype)instanceWithFrame:(CGRect)frame;

@property(nonatomic,strong)NSArray<PickerData*>* dataSource;

@property(nonatomic,strong,readonly)NSArray<PickerData*> * value;
@end
