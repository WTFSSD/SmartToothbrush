//
//  PickItemView.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/12.
//  Copyright © 2018年 wtfssd. All rights reserved.
//


typedef NS_ENUM(NSUInteger, PickItemViewType) {
    PickItemViewTypeBorder = 0,
    PickItemViewTypeNormal,
    
};
#import <UIKit/UIKit.h>

@interface PickItemView : UIControl
@property(nonatomic,copy)NSString * title;
@property(nonatomic,copy)NSString * imageName;
@property(nonatomic,assign)PickItemViewType type;
+(instancetype)instance;
@end
