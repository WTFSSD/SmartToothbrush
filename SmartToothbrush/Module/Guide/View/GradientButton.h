//
//  GradientButton.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/11.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GradientButton : UIControl
@property(nonatomic,copy)    NSString * title;
@property(nonatomic,strong)  NSArray <UIColor*>* colors;

@property(nonatomic,strong)UIColor * titleColor;
+(instancetype)instance;

+(instancetype)instanceWith:(NSString*)title;


+(instancetype)instanceWith:(NSString *)title colors:(NSArray<UIColor*>*)colors;

@end
