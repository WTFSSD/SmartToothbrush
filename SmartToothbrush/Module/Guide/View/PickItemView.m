//
//  PickItemView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/12.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "PickItemView.h"

@interface PickItemView(){
    UILabel * textLabel;
    
    UIImageView * imageView;
    
    NSLayoutConstraint * offset;
}
@end

@implementation PickItemView
+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self setUp];
    }
    return self;
}

-(void)setUp{
    self.backgroundColor = UIColorFromHEX(0xF1F3F8);
    textLabel = [UILabel makeWith:^(UILabel *label) {
        label.textColor = UIColorFromHEX(0x4A5A6A);
        label.font = [UIFont systemFontOfSize:14];
    }];
    [self addSubview:textLabel];
    [textLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self];
   offset =  [textLabel autoAlignAxis:ALAxisVertical toSameAxisOfView:self withOffset:0];
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [self addSubview:imageView];
    
    [imageView autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self withOffset:-10];
    [imageView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:self withOffset:-5];
    [imageView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionHeight ofView:imageView withOffset:0];
    [imageView autoAlignAxis:ALAxisHorizontal toSameAxisOfView:textLabel];
    
    RACSignal * s1 = [self rac_valuesForKeyPath:@"imageName" observer:self];
    RAC(imageView,hidden) = [s1 map:^id(id value) {
        offset.constant = value?-8:0;
        if(!value) return @(YES);
        return @(NO);
    }];
    RAC(imageView,image) = [s1 map:^id(id value) {
        return ImageNamed(value);
    }];
    
    
    RAC(textLabel,text) = [self rac_valuesForKeyPath:@"title" observer:self];
    
    [self bk_whenTapped:^{
        [self sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
}


-(void)layoutSubviews{
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.frame.size.height/2.f;
    
    switch (self.type) {
        case PickItemViewTypeBorder:{
            if(self.selected){
                self.layer.borderWidth = 2;
                self.layer.borderColor = UIColorFromHEX(0x12C6FC).CGColor;
            }else{
                self.layer.borderWidth = 0;
                self.layer.borderColor = UIColorFromHEX(0x12C6FC).CGColor;
            }
           
        }break;
        case PickItemViewTypeNormal:{
            if(self.selected){
                self.backgroundColor = UIColorFromHEX(0x70879E);
                textLabel.textColor = [UIColor whiteColor];
            }else{
                self.backgroundColor = UIColorFromHEX(0xF1F3F8);
                textLabel.textColor = UIColorFromHEX(0x4A5A6A);
            }
        }break;
        default:break;
    }
    self.layer.masksToBounds = YES;
}
@end
