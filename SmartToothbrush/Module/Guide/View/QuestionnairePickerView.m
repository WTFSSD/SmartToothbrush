//
//  QuestionnairePickerView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/12.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "QuestionnairePickerView.h"


@implementation PickerData

+(instancetype)instanceWith:(NSString*)label value:(id)value children:(NSArray<PickerData*>*)children{
    PickerData * data = [[self alloc] init];
    data.label = label;
    data.value = value;
    data.children = children;
    return data;
}
@end

@interface QuestionnairePickerView()<UIPickerViewDelegate,UIPickerViewDataSource>{
    UIPickerView * picker;
}

@property(nonatomic,strong,readwrite)NSArray<PickerData*> * value;@end

@implementation QuestionnairePickerView

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}

+(instancetype)instanceWithFrame:(CGRect)frame{
    return [[self alloc] initWithFrame:frame];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self setUp];
    }
    return self;
}

-(void)setUp{
    picker = [[UIPickerView alloc] initWithFrame:CGRectZero];
    [self addSubview:picker];
    [picker autoPinEdgesToSuperviewEdges];
    picker.delegate = self;
    picker.dataSource = self;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(!self.dataSource) return 0;
    return self.dataSource.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return self.dataSource[row].label;
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.value = @[self.dataSource[row]];
    [self sendActionsForControlEvents:UIControlEventValueChanged];

}
@end
