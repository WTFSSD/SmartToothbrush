//
//  QuestionInputView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/12.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "QuestionInputView.h"

@interface QuestionInputView(){
    UILabel * titleLabel;
    UITextField * textFiled;
    UIView * underLine;
}
@end

@implementation QuestionInputView


+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}
-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}
-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self setUp];
    }
    return self;
}

-(void)setUp{
    titleLabel = [UILabel makeWith:^(UILabel *label) {
        label.textColor =UIColorFromHEX(0x4A5A6A);
        label.font = [UIFont systemFontOfSize:15];
    }];
    
    [self addSubview:titleLabel];
    
    [titleLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self];
    [titleLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self];
    [titleLabel autoSetDimension:ALDimensionWidth toSize:zoom(60)];
    
    
    RAC(titleLabel,text) = [self rac_valuesForKeyPath:@"title" observer:self];
    
    
    
    textFiled = [[UITextField alloc] initWithFrame:CGRectZero];
    textFiled.textAlignment = NSTextAlignmentRight;
    [self addSubview:textFiled];
    
    [textFiled autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self];
    [textFiled autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self];
    [textFiled autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:titleLabel];
    [textFiled autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:self];
    
    RAC(textFiled,placeholder) = [self rac_valuesForKeyPath:@"placeholder" observer:self];
    
    underLine = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:underLine];
    underLine.backgroundColor = UIColorFromHEX(0xEEEEEE);
    [underLine autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self];
    [underLine autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self];
    [underLine autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self];
    [underLine autoSetDimension:ALDimensionHeight toSize:1];
    
    
    RAC(textFiled,text) = [self rac_valuesForKeyPath:@"value" observer:self];
  
    
    [textFiled addTarget:self action:@selector(textFiledTextChange:) forControlEvents:UIControlEventEditingChanged];
    
    
}




-(void)textFiledTextChange:(UITextField*)textField{
    self.value = textFiled.text;
     [self sendActionsForControlEvents:UIControlEventEditingChanged];
}

-(void)setInputView:(UIView *)inputView{
    _inputView = inputView;
    textFiled.inputView = inputView;
}

@end
