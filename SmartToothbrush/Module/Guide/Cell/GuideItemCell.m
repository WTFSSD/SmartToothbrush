//
//  GuideItemCell.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/7.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "GuideItemCell.h"


#import "GradientButton.h"

#import <SDWebImage/UIImageView+WebCache.h>
@interface GuideItemCell(){
    UIImageView*imageView;
    
    GradientButton * okButton;
    
    UIButton * cancelButton;
    
    
    UILabel * titleLabel;
    
    UILabel * subLabel;
    
    
    UIImageView * icon;
}
@end
@implementation GuideItemCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}


-(void)setUp{
    self.backgroundColor = [UIColor whiteColor];
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    
    RACSignal * hidenSignal = [[self rac_valuesForKeyPath:@"imageName" observer:self] map:^id(id value) {
        return @([value length] == 0);
    }];
    
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:imageView];
    [imageView autoPinEdgesToSuperviewEdges];
    
    [[self rac_valuesForKeyPath:@"imageName" observer:self] subscribeNext:^(id x) {
        if(!x || [x length]<=0) return;
        if([[x lowercaseString] containsString:@"http"]){
            [imageView sd_setImageWithURL:[NSURL URLWithString:x]];
        }else{
            imageView.image = [UIImage imageNamed:x];
        }
    }];
    
  
    RAC(imageView,hidden) = hidenSignal;
    
    
    
    titleLabel = [UILabel makeWith:^(UILabel *label) {
        label.text = @"让我们更好地帮助您牙刷\n推送智能牙刷报告";
        label.textColor = UIColorFromHEX(0x000000);
        label.numberOfLines = 0;
        label.font = [UIFont systemFontOfSize:20 weight:500];
        label.textAlignment = NSTextAlignmentCenter;
    }];
    
    
    [self.contentView addSubview:titleLabel];
    
    [titleLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.contentView withOffset:zoom(70)];
    [titleLabel autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.contentView withOffset:-zoom(70)];
    [titleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.contentView withOffset:zoom(145)];
    
    
    
    subLabel = [UILabel makeWith:^(UILabel *label) {
        label.text = @"请在后面的弹窗中选择“同意”";
        label.textColor = UIColorFromHEX(0x4A5A6A);
        label.numberOfLines = 0;
        label.font = [UIFont systemFontOfSize:14];
        label.textAlignment = NSTextAlignmentCenter;
    }];
    [self.contentView addSubview:subLabel];
    
    [subLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.contentView withOffset:zoom(45)];
    [subLabel autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.contentView withOffset:-zoom(45)];
    [subLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:titleLabel withOffset:zoom(12)];
    
    
    
    icon = [[UIImageView alloc] initWithImage:ImageNamed(@"start_icon.png")];
    [self.contentView addSubview:icon];
    [icon autoAlignAxis:ALAxisVertical toSameAxisOfView:subLabel];
    [icon autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:subLabel withOffset:zoom(12)];

    
    
  
    
    
    cancelButton = [UIButton makeWith:^(UIButton *button) {
       button.setTitle(@"暂时不用",UIControlStateNormal)
        .setTitleColor(UIColorFromHEX(0x70879E),UIControlStateNormal)
        .setTitleFont([UIFont systemFontOfSize:16]);
    }];
    
    [self.contentView addSubview:cancelButton];
    [cancelButton autoAlignAxis:ALAxisVertical toSameAxisOfView:self.contentView];
    [cancelButton autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.contentView withOffset:-zoom(20)];
    [cancelButton autoSetDimensionsToSize:CGSizeMake(zoom(200), zoom(40))];
    
    
    
    okButton = [GradientButton instanceWith:@"好的"];
    [self.contentView addSubview:okButton];
    [okButton autoAlignAxis:ALAxisVertical toSameAxisOfView:self.contentView];
    [okButton autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:cancelButton withOffset:-zoom(20)];
    [okButton autoSetDimensionsToSize:CGSizeMake(zoom(200), zoom(40))];
    
    
    RAC(titleLabel,hidden) = [hidenSignal map:^id(id value) {
        return @(![value boolValue]);
    }];
    RAC(subLabel,hidden) = [hidenSignal map:^id(id value) {
        return @(![value boolValue]);
    }];
    RAC(icon,hidden) = [hidenSignal map:^id(id value) {
        return @(![value boolValue]);
    }];
    RAC(okButton,hidden) = [hidenSignal map:^id(id value) {
        return @(![value boolValue]);
    }];
    
    RAC(cancelButton,hidden) = [hidenSignal map:^id(id value) {
        return @(![value boolValue]);
    }];
    
    
    [[okButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        if(self.onSkipClick){
            self.onSkipClick();
        }
    }];
    
    [[cancelButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        if(self.onCancel){
            self.onCancel();
        }
    }];
    
   
}
@end
