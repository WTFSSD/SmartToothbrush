//
//  GuideItemCell.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/7.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuideItemCell : UICollectionViewCell



@property(nonatomic,copy)NSString * imageName;
@property(nonatomic,copy)void(^onSkipClick)(void);
@property(nonatomic,copy)void(^onCancel)(void);
@end
