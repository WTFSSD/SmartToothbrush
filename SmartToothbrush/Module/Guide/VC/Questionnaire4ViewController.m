//
//  Questionnaire4ViewController.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/10.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "Questionnaire4ViewController.h"
#import "PickItemView.h"
@interface Questionnaire4ViewController (){
    NSArray * data;
    NSMutableArray<PickItemView*> * pickeItems;
    NSInteger seledtedIndex;
}

@end

@implementation Questionnaire4ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.serverSignal){
        [self.serverSignal subscribeNext:^(SurveyModel *  x) {
            NSLog(@"获取问卷详情:%@",x);
            if(x && x.neat.length>0){
                if([x.neat isEqualToString:@"1"]){
                    seledtedIndex = 0;
                }else{
                    seledtedIndex = 1;
                };
                
                
               
                [self updatePickItemView];
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUp{
    [super setUp];
    pickeItems = [NSMutableArray arrayWithCapacity:2];
    self.titleText = @"让你的私人口腔助理了解你";
    self.brieText = @"牙齿是否排列整齐？";
    self.nextButtonTitle = @"完成";
    seledtedIndex = 0;
    
    data = @[
             @{@"title":@"是",@"size":@"{70,40}"},
             @{@"title":@"否",@"size":@"{70,40}"},
             ];
    [[self.nextButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
        
        
        NSMutableDictionary *  params = [NSMutableDictionary dictionary];
        params[@"key"] = [UserInfoModel share].key;
        params[@"neat"] =@(seledtedIndex + 1);
        
        [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_MEMBER_SURVER) params:params].then(^(id res){
            if(!res){
                return [MBProgressHUD hy_showMessage:@"请求失败" inView:self.view completionBlock:nil];
            };
            
            if([res[@"code"] integerValue]==1){
                AppDelegate * delegate = (AppDelegate * )[UIApplication sharedApplication].delegate;
                [NetworkUtils requestWith:HTTPMethodGET url:REQUEST_URL(API_MEMBER_MY_TOOTHBRUSH) params:@{@"key":[UserInfoModel share].key}].then(^(id res){
                    if(!res){
                        [delegate configConnect];
                    }else{
                        [delegate configTabbar];
                    }
                });
            }else{
                return [MBProgressHUD hy_showMessage:res[@"msg"] inView:self.view completionBlock:nil];
            }
        });
    }];
    [[self.backButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    
    PickItemView * last = nil;
    for (int i = 0 ; i<data.count; i++) {
        PickItemView * item = [PickItemView instance];
         item.tag = i+2000;
        [self.view addSubview:item];
        
        pickeItems[i] = item;
        item.title = data[i][@"title"];
       
        item.type = PickItemViewTypeNormal;
        [[item rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            PickItemView *  temp = ( PickItemView * ) x;
            
            if(seledtedIndex != temp.tag){
                temp.selected = YES;
                seledtedIndex = item.tag;
                pickeItems[seledtedIndex == 0?1:0].selected = NO;
                [pickeItems[seledtedIndex == 0?1:0] setNeedsLayout];
            }
            [temp setNeedsLayout];
        }];
        item.selected = (i == 0);
        CGSize  size = CGSizeFromString(data[i][@"size"]);
        if(last){
            [item autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:last withOffset:zoom(15)];
            
        }else{
            [item autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.briefLabel withOffset:zoom(120)];
        }
        last = item;
        
        [item autoSetDimension:ALDimensionHeight toSize:zoom(size.height)];
        [item autoSetDimension:ALDimensionWidth toSize:zoom(size.width)];
        [item autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    }
}

-(void)updatePickItemView{
    for (int i = 0 ; i<data.count; i++) {
        PickItemView * item =  (PickItemView * )[self.view viewWithTag:i+2000];
        if(item.tag == seledtedIndex + 2000){
            item.selected = YES;
        }else{
            item.selected = NO;
        }
        [item setNeedsLayout];
    }
}

@end
