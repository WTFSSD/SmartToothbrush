//
//  BaseQuestionnaireViewController.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/12.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BaseQuestionnaireViewController.h"





@implementation SurveyModel

@end

@interface BaseQuestionnaireViewController (){


}
@property(nonatomic,strong)UILabel * titleLabel;
@property(nonatomic,strong)UILabel * briefLabel;
@property(nonatomic,strong)UIButton * backButton;
@property(nonatomic,strong)GradientButton * nextButton;
@end

@implementation BaseQuestionnaireViewController


+(instancetype)instance{
    return [[self alloc] init];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBarBlue = NO;
    self.navigationBarTransparent = NO;
    [self setUp];
}

-(void)baseSetBackgroundImage{}



-(RACSignal *)serverSignal{
    if(![UserInfoModel share].isLogin)return nil;
    if(!_serverSignal){
        
        _serverSignal = [[NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_MEMBER_SERVER) params:@{} headers:nil progress:nil obj:[SurveyModel class]] signal];
    }
    return _serverSignal;
}

///Gradient
-(void)setUp{
    _titleLabel = [UILabel makeWith:^(UILabel *label) {
       
        label.textColor = UIColorFromHEX(0x212830);
        label.font = [UIFont boldSystemFontOfSize:21];
    }];
    [self.view addSubview: _titleLabel];
    
    [ _titleLabel autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [ _titleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view withOffset:zoom(101)];
    
    
    _briefLabel = [UILabel makeWith:^(UILabel *label) {
    
        label.textColor = UIColorFromHEX(0x4A5A6A);
        label.font = [UIFont boldSystemFontOfSize:16];
    }];
    [self.view addSubview:_briefLabel];
    
    [_briefLabel autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [_briefLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_titleLabel withOffset:zoom(15)];
    
    _backButton = [UIButton makeWith:^(UIButton *button) {
        button.setTitle(@"返回",UIControlStateNormal)
        .setTitleColor(UIColorFromHEX(0x70879E),UIControlStateNormal)
        .setTitleFont([UIFont systemFontOfSize:16]);
    }];
    
    [self.view addSubview:_backButton];
    
    [_backButton autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [_backButton autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.view withOffset:-zoom(112.5)];

    
    _nextButton = [GradientButton instanceWith:@"下一步"];
    _nextButton.titleColor = UIColorFromHEX(0xcccccc);
    [self.view addSubview:_nextButton];
    
    [_nextButton autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [_nextButton autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.view withOffset:-zoom(48.5)];
    [_nextButton autoSetDimensionsToSize:CGSizeMake(zoom(200), zoom(40))];
    
    
    RAC(_titleLabel,text) = [self rac_valuesForKeyPath:@"titleText" observer:self];
    RAC(_briefLabel,text) = [self rac_valuesForKeyPath:@"brieText" observer:self];
    RAC(_nextButton,title) = [self rac_valuesForKeyPath:@"nextButtonTitle" observer:self];
    
}


@end
