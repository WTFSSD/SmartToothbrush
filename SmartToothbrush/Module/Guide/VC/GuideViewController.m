//
//  GuideViewController.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/7.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "GuideViewController.h"

#import "AppDelegate+Extension.h"
#import "GuideItemCell.h"

#import "LoginViewController.h"
#import "BaseNavigationController.h"
@interface GuideViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate>{
    UIButton * skip;
    UICollectionView * collectionView;
    
    
    UIPageControl * pageControl;
    NSMutableArray * dataSource;
}

@end

@implementation GuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    

   RACSignal * netSignal =  [[NetworkUtils requestWith:HTTPMethodGET url:REQUEST_URL(API_GUIDE) params:nil headers:nil progress:nil obj:nil] signal];
#ifdef USE_NET_GUIDE_IMAGE
    [netSignal subscribeNext:^(id x) {
        [dataSource removeAllObjects];
        [dataSource addObjectsFromArray:x[@"data"]];
        [dataSource addObject:@""];
        [collectionView reloadData];
    }];
#endif
    
}


-(void)baseSetBackgroundImage{
    
}
-(void)setUp{
    dataSource = [NSMutableArray arrayWithArray:@[
                                                  @"001引导页1.png",
                                                  @"002引导页2.png",
                                                  @"003引导页3.png",
                                                   @"004引导页4.png",
                                                  @"005引导页5.png",
                                                  @"",
                                                  ]];
    
    self.navigationBarHidden = YES;
    
//    skip=[UIButton makeWith:^(UIButton *button) {
//        button.setTitle(@"跳过",UIControlStateNormal)
//        .setTitleColor([UIColor blueColor],UIControlStateNormal);
//    }];
//    [self.view addSubview:skip];
//    [skip autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view withOffset:15];
//    [skip autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.view withOffset:-15];
//    [[skip rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
//        AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
//        [delegate configTabbar];
//    }];
    
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.sectionInset = UIEdgeInsetsZero;
    layout.minimumLineSpacing = 0;
    collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    collectionView.pagingEnabled = YES;
    [self.view insertSubview:collectionView atIndex:0];
    [collectionView autoPinEdgesToSuperviewEdges];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.showsHorizontalScrollIndicator = NO;
    [collectionView registerClass:[GuideItemCell class] forCellWithReuseIdentifier:@"GuideItemCell"];
    

}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return dataSource?dataSource.count:0;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return self.view.frame.size;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GuideItemCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GuideItemCell" forIndexPath:indexPath];
    
    NSString * imageName = dataSource[indexPath.row];
    cell.imageName = imageName;
    
    cell.onSkipClick = ^{
        AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        [delegate configLogin];
    };
    
    cell.onCancel = ^{
     
    };
    
    
  
    return cell;
}


-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    NSUInteger page = (NSUInteger)scrollView.contentOffset.x/self.view.frame.size.width;
    pageControl.currentPage = page;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSUInteger page = (NSUInteger)scrollView.contentOffset.x/self.view.frame.size.width;
    pageControl.currentPage = page;
}

@end
