//
//  Questionnaire2ViewController.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/10.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "Questionnaire2ViewController.h"
#import "Questionnaire3ViewController.h"

#import "PickItemView.h"
@interface Questionnaire2ViewController (){
    NSArray * data;
    NSMutableArray * arr;
    
}

@end

@implementation Questionnaire2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"调查问卷";
    if(self.serverSignal){
        [self.serverSignal subscribeNext:^(SurveyModel *  x) {
            NSLog(@"获取问卷详情:%@",x);
            if(x && x.habit.length>0){
                NSArray * temp = [x.habit componentsSeparatedByString:@","];
                [arr removeAllObjects];
                
                [temp enumerateObjectsUsingBlock:^(id  _Nonnull str, NSUInteger idx, BOOL * _Nonnull stop) {
                    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        if([obj[@"title"] isEqualToString:str]){
                            [arr addObject:@(idx+2000)];
                        }
                    }];
                }];
                [self updatePickItemView];
            }
        }];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)setUp{
    [super setUp];
    
    arr = [NSMutableArray array];
    data = @[
             
             @{@"title":@"第一支电动牙刷",@"image":@"q_ico_toothbrush.png",@"size":@"{160,40}",@"left":@(35)},
             @{@"title":@"常吃甜点",@"image":@"q_ico_doughnut.png",@"size":@"{120,40}"},
             @{@"title":@"常喝咖啡",@"image":@"q_ico_coffee.png",@"size":@"{120,40}"},
             @{@"title":@"每天吸烟",@"image":@"q_ico_cigarette.png",@"size":@"{120,40}"},
             @{@"title":@"经常喝茶",@"image":@"q_ico_tea.png",@"size":@"{120,40}"},
             @{@"title":@"常喝红酒",@"image":@"q_ico_wine.png",@"size":@"{120,40}"},
             @{@"title":@"左手刷牙",@"image":@"q_ico_lefthand.png",@"size":@"{120,40}"},
             @{@"title":@"右手刷牙",@"image":@"q_ico_righthand.png",@"size":@"(120,40)"},
             ];
    self.titleText = @"让你的私人口腔助理了解你";
    self.brieText = @"你是否有以下生活习惯？";
    self.nextButtonTitle = @"下一步";
    
    [[self.nextButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
       
        
        NSMutableString * str = [NSMutableString  string];
        
        for (int i=0; i<arr.count; i++) {
            int index = [arr[i] intValue];
            [str appendFormat:@"%@,",data[index-2000][@"title"]];
        }
        
        NSMutableDictionary *  params = [NSMutableDictionary dictionary];
        params[@"key"] = [UserInfoModel share].key;
        params[@"habit"] = str;
        
        [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_MEMBER_SURVER) params:params].then(^(id res){
            if(!res){
                return [MBProgressHUD hy_showMessage:@"请求失败" inView:self.view completionBlock:nil];
            };
            if([res[@"code"] integerValue]==1){
                Questionnaire3ViewController * vc = [Questionnaire3ViewController instance];
                [self presentViewController:vc animated:YES completion:nil];
                
            }else{
                return [MBProgressHUD hy_showMessage:res[@"msg"] inView:self.view completionBlock:nil];
            }
            
            
        });
    }];
    [[self.backButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        if(self.navigationController){
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
    }];
    PickItemView * last = nil;
    for (int i = 0 ; i<data.count; i++) {
        PickItemView * item = [PickItemView instance];
        [[item rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            PickItemView *  temp = ( PickItemView * ) x;
            temp.selected = !temp.selected;
            if([arr indexOfObject:@(temp.tag)] == NSNotFound){
                if(item.selected){
                    [arr addObject:@(temp.tag)];
                }
            }else{
                if(!item.selected){
                    [arr removeObject:@(temp.tag)];
                }
            }
            [temp setNeedsLayout];
        }];
        
        item.tag = i + 2000;
        item.type = PickItemViewTypeBorder;
        item.selected = NO;
        [self.view addSubview:item];
        item.title = data[i][@"title"];
        item.imageName = data[i][@"image"];
        CGSize  size = CGSizeFromString(data[i][@"size"]);
        CGFloat left =zoom(data[i][@"left"]?[data[i][@"left"] floatValue]:57);
        if(last){
            if(i%2){
                [item autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:last withOffset:zoom(0)];
                [item autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:last withOffset:zoom(20)];
            }else{
                //首行
                [item autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:last withOffset:zoom(22)];
                [item autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view withOffset:left];
            }
        }else{
            [item autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view withOffset:left];
            [item autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.briefLabel withOffset:zoom(60)];
        }
        last = item;
    
        [item autoSetDimension:ALDimensionHeight toSize:zoom(size.height)];
        [item autoSetDimension:ALDimensionWidth toSize:zoom(size.width)];
    }
}


-(void)updatePickItemView{
    for (int i = 0 ; i<data.count; i++) {
       PickItemView * item =  (PickItemView * )[self.view viewWithTag:i+2000];
        
        if([arr indexOfObject:@(item.tag)]!=NSNotFound){
            item.selected = YES;
        }else{
             item.selected = NO;
        }
        [item setNeedsLayout];
    }
}
@end
