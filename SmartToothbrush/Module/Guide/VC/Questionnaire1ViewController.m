//
//  Questionnaire1ViewController.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/10.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "Questionnaire1ViewController.h"

#import "GradientButton.h"

#import "Questionnaire2ViewController.h"
#import "BaseNavigationController.h"
#import "QuestionInputView.h"

#import "QuestionnairePickerView.h"
#import "UDPProtocol.h"
@interface Questionnaire1ViewController (){

    QuestionInputView * nickName;
    
    QuestionInputView * sex;
    
    
    QuestionInputView * age;
    
    
    UIPickerView * pickerView;
    
    
    NSMutableDictionary * params;

}



@end

@implementation Questionnaire1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
}

///Gradient
-(void)setUp{
    [super setUp];
    params = [NSMutableDictionary dictionary];
    params[@"key"] = [UserInfoModel share].key;
    self.titleText = @"让你的私人口腔助理了解你";
    self.brieText = @"填写问卷基本信息";
    self.nextButtonTitle = @"下一步";

    
    [[self.nextButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
        
        
        [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_MEMBER_SURVER) params:params].then(^(id res){
            if(!res){
                return [MBProgressHUD hy_showMessage:@"请求失败" inView:self.view completionBlock:nil];
            };
           
            if([res[@"code"] integerValue]==1){
               
                Questionnaire2ViewController * vc = [Questionnaire2ViewController instance];
                [self presentViewController:vc animated:YES completion:nil];
                
            }else{
                 return [MBProgressHUD hy_showMessage:res[@"msg"] inView:self.view completionBlock:nil];
            }
    
            
        });
        
        
//        Questionnaire2ViewController * vc = [Questionnaire2ViewController instance];
//        [self presentViewController:vc animated:YES completion:nil];
    }];
    [[self.backButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        AppDelegate * delegate = (AppDelegate * )[UIApplication sharedApplication].delegate;
        [delegate configGuide];
    }];
    
    
    nickName = [QuestionInputView instance];
    
    
    nickName.title = @"昵称";
    nickName.placeholder = @"请输入昵称";
    [self.view addSubview:nickName];
    [nickName autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view withMultiplier:310/375.f];
    [nickName autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:nickName withMultiplier:60/310.f];
    
    [nickName autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [nickName autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.briefLabel withOffset:20];
    
    sex = [QuestionInputView instance];
    sex.title = @"性别";
    sex.placeholder = @"请输入性别";
    QuestionnairePickerView * picker1 = [QuestionnairePickerView instanceWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
    picker1.dataSource = @[
                           [PickerData instanceWith:@"男" value:@(0) children:nil],
                           [PickerData instanceWith:@"女" value:@(1) children:nil],
                           ];
    sex.inputView = picker1;
    [self.view addSubview:sex];
    [sex autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view withMultiplier:310/375.f];
    [sex autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:sex withMultiplier:60/310.f];
    
    [sex autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [sex autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:nickName withOffset:0];
    RAC(sex,value) = [[picker1 rac_signalForControlEvents:UIControlEventAllEvents] map:^id(id value) {
        QuestionnairePickerView * picker = (QuestionnairePickerView*)value;
        return picker.value[0].label;
    }];
    
    
    
    
    QuestionnairePickerView * picker2 = [QuestionnairePickerView instanceWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
    NSMutableArray * arr = [NSMutableArray array];
    for (int i = 0 ; i<120; i++) {
        [arr addObject:[PickerData instanceWith:[NSString stringWithFormat:@"%d",i] value:@(i) children:nil]];
    }
    picker2.dataSource =arr;

    
    age = [QuestionInputView instance];
    age.title = @"年龄";
    age.placeholder = @"请输入年龄";
    age.inputView = picker2;
    [self.view addSubview:age];
    [age autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view withMultiplier:310/375.f];
    [age autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:age withMultiplier:60/310.f];
    
    [age autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [age autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:sex withOffset:0];
    RAC(age,value) = [[picker2 rac_signalForControlEvents:UIControlEventAllEvents] map:^id(id value) {
        QuestionnairePickerView * picker = (QuestionnairePickerView*)value;
        return picker.value[0].label;
    }];
    
    [[nickName rac_valuesForKeyPath:@"value" observer:self] subscribeNext:^(id x) {
        params[@"nickname"] = x;
    }];
    
    [[sex rac_valuesForKeyPath:@"value" observer:self] subscribeNext:^(id x) {
        if(!x) return ;
        if([[x lowercaseString] containsString:@"男"]){
            params[@"sex"] = @(1);
        }
        if([[x lowercaseString] containsString:@"女"]){
            params[@"sex"] = @(2);
        }
    }];
    
    [[age rac_valuesForKeyPath:@"value" observer:self] subscribeNext:^(id x) {
        if(!x) return ;
        params[@"age"] = x;
    }];
    
  
}

@end
