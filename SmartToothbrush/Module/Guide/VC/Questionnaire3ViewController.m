//
//  Questionnaire3ViewController.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/10.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "Questionnaire3ViewController.h"
#import "Questionnaire4ViewController.h"
#import "PickItemView.h"
@interface Questionnaire3ViewController (){
       NSArray * data;
    NSMutableArray * arr;
}

@end

@implementation Questionnaire3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.serverSignal){
        [self.serverSignal subscribeNext:^(SurveyModel *  x) {
            NSLog(@"获取问卷详情:%@",x);
            if(x && x.mouth.length>0){
                NSArray * temp = [x.mouth componentsSeparatedByString:@","];
                [arr removeAllObjects];
                
                [temp enumerateObjectsUsingBlock:^(id  _Nonnull str, NSUInteger idx, BOOL * _Nonnull stop) {
                    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        if([obj[@"title"] isEqualToString:str]){
                            [arr addObject:@(idx+2000)];
                        }
                    }];
                }];
                [self updatePickItemView];
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)setUp{
    [super setUp];
    arr = [NSMutableArray array];
    data = @[
             @{@"title":@"有蛀牙",@"size":@"{90,40}",@"left":@(37.5)},
              @{@"title":@"正畸牙齿",@"size":@"{100,40}",@"left":@(10)},
              @{@"title":@"牙结石",@"size":@"{90,40}",@"left":@(10)},
              @{@"title":@"牙齿敏感",@"size":@"{100,40}",@"left":@(42.5)},
              @{@"title":@"假牙",@"size":@"{70,40}",@"left":@(10)},
              @{@"title":@"常用牙线",@"size":@"{100,40}",@"left":@(10)},
              @{@"title":@"使用漱口水",@"size":@"{100,40}",@"left":@(72.5)},
              @{@"title":@"使用冲牙器",@"size":@"{100,40}",@"left":@(37.5)},
             ];
    
    self.titleText = @"让你的私人口腔助理了解你";
    self.brieText = @"你是否有以下口腔情况？";
    self.nextButtonTitle = @"下一步";
    
    [[self.nextButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
        
        NSMutableString * str = [NSMutableString  string];
        for (int i=0; i<arr.count; i++) {
            
            int index = [arr[i] intValue];
            [str appendFormat:@"%@,",data[index-2000][@"title"]];
        }
        
        
        
        NSMutableDictionary *  params = [NSMutableDictionary dictionary];
        params[@"key"] = [UserInfoModel share].key;
        params[@"mouth"] =str;
        
        [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_MEMBER_SURVER) params:params].then(^(id res){
            if(!res){
                return [MBProgressHUD hy_showMessage:@"请求失败" inView:self.view completionBlock:nil];
            };
            
            if([res[@"code"] integerValue]==1){
                
                Questionnaire4ViewController * vc = [Questionnaire4ViewController instance];
                [self presentViewController:vc animated:YES completion:nil];
                
            }else{
                return [MBProgressHUD hy_showMessage:res[@"msg"] inView:self.view completionBlock:nil];
            }
            
            
        });
       
    }];
    [[self.backButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    
    PickItemView * last = nil;
    for (int i = 0 ; i<data.count; i++) {
        PickItemView * item = [PickItemView instance];
        item.tag = i+2000;
        [[item rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            PickItemView *  temp = ( PickItemView * ) x;
            temp.selected = !temp.selected;
            if([arr indexOfObject:@(temp.tag)] == NSNotFound){
                if(item.selected){
                    [arr addObject:@(temp.tag)];
                }
            }else{
                if(!item.selected){
                    [arr removeObject:@(temp.tag)];
                }
            }
            [temp setNeedsLayout];
        }];
        
        [self.view addSubview:item];
        item.title = data[i][@"title"];
        item.type = PickItemViewTypeNormal;
        item.selected = NO;
        CGSize  size = CGSizeFromString(data[i][@"size"]);
        CGFloat left =zoom(data[i][@"left"]?[data[i][@"left"] floatValue]:57);
        if(last){
            if(i%3 == 0){
                [item autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:last withOffset:zoom(30)];
                [item autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view withOffset:left];
            }else{
                [item autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:last withOffset:zoom(0)];
                [item autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:last withOffset:zoom(20)];
            }
            
        }else{
            [item autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view withOffset:left];
            [item autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.briefLabel withOffset:zoom(60)];
        }
        last = item;
        
        [item autoSetDimension:ALDimensionHeight toSize:zoom(size.height)];
        [item autoSetDimension:ALDimensionWidth toSize:zoom(size.width)];
    }
}


-(void)updatePickItemView{
    for (int i = 0 ; i<data.count; i++) {
        PickItemView * item =  (PickItemView * )[self.view viewWithTag:i+2000];
        
        if([arr indexOfObject:@(item.tag)]!=NSNotFound){
            item.selected = YES;
        }else{
            item.selected = NO;
        }
        [item setNeedsLayout];
    }
}
@end
