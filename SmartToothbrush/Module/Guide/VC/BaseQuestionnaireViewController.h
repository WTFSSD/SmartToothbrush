//
//  BaseQuestionnaireViewController.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/12.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BaseViewController.h"

#import "GradientButton.h"
#import "BaseNavigationController.h"
#import "QuestionInputView.h"

@interface SurveyModel:NSObject

///生活习惯
@property(nonatomic,strong)NSString * habit;
///口腔情况
@property(nonatomic,strong)NSString * mouth;

///排列是否整齐
@property(nonatomic,strong)NSString * neat;


@end

@interface BaseQuestionnaireViewController : BaseViewController

@property(nonatomic,copy)NSString * titleText;

@property(nonatomic,copy)NSString * brieText;

@property(nonatomic,copy)NSString * nextButtonTitle;


@property(nonatomic,strong,readonly)UILabel * titleLabel;
@property(nonatomic,strong,readonly)UILabel * briefLabel;
@property(nonatomic,strong,readonly)UIButton * backButton;
@property(nonatomic,strong,readonly)GradientButton * nextButton;


@property(nonatomic,strong)NSDictionary * params;


@property(nonatomic,strong)RACSignal * serverSignal;
-(void)setUp;

+(instancetype)instance;
@end
