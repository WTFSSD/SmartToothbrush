//
//  BrushSchemeView.h
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/15.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrushSchemeView : UIView

@property(nonatomic,copy)void (^selectTime)(NSInteger section);
@property(nonatomic,copy)void (^switchClick)(NSInteger value);
@property(nonatomic,copy)void (^sliderChange)(NSInteger value);

+ (BrushSchemeView*)view;
- (void)configSchemeView:(NSInteger)section data:(NSDictionary*)data;

@end
