//
//  MineCell.m
//  Base
//
//  Created by 谢河源 on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "MineCell.h"

@interface MineCell (){
    id p ;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgV;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgVTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblHeight;

@end

@implementation MineCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imgVTop.constant = kScale_height*12;
    self.lblHeight.constant = kScale_height*32;
}

+ (MineCell *)cell {
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"MineUseView" owner:nil options:nil];
    MineCell* view = [nibViews objectAtIndex:1];
    return view;
}

-(void)configCell:(NSDictionary *)data {
    p = data;
    NSString* image = data[@"img"];
    NSString* title = data[@"title"];
    NSString* url = data[@"url"];
    NSDictionary * params = data[@"params"];
  
    self.imgV.image = ImageNamed(image);
    self.titleLbl.text = title;
}

@end
