//
//  BrushSchemeView.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/15.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BrushSchemeView.h"

@interface BrushSchemeView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UIView *timeView;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UISwitch *switchBtn;


@end

@implementation BrushSchemeView

+ (BrushSchemeView *)view {
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"MineUseView" owner:nil options:nil];
    BrushSchemeView* view = [nibViews objectAtIndex:3];
    return view;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.slider setThumbImage:ImageNamed(@"round_blue") forState:UIControlStateNormal];
    self.slider.minimumValue = 1;
    self.slider.maximumValue = 3;
    self.slider.continuous = NO;
}

- (void)configSchemeView:(NSInteger)section data:(NSDictionary*)data {
    NSArray* titles = @[@"清洁模式",@"美白模式",@"敏感模式"];
    self.titleLbl.text = titles[section];
    NSInteger hours = [data[@"hours"] integerValue];
    NSInteger seconds = hours % 60;
    NSInteger minutes = (hours / 60) % 60;
    self.timeLbl.text = [NSString stringWithFormat:@"%ld分%ld秒",minutes,seconds];
    [self.switchBtn setOn:[data[@"fit_status"] integerValue] == 1];
    [self.timeView bk_whenTapped:^{
        if (self.selectTime) {
            self.selectTime(section);
        }
    }];
    [[self.switchBtn rac_signalForControlEvents:UIControlEventValueChanged] subscribeNext:^(id sender) {
        UISwitch* switchBtn = (UISwitch*)sender;
        if (self.switchClick) {
            self.switchClick(switchBtn.isOn ? 1 : 0);
        }
    }];
    [[self.slider rac_signalForControlEvents:UIControlEventValueChanged] subscribeNext:^(id sender) {
        UISlider* slider = sender;
        NSUInteger index = (NSUInteger)(slider.value+0.5);
        [self.slider setValue:index animated:NO];
        if (self.sliderChange) {
            self.sliderChange(index);
        }
    }];

}

@end
