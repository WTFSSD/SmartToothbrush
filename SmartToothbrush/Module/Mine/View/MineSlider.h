//
//  MineSlider.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/13.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineSlider : UIControl
@property(nonatomic,assign)CGFloat minValue;
@property(nonatomic,assign)CGFloat maxValue;
@property(nonatomic,copy)NSString * leftSideText;
@property(nonatomic,copy)NSString * rightSideText;

@property(nonatomic,assign,readonly)CGFloat currentVaue;


+(instancetype)insatance;
@end
