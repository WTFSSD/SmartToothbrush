//
//  CommonHeadView.m
//  Base
//
//  Created by 谢河源 on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "CommonHeadView.h"

@interface CommonHeadView ()

@property (weak, nonatomic) IBOutlet UIView *topGrayView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *grayViewHeight;
@property (weak, nonatomic) IBOutlet UIView *botLine;

@end

@implementation CommonHeadView

+ (CommonHeadView *)headerView:(NSInteger)sction {
    NSArray* nibs = [[NSBundle mainBundle] loadNibNamed:@"MineUseView" owner:nil options:nil];
    CommonHeadView* view = [nibs objectAtIndex:2];
    if (sction == 0) {
        view.topGrayView.hidden = YES;
        view.grayViewHeight.constant = 0;
        view.frame = CGRectMake(0, 0, kScreen_Width, 42);
    }else{
        view.topGrayView.hidden = NO;
        view.grayViewHeight.constant = 10;
        view.frame = CGRectMake(0, 0, kScreen_Width, 52);
    }
    return view;
}

- (void)configHeaderView:(NSString *)title {
    UILabel* titleLbl = [self viewWithTag:1000];
    titleLbl.text = title;
}

@end
