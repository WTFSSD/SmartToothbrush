//
//  BrushingSettingView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/14.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BrushingSettingView.h"

#import "ContainerView.h"
#import "MineSlider.h"
#import "DateUnits.h"

#import "PersonalMadeController.h"
@interface BrushingSettingView()<UIPickerViewDelegate,UIPickerViewDataSource>{
    ContainerView * container;
    MineSlider * slider;
    UILabel * titleLabel;
    
    
    NSLayoutConstraint * icon1Constraint;
    
    NSLayoutConstraint * sliderConstraintL;
     NSLayoutConstraint * sliderConstraintT;
    UIImageView * icon1;
    
    
    
    UIView * popUpView;
    
    
    UIPickerView * timePickerView;
    
    
    NSArray * timeDataSource;
    
    
    


    
}
@end

@implementation BrushingSettingView

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}
+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self= [super initWithFrame:frame]){
        [self setUp];
    }
    return self;
}
-(void)setUp{
   self.selectedTime = BrushTimeNone;
    timeDataSource = @[
                       @{@"time":@(60),@"text":@"1分00秒"},
                        @{@"time":@(105),@"text":@"1分45秒"},
                        @{@"time":@(120),@"text":@"2分00秒"},
                        @{@"time":@(150),@"text":@"2分30秒"},
                       @{@"time":@(180),@"text":@"3分00秒"},
                       ];
    container = [ContainerView instance];
    [self addSubview:container];
    [container autoPinEdgesToSuperviewEdges];
    
    
    titleLabel =[UILabel makeWith:^(UILabel *label) {
        label.text = @"刷牙计时";
        label.textColor = UIColorFromHEX(0x4A5A6A);
        label.font = [UIFont systemFontOfSize:14];
    }];
    [self addSubview:titleLabel];
    [titleLabel autoAlignAxis:ALAxisVertical toSameAxisOfView:self];
    [titleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:zoom(17)];
    
    RACSignal * s =  [[self rac_valuesForKeyPath:@"title" observer:self] map:^id(id value) {
        if(!value) return @(YES);
        if([value length]<=0) return @(YES);
        return @(NO);
    }];
    [s subscribeNext:^(id x) {
        BOOL hiddel = [x boolValue];
         [icon1Constraint autoRemove];
        if(hiddel){
            icon1Constraint =  [icon1 autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:zoom(27)];
        }else{
            icon1Constraint =  [icon1 autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:titleLabel withOffset:zoom(10)];
        }
    }];
    
    
    RAC(titleLabel,text) = [self rac_valuesForKeyPath:@"title" observer:self];
    RAC(titleLabel,hidden) = s;
    
    icon1 = [[UIImageView alloc] initWithImage:ImageNamed(@"mode_time.png")];
    [self addSubview:icon1];
    
    UIImageView * icon1_1 = [[UIImageView alloc] initWithImage:ImageNamed(@"arrow_right.png")];
    [self addSubview:icon1_1];
    
    UILabel * label1 = [UILabel makeWith:^(UILabel *label) {
        label.text = @"刷牙计时";
        label.textColor = UIColorFromHEX(0x4A5A6A);
        label.font = [UIFont systemFontOfSize:14];
    }];
    [self addSubview:label1];
    UILabel * label1_1 = [UILabel makeWith:^(UILabel *label) {
        NSString *s = nil;
//        [DateUnits shareUnits].formatData(Today,@"mm分ss秒",&s);
        label.text = s;
        label.textColor = UIColorFromHEX(0x4A5A6A);
        label.font = [UIFont systemFontOfSize:14];
    }];
    [self addSubview:label1_1];
    
    RAC(label1_1,text) = [[self rac_valuesForKeyPath:@"selectedTime" observer:self] map:^id(id value) {
        BrushTime m = [value integerValue];
        if(m == BrushTimeNone) return nil;
        switch (m) {
            case BrushTime1:return @"1分00秒";
            case BrushTime2:return @"1分45秒";
            case BrushTime3:return @"2分00秒";
            case BrushTime4:return @"2分30秒";
            case BrushTime5:return @"3分00秒";
                
            default:return nil;
        }
        return nil;
    }];
    
    UIImageView * icon2 = [[UIImageView alloc] initWithImage:ImageNamed(@"mode_frequency.png")];
    [self addSubview:icon2];
    
  
    
    UILabel * label2 = [UILabel makeWith:^(UILabel *label) {
        label.text = @"刷牙频率";
        label.textColor = UIColorFromHEX(0x4A5A6A);
        label.font = [UIFont systemFontOfSize:14];
    }];
    
    [self addSubview:label2];
    
    slider = [MineSlider insatance];
    [self addSubview:slider];
    
    UIImageView * icon3 = [[UIImageView alloc] initWithImage:ImageNamed(@"mode_drop.png")];
    [self addSubview:icon3];
    
   
    UILabel * label3 = [UILabel makeWith:^(UILabel *label) {
        label.text = @"防溅按钮";
        label.textColor = UIColorFromHEX(0x4A5A6A);
        label.font = [UIFont systemFontOfSize:14];
    }];
    [self addSubview:label3];
    UISwitch * sw = [[UISwitch alloc] initWithFrame:CGRectZero];
    [self addSubview:sw];
    
    
    
    
    
   icon1Constraint =  [icon1 autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:zoom(27)];
    [icon1 autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:zoom(17)];
    
    [icon1_1 autoAlignAxis:ALAxisHorizontal toSameAxisOfView:icon1];
    [icon1_1 autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:self withOffset:-zoom(27)];
    
    [label1 autoAlignAxis:ALAxisHorizontal toSameAxisOfView:icon1];
    [label1 autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:icon1 withOffset:zoom(11)];
    
    [label1_1 autoAlignAxis:ALAxisHorizontal toSameAxisOfView:icon1];
    [label1_1 autoPinEdge:ALEdgeRight toEdge:ALEdgeLeft ofView:icon1_1 withOffset:-zoom(11)];
    
    [icon2 autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:icon1];
    [icon2 autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:icon1 withOffset:zoom(40)];

    [label2 autoAlignAxis:ALAxisHorizontal toSameAxisOfView:icon2];
    [label2 autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:icon2 withOffset:zoom(11)];
    
    
   sliderConstraintL =  [slider autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:zoom(30)];
    
    sliderConstraintT=[slider autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:icon2 withOffset:zoom(20)];
    
    [slider autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:label1_1];
    [slider autoSetDimension:ALDimensionHeight toSize:zoom(40)];
    
    
    [icon3 autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self withOffset:-zoom(27)];
    [icon3 autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:zoom(17)];
    
    [label3 autoAlignAxis:ALAxisHorizontal toSameAxisOfView:icon3];
    [label3 autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:icon3 withOffset:zoom(11)];
    [sw autoAlignAxis:ALAxisHorizontal toSameAxisOfView:icon3];
    [sw autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self withOffset:-zoom(27)];

    
    [[self rac_valuesForKeyPath:@"sliderInRight" observer:self] subscribeNext:^(id x) {
        BOOL sliderInRight = [x boolValue];
        [sliderConstraintL autoRemove];
        [sliderConstraintT autoRemove];
        if(sliderInRight){
            sliderConstraintL =  [slider autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:label2 withOffset:zoom(15)];
            
            sliderConstraintT=[slider autoAlignAxis:ALAxisHorizontal toSameAxisOfView:label2];
        }else{
            sliderConstraintL =  [slider autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:zoom(30)];
            
            sliderConstraintT=[slider autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:icon2 withOffset:zoom(20)];
        }
    }];
    
    
    [container addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)]];
}


-(void)onTap:(UITapGestureRecognizer*)ges{

    CGPoint p = [ges locationInView:container];
    if(p.y>0 && p.y<50){
       
        [self showTimePopUp];
        
    }else if(p.y>50 && p.y<100){
        
    }else if(p.y>100 && p.y<150){
        
    }else if(p.y>150 && p.y<200){
        
    }
}
-(void)configViewWithData:(NSDictionary *)data {
    
}



-(void)showTimePopUp{
    UIWindow * w = ((AppDelegate*)[UIApplication sharedApplication].delegate).window;
    
    popUpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, kScreen_Height)];
    [w addSubview:popUpView];
    popUpView.backgroundColor = [UIColor clearColor];
    
    
    timePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, kScreen_Height, kScreen_Width, zoom(200))];
    timePickerView.showsSelectionIndicator = YES;
    timePickerView.backgroundColor = [UIColor whiteColor];
    [popUpView addSubview:timePickerView];
    
    timePickerView.delegate = self;
    timePickerView.dataSource = self;
    
    
    [UIView animateWithDuration:0.3 animations:^{
        popUpView.backgroundColor = RGB(0, 0, 0, 0.4);
        timePickerView.transform = CGAffineTransformTranslate(timePickerView.transform, 0, -zoom(200));
    }];
    
    
    
    UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, kScreen_Height-zoom(200))];
    [popUpView addSubview:btn];
    
    [btn addTarget:self action:@selector(hideTimePopUp) forControlEvents:UIControlEventTouchUpInside];
}

-(void)hideTimePopUp{
    if(!popUpView || !timePickerView ) return;
        [UIView animateWithDuration:0.3 animations:^{
            timePickerView.transform = CGAffineTransformTranslate(timePickerView.transform, 0, zoom(200));
            popUpView.backgroundColor = [UIColor clearColor];
        } completion:^(BOOL finished) {
            [timePickerView removeFromSuperview];
            [popUpView removeFromSuperview];
            timePickerView = nil;
            popUpView = nil;
        }];
    
}



-(void)layoutSubviews{
    [super layoutSubviews];
    [container drawRoundRect:5 rt:5 lb:5 rb:5];
}

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return  1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return timeDataSource.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return timeDataSource[row][@"text"];
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    switch (row) {
        case 0:
            self.selectedTime = BrushTime1;
            break;
        case 1:
            self.selectedTime = BrushTime2;
            break;
        case 2:
            self.selectedTime = BrushTime3;
            break;
        case 3:
            self.selectedTime = BrushTime4;
            break;
        case 4:
            self.selectedTime = BrushTime5;
            break;
            
        default:self.selectedTime = BrushTimeNone;
            break;
    }
}


@end
