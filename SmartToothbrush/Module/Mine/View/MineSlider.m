//
//  MineSlider.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/13.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "MineSlider.h"

@interface MineSlider(){
    UILabel * leftSide;
    UILabel * rightSide;
    UISlider * slider;
}

@property(nonatomic,assign)CGFloat currentVaue;
@end


@implementation MineSlider

-(void)awakeFromNib{
    [super awakeFromNib];
}

+(instancetype)insatance{
    return [[self alloc] initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

-(void)setUp{
    leftSide = [UILabel makeWith:^(UILabel *label) {
        label.textColor = UIColorFromHEX(0x4A5A6A);
        label.font = [UIFont systemFontOfSize:12];
        label.text = @"弱";
    }];
    [self addSubview:leftSide];
    rightSide = [UILabel makeWith:^(UILabel *label) {
        label.textColor = UIColorFromHEX(0x4A5A6A);
        label.font = [UIFont systemFontOfSize:12];
        label.text = @"强";
    }];
    [self addSubview:rightSide];
    
    slider = [[UISlider alloc] initWithFrame:CGRectZero];
    [self addSubview:slider];
    [slider setThumbImage:ImageNamed(@"round_blue.png") forState:UIControlStateNormal];
    [leftSide autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self withOffset:-zoom(2)];
    [rightSide autoAlignAxis:ALAxisHorizontal toSameAxisOfView:leftSide withOffset:0];
    [leftSide autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:5];
    [rightSide autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self withOffset:-5];
    
    [slider autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self withOffset:-4];
    [slider autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:1];
    [slider autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self withOffset:-1];
    [slider autoSetDimension:ALDimensionHeight toSize:15];

    
//    RAC(slider,maximumValue) = [self rac_valuesForKeyPath:@"maxValue" observer:self];

//    RAC(slider,minimumValue) = [self rac_valuesForKeyPath:@"minValue" observer:self];
    
    RAC(self,currentVaue) = [slider rac_valuesForKeyPath:@"value" observer:self];
}


-(UIImage*)getThiImage{
    UIGraphicsBeginImageContext(CGSizeMake(zoom(18), zoom(18)));//size为CGSize类型，即你所需要的图片尺寸
    
    
    CGContextRef contex = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(contex, [UIColor whiteColor].CGColor);
    
    CGRect bgRect = CGRectMake(-1, -1 , zoom(19.5), zoom(19.5));
    
    CGContextAddRect(contex, bgRect);
    CGContextDrawPath(contex, kCGPathFillStroke);
    CGContextSetStrokeColorWithColor(contex, UIColorFromHEX(0x12C6FC).CGColor);
    CGContextSetLineWidth(contex, 2);
    CGContextAddArc(contex, zoom(9), zoom(9), zoom(9-2), 0, M_PI * 2, YES);
   
    CGContextStrokePath(contex);

    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;

}

@end
