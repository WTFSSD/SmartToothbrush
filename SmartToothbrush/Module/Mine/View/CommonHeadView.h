//
//  CommonHeadView.h
//  Base
//
//  Created by 谢河源 on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 tableView section 通用headerView
 */
@interface CommonHeadView : UIView

+ (CommonHeadView*)headerView:(NSInteger)sction;

- (void)configHeaderView:(NSString*)title;

@end
