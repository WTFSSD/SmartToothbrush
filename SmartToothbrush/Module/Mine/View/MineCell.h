//
//  MineCell.h
//  Base
//
//  Created by 谢河源 on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineCell : UIView

+(MineCell*)cell;
-(void)configCell:(NSDictionary*)data;

@end
