//
//  MineTopView.m
//  Base
//
//  Created by 谢河源 on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "MineTopView.h"

@interface MineTopView ()

@property (weak, nonatomic) IBOutlet UIImageView *imgV;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;

@end

@implementation MineTopView

+ (MineTopView *)view {
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"MineUseView" owner:nil options:nil];
    MineTopView* view = [nibViews objectAtIndex:0];
    [HYTool configViewLayer:view.imgV size:29];
    [HYTool configViewLayerFrame:view.imgV WithColor:[UIColor whiteColor]];
    return view;
}

- (void)configTopView {
    [self.nameLbl bk_whenTapped:^{
        APPROUTE(kLoginViewController);
    }];
    if (APIHELPER.userInfo.isLogin == NO) {
        self.nameLbl.userInteractionEnabled = YES;
        self.nameLbl.text = @"hi，等你好久了，去登录";
        self.imgV.image = ImageNamed(@"photo_mine");
    }else{
        self.nameLbl.userInteractionEnabled = NO;
        self.nameLbl.text = APIHELPER.userInfo.nickName.isEmpty ? APIHELPER.userInfo.username : APIHELPER.userInfo.nickName;
        [self.imgV sd_setImageWithURL:[NSURL URLWithString:APIHELPER.userInfo.avatar] placeholderImage:ImageNamed(@"photo_mine")];
        @weakify(self);
        [self.imgV bk_whenTapped:^{
            @strongify(self);
            if (self.selectImg) {
                self.selectImg();
            }
        }];
    }
}

@end
