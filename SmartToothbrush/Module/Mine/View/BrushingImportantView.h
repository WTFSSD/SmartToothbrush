//
//  BrushingImportantView.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/14.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrushingImportantView : UIView
+(instancetype)instance;
-(void)configViewWithData:(NSString*)data;
@end
