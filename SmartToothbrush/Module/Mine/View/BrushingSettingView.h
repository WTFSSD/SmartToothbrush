//
//  BrushingSettingView.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/14.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "ContainerView.h"


@class PersonalMadeModel;
@interface BrushingSettingView : UIView


@property(nonatomic,copy)NSString * title;

@property(nonatomic,assign)BOOL sliderInRight;

@property(nonatomic,strong)PersonalMadeModel * mode;

@property(nonatomic,assign)BrushTime   selectedTime;

+(instancetype)instance;
-(void)configViewWithData:(NSDictionary*)data;

@end
