//
//  BrushingImportantView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/5/14.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BrushingImportantView.h"
#import "ContainerView.h"
@interface BrushingImportantView(){
    ContainerView *container;
}
@end;

@implementation BrushingImportantView

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        [self setUp];
    }
    return self;
}

+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}

-(void)setUp{
    container = [ContainerView instance];
    [self addSubview:container];
    [container autoPinEdgesToSuperviewEdges];
    
    
    UILabel * label1 = [UILabel makeWith:^(UILabel *label) {
        label.text = @"重点区域";
        label.textColor = UIColorFromHEX(0x4A5A6A);
        label.font = [UIFont systemFontOfSize:14];
    }];
    [self addSubview:label1];
    [label1 autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:zoom(27)];
    [label1 autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:zoom(17)];
}

-(void)configViewWithData:(NSString *)data {
    
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [container drawRoundRect:5 rt:5 lb:5 rb:5];
}
@end
