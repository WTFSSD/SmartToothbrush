//
//  MusicSelectController.m
//  Base
//
//  Created by 谢河源 on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "MusicSelectController.h"

@interface MusicSelectController ()

@property (nonatomic,strong)NSMutableArray* dataArray;

@end

@implementation MusicSelectController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBarBlue = NO;
    [self subviewStyle];
    [self fetchData];
   
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     [self fetchData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellId = @"musicCell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [UITableViewCell new];
        [HYTool configTableViewCellDefault:cell];
    }
    MPMediaItem* music = self.dataArray[indexPath.section];
    cell.textLabel.text = [music valueForProperty:MPMediaItemPropertyTitle];
    [cell addLine:(indexPath.section-1 == self.dataArray.count) leftOffSet:0 rightOffSet:0];
    return cell;
}

#pragma mark - tableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kScale_height * 71;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectMusic) {
        MPMediaItem* item = self.dataArray[indexPath.section];
        self.selectMusic(item);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - private methods
- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)subviewStyle {
    self.title = @"选择音乐";
    [self baseSetupTableView:UITableViewStylePlain InSets:UIEdgeInsetsZero];
    
}

- (void)fetchData {
    MPMediaQuery *query = [MPMediaQuery songsQuery];
    for (MPMediaItemCollection *conllection in query.collections) {
        for (MPMediaItem *item in conllection.items) {
            [self.dataArray addObject:item];
        }
    }
    [self.tableView reloadData];
}

@end
