//
//  UserInfoViewController.m
//  SmartToothbrush
//
//  Created by admin on 2018/7/6.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "UserInfoViewController.h"
#import "ValidateMethodsViewController.h"
@interface UserInfoViewController ()

@property(nonatomic,strong)NSArray* infos;
@property(nonatomic,strong)NSMutableDictionary* userInfo;

@end

@implementation UserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.infos = @[@[@"昵称",@"性别",@"年龄"],@[@"修改密码"]];
    [self subviewStyle];
    [self fetchData];
}

- (void)baseSetBackgroundImage {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableView dataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.infos.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray* arr = self.infos[section];
    return arr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellId = @"commonCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [HYTool configTableViewCellDefault:cell];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.textColor = [UIColor colorWithString:@"4a5a6a"];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        UILabel* detailLbl = [HYTool getLabelWithFrame:CGRectMake(kScreen_Width-220, 0, 180, 50) text:@"" fontSize:15 textColor:[UIColor colorWithString:@"4a5a6a"] textAlignment:NSTextAlignmentRight];
        detailLbl.tag = 1000;
        [cell.contentView addSubview:detailLbl];
    }
    UILabel* detailLbl = [cell.contentView viewWithTag:1000];
    cell.textLabel.text = self.infos[indexPath.section][indexPath.row];
    detailLbl.hidden = indexPath.section == 1;
    if (indexPath.row == 0) {
        detailLbl.text = [self.userInfo[@"nickname"] isEmpty] ? @"请设置昵称" : self.userInfo[@"nickname"];
    }else if (indexPath.row == 1) {
        detailLbl.text = [self.userInfo[@"sex"] integerValue] == 1 ? @"男" : @"女";
    }else{
        detailLbl.text = [self.userInfo[@"age"] integerValue] == 0 ? @"请设置年龄" : self.userInfo[@"age"];
    }
    if (indexPath.section != 1 && indexPath.row != 2) {
        [cell addLineLeftOffSet:0 rightOffSet:60];
    }
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 10)];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - tableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        //修改密码
        APPROUTE(@"ValidateMethodsViewController");
        return;
    }
    if (indexPath.row == 1) {
        [self chooseGender];
        return;
    }
    NSString* title = indexPath.row == 0 ? @"请输入昵称" : @"请输入年龄";
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = title;
        textField.keyboardType = indexPath.row == 0 ? UIKeyboardTypeDefault : UIKeyboardTypeNumberPad;
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        UITextField *tf = alert.textFields.firstObject;
        if ([tf.text isEmpty]) {
            [self showMessage:title];
            return ;
        }
        [alert dismissViewControllerAnimated:YES completion:nil];
        if (indexPath.row == 0) {
            [self.userInfo setValue:tf.text forKey:@"nickname"];
        }else{
            [self.userInfo setValue:tf.text forKey:@"age"];
        }
        [self.tableView reloadData];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - private methods
- (NSMutableDictionary *)userInfo {
    if (!_userInfo) {
        _userInfo = [NSMutableDictionary dictionaryWithDictionary:@{@"nickname":@"",@"sex":@1,@"age":@0}];
    }
    return _userInfo;
}

- (void)subviewStyle {
    self.title = @"个人资料";
    self.navigationBarBlue = NO;
    self.navigationBarTransparent = NO;
    self.view.backgroundColor = [UIColor colorWithString:@"f1f3f8"];
    [self baseSetupTableView:UITableViewStylePlain InSets:UIEdgeInsetsMake(0, 0, 175/2, 0)];
    self.tableView.scrollEnabled = NO;
    self.tableView.backgroundColor = [UIColor colorWithString:@"#f1f3f8"];
    UIButton* saveBtn = [HYTool getButtonWithFrame:CGRectMake((kScreen_Width-200)/2, kScreen_Height-150, 200, 78/2) title:@"保存" titleSize:15 titleColor:[UIColor whiteColor] backgroundColor:nil blockForClick:^(id sender) {
        //保存
        [self saveData];
    }];
    [HYTool configGradualBlueView:saveBtn];
    [HYTool configViewLayer:saveBtn size:39/2];
    [self.view addSubview:saveBtn];
}

- (void)chooseGender {
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"性别" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    @weakify(self);
    [alertC addAction:[UIAlertAction actionWithTitle:@"男" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [self.userInfo setValue:@1 forKey:@"sex"];
        [self.tableView reloadData];
    }]];
    [alertC addAction:[UIAlertAction actionWithTitle:@"女" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [self.userInfo setValue:@2 forKey:@"sex"];
        [self.tableView reloadData];
    }]];
    [alertC addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertC animated:YES completion:nil];
}

- (void)fetchData {
    [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_PERSONAL_INFOMARION) params:@{@"key":[UserInfoModel share].key}].then(^(id res){
        if (!res) { return;}
        self.userInfo = [res[@"data"] mutableCopy];
        [self.tableView reloadData];
    });
}

- (void)saveData {
    NSDictionary* params = @{
                             @"nickname":self.userInfo[@"nickname"],
                             @"sex":self.userInfo[@"sex"],
                             @"age":self.userInfo[@"age"]
                             };
    [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_CHANGE_PERSONAL_INFOMATION) params:params].then(^(id res){
        if (!res) { return;}
        UserInfoModel* model = [UserInfoModel share];
        model.username = self.userInfo[@"nickname"];
        [UserInfoModel updateWithData:[model yy_modelToJSONObject]];
        [self showMessage:@"修改成功" complete:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    });
}

@end
