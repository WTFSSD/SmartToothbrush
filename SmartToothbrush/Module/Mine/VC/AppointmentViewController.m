//
//  AppointmentViewController.m
//  SmartToothbrush
//
//  Created by admin on 2018/7/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "AppointmentViewController.h"

@interface AppointmentViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameTf;
@property (weak, nonatomic) IBOutlet UITextField *phoneTf;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *commitBtn;
@property (weak, nonatomic) IBOutlet UIView *imageBackView;
@property (strong,nonatomic) NSMutableArray* images;//晒图的数组
@property(strong,nonatomic)NSMutableArray* imageUrls;//晒图链接的数组

@end

@implementation AppointmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self subviewStyle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)baseSetBackgroundImage {
    
}

- (NSMutableArray *)images {
    if (!_images) {
        _images = [NSMutableArray array];
    }
    return _images;
}

- (NSMutableArray *)imageUrls {
    if (!_imageUrls) {
        _imageUrls = [NSMutableArray array];
    }
    return _imageUrls;
}

- (void)subviewStyle {
    self.navigationBarBlue = NO;
    self.navigationBarTransparent = NO;
    [HYTool configViewLayer:self.textView size:4];
    [HYTool configViewLayerFrame:self.textView WithColor:[UIColor colorWithString:@"dddddd"] borderWidth:0.5];
    
    
    UIColor *color = [UIColor colorWithString:@"BFCDDB"];
    self.nameTf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.nameTf.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    self.phoneTf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.phoneTf.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    
    UILabel *placeHolderLabel = [[UILabel alloc] init];
    placeHolderLabel.text = @"请告诉我们详细症状...";
    placeHolderLabel.numberOfLines = 0;
    placeHolderLabel.textColor = [UIColor lightGrayColor];
    [placeHolderLabel sizeToFit];
    [self.textView addSubview:placeHolderLabel];
    
    placeHolderLabel.font = [UIFont systemFontOfSize:16];
    placeHolderLabel.textColor = [UIColor colorWithString:@"BFCDDB"];
    [self.textView setValue:placeHolderLabel forKey:@"_placeholderLabel"];
    
    [HYTool configGradualBlueView:self.commitBtn];
    
    [self initChooseImageView];
}

- (void)initChooseImageView {
    for (UIView* subview in self.imageBackView.subviews) {
        [subview removeFromSuperview];
    }
    
    CGFloat width = 66;
    for (int i=0; i<self.images.count; i++) {
        UIView* view = [[UIView alloc] initWithFrame:CGRectMake(i*(width+10), 0, width, width)];
        UIImageView* imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 6, 60, 60)];
        imgV.image = self.images[i];
        UIImageView* deleteBtn = [[UIImageView alloc] initWithFrame:CGRectMake(width-17, 0, 17, 17)];
        deleteBtn.image = ImageNamed(@"ico_deletepic");
        deleteBtn.userInteractionEnabled = YES;
        [deleteBtn bk_whenTapped:^{
            //删除image
            [self.imageUrls removeObjectAtIndex:i];
            [self.images removeObjectAtIndex:i];
            [self initChooseImageView];
        }];
        [view addSubview:imgV];
        [view addSubview:deleteBtn];
        [self.imageBackView addSubview:view];
    }
    UIButton* btn = [HYTool getButtonWithFrame:CGRectMake((10+width)*self.images.count, 6, 60, 60) title:nil titleSize:0 titleColor:nil backgroundColor:nil blockForClick:^(id sender) {
        UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"晒图" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [alertC addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self showCamera];
        }]];
        [alertC addAction:[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self showPhotoLibrary];
        }]];
        [alertC addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertC animated:YES completion:nil];
    }];
    [btn setImage:ImageNamed(@"ico_addpic") forState:(UIControlStateNormal)];
    btn.hidden = self.imageUrls.count == 3;
    [self.imageBackView addSubview:btn];
}

- (IBAction)commit:(id)sender {
    //提交
    if ([self.nameTf.text isEmpty]) {
        [self showMessage:@"请填写患者姓名"];
        return;
    }
    if ([self.phoneTf.text isEmpty]) {
        [self showMessage:@"请填写患者联系方式"];
        return;
    }
    if ([self.textView.text isEmpty]) {
        [self showMessage:@"请填写您的意见"];
        return;
    }
    //提交预约
    [self showLoadingAnimation];
    NSDictionary* params = @{@"name":self.nameTf.text, @"mobile":self.phoneTf.text, @"symptom":self.textView.text,@"pictures":self.imageUrls};
    [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_APPOINTMENT) params:params].then(^(id res) {
        [self hideLoadingAnimation];
        if(!res) return;
        [self showMessage:@"预约成功" complete:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    });
}

#pragma mark - imagePicker
- (UIImagePickerController*)createImagePicker {
    UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.navigationBar.barTintColor = [UIColor hyBarTintColor];
    return imagePicker;
}
- (void)showCamera{
    UIImagePickerController* imagePicker = [self createImagePicker];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)showPhotoLibrary{
    UIImagePickerController * imagePicker = [self createImagePicker];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage * img = [info objectForKey:UIImagePickerControllerEditedImage];
    NSData *data = UIImageJPEGRepresentation(img, 1.0);
    
    [picker dismissViewControllerAnimated:YES completion:^{
        @weakify(self);
        [MBProgressHUD hy_showLoadingHUDAddedTo:self.view animated:YES];
        [APIHELPER uploadFileWithURL:@"Member/upload_picture" file:data progress:nil complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            @strongify(self);
            if (isSuccess) {
                [self.imageUrls addObject:responseObject[@"data"][@"url"]];
                [self.images addObject:img];
                [self initChooseImageView];
            }else{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if (error) {
                    [MBProgressHUD hy_showMessage:error.userInfo[NSLocalizedDescriptionKey] inView:self.view];
                }
            }
        }];
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
