//
//  UsingIntroductionController.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/6/4.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "UsingIntroductionController.h"

@interface UsingIntroductionController ()<UIWebViewDelegate>

@property (nonatomic,strong)UIWebView* webView;

@end

@implementation UsingIntroductionController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self subviewStyle];
    [self fetchData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)subviewStyle {
    self.title = @"使用说明";
    self.webView = [[UIWebView alloc] init];
    self.webView.delegate = self;
    self.webView.scrollView.showsVerticalScrollIndicator = NO;
    self.webView.scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.webView];
    [self.webView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
}

- (void)fetchData {
    [APIHELPER fetchUsingIntroductionComplete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
        if (isSuccess) {
            NSString* title = responseObject[@"data"][@"title"];
            NSString* htmlStr = responseObject[@"data"][@"content"];
            [self.webView loadHTMLString:htmlStr baseURL:nil];
        }else{
            [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }
    }];
}

@end
