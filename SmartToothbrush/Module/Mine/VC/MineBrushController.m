//
//  MineBrushController.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/13.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "MineBrushController.h"
#import "MineBrushCell.h"
#import "BrushModel.h"
@interface MineBrushController ()

@property(nonatomic,strong)NSDictionary* data;


@property(nonatomic,strong)NSMutableArray<BrushModel*>* brushs;
@end

@implementation MineBrushController


-(NSMutableArray<BrushModel *> *)brushs{
    if(!_brushs){
        _brushs = [NSMutableArray array];
    }
    return _brushs;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self subviewStyle];
    [self fetchData];
}


-(void)baseSetBackgroundImage{
    //取消父类设置de背景
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - tableView dataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.brushs.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* cellId = [MineBrushCell identify];
    MineBrushCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    [cell configMineBrushCell:self.brushs[indexPath.row]];
    return cell;
}


#pragma mark - tableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - private methods
- (void)subviewStyle {
    self.navigationBarTransparent = YES;
    [self baseSetupTableView:UITableViewStylePlain InSets:UIEdgeInsetsMake(0, 0, 54, 0)];
    self.tableView.estimatedRowHeight = 500;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView registerNib:[UINib nibWithNibName:[MineBrushCell identify] bundle:nil] forCellReuseIdentifier:[MineBrushCell identify]];
}

- (void)fetchData {
    [self showLoadingAnimation];
    
    
    
    
    [NetworkUtils requestWith:HTTPMethodGET url:REQUEST_URL(API_MEMBER_MY_TOOTHBRUSH) params:@{@"key":[UserInfoModel share].key}].then(^(id res){
        [self hideLoadingAnimation];
        if([res[@"code"] integerValue] == 1){
            [self.brushs removeAllObjects];
           NSArray * temp =  [res[@"data"] bk_map:^id(id obj) {
               BrushModel * model = [[BrushModel alloc] init];
               [model yy_modelSetWithJSON:obj];
               return model;
            }];
            [self.brushs addObjectsFromArray:temp];
            [self.tableView reloadData];
        }
    });
//    [APIHELPER mineBrushComplete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
//        [self hideLoadingAnimation];
//        if (isSuccess) {
//            self.data = responseObject[@"data"];
//            [self.tableView reloadData];
//        }else{
//            [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
//        }
//    }];
}

@end
