//
//  ValidateMethodsViewController.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/8/5.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "ValidateMethodsViewController.h"
#import "CheckCodeController.h"

@interface ValidateMethodsViewController ()
@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet UIView *emailView;

@end

@implementation ValidateMethodsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self subviewStyle];
    [self subviewBind];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)baseSetBackgroundImage {
    
}

- (void)subviewStyle {
    self.view.backgroundColor = [UIColor colorWithString:@"f1f3f8"];
    self.navigationBarBlue = NO;
    self.navigationBarTransparent = NO;
}

- (void)subviewBind {
    
    [self.phoneView bk_whenTapped:^{
        CheckCodeController * vc = [[UIStoryboard storyboardWithName:@"Login" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"CheckCodeController"];
        
        [self.navigationController pushViewController:vc animated:YES];
        vc.contentType = TypePhone;
        vc.successCallBack = ^{
             APPROUTE(kChangePasswordController);
        };
        
     
    }];
    [self.emailView bk_whenTapped:^{
      
        
        CheckCodeController * vc = [[UIStoryboard storyboardWithName:@"Login" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"CheckCodeController"];
           vc.contentType = TypeEmail;
        vc.successCallBack = ^{
             APPROUTE(kChangePasswordController);
        };
        [self.navigationController pushViewController:vc animated:YES];
    }];
}

@end
