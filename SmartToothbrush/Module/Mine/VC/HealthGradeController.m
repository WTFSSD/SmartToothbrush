//
//  HealthGradeController.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/8.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "HealthGradeController.h"
#import "NSString+Extension.h"
#import "UIButton+HYButtons.h"
#import "HealthSelectCell.h"

@interface HealthGradeController ()

@property(nonatomic,strong)NSMutableArray* infos;
@property(nonatomic,strong)NSMutableArray* selects;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end

@implementation HealthGradeController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self subviewStyle];
    [self dataInit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - tableView dataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.infos.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HealthSelectCell* cell = [tableView dequeueReusableCellWithIdentifier:[HealthSelectCell identify]];
    NSDictionary* data = self.infos[indexPath.section];
    [cell configSelectCell:data section:indexPath.section selects:self.selects];
    [cell setSelect:^(NSInteger index, NSInteger section) {
        //选择答案
        [self.selects replaceObjectAtIndex:section withObject:@(index)];
        [self.tableView reloadData];
    }];
    return cell;
}

#pragma mark - tableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 82;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - pirvate methods
- (NSArray *)infos {
    if (!_infos) {
        _infos = [NSMutableArray array];
    }
    return _infos;
}

- (NSMutableArray *)selects {
    if (!_selects) {
        _selects = [NSMutableArray array];
    }
    return _selects;
}

- (void)subviewStyle {
    self.navigationBarTransparent = YES;
    [self baseSetupTableView:UITableViewStylePlain InSets:UIEdgeInsetsMake(12, 12, 59, 12)];
    [self.tableView registerNib:[UINib nibWithNibName:[HealthSelectCell identify] bundle:nil] forCellReuseIdentifier:[HealthSelectCell identify]];
    [HYTool configViewLayer:self.tableView];
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self setTableHeaderView];
    [self setFooterView];
}

- (void)setTableHeaderView {
    UIView* headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width-24, 95)];
    UILabel* lbl = [HYTool getLabelWithFrame:CGRectZero text:@"" fontSize:13 textColor:[UIColor colorWithString:@"4a5a6a"] textAlignment:NSTextAlignmentCenter];
    lbl.numberOfLines = 0;
    NSString* str = @"我们会记录您的前三次刷牙数据，基于您的生活习惯以及您的刷牙习惯对您目前的牙齿健康等级进行A-E的评级，我们也会给出相关建议，请您刷牙三次后到健康等级这里查看评级结果。";
    NSAttributedString* attr = [str attributedStringWithStrings:@[@"前三次",@"A-E",@"三次后"] andWithColors:@[[UIColor hyBlueColor],[UIColor hyBlueColor],[UIColor hyBlueColor]]];
    NSMutableAttributedString* mAttr = [[NSMutableAttributedString alloc] initWithAttributedString: attr];
    NSMutableParagraphStyle* style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = 2;
    [mAttr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, attr.length)];
    lbl.attributedText = mAttr;
    [headView addSubview:lbl];
    [lbl autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(16, 20, 4, 17)];
    self.tableView.tableHeaderView = headView;
}

- (void)setFooterView {
    [HYTool configGradualBlueView:self.submitBtn];
    [HYTool configViewLayer:self.submitBtn size:20];
    [self.submitBtn bk_whenTapped:^{
        NSMutableDictionary* dict = [NSMutableDictionary dictionary];
        for (int i=0; i<self.selects.count; i++) {
            NSInteger selectIndex = [self.selects[i] integerValue];
            if (selectIndex == 100) {
                [self showMessage:@"请答完所有问题后再进行提交"];
                return ;
            }
            NSString* questionId = self.infos[i][@"id"];
            NSString* option = selectIndex == 0 ? @"A" : (selectIndex == 1 ? @"B" : @"C");
            [dict setValue:option forKey:questionId];
        }
        [APIHELPER healthSubmitAnswer:dict complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            if (isSuccess) {
                //TODO:提交,跳到结果页或者详情页
                APPROUTE(kHealthResultController);
            }else{
                [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
            }
        }];
    }];
}

- (void)dataInit {
    [APIHELPER healthQuestionComplete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
        if (isSuccess) {
            [self.infos addObjectsFromArray:responseObject[@"data"]];
            for (id obj in self.infos) {
                [self.selects addObject:@100];
            }
            [self.tableView reloadData];
        }else{
            [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }
    }];
    [self.tableView reloadData];
}
@end
