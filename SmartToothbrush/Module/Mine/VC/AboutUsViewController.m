//
//  AboutUsViewController.m
//  Base
//
//  Created by admin on 2017/4/13.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "AboutUsViewController.h"

@interface AboutUsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *versionLbl;
@property(nonatomic,strong)NSArray* infos;

@end

@implementation AboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self subviewStyle];
    [self dataInit];
}

-(void)baseSetBackgroundImage {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableView dataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.infos.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellId = @"cellId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [HYTool configTableViewCellDefault:cell];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.textLabel.textColor = [UIColor colorWithString:@"4a5a6a"];
    }
    NSDictionary* data = self.infos[indexPath.section];
    cell.textLabel.text = data[@"title"];
    [cell addLine:indexPath.section == self.infos.count-1 leftOffSet:0 rightOffSet:60];
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView* header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, zoomHeight(10))];
    header.backgroundColor = tableViewdefaultBackgroundColor;
    return section == 0 ? header : nil;
}

#pragma mark - tableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return zoomHeight(50);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return section == 0 ? zoomHeight(10) : 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary* data = self.infos[indexPath.section];
    NSString* url = data[@"url"];
    APPROUTE(url);
}

#pragma mark - private methods
-(void)subviewStyle {
    self.navigationBarBlue = NO;
    self.navigationBarTransparent = NO;
    [self baseSetupTableView:UITableViewStylePlain InSets:UIEdgeInsetsMake(0, 0, 78, 0)];
    self.view.backgroundColor = tableViewdefaultBackgroundColor;
    self.versionLbl.text = [NSString stringWithFormat:@"版本: %@",APP_VERSION];
}

-(void)dataInit {
    self.infos = @[
                   @{@"title":@"意见反馈",@"url":kFeedbackController},
                   @{@"title":@"联系我们",@"url":kContactViewController},
                   ];
}

@end
