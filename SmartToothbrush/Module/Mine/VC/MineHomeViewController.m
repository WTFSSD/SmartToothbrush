//
//  MineHomeViewController.m
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "MineHomeViewController.h"
#import "MineTopView.h"
#import "MineCell.h"
#import "ConectStep1ViewController.h"

#import "Questionnaire1ViewController.h"

#import "WKWebViewController.h"
@interface MineHomeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic,strong)NSArray* infos;
@property(nonatomic,strong)UICollectionView* collectionView;
@property(nonatomic,strong)UIImagePickerController* imagePicker;

@property(nonatomic,assign)NSInteger gradeStatus;
@property(nonatomic,strong)NSString* grade;
@property(nonatomic,strong)NSString* advise;

@end

@implementation MineHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBarHidden = YES;
    [self subviewStyle];
    [self fetchGradeStatus];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self dataInit];
}

-(void)baseSetBackgroundImage{
    //
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private methods
- (void)subviewStyle {
    if (@available(iOS 11.0, *)) {
        self.extendedLayoutIncludesOpaqueBars = YES;
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.itemSize = CGSizeMake(kScreen_Width/3, kScale_height*90);
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 10, 0);
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    self.collectionView.backgroundColor = [UIColor colorWithString:@"f1f3f8"];
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.view addSubview:self.collectionView];
    if (@available(iOS 11.0, *)) {
        [self.collectionView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(-20, 0, 0, 0)];
    }else{
        [self.collectionView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellId"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footerView"];
}

- (void)fetchGradeStatus {
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    params[@"key"] = [UserInfoModel share].key;
    [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_GRADE_STATE) params:params].then(^(id res){
        
        NSLog(@"fetchGradeStatus----->\n%@",res);
        if ([res[@"code"] integerValue] == 1) {
            NSDictionary* data = res[@"data"];
            self.gradeStatus = [data[@"status"] integerValue];
            if ([data.allKeys containsObject:@"grade"]) {
                self.grade = data[@"grade"];
                self.advise = data[@"advise"];
            }
        }
    });
}

- (void)dataInit {
    self.infos = @[
                   @[
                       @{@"img":@"mine_toothbrush",@"title":@"我的牙刷",@"url":kMineBrushController},
                       @{@"img":@"mine_linkbrush",@"title":@"连接牙刷",@"url":kConectToothBrush},
                       @{@"img":@"mine_programme",@"title":@"刷牙方案",@"url":kBrushSchemeController},
                       @{@"img":@"mine_customized",@"title":@"私人定制",@"url":kPersonalMadeController},
                       @{@"img":@"mine_user",@"title":@"用户管理",@"url":kUserSettlementController},
                       @{@"img":@"mine_message",@"title":@"历史消息",@"url":kMessageListController},
                       @{@"img":@"mine_voice",@"title":@"声音设置",@"url":kVoiceSetController},
                       @{@"img":@"mine_grade",@"title":@"健康等级",@"url":kHealthGradeController},

                       @{@"img":@"mine_data",@"title":@"个人资料",@"url":kUserInfoViewController}
                       ],
                   @[
                       @{@"img":@"mine_problem",@"title":@"常见问题",@"url":kCommonProblemsController},
                       @{@"img":@"mine_instructions",@"title":@"使用说明",@"url":kUsingIntroductionController},
                       @{@"img":@"mine_mall",@"title":@"购物商城",@"url":kWKWebViewController},
                       @{@"img":@"mine_appointment",@"title":@"牙医预约",@"url":kAppointmentController},
                       @{@"img":@"mine_aboutus",@"title":@"关于我们",@"url":kAboutUsViewController},
                       @{@"img":@"",@"title":@"",@"url":@""}
                       ]
                   ];
    [self.collectionView reloadData];
}


- (void)selectImage {
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"设置头像" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    @weakify(self);
    [alertC addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [self showCamera];
    }]];
    [alertC addAction:[UIAlertAction actionWithTitle:@"照片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [self showPhotoLibrary];
    }]];
    [alertC addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertC animated:YES completion:nil];
}

#pragma mark - UICollection delegate && datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.infos.count;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSArray* arr = self.infos[section];
    return arr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellId" forIndexPath:indexPath];
    if (![cell.contentView viewWithTag:1000]) {
        MineCell* view = [MineCell cell];
        view.tag = 1000;
        [cell.contentView addSubview:view];
        [view autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
    }
    MineCell* view = [cell.contentView viewWithTag:1000];
    NSDictionary* data = self.infos[indexPath.section][indexPath.row];
    [view configCell:data];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView* headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView" forIndexPath:indexPath];
        headView.frame = CGRectMake(0, 0, kScreen_Width, 145);
        if (![headView viewWithTag:1000]) {
            MineTopView* view = [MineTopView view];
            view.tag = 1000;
            [headView addSubview:view];
            [view autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
        }
        MineTopView* view = [headView viewWithTag:1000];
        [view configTopView];
        @weakify(self);
        [view setSelectImg:^{
//            @strongify(self);
//            [self selectImage];
            APPROUTE(kUserInfoViewController);
        }];
        return headView;
    }
    UICollectionReusableView* footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footerView" forIndexPath:indexPath];
    footerView.backgroundColor = [UIColor hyDefaultBackColor];
    if (![footerView viewWithTag:1000]) {
        UIButton* logoutBtn = [HYTool getButtonWithFrame:CGRectMake((kScreen_Width-zoom(200))/2, zoomHeight(37/2)-10, zoom(200), zoomHeight(40)) title:@"退出登录" titleSize:16 titleColor:[UIColor whiteColor] backgroundColor:[UIColor colorWithString:@"7f95ab"] blockForClick:^(id sender) {
            //退出登录
            [APIHELPER logout];
            [UserInfoModel logout];
            [self.collectionView reloadData];
             AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [delegate configLogin];
        }];
        logoutBtn.tag = 1000;
        [HYTool configViewLayer:logoutBtn size:zoomHeight(20)];
        [footerView addSubview:logoutBtn];
    }
    return (indexPath.section == self.infos.count-1 && [UserInfoModel share].isLogin) ? footerView : nil;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return CGSizeMake(kScreen_Width, 145);
    }
    return CGSizeMake(0, 0);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return (section == self.infos.count-1 && [UserInfoModel share].isLogin) ? CGSizeMake(zoom(200), zoomHeight(77)-10) : CGSizeMake(0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary* data = self.infos[indexPath.section][indexPath.row];
    NSString* url = data[@"url"];
    if([url isEqualToString:kWKWebViewController]){
        WKWebViewController * vc = [[WKWebViewController alloc] init];
        vc.url = @"https://shop428549885.taobao.com/";
        vc.autoLoad = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
   else if([url isEqualToString:kConectToothBrush]){
    
        ConectStep1ViewController * cvc = [ConectStep1ViewController instance];
        cvc.params = @{@"from":self};
    
        [self.navigationController presentViewController:cvc animated:YES completion:nil];
    
    }else if ([url isEqualToString:kHealthGradeController]){
        if (self.gradeStatus == 0) {
            APPROUTE(kHealthGradeController);
        }else if (self.gradeStatus == 1) {
            APPROUTE(kHealthResultController);
        }else{
            Questionnaire1ViewController * vc = [Questionnaire1ViewController instance];
            vc.params = @{@"from":self};
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else{
         APPROUTE(url);
    }
   
}

#pragma mark - iamge picker
- (UIImagePickerController*)createImagePicker {
    if (!_imagePicker) {
        _imagePicker = [[UIImagePickerController alloc] init];
        _imagePicker.navigationBar.barTintColor = [UIColor hyBlueTextColor];
    }
    return _imagePicker;
}

- (void)showCamera{
    UIImagePickerController* imagePicker = [self createImagePicker];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)showPhotoLibrary{
    UIImagePickerController * imagePicker = [self createImagePicker];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage * img = [info objectForKey:UIImagePickerControllerEditedImage];
    NSData *data = UIImageJPEGRepresentation(img, 1.0);
    
    [picker dismissViewControllerAnimated:YES completion:^{
        @weakify(self);
        [MBProgressHUD hy_showLoadingHUDAddedTo:self.view animated:YES];
        [APIHELPER uploadFileWithURL:@"Member/modify_avatar" file:data progress:nil complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            @strongify(self);
            if (isSuccess) {
                //上传成功，替换用户头像
                UserInfoModel* model = APIHELPER.userInfo;
                model.avatar = responseObject[@"avatar"];
                NSDictionary* data = [model yy_modelToJSONObject];
                [UserInfoModel updateWithData:data];
                [self.collectionView reloadData];
            }else{
                if (error) {
                    [MBProgressHUD hy_showMessage:error.userInfo[NSLocalizedDescriptionKey] inView:self.view];
                }
            }
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
