//
//  UserSettlementController.m
//  Base
//
//  Created by 谢河源 on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "UserSettlementController.h"


#import "ConectStep1ViewController.h"

@interface UserSettlementController ()

@property (nonatomic,assign)NSInteger p;
@property (nonatomic,strong)NSMutableArray* dataArray;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;

@end

@implementation UserSettlementController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBarBlue = NO;
    self.p = 0;
    [self subviewStyle];
    [self headerViewInit];
    [self fetchData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellId = @"userCell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [UITableViewCell new];
        [HYTool configTableViewCellDefault:cell];
        UIButton* deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteBtn setImage:ImageNamed(@"ico_delete") forState:UIControlStateNormal];
        deleteBtn.tag = 1000;
        [cell.contentView addSubview:deleteBtn];
        [deleteBtn autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, kScreen_Width-50, 0, 0)];
    }
    UIButton* deleteBtn = [cell.contentView viewWithTag:1000];
    NSDictionary* data = self.dataArray[indexPath.section];
    [deleteBtn bk_whenTapped:^{
        //解绑牙刷
        NSDictionary* data = self.dataArray[indexPath.section];
        [self unbindBrush:data];
    }];
    
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:data[@"avatar"]] placeholderImage:ImageNamed(@"photo_mine_s")];
    cell.textLabel.text = data[@"nickname"];
    [cell addLine:(indexPath.section-1 == self.dataArray.count) leftOffSet:0 rightOffSet:0];
    return cell;
}

#pragma mark - tableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kScale_height * 71;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - private methods
- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)subviewStyle {
    [self baseSetupTableView:UITableViewStylePlain InSets:UIEdgeInsetsMake(0, 0, 70, 0)];
    [HYTool configViewLayer:self.addBtn size:20];
    [HYTool configGradualBlueView:self.addBtn];
    [self.addBtn bk_whenTapped:^{
        [self addBrush];
    }];
}

- (void)unbindBrush:(NSDictionary*)data {
    NSInteger Id = [data[@"id"] integerValue];
    [APIHELPER unBindBrushWithId:Id complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
        if (isSuccess) {
            [self.dataArray removeObject:data];
            [self.tableView reloadData];
        }else{
            [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }
    }];
}

-(void)addBrush {
    //TODO:添加牙刷
    ConectStep1ViewController * vc = [ConectStep1ViewController instance];
    vc.params = @{@"from":self};
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (void)fetchData {
    [self showLoadingAnimation];
    [APIHELPER mineBindBrushListWithP:self.p limit:10 complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
        [self hideLoadingAnimation];
        
        if (isSuccess) {
            NSArray* data = responseObject[@"data"];
            [self.dataArray addObjectsFromArray:data];
            [self.tableView reloadData];
            self.haveNext = data.count == 10;
            self.p += 1;
            if (self.haveNext) {
                [self appendFooterView];
            }else{
                [self removeFooterRefresh];
            }
        }else{
            [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }
    }];
}

-(void)headerViewInit {
    self.p = 0;
    @weakify(self);
    [self addHeaderRefresh:^{
        @strongify(self);
        [self showLoadingAnimation];
        [APIHELPER mineBindBrushListWithP:self.p limit:10 complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            [self hideLoadingAnimation];
            
            [self.dataArray removeAllObjects];
            [self.tableView reloadData];
            if (isSuccess) {
                NSArray* data = responseObject[@"data"];
                [self.dataArray addObjectsFromArray:data];
                [self.tableView reloadData];
                
                self.haveNext = data.count == 10;
                if (self.haveNext) {
                    [self appendFooterView];
                }else{
                    [self removeFooterRefresh];
                }
            }else{
                [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
            }
            [self endRefreshing];
        }];
    }];
}

-(void)appendFooterView {
    @weakify(self);
    [self addFooterRefresh:^{
        @strongify(self);
        [self showLoadingAnimation];
        [APIHELPER mineBindBrushListWithP:self.p limit:10 complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            [self hideLoadingAnimation];
            
            if (isSuccess) {
                NSArray* data = responseObject[@"data"];
                [self.dataArray addObjectsFromArray:data];
                [self.tableView reloadData];
                
                self.haveNext = data.count == 10;
                if (self.haveNext) {
                    [self appendFooterView];
                }else{
                    [self removeFooterRefresh];
                }
            }else{
                [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
            }
            [self endRefreshing];
        }];
    }];
}

@end
