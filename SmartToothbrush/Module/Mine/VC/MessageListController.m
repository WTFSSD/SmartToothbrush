//
//  MessageListController.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/13.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "MessageListController.h"
#import "MessageCell.h"

@interface MessageListController ()

@property (nonatomic,assign)NSInteger page;
@property (nonatomic,strong)NSMutableArray* dataArray;

@end

@implementation MessageListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    [self subviewStyle];
    [self fetchData];
    [self headerViewInit];
    self.navigationBarTransparent = false;
     [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)baseSetBackgroundImage {
    self.backgroundImage.hidden = YES;
}

#pragma mark - tableView dataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageCell* cell = [tableView dequeueReusableCellWithIdentifier:[MessageCell identify]];
    [HYTool configTableViewCellDefault:cell];
    cell.contentView.backgroundColor = [UIColor clearColor];
    NSDictionary* message = self.dataArray[indexPath.section];
    [cell configMessageCell:message];
    return cell;
}

#pragma mark - tableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        
}

#pragma mark - private methods
- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)subviewStyle {
    self.navigationBarBlue = NO;
    self.navigationBarTransparent = NO;
    [self baseSetupTableView:UITableViewStylePlain InSets:UIEdgeInsetsZero];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.estimatedRowHeight = 110;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView registerNib:[UINib nibWithNibName:[MessageCell identify] bundle:nil] forCellReuseIdentifier:[MessageCell identify]];

}

- (void)fetchData {
    [self.dataArray removeAllObjects];
    [self.tableView reloadData];
    self.tableView.tableFooterView = nil;
    [self showLoadingAnimation];
    [APIHELPER messageListPage:self.page limit:10 complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
        [self hideLoadingAnimation];
        
        if (isSuccess) {
            NSArray* data = responseObject[@"data"];
            [self.dataArray addObjectsFromArray:data];
            [self.tableView reloadData];
            
            self.haveNext = data.count == 10;
            self.page += 1;
            if (self.haveNext) {
                [self appendFooterView];
            }else{
                [self removeFooterRefresh];
            }
        }else{
            [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }
    }];
}

-(void)headerViewInit {
    @weakify(self);
    self.page = 0;
    [self addHeaderRefresh:^{
        @strongify(self);
        [self showLoadingAnimation];
        [APIHELPER messageListPage:self.page limit:10 complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            [self hideLoadingAnimation];
            
            [self.dataArray removeAllObjects];
            [self.tableView reloadData];
            if (isSuccess) {
                NSArray* data = responseObject[@"data"];
                [self.dataArray addObjectsFromArray:data];
                [self.tableView reloadData];
                
                self.haveNext = data.count == 10;
                self.page += 1;
                if (self.haveNext) {
                    [self appendFooterView];
                }else{
                    [self removeFooterRefresh];
                }
            }else{
                [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
            }
            [self endRefreshing];
        }];
    }];
}

-(void)appendFooterView {
    @weakify(self);
    [self addFooterRefresh:^{
        @strongify(self);
        [self showLoadingAnimation];
        [APIHELPER messageListPage:self.page limit:10 complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            [self hideLoadingAnimation];
            
            if (isSuccess) {
                NSArray* data = responseObject[@"data"];
                [self.dataArray addObjectsFromArray:data];
                [self.tableView reloadData];
                
                self.haveNext = data.count == 10;
                self.page += 1;
                if (self.haveNext) {
                    [self appendFooterView];
                }else{
                    [self removeFooterRefresh];
                }
            }else{
                [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
            }
            [self endRefreshing];
        }];
    }];
}

@end
