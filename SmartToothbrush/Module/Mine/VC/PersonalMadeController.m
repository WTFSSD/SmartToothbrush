//
//  PersonalMadeController.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/13.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "PersonalMadeController.h"
#import "BrushingSettingView.h"
#import "BrushingImportantView.h"


#import "ClickableToothStatusView.h"





@implementation PersonalMadeModel
@end

/*私人定制*/
@interface PersonalMadeController (){
    UIScrollView * container;
    
    BrushingSettingView * settingView;
    BrushingImportantView * importantView;
    ClickableToothStatusView * toothStatusView;
    
    
    PersonalMadeModel * model;
}



@end

@implementation PersonalMadeController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self fetchData];
    self.navigationItem.title = @"私人定制";
    [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_BRUSH_GTE_CUSTOM) params:@{}].then(^(NSDictionary*res){
        if(res && res[@"data"] && [res[@"data"] count]>0){
            [model yy_modelSetWithJSON:res[@"data"][0]];
        }
    });
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)setUp{
    
    model = [[PersonalMadeModel alloc] init];
    
    container = [[UIScrollView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:container];
    [container autoPinEdgesToSuperviewEdges];
    
    settingView = [BrushingSettingView instance];
    importantView = [BrushingImportantView instance];
    [container addSubview:settingView];
    [container addSubview:importantView];
    
    [settingView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view withOffset:-zoom(24)];
    [settingView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:settingView withMultiplier:231/351.f];
    [settingView autoAlignAxis:ALAxisVertical toSameAxisOfView:container];
    [settingView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:container withOffset:10];
    
    [importantView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view withOffset:-zoom(24)];
    [importantView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:importantView withMultiplier:337/351.f];
    [importantView autoAlignAxis:ALAxisVertical toSameAxisOfView:container];
    [importantView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:settingView withOffset:zoom(15)];
    
    
    toothStatusView = [ClickableToothStatusView instance];
    
    [importantView addSubview:toothStatusView];
    
    [toothStatusView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:importantView withOffset:zoom(36)];
    [toothStatusView autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:importantView withOffset:-zoom(24)];
    
    [toothStatusView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionHeight ofView:toothStatusView withMultiplier:188.f/288];
    
    [toothStatusView autoAlignAxis:ALAxisVertical toSameAxisOfView:importantView];
}

- (void)fetchData {
    [APIHELPER brushCustomSolutionComplete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
        if (isSuccess) {
            //TODO:刷新settingView、importantView
            [settingView configViewWithData:responseObject[@"data"]];
            
//            if(responseObject[@"data"][@"imp_area"]){
//                [importantView configViewWithData:responseObject[@"data"][@"imp_area"]];
//            }
        }else{
            [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }
    }];
}
@end
