//
//  MusicSelectController.h
//  Base
//
//  Created by 谢河源 on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "BaseTableViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface MusicSelectController : BaseTableViewController

@property(nonatomic,copy)void(^selectMusic)(MPMediaItem* music);

@end
