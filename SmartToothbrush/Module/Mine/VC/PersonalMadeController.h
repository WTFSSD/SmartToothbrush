//
//  PersonalMadeController.h
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/13.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BaseTableViewController.h"


@interface PersonalMadeModel:NSObject
@property(nonatomic,copy)NSString * bacterium_one ;
@property(nonatomic,copy)NSString * bacterium_two ;
@property(nonatomic,copy)NSString * caries_one ;
@property(nonatomic,copy)NSString * caries_two ;
@property(nonatomic,copy)NSString * fit_status;
@property(nonatomic,copy)NSString * hours;
@property(nonatomic,copy)NSString * mode_id;
@property(nonatomic,copy)NSString * press;
@property(nonatomic,copy)NSString * sensitivity_one5;
@property(nonatomic,copy)NSString * sensitivity_two5;
@property(nonatomic,copy)NSString * tb_noFE6BF3A076;
@end

@interface PersonalMadeController : BaseViewController

@end
