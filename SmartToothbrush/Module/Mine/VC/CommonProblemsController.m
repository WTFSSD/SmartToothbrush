//
//  CommonProblemsController.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/22.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "CommonProblemsController.h"


#import "ProblemViewCell.h"
@interface CommonProblemsController ()

@property(nonatomic,strong)NSMutableArray* dataArray;
@property(nonatomic,strong)NSMutableArray* foldArray;

@end

@implementation CommonProblemsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self subviewStyle];
    [self fetchData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)baseSetBackgroundImage {
    
}

#pragma mark - tableView dataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        static NSString* cellId = @"problemCell";
        ProblemViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (cell == nil) {
            cell = [[ProblemViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        
        }
        NSDictionary* data = self.dataArray[indexPath.section];
        cell.text = data[@"question"];
        cell.isFold = [self.foldArray[indexPath.section] boolValue];
        return cell;
    }else{
        return [self answerCellForTableView:tableView indexPath:indexPath];
    }
}

-(UITableViewCell*)answerCellForTableView:(UITableView*)tableView indexPath:(NSIndexPath*)indexPath {
    static NSString* cellId = @"answerCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [HYTool configTableViewCellDefault:cell];
        UILabel* answerLbl = [[UILabel alloc] init];
        [cell.contentView addSubview:answerLbl];
        [answerLbl autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(zoom(15), zoom(15), zoom(14), zoom(15))];
        answerLbl.tag = 1000;
    }
    UILabel* answerLbl = [cell.contentView viewWithTag:1000];
    answerLbl.numberOfLines = 0;
    NSDictionary* data = self.dataArray[indexPath.section];
    NSString* htmlStr = [self htmlEntityDecode: data[@"answer"]];
    NSMutableAttributedString * attrString = [[NSMutableAttributedString alloc] initWithData:[htmlStr dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    
    [attrString addAttributes:@{NSBaselineOffsetAttributeName: @(5),
                                NSFontAttributeName:[UIFont systemFontOfSize:15],
                                NSForegroundColorAttributeName: [UIColor colorWithString:@"70879e"]
                                } range:NSMakeRange(0, attrString.length)];
    
    answerLbl.attributedText = attrString;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView* footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 0.5)];
    footer.backgroundColor = [UIColor hySeparatorColor];
    return footer;
}

#pragma mark - tableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return UITableViewAutomaticDimension;
    }else{
        BOOL isFold = [self.foldArray[indexPath.section] boolValue];
        return isFold ? 0 : UITableViewAutomaticDimension;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.5;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        
        
        
        
        BOOL isFold = [self.foldArray[indexPath.section] boolValue];
        
        NSInteger foldCell = -1;
        if(!isFold){
            for (int i =0  ; i<self.foldArray.count; i++) {
                if(![self.foldArray[i] integerValue]){
                    foldCell = i;
                }
                self.foldArray[i] = @(true);
            }
        }else{
            for (int i =0  ; i<self.foldArray.count; i++) {
                if(![self.foldArray[i] integerValue]){
                    foldCell = i;
                }
                self.foldArray[i] = @(true);
            }
             [self.foldArray replaceObjectAtIndex:indexPath.section withObject:@(false)];
        }
         NSMutableIndexSet * set = [NSMutableIndexSet indexSetWithIndex:indexPath.section];
        if(foldCell != -1){
            [set addIndex:foldCell];
            [self.tableView reloadSections:set withRowAnimation:UITableViewRowAnimationNone];
        }else{
                [self.tableView reloadSections:set withRowAnimation:UITableViewRowAnimationNone];
        }

    }
}

#pragma mark - private methods
- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)foldArray {
    if (!_foldArray) {
        _foldArray = [NSMutableArray array];
    }
    return _foldArray;
}

- (void)subviewStyle {
    self.title = @"常见问题";
    self.view.backgroundColor = [UIColor hyDefaultBackColor];
    self.navigationBarTransparent = NO;
    self.navigationBarBlue = NO;
    [self baseSetupTableView:UITableViewStylePlain InSets:UIEdgeInsetsMake(zoom(10), 0, 0, 0)];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = zoom(50);
}

- (NSString *)htmlEntityDecode:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    string = [string stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    string = [string stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    string = [string stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]; // Do this last so that, e.g. @"&amp;lt;" goes to @"&lt;" not @"<"
    
    return string;
}

- (void)fetchData {
    [self.dataArray removeAllObjects];
    [self.tableView reloadData];
    self.tableView.tableFooterView = nil;
    [self showLoadingAnimation];
    [APIHELPER fetchCommonProblemsComplete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
        [self hideLoadingAnimation];
        
        if (isSuccess) {
            [self.dataArray addObjectsFromArray:responseObject[@"data"]];
            for (id item in self.dataArray) {
                [self.foldArray addObject:@(true)];
            }
            [self.tableView reloadData];
        }else{
            [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }
    }];
}

@end
