//
//  VoiceSetController.m
//  Base
//
//  Created by 谢河源 on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "VoiceSetController.h"
#import "MusicSelectController.h"
#import "CommonHeadView.h"

@interface VoiceSetController ()

@property (nonatomic,strong)NSMutableArray* datas1;
@property (nonatomic,strong)NSMutableArray* datas2;
@property (nonatomic,strong)NSString* selectId;
@property (nonatomic,strong)UIView* footer;

@end

@implementation VoiceSetController

- (void)viewDidLoad {
    [super viewDidLoad];
    MPMediaQuery *query = [MPMediaQuery songsQuery];
    self.navigationBarBlue = NO;
    [self subviewStyle];
    [self fetchData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return section == 0 ? self.datas1.count : self.datas2.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellId = @"voiceCell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [UITableViewCell new];
        [HYTool configTableViewCellDefault:cell];
    }
    NSArray* datas = indexPath.section == 0 ? self.datas1 : self.datas2;
    NSDictionary* voice = datas[indexPath.row];
    NSString* imageName = self.selectId.integerValue == [voice[@"id"] integerValue] ? @"checked_in" : @"checked";
    cell.imageView.image = ImageNamed(imageName);
    cell.textLabel.text = voice[@"title"];
    [cell addLine:(indexPath.section-1 == datas.count) leftOffSet:0 rightOffSet:0];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CommonHeadView* head = [CommonHeadView headerView:section];
    NSString* title = section == 0 ? @"内置音乐" : @"本地音乐";
    [head configHeaderView:title];
    return head;
}

#pragma mark - tableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kScale_height * 71;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kScale_height * 42;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray* datas = indexPath.section == 0 ? self.datas1 : self.datas2;
    NSString* Id = [datas[indexPath.row] objectForKey:@"id"];
    self.selectId = Id;
    [self.tableView reloadData];
}

#pragma mark - private methods
- (NSMutableArray *)datas1 {
    if (!_datas1) {
        _datas1 = [NSMutableArray array];
    }
    return _datas1;
}

- (NSMutableArray *)datas2 {
    if (!_datas2) {
        _datas2 = [NSMutableArray array];
    }
    return _datas2;
}

- (UIView *)footer {
    if (!_footer) {
        _footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, zoomHeight(70))];
        _footer.backgroundColor = [UIColor whiteColor];
        UIButton* btn = [HYTool getButtonWithFrame:CGRectMake(0, 0, zoom(201), zoomHeight(39)) title:@"添加本地音乐" titleSize:16 titleColor:[UIColor whiteColor] backgroundColor:nil blockForClick:^(id sender) {
            MusicSelectController* vc = [[MusicSelectController alloc] init];
            @weakify(self);
            [vc setSelectMusic:^(MPMediaItem *music) {
                @strongify(self);
                //TODO:上传
                NSDictionary* dic = @{@"title":[music valueForProperty:MPMediaItemPropertyTitle],@"id":@"3"};
                [self.datas2 addObject:dic];
                [self.tableView reloadData];
            }];
            [self.navigationController pushViewController:vc animated:YES];
        }];
        [_footer addSubview:btn];
        [btn autoSetDimensionsToSize:CGSizeMake(zoom(201), zoomHeight(39))];
        [btn autoCenterInSuperview];
        btn.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [HYTool configViewLayer:btn size:zoomHeight(19.5)];
        [HYTool configGradualBlueView:btn];
    }
    return _footer;
}

- (void)subviewStyle {
    [self baseSetupTableView:UITableViewStylePlain InSets:UIEdgeInsetsZero];
    self.tableView.tableFooterView = self.footer;
}

- (void)fetchData {
    self.datas1 = @[
                    @{@"title":@"做我的猫",@"id":@"0"},
                    @{@"title":@"LALALA",@"id":@"1"},
                    @{@"title":@"风景",@"id":@"2"}
                    ];
    self.datas2 = [@[
//                    @{@"title":@"陌生人",@"id":@"3"}
                    ] mutableCopy];
    self.selectId = @"0";
    if (self.datas2.count != 0) {
        self.tableView.tableFooterView = nil;
    }
    [self.tableView reloadData];
}

@end
