//
//  BrushSchemeController.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/13.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "BrushSchemeController.h"
#import "BrushSchemeView.h"

@interface BrushSchemeController ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property(nonatomic,strong)NSArray* dataArray;
@property(nonatomic,strong)NSArray* hourArray;
@property(nonatomic,assign)NSInteger setIndex;  //设置的是第几个模式

@end

@implementation BrushSchemeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.setIndex = 0;
    self.hourArray = @[
                       @{@"title":@"1分00秒",@"value":@60},
                       @{@"title":@"1分45秒",@"value":@105},
                       @{@"title":@"2分00秒",@"value":@120},
                       @{@"title":@"2分30秒",@"value":@150},
                       @{@"title":@"3分00秒",@"value":@180}
                       ];
    [self subviewStyle];
    self.dataArray = @[
                       @{@"tb_no":@"123456",@"mode_id":@"1"},
                        @{@"tb_no":@"123457",@"mode_id":@"2"},
                        @{@"tb_no":@"123458",@"mode_id":@"3"},
                       ];
    [self fetchData];
}

#pragma mark - pickerview delegate and datasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.hourArray.count;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 45;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSDictionary* hour = self.hourArray[row];
    return hour[@"title"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSInteger hour = [self.hourArray[row][@"value"] integerValue];
    NSString* tbNo = self.dataArray[self.setIndex][@"tb_no"];
    NSInteger modeId = [self.dataArray[self.setIndex][@"mode_id"] integerValue];
    [APIHELPER brushSolutionSubmitWithTbNo:tbNo modeId:modeId time:hour press:-1 fitStatus:-1 impArea:nil complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
        if (isSuccess) {
            [self showMessage:@"设置成功"];
        }else{
            [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }
    }];
}

#pragma mark - tableView dataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellId = @"schemeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        [HYTool configTableViewCellDefault:cell];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
    }
    for (UIView* view in cell.contentView.subviews) {
        if (view.tag == 1000) {
            [view removeFromSuperview];
        }
    }
    NSDictionary* data = self.dataArray[indexPath.section];
    NSString* tbNo = data[@"tb_no"];
    NSInteger modeId = [data[@"mode_id"] integerValue];
    self.setIndex = indexPath.section;
    BrushSchemeView* view = [BrushSchemeView view];
    [cell.contentView addSubview:view];
    [view autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, 12, 15, 12)];
    view.tag = 1000;
    [view configSchemeView:indexPath.section data:data];
    [view setSelectTime:^(NSInteger section) {
        
        UIPickerView* picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, kScreen_Height-120, kScreen_Width, 120)];
        picker.delegate = self;
        picker.dataSource = self;
        [self.view addSubview:picker];
        

    }];
    [view setSwitchClick:^(NSInteger value) {
        //防溅设置
        [APIHELPER brushSolutionSubmitWithTbNo:tbNo modeId:modeId time:-1 press:-1 fitStatus:value impArea:nil complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            if (isSuccess) {
                [self showMessage:@"设置成功"];
            }else{
                [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
            }
        }];
    }];
    [view setSliderChange:^(NSInteger value) {
       //设置强度
        [APIHELPER brushSolutionSubmitWithTbNo:tbNo modeId:modeId time:-1 press:value fitStatus:-1 impArea:nil complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            if (isSuccess) {
                [self showMessage:@"设置成功"];
            }else{
                [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
            }
        }];
    }];
    return cell;
}

#pragma mark - tableView delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 220 + 15;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - private methods
- (void)subviewStyle {
    self.title = @"刷牙方案";
    self.navigationBarTransparent = YES;
    [self baseSetupTableView:UITableViewStylePlain InSets:UIEdgeInsetsZero];
    self.tableView.backgroundColor = [UIColor clearColor];
    UIView* header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 10)];
    header.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = header;
}

- (void)fetchData {
    [self showLoadingAnimation];
    [APIHELPER brushSolutionComplete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
        [self hideLoadingAnimation];
        if (isSuccess) {
            NSLog(@"%@",responseObject);
            self.dataArray = [NSArray arrayWithArray:responseObject[@"data"]];
            [self.tableView reloadData];
        }else{
            [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }
    }];
}

@end
