//
//  FeedbackController.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/13.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "FeedbackController.h"

@interface FeedbackController ()

@property (weak, nonatomic) IBOutlet UITextField *phoneTf;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *commitBtn;

@end

@implementation FeedbackController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self subviewStyle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)baseSetBackgroundImage {
    
}

- (void)subviewStyle {
    self.navigationBarBlue = NO;
    self.navigationBarTransparent = NO;
    [HYTool configViewLayer:self.textView size:4];
    [HYTool configViewLayerFrame:self.textView WithColor:[UIColor colorWithString:@"dddddd"] borderWidth:0.5];

    
    UIColor *color = [UIColor colorWithString:@"BFCDDB"];
    self.phoneTf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.phoneTf.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    
    UILabel *placeHolderLabel = [[UILabel alloc] init];
    placeHolderLabel.text = @"请告诉我们你的建议和意见，我们会不断改进";
    placeHolderLabel.numberOfLines = 0;
    placeHolderLabel.textColor = [UIColor lightGrayColor];
    [placeHolderLabel sizeToFit];
    [self.textView addSubview:placeHolderLabel];
    
    placeHolderLabel.font = [UIFont systemFontOfSize:16];
    placeHolderLabel.textColor = [UIColor colorWithString:@"BFCDDB"];
    
    [self.textView setValue:placeHolderLabel forKey:@"_placeholderLabel"];
    
    [HYTool configGradualBlueView:self.commitBtn];
}

- (IBAction)commit:(id)sender {
    //提交反馈
    if ([self.phoneTf.text isEmpty]) {
        [self showMessage:@"请填写联系方式"];
        return;
    }
    if ([self.textView.text isEmpty]) {
        [self showMessage:@"请填写您的意见"];
        return;
    }
    [self showLoadingAnimation];
    [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_FEEDBACK) params:@{@"link":self.phoneTf.text,@"content":self.textView.text}].then(^(id res){
        [self hideLoadingAnimation];
        if(!res) return;
        if (res[@"msg"]) {
            [self showMessage:res[@"msg"] complete:^{
                if ([res[@"code"] integerValue] == 1) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
    });
}

@end
