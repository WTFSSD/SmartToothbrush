//
//  ContactViewController.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/13.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "ContactViewController.h"
#import "NSString+Extension.h"

@interface ContactViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topImageTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botLblBottom;
@property (weak, nonatomic) IBOutlet UILabel *buttonLbl;

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self subviewStyle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)baseSetBackgroundImage {
    
}

- (void)subviewStyle {
    self.navigationBarBlue = NO;
    self.navigationBarTransparent = NO;
    self.topImageTop.constant = zoomHeight(74);
    self.botLblBottom.constant = zoomHeight(84);
    
    NSString* text = self.buttonLbl.text;
    NSAttributedString* attr = [text addAttribute:@[NSFontAttributeName,NSForegroundColorAttributeName] values:@[[UIFont boldSystemFontOfSize:15],[UIColor colorWithString:@"027afe"]] subStrings:@[@"优动牙齿科学",@"优动牙齿科学"]];
    self.buttonLbl.attributedText = attr;
    [self.buttonLbl bk_whenTapped:^{
        //TODO:预留点击跳转
        
    }];
}

@end
