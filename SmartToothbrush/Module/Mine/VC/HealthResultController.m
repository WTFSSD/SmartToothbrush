//
//  HealthResultController.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/8.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "HealthResultController.h"
#import "HealthResultModel.h"

#import "Questionnaire2ViewController.h"

#import "GradientButton.h"
@interface HealthResultController ()
@property (weak, nonatomic) IBOutlet UILabel *levelLbl;
@property (weak, nonatomic) IBOutlet UITextView *adviseTextView;

///重新评估
@property(nonatomic,strong)GradientButton * assessment;


@end

@implementation HealthResultController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self subviewStyle];
    [self setGradeData];
    [self getData];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)getData{
    NSDictionary * params =@{@"key":[UserInfoModel share].key};
   RACSignal * sig =  [[NetworkUtils requestWith:HTTPMethodGET url:REQUEST_URL(API_GRADE_STATE) params:params headers:nil progress:nil obj:[HealthResultModel class]] signal];
    
    RAC(self.levelLbl,text) = [sig map:^id(HealthResultModel  * value) {
        if(!value) return nil;
        return value.grade;
    }];
    RAC(self.adviseTextView,text) = [sig map:^id(HealthResultModel  * value) {
        if(!value) return nil;
        return value.advice;
    }];
}

- (void)subviewStyle {
    self.navigationBarTransparent = YES;
    [HYTool configViewLayerFrame:self.levelLbl WithColor:[UIColor colorWithString:@"12c6fc"] borderWidth:3.5];
    [HYTool configViewLayer:self.levelLbl size:37];
    
    self.assessment = [GradientButton instanceWith:@"查看或重新评估"];
    self.assessment.titleColor = [UIColor whiteColor];
    [self.view addSubview:self.assessment];
    [self.assessment autoAlignAxis:ALAxisVertical toSameAxisOfView:self.view];
    [self.assessment autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.view withOffset:-zoom(20)];
    [self.assessment autoSetDimensionsToSize:CGSizeMake(zoom(200), zoom(40))];
    [[self.assessment rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        Questionnaire2ViewController * vc = [Questionnaire2ViewController instance];
        [self.navigationController pushViewController:vc animated:YES];
    }];
}

- (void)setGradeData {
    if ([self.schemaArgu objectForKey:@"grade"]) {
        self.levelLbl.text = self.schemaArgu[@"grade"];
    }
    if ([self.schemaArgu objectForKey:@"advise"]) {
        self.adviseTextView.text = self.schemaArgu[@"advise"];
    }
}

@end
