//
//  ProblemViewCell.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/7/30.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "ProblemViewCell.h"


@interface ProblemViewCell(){
    UILabel * t;
    UIImageView * img;
    UIView * l;
}
@end

@implementation ProblemViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self setUp];
    }
    return  self;
}

-(void)setUp{
    
    /*
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
     cell.textLabel.font = tableViewCellDefaultTextFont;
     cell.textLabel.textColor = [UIColor hyBlackTextColor];
     cell.contentView.backgroundColor = tableViewCellDefaultBackgroundColor;
     cell.detailTextLabel.font = tableViewCellDefaultDetailTextFont;
     cell.detailTextLabel.textColor = tableViewCellDefaultDetailTextColor;
     cell.textLabel.numberOfLines = 0;
     */
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    self.contentView.backgroundColor = tableViewCellDefaultBackgroundColor;
    
    
    t = [UILabel makeWith:^(UILabel *label) {
        label.font = tableViewCellDefaultTextFont;
        label.textColor = [UIColor hyBlackTextColor];
        label.numberOfLines = 0;
    }];
    [self.contentView addSubview:t];
    
    img = [[UIImageView alloc] initWithImage:ImageNamed(@"arrow_down")];
    [self.contentView addSubview:img];
    [img autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.contentView withOffset:-8];
    [img autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self.contentView];
    [img autoSetDimensionsToSize:CGSizeMake(zoom(16), zoom(10))];
    
    [t autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.contentView withOffset:8];
    [t autoPinEdge:ALEdgeRight toEdge:ALEdgeLeft ofView:img withOffset:-8];
    [t autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.contentView withOffset:8];
    [t autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.contentView withOffset:-8];
    
    
    l = [[UIView alloc] initWithFrame:CGRectZero];
    l.backgroundColor = [UIColor hyDefaultBackColor];
    [self.contentView addSubview:l];
    
    [l autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.contentView withOffset:0];
    [l autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.contentView withOffset:0];
    [l autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.contentView withOffset:0];
    [l autoSetDimension:ALDimensionHeight toSize:1];

    RAC(t,text) = [self rac_valuesForKeyPath:@"text" observer:self];
    
    RAC(img,image) = [[self rac_valuesForKeyPath:@"isFold" observer:self] map:^id(id value) {
        if([value boolValue]){
            return ImageNamed(@"arrow_down");
        }
       return ImageNamed(@"arrow_top");
    }];
    
    RAC(l,hidden) =[self rac_valuesForKeyPath:@"isFold" observer:self];
    [img bk_whenTapped:self.onImageClick];
}

@end
