//
//  HealthSelectCell.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/9.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "HealthSelectCell.h"

@interface HealthSelectCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UILabel *leftLbl;
@property (weak, nonatomic) IBOutlet UIImageView *leftImg;
@property (weak, nonatomic) IBOutlet UIView *centerView;
@property (weak, nonatomic) IBOutlet UILabel *centerLbl;
@property (weak, nonatomic) IBOutlet UIImageView *centerImg;
@property (weak, nonatomic) IBOutlet UIView *rightView;
@property (weak, nonatomic) IBOutlet UILabel *rightLbl;
@property (weak, nonatomic) IBOutlet UIImageView *rightImg;

@end

@implementation HealthSelectCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (NSString *)identify {
    return NSStringFromClass([HealthSelectCell class]);
}

- (void)configSelectCell:(id)model section:(NSInteger)section selects:(NSArray*)selects {
    NSDictionary* dic = model;
    NSArray* options = dic[@"options"];
    self.titleLbl.text = dic[@"question"];
    NSInteger index = 0;
    for (UILabel* lbl in @[self.leftLbl,self.centerLbl,self.rightLbl]) {
        NSDictionary* option = options[index];
        lbl.text = option[@"label"];
        index++;
    }
    index = 0;
    for (UIView* view in @[self.leftView,self.centerView,self.rightView]) {
        [view bk_whenTapped:^{
            if (self.select) {
                self.select(index, section);
            }
        }];
        index++;
    }
    index = 0;
    for (UIImageView* imgV in @[self.leftImg,self.centerImg,self.rightImg]) {
        NSInteger select = [selects[section] integerValue];
        imgV.image = select == index ? ImageNamed(@"checked_in") : ImageNamed(@"checked");
        index++;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
