//
//  MessageCell.h
//  SmartToothbrush
//
//  Created by admin on 2018/5/14.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCell : UITableViewCell

+(NSString*)identify;
-(void)configMessageCell:(id)model;

@end
