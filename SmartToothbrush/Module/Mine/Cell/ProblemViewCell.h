//
//  ProblemViewCell.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/7/30.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProblemViewCell : UITableViewCell


@property(nonatomic,copy)NSString * text;

@property(nonatomic,assign)BOOL isFold;


@property(nonatomic)void(^onImageClick)(void);

@end
