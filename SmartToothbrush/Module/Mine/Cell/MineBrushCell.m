//
//  MineBrushCell.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/13.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "MineBrushCell.h"
#import "CustomCircleView.h"
#import "NSString+Extension.h"
#import "BrushModel.h"
@interface MineBrushCell ()

@property (weak, nonatomic) IBOutlet UILabel *tbVersionLbl;
@property (weak, nonatomic) IBOutlet UILabel *tbNoLbl;
@property (weak, nonatomic) IBOutlet UILabel *firmwareVersionLbl;
@property (weak, nonatomic) IBOutlet UILabel *buyTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *lifetimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLbl;
@property (weak, nonatomic) IBOutlet UIView *rateV;

@end

@implementation MineBrushCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (NSString *)identify {
    return NSStringFromClass([self class]);
}

- (void)configMineBrushCell:(id)model {
    if (model == nil) {
        return;
    }
    if([model isKindOfClass:[BrushModel class]]){
        BrushModel * m = (BrushModel *)model;
        
        NSString* lifeTime = m.lifeTime;
        NSArray* arr = [lifeTime componentsSeparatedByString:@"/"];
        if (arr.count == 2) {
            NSInteger left = [arr[0] integerValue];
            NSInteger total = [arr[1] integerValue];
            double rate = (double)left/total;
            //TODO:设置rateView
            
            
        }
       
        self.tbVersionLbl.text = m.tbVersion;
        self.tbNoLbl.text = m.tbNo;
        self.firmwareVersionLbl.text = m.firmwareVersion;
        self.lifetimeLbl.attributedText = [[NSString stringWithFormat:@"刷头寿命%@天",lifeTime] addAttribute:@[NSFontAttributeName] values:@[[UIFont systemFontOfSize:29]] subStrings:@[lifeTime]];
        self.startTimeLbl.text = [HYTool dateStringWithTimestamp:m.enableTime  andFormat:@"yyyy-MM-dd"];
    }
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
