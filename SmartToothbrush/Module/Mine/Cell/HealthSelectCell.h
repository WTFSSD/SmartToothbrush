//
//  HealthSelectCell.h
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/9.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HealthSelectCell : UITableViewCell

@property(nonatomic,copy)void(^select)(NSInteger index, NSInteger section);
+(NSString*)identify;
-(void)configSelectCell:(id)model section:(NSInteger)section selects:(NSArray*)selects;

@end
