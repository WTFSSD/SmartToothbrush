//
//  MineBrushCell.h
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/5/13.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineBrushCell : UITableViewCell

+(NSString*)identify;
-(void)configMineBrushCell:(id)model;

@end
