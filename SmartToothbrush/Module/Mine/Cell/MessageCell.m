//
//  MessageCell.m
//  SmartToothbrush
//
//  Created by admin on 2018/5/14.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "MessageCell.h"

@interface MessageCell ()

@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UIView *roundV;
@property (weak, nonatomic) IBOutlet UILabel *messageLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;

@end

@implementation MessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [HYTool configViewLayer:self.roundV size:zoom(5)];
    [HYTool configViewLayerFrame:self.roundV WithColor:[UIColor colorWithString:@"0091E7"] borderWidth:1];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (NSString *)identify {
    return  NSStringFromClass([self class]);
}

- (void)configMessageCell:(id)model {
    NSDictionary* message = model;
    self.messageLbl.text = message[@"content"];
    
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy年MM月dd"];
    NSDate * date = [formatter dateFromString: message[@"date"]];
    
    [formatter setDateFormat:@"yyyy"];
    NSString * year = [formatter stringFromDate:date];
    [formatter setDateFormat:@"MM-dd"];
    NSString * md = [formatter stringFromDate:date];


    NSString * dateStr = [NSString stringWithFormat:@"%@\n%@",year,md];
    
    
    self.dateLbl.text = dateStr;
    self.timeLbl.text = message[@"time"];
}

@end
