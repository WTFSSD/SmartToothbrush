//
//  KindlyReminderController.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/6/9.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "KindlyReminderController.h"

@interface KindlyReminderController ()

@property (weak, nonatomic) IBOutlet UIButton *setBtn;

@end

@implementation KindlyReminderController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private methods
- (void)subviewStyle {
    [HYTool configGradualBlueView:self.setBtn];
}

- (void)subviewBind {
    [self.setBtn bk_whenTapped:^{
        //TODO:一键设定
        
    }];
}

@end
