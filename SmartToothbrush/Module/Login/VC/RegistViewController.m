//
//  RegistViewController.m
//  Base
//
//  Created by admin on 2017/3/2.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "RegistViewController.h"
#import "UIViewController+Extension.h"
#import "HYCountDown.h"

#import "AppDelegate+Extension.h"
@interface RegistViewController ()

@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UITextField *phoneTf;
@property (weak, nonatomic) IBOutlet UITextField *codeTf;
@property (weak, nonatomic) IBOutlet UITextField *passwordTf;

@property (weak, nonatomic) IBOutlet UIButton *getCodeBtn;

@property (weak, nonatomic) IBOutlet UIButton *registBtn;

@property (strong, nonatomic) HYCountDown* countDown;



///verfi
@property(nonatomic,strong)NSMutableDictionary * params;
@end

@implementation RegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self subviewStyle];
    [self subviewBind];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)baseSetBackgroundImage {
    
}

- (IBAction)onRegisterLater:(UIButton *)sender {
    
    AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [delegate configTabbar];
}




#pragma mark - IBAction
- (void)regiest {
    if ([self.phoneTf.text isEmpty]) {
        [MBProgressHUD hy_showMessage:@"请输入手机号" inView:self.view];
        return;
    }
    if (![self.phoneTf.text validateWithValidateType:ValidateTypeForMobile]) {
        [MBProgressHUD hy_showMessage:@"请输入正确的手机号" inView:self.view];
        return;
    }
    if ([self.codeTf.text isEmpty]) {
        [MBProgressHUD hy_showMessage:@"请输入验证码" inView:self.view];
        return;
    }
    if ([self.passwordTf.text isEmpty]) {
        [MBProgressHUD hy_showMessage:@"请输入密码" inView:self.view];
        return;
    }
    [self showLoadingAnimation];
    
    
    
    //    注册
    [APIHELPER regist:self.phoneTf.text password:self.passwordTf.text code:self.codeTf.text complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
        [self hideLoadingAnimation];
        if (isSuccess) {
            [Global setUserAuth:responseObject[@"data"][@"auth"]];
            APIHELPER.userInfo = [UserInfoModel yy_modelWithDictionary:responseObject[@"data"]];
            APPROUTE(([NSString stringWithFormat:@"%@?phone=%@",kLoginViewController,self.phoneTf.text]));
        }else{
            [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
        }
    }];
}

- (IBAction)login:(id)sender {
    [self backAction];
}

#pragma mark - system delegate


#pragma mark - private methods
- (void)subviewStyle {
    self.navigationBarBlue = NO;
    self.navigationBarTransparent = NO;
    for (UIView* view in @[self.phoneView,self.codeView,self.passwordView,self.getCodeBtn,self.registBtn]) {
        [HYTool configViewLayer:view size:5];
        [HYTool configViewLayer:view withColor:[UIColor colorWithString:@"dddddd"]];
    }
    self.registBtn.bounds = CGRectMake(0, 0, zoom(200), 40);
    [HYTool configViewLayer:self.registBtn size:zoomHeight(20)];
    [HYTool configGradualBlueView:self.registBtn];
    [HYTool configViewLayer:self.getCodeBtn withColor:[UIColor clearColor]];
    self.codeTf.keyboardType = UIKeyboardTypeNumberPad;
    
    
    [self.phoneTf rac_textSignal];
    [self.codeTf rac_textSignal];
    [self.passwordTf rac_textSignal];
    
    RAC(self,params) = [[RACSignal combineLatest:@[[self.phoneTf rac_textSignal],[self.codeTf rac_textSignal],[self.passwordTf rac_textSignal]]] map:^id(id value) {
        return [[NSMutableDictionary alloc] initWithDictionary:@{@"account":value[0],@"code":value[1],@"password":value[2]}];
    }];
}

-(void)subviewBind {
    [self.getCodeBtn addTarget:self action:@selector(fetchCode:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.registBtn addTarget:self action:@selector(regist) forControlEvents:UIControlEventTouchUpInside];
}

-(void)fetchCode:(UIButton*)btn {
    
    if(!_params) return [self showMessage:@"请输入手机号或邮箱"];
    if([_params[@"account"] isEmpty]) return [self showMessage:@"请输入手机号或邮箱"];
    [_params setValue:@(1) forKey:@"type"];
    [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_VERIFY_CODE) params:_params].then(^(id response){
        if(!response) return;
        self.getCodeBtn.enabled = NO;
        @weakify(self);
        self.countDown = [HYCountDown countDownWithWholeSecond:60 WithEachSecondBlock:^(NSInteger currentTime) {
            @strongify(self);
            [self.getCodeBtn setTitle:[NSString stringWithFormat:@"%lds",currentTime] forState:UIControlStateDisabled];
                    } WithCompletionHandler:^{
                        @strongify(self);
                        self.getCodeBtn.enabled = YES;
                        [self.getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
                    }];
    });
    
    
    
    
}

- (void)regist {
    if(!_params) return [self showMessage:@"请输入手机号或邮箱"];
    if ([_params[@"account"] isEmpty]) {
        return [self showMessage:@"请输入手机号或邮箱"];
    }
    if ([_params[@"code"] isEmpty]) {
        return [self showMessage:@"请输入验证码"];
        
    }
    if ([_params[@"password"] isEmpty]) {
        return [self showMessage:@"请输入密码"];
    }
    [self showLoadingAnimation];
    
    
    [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_REGEISTER) params:_params].then(^(id obj){
        if(!obj) return;
        kSaveAccountAndPassword(self.phoneTf.text, self.passwordTf.text);
        [self.navigationController popViewControllerAnimated:YES];
    });

}

@end
