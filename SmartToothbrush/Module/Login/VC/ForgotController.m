//
//  ForgotController.m
//  Base
//
//  Created by 谢河源 on 2018/4/29.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "ForgotController.h"

@interface ForgotController ()

@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet UIView *emailView;


@end

@implementation ForgotController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self subviewStyle];
    [self subviewBind];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)baseSetBackgroundImage {
    
}

- (void)subviewStyle {
    self.view.backgroundColor = [UIColor colorWithString:@"f1f3f8"];
    self.navigationBarBlue = NO;
    self.navigationBarTransparent = NO;
}

- (void)subviewBind {
    [self.phoneView bk_whenTapped:^{
        APPROUTE(([NSString stringWithFormat:@"%@?contentType=0",kCheckCodeController]));
    }];
    [self.emailView bk_whenTapped:^{
        APPROUTE(([NSString stringWithFormat:@"%@?contentType=1",kCheckCodeController]));
    }];
}

@end
