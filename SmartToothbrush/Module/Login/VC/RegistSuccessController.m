//
//  RegistSuccessController.m
//  SmartToothbrush
//
//  Created by 谢河源 on 2018/6/9.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "RegistSuccessController.h"

@interface RegistSuccessController ()

@property (weak, nonatomic) IBOutlet UIButton *setBtn;

@end

@implementation RegistSuccessController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self subviewStyle];
    [self subviewBind];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private methods
- (void)subviewStyle {
    [HYTool configGradualBlueView:self.setBtn];
}

- (void)subviewBind {
    [self.setBtn bk_whenTapped:^{
        //TODO:一键设定
        
    }];
}
@end
