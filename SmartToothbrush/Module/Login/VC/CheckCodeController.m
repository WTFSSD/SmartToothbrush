//
//  CheckCodeController.m
//  Base
//
//  Created by admin on 2017/4/7.
//  Copyright © 2017年 XHY. All rights reserved.
//

//修改登陆密码、修改绑定手机的中间验证页面
#import "CheckCodeController.h"
#import "HYCountDown.h"
#import "NSString+HYMobileInsertInterval.h"

@interface CheckCodeController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet UITextField *phoneTf;
@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UITextField *codeTf;
@property (weak, nonatomic) IBOutlet UIButton *getCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (strong,nonatomic) HYCountDown* countDown;

@end

@implementation CheckCodeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.schemaArgu[@"contentType"]) {
        self.contentType = [[self.schemaArgu objectForKey:@"contentType"] integerValue];
    }
    
    [self subviewStyle];
    [self subviewBind];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)baseSetBackgroundImage {
    
}

#pragma mark - textField delegate
-(void)textFieldDidChanged:(UITextField*)textField {
    if (self.codeTf.text.length == 5) {
        self.nextBtn.backgroundColor = [UIColor hyRedColor];
    }else{
        self.nextBtn.backgroundColor = RGB(211, 211, 211, 1.0);
    }
}

#pragma mark - private methods
-(void)subviewStyle {
    self.navigationBarBlue = NO;
    self.navigationBarTransparent = NO;
    for (UIView* view in @[self.phoneView,self.codeView]) {
        [HYTool configViewLayer:view withColor:[UIColor colorWithString:@"dddddd"]];
        [HYTool configViewLayer:view];
    }
    self.nextBtn.bounds = CGRectMake(0, 0, zoom(200), 40);
    [HYTool configViewLayer:self.nextBtn size:zoomHeight(20)];
    [HYTool configGradualBlueView:self.nextBtn];
    self.codeTf.keyboardType = UIKeyboardTypeNumberPad;
    self.phoneTf.placeholder = self.contentType == TypePhone ? @"手机号" : @"邮箱";
    self.title = self.contentType == TypePhone ? @"手机号验证" : @"邮箱验证";
}

-(void)subviewBind {
    [self.getCodeBtn addTarget:self action:@selector(fetchCode:) forControlEvents:UIControlEventTouchUpInside];
    [self.codeTf addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.nextBtn addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
}

-(void)fetchCode:(UIButton*)btn {
    NSString* type = self.contentType == TypePhone ? @"手机号" : @"邮箱";
    if (self.phoneTf.text.isEmpty) {
        [self showMessage:[@"请输入" stringByAppendingString:type]];
        return;
    }
    if (self.codeTf.text.isEmpty) {
        [self showMessage:@"请输入验证码"];
        return;
    }
    @weakify(self);
    void (^fetchCode)(BOOL isSuccess,NSString* msg) = ^(BOOL isSuccess, NSString* msg) {
        @strongify(self);
        if (!isSuccess) {
            [self showMessage:msg];
        }else{
            self.getCodeBtn.enabled = NO;
            @weakify(self);
            
            
            self.countDown = [HYCountDown countDownWithWholeSecond:60 WithEachSecondBlock:^(NSInteger currentTime) {
                @strongify(self);
                [self.getCodeBtn setTitle:[NSString stringWithFormat:@"%lds",currentTime] forState:UIControlStateDisabled];
            } WithCompletionHandler:^{
                @strongify(self);
                self.getCodeBtn.enabled = YES;
                [self.getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
            }];
            
            if(self.successCallBack){
                self.successCallBack();
            }
        }
    };
    
    [APIHELPER fetchCode:self.phoneTf.text type:1 complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
        [self hideLoadingAnimation];
        fetchCode(isSuccess,error.userInfo[NSLocalizedDescriptionKey]);
    }];
}

- (void)next {
    APPROUTE(kChangePasswordController);
    if ([self.codeTf.text isEmpty]) {
        [self showMessage:@"请输入验证码"];
        return;
    }
}

@end
