//
//  ChangePasswordController.m
//  Base
//
//  Created by admin on 2017/3/17.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "ChangePasswordController.h"

@interface ChangePasswordController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet UITextField *firstTf;
@property (weak, nonatomic) IBOutlet UITextField *secondTf;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;


@end

@implementation ChangePasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self subviewStyle];
    [self subviewBind];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)baseSetBackgroundImage {
    
}

#pragma mark - subviewStyle
-(void)subviewStyle {
    self.navigationBarBlue = NO;
    self.navigationBarTransparent = NO;
    for (UIView* view in @[self.firstView,self.secondView]) {
        [HYTool configViewLayer:view];
        [HYTool configViewLayer:view withColor:[UIColor colorWithString:@"dddddd"]];
    }
    self.confirmBtn.bounds = CGRectMake(0, 0, zoom(200), 40);
    [HYTool configViewLayer:self.confirmBtn size:zoomHeight(20)];
    [HYTool configGradualBlueView:self.confirmBtn];
    [self.firstTf addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.secondTf addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
}

-(void)textFieldDidChanged:(UITextField*)textField {
    if (![self.firstTf.text isEmpty] && ![self.secondTf.text isEmpty]) {
        self.confirmBtn.backgroundColor = [UIColor hyRedColor];
    }else{
        self.confirmBtn.backgroundColor = RGB(211, 211, 211, 1.0);
    }
}

#pragma mark - subviewBind
-(void)subviewBind {
    [self.confirmBtn addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
}

-(void)submit {
    if ([self.firstTf.text isEmpty]) {
        [self showMessage:@"请设置新密码"];
        return;
    }
    if ([self.secondTf.text isEmpty]) {
        [self showMessage:@"请确认新密码"];
        return;
    }
    if (![self.firstTf.text isEqualToString:self.secondTf.text]) {
        [self showMessage:@"两次输入密码不一致,请重新输入"];
        return;
    }
    
    /*if (self.contentType == TypeForget) {
        //忘记密码重置
        [APIHELPER resetPW:self.phone code:self.secondTf.text password:self.firstTf.text complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            if (isSuccess) {
                [self showMessage:@"重置密码成功"];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
            }
        }];
    }else{
        //修改密码
        [APIHELPER changePassword:self.firstTf.text captcha:self.secondTf.text complete:^(BOOL isSuccess, NSDictionary *responseObject, NSError *error) {
            if (isSuccess) {
                [self showMessage:@"修改密码成功"];
                [APIHELPER logout];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                [self showMessage:error.userInfo[NSLocalizedDescriptionKey]];
            }
        }];
    }*/
}

@end
