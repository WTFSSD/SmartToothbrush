//
//  LoginViewController.m
//  Base
//
//  Created by admin on 2017/1/17.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "LoginViewController.h"
#import "UIViewController+Extension.h"
#import "NSString+HYUtilities.h"
#import "JPUSHService.h"
#import "RegistViewController.h"
#import "ForgotController.h"
#import "ShareSDKThirdLoginHelper.h"
#import "ConectStep1ViewController.h"

#import "WKWebViewController.h"
@interface LoginViewController ()<ShareSDKThirdLoginHelperDelegate>

@property (weak, nonatomic) IBOutlet UIView* accountView;
@property (weak, nonatomic) IBOutlet UIView* passwordView;
@property (weak, nonatomic) IBOutlet UITextField *accountTf;
@property (weak, nonatomic) IBOutlet UITextField *passwordTf;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIView *weixinView;
@property (weak, nonatomic) IBOutlet UIView *facebookView;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.passwordTf.secureTextEntry = YES;
    if (self.schemaArgu[@"phone"]) {
        self.accountTf.text = [self.schemaArgu objectForKey:@"phone"];
    }
    [self subviewStyle];
    [self subviewBind];
    
//    self.accountTf.text = @"1114784112@qq.com";
//    self.passwordTf.text = @"111111";

//    self.accountTf.text = @"378205481@qq.com";
//    self.passwordTf.text = @"000000";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNotice:) name:@"com.udo.nav.backButtonClick" object:nil];
}


-(void)onNotice:(NSNotification*)notice{
    if([notice.name isEqualToString:@"com.udo.nav.backButtonClick"]){
        if(self.navigationController.viewControllers.count==1){
            AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [delegate configTabbar];
        }
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)baseSetBackgroundImage{}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)subviewStyle {
    self.title = @"登陆";
    self.navigationItem.title = @"登录";
    self.navigationBarBlue = NO;
    self.navigationBarTransparent = NO;
    for (UIView* view  in @[self.accountView,self.passwordView]) {
        [HYTool configViewLayer:view size:5];
        [HYTool configViewLayer:view withColor:[UIColor colorWithString:@"dddddd"]];
    }
    [HYTool configViewLayer:self.loginBtn size:20];
    self.loginBtn.bounds = CGRectMake(0, 0, zoom(200), 40);
    [HYTool configGradualBlueView:self.loginBtn];
    if (kAccount) {
        self.accountTf.text = kAccount;
    }
    if (kPassword) {
        self.passwordTf.text = kPassword;
    }
}

- (void)subviewBind {
    [self.loginBtn addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    ShareSDKThirdLoginHelper* helper = [ShareSDKThirdLoginHelper shareThirdLoginHelper];
    helper.delegate = self;
    [self.weixinView bk_whenTapped:^{
        //微信登录
        [helper loginWithIndex:0];
    }];
    [self.facebookView bk_whenTapped:^{
        WKWebViewController * vc = [WKWebViewController instance];
        vc.vcTitle = @"facebook登录";
        vc.url = API_USER_FBLOGIN;
        vc.autoLoad = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }];
}

- (void)login{
    if ([self.accountTf.text isEmpty]) {
        [MBProgressHUD hy_showMessage:@"请输入手机号或邮箱" inView:self.view];
        return;
    }
    if ([self.passwordTf.text isEmpty]) {
        [MBProgressHUD hy_showMessage:@"请输入密码" inView:self.view];
        return;
    }
    [MBProgressHUD hy_showLoadingHUDAddedTo:self.view animated:YES];
    
    
    
    [NetworkUtils requestWith:HTTPMethodPOST
                          url:REQUEST_URL(API_LOGIN) params:@{@"account":self.accountTf.text,@"password":self.passwordTf.text}
                      headers:nil
                     progress:nil
                          obj:[UserInfoModel class]]
    .then(^AnyPromise*(id res){
        if(!res) return nil;
        [self hideLoadingAnimation];
        [[UserInfoModel share] yy_modelSetWithJSON:[res yy_modelToJSONString]];
        [UserInfoModel share].isLogin = YES;
        [UserInfoModel store];

        
        return [NetworkUtils requestWith:HTTPMethodGET url:REQUEST_URL(API_MEMBER_MY_TOOTHBRUSH) params:@{@"key":[UserInfoModel share].key}];
    }).then(^(id res){
        if(res && [res[@"code"] integerValue] == 1) {
            kSaveAccountAndPassword(self.accountTf.text, self.passwordTf.text)
        }
        if(res && [res[@"code"] integerValue] == 1 && [res[@"data"] count]>0){
            AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [delegate configTabbar];
        }else{
            AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [delegate configConnectWith:@{@"from":self}];
        }
        
    });
}
- (IBAction)regist:(id)sender {
    APPROUTE(kRegistViewController);
}

- (IBAction)forgot:(id)sender {
    APPROUTE(kForgotViewController);

   }

#pragma mark - ShareSDKThirdLoginHelperDelegate
- (void)thirdLogin:(NSString *)openId platform:(NSInteger)platform {
    
    
    ///微信登录
    if(platform == 0){
        [NetworkUtils requestWith:HTTPMethodPOST
                              url:REQUEST_URL(API_USER_WXLOGIN) params:@{@"code":openId} headers:nil progress:nil obj:[UserInfoModel class]].then(^AnyPromise*(id res){
            if(!res) return nil;
            [self hideLoadingAnimation];
            [[UserInfoModel share] yy_modelSetWithJSON:[res yy_modelToJSONString]];
            [UserInfoModel share].isLogin = YES;
            [UserInfoModel store];
            kSaveAccountAndPassword(self.accountTf.text, self.passwordTf.text)
            
            return [NetworkUtils requestWith:HTTPMethodGET url:REQUEST_URL(API_MEMBER_MY_TOOTHBRUSH) params:@{@"key":[UserInfoModel share].key}];
        }).then(^(id res){
            if(res && [res[@"code"] integerValue] == 1 && [res[@"data"] count]>0){
                AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [delegate configTabbar];
            }else{
                AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [delegate configConnectWith:@{@"from":self}];
            }
        });
    }
    
    
}



@end
