//
//  HomeViewController.m
//  Base
//
//  Created by admin on 17/1/16.
//  Copyright © 2017年 XHY. All rights reserved.
//


#import "HomeViewController.h"


#import "DHGuidePageHUD.h"


#import "UIViewController+KNSemiModal.h"

#import "HomeTopCell.h"


#import "HomeToothInfoView.h"
#import "HomeToothMoreInfoView.h"


#import "HomeDataModel.h"
@interface HomeViewController ()<UIScrollViewDelegate> {
    UIScrollView * containerView;
    HomeToothInfoView * infoView;
    
    HomeToothMoreInfoView * moreInfoView;
    
    
    HomeModel * model;
}
@property(nonatomic,retain) ToothStatusTimerView * statusView;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    
    self.navigationBarTransparent = YES;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onLocalNotice:) name:@"com.udo.brush.startBrushing" object:nil];
    
    
    
    
    
//    NSMutableDictionary * params = [NSMutableDictionary dictionary];
//    params[@"key"] = [UserInfoModel share].key;
//
//
//    @weakify(infoView);
//    [NetworkUtils requestWith:HTTPMethodGET url:REQUEST_URL(API_MEMBER_INDEX) params:params headers:nil progress:nil obj:[HomeModel  class]].then(^(HomeModel * res){
//        @strongify(infoView)
//        if(res){
//            infoView.battery.quantity = [res.electric floatValue];
//            infoView.brush.replaceDay = [res.day floatValue];
//            [moreInfoView.score.chart setDatas:[res.week bk_map:^id(ScoreModel *  obj) {
//                return [ChartViewData instanceWith:obj.date score:[obj.score floatValue]];
//            }]];
//        }
//    });
}



-(void)onLocalNotice:(NSNotification*)notice{
    if([notice.name isEqualToString:@"com.udo.brush.startBrushing"]){
        [infoView startTimerWith:BrushTime1];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(!containerView) return;
     containerView.contentSize = CGSizeMake(0, infoView.frame.size.height + moreInfoView.frame.size.height);
}

#pragma mark - private methods
- (ToothStatusTimerView *)statusView {
    if (!_statusView) {
        _statusView = [ToothStatusTimerView instance];
    }
    return _statusView;
}

-(void)setUp{
    self.view.backgroundColor = [UIColor whiteColor];
//    self.backItemHidden = YES;
//    self.navigationLineHidden = YES;
    
    UIBarButtonItem* message = [[UIBarButtonItem alloc] initWithImage:[ImageNamed(@"ico_message") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(message)];
    self.navigationItem.leftBarButtonItem = message;
    UIBarButtonItem* voice = [[UIBarButtonItem alloc] initWithImage:[ImageNamed(@"ico_voice") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(voice)];
    self.navigationItem.rightBarButtonItem = voice;
    
  
    containerView = [[UIScrollView alloc] initWithFrame:CGRectZero];
    [self.view insertSubview:containerView aboveSubview:self.backgroundImage];
    containerView.backgroundColor = [UIColor clearColor];
    
    
    [containerView autoCenterInSuperview];
    [containerView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:self.view];
    [containerView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view];
    
    infoView = [HomeToothInfoView instance];
    [containerView addSubview:infoView];
    [infoView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view];
    [infoView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:self.view];
    [infoView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:containerView];
    [infoView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:containerView];
    
    
    moreInfoView = [HomeToothMoreInfoView instance];
    [containerView addSubview:moreInfoView];
    
    [moreInfoView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view];
    [moreInfoView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:self.view];
    [moreInfoView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:infoView];
    [moreInfoView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:containerView];
    
    containerView.contentSize = CGSizeMake(0, 2*kScreen_Height);
    
    containerView.pagingEnabled = YES;
}

- (void)message {
    
    
    NSString * userID =  [UserInfoModel share].userID;
    
    
    [NetworkUtils requestWith:HTTPMethodPOST url:REQUEST_URL(API_MEMBER_BIND_TOOTHBRUSH) params:@{@"tb_no":@"F0FE6BF3A076",@"nickname":@"23333",@"is_my":@"1"}].then(^(NSDictionary * res){
        if(res){
            NSLog(@"牙刷绑定成功!!!!");
        }
    });
}

- (void)voice {
    APPROUTE(kVoiceSetController);
}





@end
