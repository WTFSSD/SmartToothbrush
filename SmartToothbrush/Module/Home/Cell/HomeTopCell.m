//
//  HomeTopCell.m
//  Base
//
//  Created by admin on 2017/5/4.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import "HomeTopCell.h"
#import "CustomJumpBtns.h"

@interface HomeTopCell ()


@property (weak, nonatomic) IBOutlet UIView *timeView;
@property (weak, nonatomic) IBOutlet UIView *toothView;

@property (weak, nonatomic) IBOutlet UIView *modeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modeViewHeight;
@property (weak, nonatomic) IBOutlet UIImageView *powerImgV;
@property (weak, nonatomic) IBOutlet UILabel *powerLbl;
@property (weak, nonatomic) IBOutlet UIView *shadowView;

@property (strong,nonatomic) ToothStatusTimerView *statusView;

@end

@implementation HomeTopCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.modeViewHeight.constant = kScale_height*42;
}

+(NSString *)identify {
    return NSStringFromClass([self class]);
}

-(void)configTopCell:(id)model statusView:(ToothStatusTimerView*)statusView {
    if (self.modeView.subviews.count == 0) {
        CustomJumpBtns* modes = [CustomJumpBtns customBtnsWithFrame:CGRectMake(0, 0, kScreen_Width, kScale_height*42) menuTitles:@[@"美白",@"清洁",@"敏感",@"定制"] textColorForNormal:[UIColor colorWithString:@"cde8fe"] textColorForSelect:[UIColor whiteColor] botImage:@"button_tab" isLineAdaptText:YES];
        @weakify(self);
        [modes setFinished:^(NSInteger index) {
            @strongify(self);
            if (self.selectMode) {
                self.selectMode(index);
            }
        }];
        [self.modeView addSubview:modes];
        [modes autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, 0, 0, 0) excludingEdge:ALEdgeTop];
        [modes autoSetDimension:ALDimensionHeight toSize:kScale_height*42];
    }
    if (self.timeView.subviews.count == 0) {
        self.statusView = statusView;
        [self.timeView addSubview:self.statusView];
        [self.statusView setSize:CGSizeMake(zoom(117), zoom(117))];
        [self.timeView addSubview:self.statusView];
        [self.statusView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
    }
}

+(CGFloat)height {
    return kScale_height*470;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
