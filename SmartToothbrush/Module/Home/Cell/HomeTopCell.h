//
//  HomeTopCell.h
//  Base
//
//  Created by admin on 2017/5/4.
//  Copyright © 2017年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToothStatusTimerView.h"
#import "BatteryView.h"
@interface HomeTopCell : UITableViewCell

@property(nonatomic,copy)void (^selectMode)(NSInteger index);
@property(nonatomic,copy)void (^startBrush)();

+(NSString*)identify;
-(void)configTopCell:(id)model statusView:(ToothStatusTimerView*)statusView;
+(CGFloat)height;

@end
