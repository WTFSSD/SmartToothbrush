//
//  ToothStatusView.h
//  Base
//
//  Created by WTFSSD on 2018/4/25.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToothStatusTimerView.h"

typedef NS_ENUM(NSUInteger, ToothStatusViewStauts) {
    
    /** 区域 无*/
    a_no = 1<<1,
    ///区域 全
    a_all=1<<2,
    ///区域 内圈
    a_side1 =1<<3,
    ///区域 外圈
    a_side2 =1<<4,
    
    ///颜色 红
    c_red = 1<<5,
    ///颜色 绿
    c_green = 1<<6,
    ///颜色 黄              
    c_yellow = 1<<7,
};

@interface ToothStatusView : UIView



/// 工厂方法 生成实例
+(instancetype)instance;



-(void)startAnimateWith:(BrushTime)timeMode;

-(void)stopAnimate;


/**设置牙齿状态*/
-(void)setStatusWithT:(ToothStatusViewStauts)T
                    B:(ToothStatusViewStauts)B
                    LT:(ToothStatusViewStauts)LT
                    LB:(ToothStatusViewStauts)LB
                    RT:(ToothStatusViewStauts)RT
                    RB:(ToothStatusViewStauts)RB;

@end
