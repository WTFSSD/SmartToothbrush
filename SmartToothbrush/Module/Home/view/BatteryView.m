//
//  BatteryView.m
//  Base
//
//  Created by WTFSSD on 2018/4/27.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "BatteryView.h"


@interface BatteryView (){
    CAShapeLayer * cycleLayer;
    
    
    
    UILabel * quantityLabel ;
    
    
    UIColor * statusColor;
}
@end

@implementation BatteryView

+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectMake(0, 0, 84, 100)];
}


-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}

//-(void)setQuantity:(CGFloat)quantity{
//    if(quantity<0 || quantity > 1) return;
//    _quantity = quantity;
//     quantityLabel.text = [NSString stringWithFormat:@" 剩余:%.0f%%",_quantity * 100];
//    [self setNeedsDisplay];
//}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
        
    
    }
    return self;
}


-(void)setUp{
    self.backgroundColor = [UIColor clearColor];
    _quantity = 1;
    
    
    _enough = [UIColor greenColor];
    
    _warn = [UIColor orangeColor];
    
    _danger = [UIColor redColor];
    
    quantityLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    
    quantityLabel.textColor = UIColorFromHEX(0xffffff);
    
    quantityLabel.font = [UIFont systemFontOfSize:14];
    
    quantityLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:quantityLabel];
    
    [quantityLabel autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self];
    [quantityLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
    
    RACSignal * signal = [self rac_valuesForKeyPath:@"quantity" observer:self];
    
    
    
 [signal subscribeNext:^(id value) {
        CGFloat q = [value floatValue];

        quantityLabel.text = [NSString stringWithFormat:@"电量:%.0f%%",q * 100];
     [self setNeedsDisplay];
        
    }];
}


-(void)drawRect:(CGRect)rect{
    [self drawCycle:rect];
    
    CGPoint center = CGPointMake(rect.size.width/2, zoom(20) + 2);
    CGContextRef contex = UIGraphicsGetCurrentContext();

    

    //矩形1宽度
    CGFloat rect1w = 6;
    //矩形1高度
    CGFloat rect1h = 4;
     //矩形2宽度
    CGFloat rect2w = 8;
    //矩形2高度
    CGFloat rect2h = 16;
    
    
  
    //画笔颜色
    UIColor * c = UIColorFromHEX(0xffffff);
    

    CGContextSetLineWidth(contex, 2);
    CGContextSetStrokeColorWithColor(contex, c.CGColor);
    
    CGRect rect1 =  CGRectMake(center.x-rect1w/2, center.y-rect2h/2-rect1h, rect1w, rect1h);
    
    CGRect rect2 =  CGRectMake(center.x-rect2w/2, center.y-rect2h/2, rect2w, rect2h);
//    CGContextAddRect(contex,rect1);
    
    [self drawRoundRect:contex rect:rect1 r1:1 r2:1 r3:0 r4:0];
    CGContextStrokePath(contex);
    
//    CGContextAddRect(contex, rect2);
     [self drawRoundRect:contex rect:rect2 r1:1 r2:1 r3:1 r4:1];
    
    CGContextStrokePath(contex);
    
    
    CGContextSetLineWidth(contex, 0);
    
    
    
    if(_quantity <= 0){
        return ;
    }
    CGFloat offset = 3;
    if(_quantity>=0){
        offset = 0;
    }
    
    //画笔颜色
    // 电池刻度线
    UIColor * clear =[UIColor clearColor];
    

    CGContextSetStrokeColorWithColor(contex, clear.CGColor);
    
    
    //填充颜色
    CGContextSetFillColorWithColor(contex, [self getBatterColor].CGColor);
    CGPoint lb = [self getLBPoint:rect2];
    CGPoint rb = [self getRBPoint:rect2];
    CGFloat pd = 1.5;
    
    
    CGContextMoveToPoint(contex, lb.x  + pd, lb.y - pd);
    CGContextAddLineToPoint(contex, rb.x - pd , rb.y - pd);
    

    
    CGFloat _q = _quantity<0?1:_quantity>1?1:_quantity;
    
    //电池刻度线开始点
    CGPoint start = CGPointMake( rb.x - pd , rb.y -rect2h * _q);
    
    //电池刻度线结束点
    CGPoint end = CGPointMake(lb.x + pd , rb.y - rect2h * _q);
    
    CGContextAddLineToPoint(contex,start.x , start.y);
   
    
    
   
#ifndef BatteryViewBezierLine
    
    CGContextAddLineToPoint(contex, end.x ,end.y - offset);
#else
    //二次贝塞尔曲线
    
    CGPoint c1 = CGPointMake(end.x + (start.x - end.x)/4 * 3 + 2, end.y + 2);
    CGPoint c2 = CGPointMake(end.x + (start.x - end.x)/4 * 1 + 2, end.y - 2);
    CGContextAddCurveToPoint(contex, c1.x, c1.y, c2.x, c2.y, end.x, end.y);
#endif
    

    CGContextFillPath(contex);

}


//顺时钟绘画 左上角 圆角矩形
-(void)drawRoundRect:(CGContextRef)context rect:(CGRect)rect r1:(CGFloat)r1 r2:(CGFloat)r2 r3:(CGFloat)r3 r4:(CGFloat)r4{
    
    CGContextMoveToPoint(context, rect.origin.x, rect.origin.y+r1);
    
    
    CGContextAddArc(context, CGRectGetMinX(rect)+r1, rect.origin.y+r1, r1, M_PI, -M_PI/2, NO);
    
    CGContextAddLineToPoint(context, CGRectGetMaxX(rect)-r2, CGRectGetMinY(rect));
    
    
    CGContextAddArc(context, CGRectGetMaxX(rect)-r2, CGRectGetMinY(rect)+r2, r2, -M_PI/2, 0, NO);
    
    CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMaxY(rect)-r3);
    
    
    CGContextAddArc(context, CGRectGetMaxX(rect)-r3, CGRectGetMaxY(rect)-r3, r3, 0 , M_PI/2, NO);
    
    CGContextAddLineToPoint(context, CGRectGetMinX(rect)+r4, CGRectGetMaxY(rect));
    
    
    CGContextAddArc(context, CGRectGetMinX(rect)+r4, CGRectGetMaxY(rect)-r4, r4, M_PI/2, M_PI ,NO);
    
    CGContextAddLineToPoint(context, CGRectGetMinX(rect), CGRectGetMinY(rect) + r1);
    
    //     CGContextAddRect(context, rect);
}



/**
 *  获得电量显示颜色
 *
 * @return UIColor *
 */
-(UIColor * )getBatterColor{
    
    
//    assert(_quantity<0 || _quantity>1 ,@"电量不合法");
    NSAssert(_quantity>=0 && _quantity<=1 ,@"电量不合法");
    
    
    if(_quantity<= 0.2){
        if(_danger) return _danger;
        return [UIColor redColor];
    }
    if(_quantity>= 0.8){
        if(_enough) return _enough;
        return [UIColor greenColor];
    }
    
    if(_warn) return _warn;
    
    return [UIColor orangeColor];
}

/// 获得 矩形左下角 坐标
-(CGPoint)getLBPoint:(CGRect)rect{
    return CGPointMake(rect.origin.x, rect.origin.y + rect.size.height);
}


/// 获得 矩形右下角 坐标
-(CGPoint)getRBPoint:(CGRect)rect{
    return CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height);
}

-(void)drawCycle:(CGRect)rect{
    
    CGPoint center = CGPointMake(rect.size.width/2, zoom(20) + 2);
    
    if(cycleLayer){
        [cycleLayer removeFromSuperlayer];
        [cycleLayer removeAllAnimations];
    }else{
        cycleLayer = [CAShapeLayer layer];
    }
    [cycleLayer setFillColor:[[UIColor clearColor] CGColor]];
    cycleLayer.lineWidth = 4;
    cycleLayer.strokeColor = [UIColorFromHEX(0x3ab4e8) CGColor];
    
    
    UIBezierPath * path = [UIBezierPath bezierPath];
    [path addArcWithCenter:center radius:zoom(20) startAngle:0 endAngle:M_PI * 2 clockwise:YES];
    cycleLayer.path = path.CGPath;
    [self.layer addSublayer:cycleLayer];
}


@end
