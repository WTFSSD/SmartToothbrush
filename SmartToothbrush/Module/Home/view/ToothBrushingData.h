//
//  ToothBrushingData.h
//  Base
//
//  Created by WTFSSD on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToothBrushingData : UIView

/// 标题 与 titles 属性 二取其一 titles 优先级高
@property(nonatomic,copy)NSString* title;


/// 两个牙齿图标题 与 titles 属性 二取其一 titles 优先级高
@property(nonatomic,retain)NSArray<NSString*>*titles;



@property(nonatomic,assign)CGFloat test;
+(instancetype)instance;
@end
