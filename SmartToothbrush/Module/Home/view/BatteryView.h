//
//  BatteryView.h
//  Base
//
//  Created by WTFSSD on 2018/4/27.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>

//#define BatteryViewBezierLine


@interface BatteryView : UIView




/** 电量充足颜色 quantity > 0.8 */
@property(nonatomic,strong) UIColor * enough;

/** 电量一般颜色 quantity >0.3 <0.8 */
@property(nonatomic,strong) UIColor * warn;

/** 电量 不足 颜色 quantity<0.3 */
@property(nonatomic,strong) UIColor * danger;


/**电量 范围 0.0 ~ 1.0 default = 1.0 */
@property(nonatomic,assign)CGFloat quantity;

/**
 *  工厂方法获取实例
 */
+(instancetype)instance;

@end
