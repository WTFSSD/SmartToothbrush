//
//  ToothStatusTimerView.m
//  Base
//
//  Created by WTFSSD on 2018/4/25.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "ToothStatusTimerView.h"




@interface ToothStatusTimerView(){
    UIView * containerView;
    
    
    CAShapeLayer * borderLayer;
    
    CAShapeLayer * cycleLayer;
    
    
    
    NSMutableArray * lines;
    
    
    
    NSInteger time;
    
    
    
    UILabel * timeLabel;

    
    NSTimer * timer;
    
}


@end
@implementation ToothStatusTimerView


static const NSInteger MAX_COUNT = 60;


-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}
-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
    
        self.max = 60;
        [self setUp];

    }
    return self;
}

-(void)setUp{
   
    _normalColor = UIColorFromHEX(0xffffff);
    
    self.timeMode = BrushTimeNone;
    
    _heightColor = UIColorFromHEX(0x1584e4);
    
    self.backgroundColor = UIColorFromHEX(0x0d57c9);
    
    timeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    timeLabel.textColor = UIColorFromHEX(0xeddd37);
    
    
    timeLabel.text = [NSString stringWithFormat:@"%lds",(long)self.max];
    timeLabel.font = [UIFont systemFontOfSize:25];
    [self addSubview:timeLabel];
    [timeLabel autoCenterInSuperview];
    [timeLabel sizeToFit];
  

   
}

+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}

-(void)timerEvent{
    time ++;
    timeLabel.text = [NSString stringWithFormat:@"%lds",(long)self.max - time];
    [self setNeedsDisplay];
    if(time >= self.max){
        [self stop];
    }
}


//开始
-(void)start{
    if(timer){
        return;
    }else{
        time = 0;
        switch (self.timeMode) {
            case BrushTimeNone: self.max = 0;break;
            case BrushTime1: self.max = 60;break;
            case BrushTime2: self.max = 105;break;
            case BrushTime3: self.max = 120;break;
            case BrushTime4: self.max = 150;break;
            case BrushTime5: self.max = 180;break;
            default:
                break;
        }
        if(self.max == 0) return;
        [self reset];
        
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerEvent) userInfo:nil repeats:YES];
    }
}

//暂停
-(void)pause{
    if(!timer) return;
    [timer setFireDate:[NSDate distantFuture]];
}

//停止
-(void)stop{
    if(!timer) return;
    [timer invalidate];
    timer = nil;
    [self reset];
    if(self.finish){
        self.finish();
    }
}

//重置
-(void)reset{
    time = 0;
    timeLabel.text = [NSString stringWithFormat:@"%lds",(long)self.max];
    [self setNeedsDisplay];
}


-(void)layoutSubviews{
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.frame.size.width/2;
    self.layer.masksToBounds = YES;
    
}

-(void)drawRect:(CGRect)rect{
    

    if(borderLayer){
        [borderLayer removeFromSuperlayer];
        [cycleLayer removeFromSuperlayer];
       
    }else{
        borderLayer = [[CAShapeLayer alloc] init];
        borderLayer.frame = CGRectMake(0, 0, rect.size.width, rect.size.height);
        cycleLayer = [[CAShapeLayer alloc] init];
        cycleLayer.frame = CGRectMake(0, 0, rect.size.width, rect.size.height);
    }
    
    
     //外围边框虚线
    borderLayer.lineWidth = 1.5;
    [borderLayer setStrokeColor:[RGB(255, 255, 255, 0.6) CGColor]];
    [borderLayer setLineDashPattern:@[@(4),@(4)]];
    [borderLayer setFillColor:[[UIColor clearColor] CGColor]];
    
    UIBezierPath *borderPath =   [UIBezierPath bezierPath];
    [borderPath addArcWithCenter:CGPointMake(rect.size.width/2, rect.size.width/2) radius:rect.size.width/2 - 5 startAngle:0 endAngle:M_PI * 2 clockwise:true];
    borderLayer.path = borderPath.CGPath;
    
    
    
    //圆形覆盖层
    UIBezierPath * path = [UIBezierPath bezierPath];
    [path addArcWithCenter:CGPointMake(rect.size.width/2, rect.size.width/2) radius:rect.size.width/2 startAngle:0 endAngle:M_PI * 2 clockwise:true];
    [cycleLayer setFillColor:[UIColorFromHEX(0x0d57c9) CGColor]];
    [cycleLayer setLineWidth:1];
    [self.layer addSublayer:cycleLayer];
    [self.layer addSublayer:borderLayer];
    [self drawTicks:rect];
}

-(CGPoint)centerFrom:(CGFloat)x1 y1:(CGFloat)y1 x2:(CGFloat)x2 y2:(CGFloat)y2{
    return CGPointMake((x1+x2)/2.f, (y1+y2)/2.f);
}




///绘制 刻度
-(void)drawTicks:(CGRect)rect{
    
    if(self.max<=0) return;
    CGPoint center = CGPointMake(rect.size.width/2, rect.size.width/2);
    CGFloat radius = rect.size.width/2 - 25;
    CGFloat length = 8;
    //获得绘画句柄
    CGContextRef contex = UIGraphicsGetCurrentContext();
    //设置画笔 宽度
    CGContextSetLineWidth(contex, 1);
    for (NSInteger index = 0; index<MAX_COUNT; index++) {
        CGFloat angle = (M_PI * 2.0) /MAX_COUNT * index + M_PI;
            CGFloat startX1 = sinf(angle) * radius + center.x;
            CGFloat startY1 = cosf(angle) * radius + center.x;
            CGFloat endX1 = sinf(angle) * (radius + length) + center.x;
            CGFloat endY1 = cosf(angle) * (radius + length) + center.y;
        if((_max - ceil(index * _max / MAX_COUNT)) <= time){
            
            CGContextSetStrokeColorWithColor(contex, _normalColor.CGColor);
        }else{
            CGContextSetStrokeColorWithColor(contex, _heightColor.CGColor);
        }
        CGContextMoveToPoint(contex, startX1, startY1);
        CGContextAddLineToPoint(contex, endX1, endY1);
        CGContextStrokePath(contex);
    }
}

- (void)setSize:(CGSize)size {
    [self autoSetDimensionsToSize:size];
}

@end
