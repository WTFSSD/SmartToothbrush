//
//  ToothbrushStatusView.m
//  Base
//
//  Created by WTFSSD on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "ToothbrushStatusView.h"

@interface ToothbrushStatusView(){
    UIImageView * brushImg;
    UILabel * tip;
}

@end;

@implementation ToothbrushStatusView

+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}

-(void)setUp{
    brushImg = [[UIImageView alloc] initWithImage:ImageNamed(@"expression_toothbrush")];
    [self addSubview:brushImg];
    tip = [UILabel makeWith:^(UILabel *label) {
        label.textColor = [UIColor whiteColor];
    }];
    [self addSubview:tip];
    
    
    [brushImg autoSetDimension:ALDimensionWidth toSize:zoom(40)];
    [brushImg autoSetDimension:ALDimensionHeight toSize:zoom(40)];
    [brushImg autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self];
    [brushImg autoAlignAxis:ALAxisVertical toSameAxisOfView:self];
    
    
    [tip autoAlignAxis:ALAxisVertical toSameAxisOfView:self];
    [tip autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:brushImg];
    [tip autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self];
    
    RAC(tip,attributedText) = [[self rac_valuesForKeyPath:@"replaceDay" observer:self] map:^id(id value) {
        
        CGFloat f = [value floatValue];
        NSMutableAttributedString * s = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"刷头%02.0f天后更换",f]];
        UIFont  * str = [UIFont systemFontOfSize:13];
        UIFont  * n = [UIFont systemFontOfSize:20];
        [s setAttributes:@{NSFontAttributeName:str} range:NSMakeRange(0, 2)];
        [s setAttributes:@{NSFontAttributeName:n} range:NSMakeRange(2, 2)];
        [s setAttributes:@{NSFontAttributeName:str} range:NSMakeRange(4, 4)];
        return s;
    }];
}
@end
