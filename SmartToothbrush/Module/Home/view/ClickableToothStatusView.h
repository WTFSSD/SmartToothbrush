//
//  ClickableToothStatusView.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/8/8.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClickableToothStatusView : UIControl


///显示每一颗牙齿的边框
@property(nonatomic,assign)BOOL showBorder;
+(instancetype)instance;
@end
