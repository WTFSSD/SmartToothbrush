//
//  ChartView.h
//  Base
//
//  Created by WTFSSD on 2018/4/29.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 * 图表数据
 */
@interface ChartViewData:NSObject

@property(nonatomic,copy)NSString * time;

@property(nonatomic,assign)CGFloat score;

+(instancetype)instanceWith:(NSString *)time score:(CGFloat)score;


/** 将图表数据转换成json*/
+(NSString * )tramforArray:(NSArray<ChartViewData*> *)array;

@end


@interface ChartView : UIView


//测试随机数据
@property(nonatomic,assign)BOOL testData;

///获取 图表空间实例
+(instancetype)instance;


/**
 *  设置图表 数据源
 */
-(void)setDatas:(NSArray<ChartViewData*>*)data;


/** 刷新 */
-(void)refresh;

/** 随机图表数据*/
-(void)randData;

@end


