//
//  ToothStatusTimerView.h
//  Base
//
//  Created by WTFSSD on 2018/4/25.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ToothStatusTimerView : UIView


//定时最大值 default 60 (单位s)
@property(nonatomic,assign)NSInteger max;


//刻度 常态颜色 default = UIColorFromHEX(0xffffff)
@property(nonatomic,retain)UIColor * normalColor;

//刻度 高亮颜色 default = UIColorFromHEX(0x1584e4)
@property(nonatomic,retain)UIColor * heightColor;


@property(nonatomic,assign)BrushTime timeMode;

@property(nonatomic,copy)void (^finish)(void);

//开始
-(void)start;

//暂停
-(void)pause;

//停止
-(void)stop;

//重置
-(void)reset;


- (void)setSize:(CGSize)size;


+(instancetype)instance;
@end
