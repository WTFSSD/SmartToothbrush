//
//  ToothStatusView.m
//  Base
//
//  Created by WTFSSD on 2018/4/25.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "ToothStatusView.h"

@interface ToothStatusView(){
//    //左上
//    UIImageView * lt;
//    //右上
//    UIImageView * rt;
//    //左下
//    UIImageView * lb;
//    //右下
//    UIImageView * rb;
//    //上
//    UIImageView * t;
//    //下
//    UIImageView * b;
    UIImageView * container;
    
    
    NSMutableArray * images;
    
    
    NSArray * cycleImageNames;
    
    
    NSInteger cycleImageIndex;
    
    NSTimer * cycleAnimateTimer;

    NSArray * cycleAnimateSchedules;
    
    
    
}

@end

@implementation ToothStatusView

const static NSInteger t_index = 1;
const static NSInteger b_index = 2;
const static NSInteger lt_index = 3;
const static NSInteger lb_index = 4;
const static NSInteger rt_index = 5;
const static NSInteger rb_index = 6;




-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}
+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        self.backgroundColor = [UIColor clearColor];
        [self setUp];
    }
    return self;
}



-(void)startAnimateWith:(BrushTime)timeMode{
    if(timeMode == BrushTimeNone) return;
    if(cycleAnimateTimer) return;
    
    
    __block NSInteger count = 0;
    
    NSMutableArray * changeIndex = nil;
    
    switch (timeMode) {
        case BrushTime1:
            changeIndex = [NSMutableArray arrayWithArray:cycleAnimateSchedules[0]];
            break;
        case BrushTime2:
            changeIndex = [NSMutableArray arrayWithArray:cycleAnimateSchedules[1]];
            
            break;
        case BrushTime3:
            changeIndex = [NSMutableArray arrayWithArray:cycleAnimateSchedules[2]];
            break;
        case BrushTime4:
            changeIndex = [NSMutableArray arrayWithArray:cycleAnimateSchedules[3]];
            break;
        case BrushTime5:
            changeIndex = [NSMutableArray arrayWithArray:cycleAnimateSchedules[4]];
            break;
            
        default:
            break;
    }
    
    if(!changeIndex || changeIndex.count== 0 ) return;
    
    
    NSInteger max = [changeIndex.lastObject integerValue];
    
    cycleAnimateTimer = [NSTimer bk_scheduledTimerWithTimeInterval:1 block:^(NSTimer *timer) {
        
        count++;
        
        if(count>=max){
            [self stopAnimate];
        }
        
        for (NSInteger i = changeIndex.count-1; i>=0; i--) {
            NSInteger temp = [changeIndex[i] integerValue];
            if( count > temp && temp!=-1 ){
                changeIndex[i] = @(-1);
                cycleImageIndex++;
                break;
            }
        }
        
//        cycleImageIndex ++ ;
        if(cycleImageIndex == cycleImageNames.count){
            cycleImageIndex = 0;
        }
         [self setNeedsLayout];
    } repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:cycleAnimateTimer forMode:NSRunLoopCommonModes];
}

-(void)stopAnimate{
    cycleImageIndex = -1;
    [cycleAnimateTimer invalidate];
    cycleAnimateTimer = nil;
    [self setNeedsLayout];
}
///初始化UI
-(void)setUp {
    container = [[UIImageView alloc] init];
    cycleImageNames = @[
                        
                        @"area_brush1",
                        @"area_brush2",
                        @"area_brush3",
                        @"area_brush4",
                        @"area_brush5",
                        @"area_brush6",
                        ];
    cycleImageIndex = -1;
    images = [NSMutableArray arrayWithObjects:ImageNamed(@"tooth0_all"),
                [UIImage new],
                [UIImage new],
                [UIImage new],
                [UIImage new],
                [UIImage new],
                [UIImage new],
              nil];
    
    cycleAnimateSchedules = @[
                              
                              //1      2      3     4    5    6
                              @[@(0),@(12),@(18),@(30),@(42),@(48),@(60)],
                              @[@(0),@(18),@(29),@(47),@(65),@(87),@(105)],
                              @[@(0),@(20),@(40),@(60),@(80),@(100),@(120)],
                              @[@(0),@(30),@(45),@(75),@(105),@(120),@(150)],
                              @[@(0),@(35),@(55),@(90),@(125),@(145),@(180)],
                              ];
    
    //状态 red green yellow
    //图片 tooth + 数字 + _面积+_状态
    //上下图片  1(上) 2(下) all(满) b(半) t(1/4)
    //
    [self addSubview:container];

    [container autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];

}

-(void)clear{
//    t.image = [UIImage new];
//    b.image = [UIImage new];
//    lb.image = [UIImage new];
//    lt.image = [UIImage new];
//    rb.image = [UIImage new];
//    rt.image = [UIImage new];
    
    
    
    images[t_index] =[UIImage new];
    images[t_index] =[UIImage new];
    images[rt_index] =[UIImage new];
    images[rb_index] =[UIImage new];
    images[lt_index] =[UIImage new];
    images[lb_index] =[UIImage new];
   
}
-(void)setStatusWithT:(ToothStatusViewStauts)T
                    B:(ToothStatusViewStauts)B
                    LT:(ToothStatusViewStauts)LT
                    LB:(ToothStatusViewStauts)LB
                    RT:(ToothStatusViewStauts)RT
                    RB:(ToothStatusViewStauts)RB{
    [self clear];
    if([self getTBA:T]){
        
        NSString * s = [NSString stringWithFormat:@"tooth1_%@_%@",[self getTBA:T],[self getColorWith:T]];
        NSLog(@"t:%@",s);
//        t.image = ImageNamed(s);
        images[t_index] = ImageNamed(s);
    }
    if([self getTBA:B]){
        
        NSString * s = [NSString stringWithFormat:@"tooth2_%@_%@",[self getTBA:B],[self getColorWith:B]];
        NSLog(@"b:%@",s);
//        b.image = ImageNamed(s);
        images[b_index] = ImageNamed(s);
    }
    if([self getOA:LT]){
        
        NSString * s = [NSString stringWithFormat:@"tooth3_%@_%@",[self getOA:LT],[self getColorWith:LT]];
         NSLog(@"lt:%@",s);
//        lt.image = ImageNamed(s);
    
        images[lt_index] = ImageNamed(s);
    }
 
    if([self getOA:LB]){
        
        NSString * s = [NSString stringWithFormat:@"tooth5_%@_%@",[self getOA:LB],[self getColorWith:LB]];
        NSLog(@"lb:%@",s);
//        lb.image = ImageNamed(s);
        images[lb_index] = ImageNamed(s);
    }
    
    if([self getOA:RT]){
        
        NSString * s = [NSString stringWithFormat:@"tooth4_%@_%@",[self getOA:RT],[self getColorWith:RT]];
        NSLog(@"rt:%@",s);
//        rt.image = ImageNamed(s);
        images[rt_index] = ImageNamed(s);
    }
    if([self getOA:RB]){
        
        NSString * s = [NSString stringWithFormat:@"tooth6_%@_%@",[self getOA:RB],[self getColorWith:RB]];
        NSLog(@"rb:%@",s);
//        rb.image = ImageNamed(s);
        images[rb_index] = ImageNamed(s);
    }
    
    [self layoutIfNeeded];
    
   
}

-(void)layoutSubviews{
    [super layoutSubviews];
//    [self.subviews bk_each:^(UIView* obj) {
//        [obj removeFromSuperview];
//    }];
//
//    [images bk_each:^(UIImage* obj) {
//        UIImageView * imgv = [[UIImageView alloc] initWithImage:obj];
//        [self addSubview:imgv];
//        [imgv autoPinEdgesToSuperviewEdges];
//    }];
    
    container.image =  [self mergedImageOnMainImage:images[0] WithImageArray:images];
}


//上下牙齿
-(NSString*)getTBA:(ToothStatusViewStauts)S{
    
    if( S >>1 & 0x1) {
        return  nil;
    }
    if( S >>2 & 0x1 ){
        return  @"all";
    }
    if( S >>3 & 0x1 ){
        return  @"b";
    }
    if( S >>4 & 0x1 ){
        return  @"t";
    }
    return nil;
}

//其他牙齿
-(NSString * )getOA:(ToothStatusViewStauts)S{
    if( S >>1 & 0x1 ){
        return  nil;
    }
    if( S >>2 & 0x1 ){
        return  @"all";
    }
    if( S >>3 & 0x1 ){
        return  @"i";
    }
    if( S >>4 & 0x1 ){
        return  @"o";
    }
    return nil;
}

-(NSString*)getColorWith:(ToothStatusViewStauts)S{
    if( S >>5 & 0x1 ){
        return  @"red";
    }
    if( S>>6& 0x1 ){
        return  @"green";
    }
    if( S>>7& 0x1 ){
        return  @"yellow";
    }
    return @"";
}


- (UIImage*) mergedImageOnMainImage:(UIImage *)mainImg WithImageArray:(NSArray *)imgArray
{
    
    UIGraphicsBeginImageContext(self.frame.size);
    CGFloat w = self.frame.size.width;
    CGFloat h = self.frame.size.height;
    [mainImg drawInRect:CGRectMake(0, 0, w,h)];
    int i = 0;
    NSMutableArray * temp = [NSMutableArray arrayWithArray:imgArray];
    
    
    if(cycleImageIndex!=-1 && cycleImageIndex<cycleImageNames.count){
        [temp addObject:ImageNamed(cycleImageNames[cycleImageIndex])];
    }
    
    for (UIImage *img in temp) {
        if(i++ == 0) continue;
        [img drawInRect:self.bounds];
        i++;
    }
    
    CGImageRef NewMergeImg = CGImageCreateWithImageInRect(UIGraphicsGetImageFromCurrentImageContext().CGImage,self.bounds);
    
    UIGraphicsEndImageContext();
    if (NewMergeImg == nil) {
        return nil;
    }
    else {
        
        UIImage * img = [UIImage imageWithCGImage:NewMergeImg];
        UIGraphicsEndImageContext();
        return img;
    }
    
    return nil;
}

@end
