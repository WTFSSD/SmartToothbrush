//
//  HomeToothInfoView.m
//  Base
//
//  Created by WTFSSD on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "HomeToothInfoView.h"

@interface HomeToothInfoView(){
    UIImageView * bg;

    
    
    UIView * actionContainer;
    
    
    ///开始刷牙
    UIView * startBrushing;
    ///渐变色 layer
    CAGradientLayer *gradientLayer;
}


@property(nonatomic,strong,readwrite)ToothStatusView * toothStatus;
@property(nonatomic,strong,readwrite)ToothStatusTimerView * timerView;
@property(nonatomic,strong,readwrite)BatteryView * battery;
@property(nonatomic,strong,readwrite)ToothbrushStatusView * brush;
@property(nonatomic,strong,readwrite)CustomJumpBtns* modes;
@end;


@implementation HomeToothInfoView


+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}


-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
    
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}


-(void)setUp{
    self.backgroundColor = [UIColor clearColor];
  
    _toothStatus = [ToothStatusView instance];
    [self addSubview:_toothStatus];
    [_toothStatus autoAlignAxis:ALAxisVertical toSameAxisOfView:self];
    [_toothStatus autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:zoom(20)];
    [_toothStatus autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self withMultiplier:211/375.f];
    [_toothStatus autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:_toothStatus withMultiplier:300/211.f];
    
    [_toothStatus setStatusWithT:a_side2|c_yellow
                              B:a_no
                             LT:a_side1|c_red
                             LB:a_side1|c_red
                             RT:a_side1|c_yellow
                             RB:a_all|c_yellow];
    
    _timerView = [ToothStatusTimerView instance];
    [self insertSubview:_timerView aboveSubview:_toothStatus];
    [_timerView autoAlignAxis:ALAxisVertical toSameAxisOfView:_toothStatus];
    [_timerView autoAlignAxis:ALAxisHorizontal toSameAxisOfView:_toothStatus];
    [_timerView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self withMultiplier:118/375.f];
    [_timerView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:_timerView];
    
    _battery = [BatteryView instance];
    [self addSubview:_battery];
    [_battery autoSetDimensionsToSize:CGSizeMake(zoom(85), 65)];
    [_battery autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_toothStatus withOffset:10];
    [_battery autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:20];
    
    _brush = [ToothbrushStatusView instance];

    [self addSubview:_brush];
    [_brush autoSetDimensionsToSize:CGSizeMake(zoom(85), 65)];
    [_brush autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_toothStatus withOffset:10];
    [_brush autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self withOffset:-20];
//    _brush.replaceDay = 20;
 
    
    //mode_id 2 1 3 6
    _modes = [CustomJumpBtns customBtnsWithFrame:CGRectMake(0, 0, kScreen_Width, kScale_height*42) menuTitles:@[@"美白",@"清洁",@"敏感",@"定制"] textColorForNormal:[UIColor colorWithString:@"cde8fe"] textColorForSelect:[UIColor whiteColor] botImage:@"button_tab" isLineAdaptText:YES];
    
    [self addSubview:_modes];
    _modes.clickCallBack = ^(UIButton *btn) {
        
        BrushSettingModel m;
        if([btn.currentTitle isEqualToString:@"美白"]){
            m = BrushSettingModelWhite;
        }
        if([btn.currentTitle isEqualToString:@"清洁"]){
            m = BrushSettingModelClean;
        }
        if([btn.currentTitle isEqualToString:@"敏感"]){
            m = BrushSettingModelSensitive;
        }
        if([btn.currentTitle isEqualToString:@"定制"]){
            m = BrushSettingModelCustom;
        }
    
        
        ///切换刷牙模式
    [BrushSettingControl toggleBrushModelWith:m].then(^(NSDictionary* res){
        NSLog(@"切换刷牙模式:%@",res);
    });
        
    };
    [_modes autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_battery withOffset:20];
    [_modes autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self];
    [_modes autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self];
    [_modes autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:_modes withMultiplier:43/374.f];
    
    actionContainer  = [[UIView alloc] initWithFrame:CGRectZero];
    actionContainer.backgroundColor = [UIColor whiteColor];
    [self addSubview:actionContainer];
    
   [actionContainer autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self];
    
    [actionContainer autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self];
    [actionContainer autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self];
    [actionContainer autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:_modes];
    
    
    
    
    
    startBrushing = [[UIView alloc] initWithFrame:CGRectZero];
    startBrushing.backgroundColor = [UIColor redColor];
    [self addSubview:startBrushing];
    
    [startBrushing autoAlignAxis:ALAxisVertical toSameAxisOfView:actionContainer];
    
    [startBrushing autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:actionContainer withOffset:zoom(20)];
    [startBrushing autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:actionContainer withMultiplier:200/375.f];
    [startBrushing autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:startBrushing withMultiplier:40/200.f];
    
    UILabel * t1 = [UILabel makeWith:^(UILabel *label) {
        label.text  = @"开始刷牙";
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont systemFontOfSize:16];
    }];
    [startBrushing addSubview:t1];
    [t1 autoCenterInSuperview];
    
    UIImageView * iv1 = [[UIImageView alloc] initWithImage:ImageNamed(@"graph_toothbrush")];
    [self addSubview:iv1];
    [iv1 autoAlignAxis:ALAxisHorizontal
      toSameAxisOfView:startBrushing];
    [iv1 autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:startBrushing withOffset:-20];
    [iv1 autoSetDimensionsToSize:CGSizeMake(zoom(11), zoom(16))];
    
    

    
//
    UILabel * tip = [UILabel makeWith:^(UILabel *label) {
        label.text = @"滑动查看更多";
        label.font = [UIFont systemFontOfSize:14];
        label.textColor = [UIColor grayColor];
    }];

    [actionContainer addSubview:tip];
    [tip autoAlignAxis:ALAxisVertical toSameAxisOfView:actionContainer];
    
    [tip autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:startBrushing withOffset:5];
    
    UIImageView * iv2 = [[UIImageView alloc] initWithImage:ImageNamed(@"arrow_down")];
    [self addSubview:iv2];
     [iv2 autoAlignAxis:ALAxisHorizontal toSameAxisOfView:tip];
    [iv2 autoPinEdge:ALEdgeRight toEdge:ALEdgeLeft ofView:tip withOffset:-4];
     [iv2 autoSetDimensionsToSize:CGSizeMake(zoom(12), zoom(6))];
    
    
    
    
    [startBrushing bk_whenTapped:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"com.udo.brush.startBrushing" object:nil];
//        [_timerView start];
//        [[BrushSettingControl getBurshingData] subscribeNext:^(id x) {
//            NSLog(@"获取刷牙实时数据:%@",x);
//        }];
    }];
    
    _timerView.finish = ^{
        [BrushSettingControl stopGetBurshingData];
    };
  
}



-(void)startTimerWith:(BrushTime)mode{
    self.timerView.timeMode = mode;
    [self.timerView start];
    [self.toothStatus startAnimateWith:mode];
}

-(void)stopTimer{
    
}
-(void)layoutSubviews{
    [super layoutSubviews];
    if(!startBrushing) return;
    startBrushing.layer.cornerRadius = startBrushing.frame.size.height/2.f;
    
    if(gradientLayer){
         [gradientLayer removeFromSuperlayer];
    }
   
    startBrushing.layer.masksToBounds = YES;
    

    gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = startBrushing.bounds;  // 设置显示的frame
    gradientLayer.colors = @[(id)UIColorFromHEX(0x11CEFE).CGColor,(id)UIColorFromHEX(0x1A6CE3).CGColor];  // 设置渐变颜色
    gradientLayer.startPoint = CGPointMake(0, 0.5);   //渐变色起点 在frame中的位置占比
    gradientLayer.endPoint = CGPointMake(1, 0.5);     //渐变色终点点 在frame中的位置占比
    
    //添加layer 防止覆盖subviews
    [startBrushing.layer insertSublayer:gradientLayer atIndex:0];
}



@end
