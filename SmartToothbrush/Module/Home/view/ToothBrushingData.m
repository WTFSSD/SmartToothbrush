
//
//  ToothBrushingData.m
//  Base
//
//  Created by WTFSSD on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "ToothBrushingData.h"

#import "ToothStatusView.h"
@interface ToothBrushingData(){
    UILabel * titleLabel;
    
    UILabel * tip1;
    UILabel * tip2;
    ToothStatusView * status1;
    ToothStatusView * status2;
    
    
    UILabel * status1Label;
    UILabel * status2Label;
    
    UIStackView * containerView;
}
@end

@implementation ToothBrushingData

+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}


-(void)awakeFromNib{
    [super awakeFromNib];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
        self.titles = @[];
        self.title = @"本次刷牙数据";
        
      
      
    }
    return self;
}
-(void)setUp{
    titleLabel = [UILabel makeWith:^(UILabel *label) {
        label.textColor = UIColorFromHEX(0x70879E);
        label.font = [UIFont systemFontOfSize:16];
        label.text = @"本次刷牙数据";
        
    }];
    
    status1Label =  [UILabel makeWith:^(UILabel *label) {
        label.textColor = UIColorFromHEX(0x70879E);
        label.font = [UIFont systemFontOfSize:16];
       
    }];;
    status2Label =  [UILabel makeWith:^(UILabel *label) {
        label.textColor = UIColorFromHEX(0x70879E);
        label.font = [UIFont systemFontOfSize:16];
       
    }];
    
    [self addSubview:status1Label];
    [self addSubview:status2Label];
    [self addSubview:titleLabel];
    tip1 = [UILabel makeWith:^(UILabel *label) {
        label.textColor = UIColorFromHEX(0xFF7586);
        label.font = [UIFont systemFontOfSize:14];
        label.numberOfLines = 2;
        label.textAlignment = NSTextAlignmentCenter;
        label.text = @"红色区域表示刷牙力度过重";
       
    }];
    [self addSubview:tip1];
    tip2 = [UILabel makeWith:^(UILabel *label) {
        label.textColor = UIColorFromHEX(0xE8BB1A);
        label.font = [UIFont systemFontOfSize:14];
        label.numberOfLines = 2;
        label.textAlignment = NSTextAlignmentCenter;
        label.text = @"黄色区域表示清洁情况待改善";
    }];
    [self addSubview:tip2];
    
    [titleLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [titleLabel autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:10];
    
    
    status1 = [ToothStatusView instance];
    status2 = [ToothStatusView instance];
    
    
    containerView = [[UIStackView alloc] initWithArrangedSubviews:@[status1,status2]];
    
    
    containerView.axis = UILayoutConstraintAxisHorizontal;
  
    containerView.distribution = UIStackViewDistributionFillEqually;
    containerView.spacing = 10;


    [self addSubview:containerView];
    
    [containerView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self];
    [containerView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:25];
    [containerView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self];
    [containerView autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:tip1 withOffset:-5];
    
    [self addSubview:tip1];
    [self addSubview:tip2];
    
    
    [tip1 autoAlignAxis:ALAxisVertical toSameAxisOfView:status1];
    [tip1 autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self withOffset:-5];
    [tip1 autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:status1 withMultiplier:0.6];
    
    [tip2 autoAlignAxis:ALAxisVertical toSameAxisOfView:status2];
    [tip2 autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self withOffset:-5];
    [tip2 autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:status2 withMultiplier:0.6];
    
    
    [status1Label autoAlignAxis:ALAxisVertical toSameAxisOfView:status1];
    [status1Label autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:containerView withOffset:0];
    [status2Label autoAlignAxis:ALAxisVertical toSameAxisOfView:status2];
    [status2Label autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:containerView withOffset:0];
    
    
    
    [status1 setStatusWithT:a_no
                          B:a_no
                         LT:a_side1|c_red
                         LB:a_side1|c_red
                         RT:a_no
                         RB:a_no];
    [status2 setStatusWithT:a_side2|c_yellow
                          B:a_no
                         LT:a_no|c_red
                         LB:a_no|c_red
                         RT:a_side1|c_yellow
                         RB:a_all|c_yellow];
    
                RACSignal * titleSignal =  [[self rac_valuesForKeyPath:@"title" observer:nil] map:^id(id value) {
                    if(value){
                        return @[@(YES),value];
                    }else{
        
                    } return @[@(NO),@""];
                }];
    
    
    
    RACSignal * titlesSignal =  [[self rac_valuesForKeyPath:@"titles" observer:nil] map:^id(id value) {
        if(value && [value count] == 2){
            return @[@(YES),value];
        }else{
            return @[@(NO),@[@"",@""]];
        }
    }];
     RACSignal * resSignal = [RACSignal combineLatest:@[titleSignal,titlesSignal]
                       reduce:^id(NSArray * titleArray,NSArray * titlesArray){
                           if([titlesArray[0] boolValue]){
                              return @[@(YES),titlesArray[1]];
                           }else{
                                return @[@(NO),titleArray[1]];
                           }
                       }];
    
    [resSignal map:^id(id value) {
        return value;
    }];
    RAC(titleLabel,hidden) = [resSignal map:^id(id value) {
        return value[0];
    }];
    RAC(status1Label,hidden) = [resSignal map:^id(id value) {
        return @(![value[0] boolValue]);
    }];
    RAC(status2Label,hidden) = [resSignal map:^id(id value) {
        return @(![value[0] boolValue]);
    }];
    RAC(titleLabel,text) = [resSignal map:^id(id value) {
         return ![value[0] boolValue] ? value[1]:nil;
    }];
    RAC(status1Label,text) = [resSignal map:^id(id value) {
        
        return [value[0] boolValue] ? value[1][0]:nil;
    }];
    RAC(status2Label,text) = [resSignal map:^id(id value) {
        return [value[0] boolValue] ? value[1][1]:nil;
    }];
}


@end
