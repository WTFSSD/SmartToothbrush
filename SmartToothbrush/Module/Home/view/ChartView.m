//
//  ChartView.m
//  Base
//
//  Created by WTFSSD on 2018/4/29.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "ChartView.h"

#import "WKWebViewController.h"
#import <WebKit/WebKit.h>

#import "HYCacheURLProtocol.h"


@implementation ChartViewData

+(instancetype)instanceWith:(NSString *)time score:(CGFloat)score{
   ChartViewData * data =  [[self alloc] init];
    data.time = time;
    data.score = score;
    return data;
}



+(NSString * )tramforArray:(NSArray<ChartViewData*> *)array{
    if(!array) return  @"";
    NSMutableString * json = [NSMutableString stringWithFormat:@"["];
    

    [array enumerateObjectsUsingBlock:^(ChartViewData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [json appendFormat:@"{\"time\":\"%@\",\"score\":%.0f},",obj.time,obj.score];
    }];
    
    [json appendString:@"]"];
    return json;
}
@end

@interface ChartView()<WKNavigationDelegate,WKScriptMessageHandler,WKUIDelegate>{
    WKUserContentController* userCC;
    WKWebView* webView;
    
    WKUserScript * script;
    NSString * url;
    
    NSArray * datas;
    
    NSTimer * timer;
}
@end


@implementation ChartView

+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}


-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor grayColor];
        [self setUp];
    }
    return self;
}





-(void)setUp{
    url = @"http://192.168.0.101/tooth/chart.html";
    
    userCC = [[WKUserContentController alloc] init];
    
    NSString* jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);user-scalable=no;";
    WKUserScript* userScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    [userCC addUserScript:userScript];
    [userCC addScriptMessageHandler:self name:@"onMessage"];
    WKWebViewConfiguration* config = [[WKWebViewConfiguration alloc] init];
    config.preferences.javaScriptCanOpenWindowsAutomatically = YES;
    config.preferences.javaScriptEnabled = true;
    config.userContentController = userCC;
    
    webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:config];
    webView.navigationDelegate = self;

    [self addSubview:webView];
    [webView autoPinEdgesToSuperviewEdges];
    [self loadFile];
    webView.scrollView.scrollEnabled = false;
    [HYCacheURLProtocol startListeningNetworking];
   
}

-(void)setTestData:(BOOL)testData{
    if (testData) {
        if(timer) return;
        @weakify(self);
        [NSTimer bk_scheduledTimerWithTimeInterval:2 block:^(NSTimer *t) {
            @strongify(self);
            [self randData];
        } repeats:YES];
    }else{
         if(!timer) return;
        [timer invalidate];
        timer = nil;
    }
}

-(void)loadFile{
    NSURL * path = [[NSBundle mainBundle] URLForResource:@"chart" withExtension:@"html"];
    [webView loadFileURL:path allowingReadAccessToURL:path];
}

-(void)refresh{
    [self loadFile];
    
}

-(void)randData{
    [self postMessage:@"drawChart" data:@[
                                          [ChartViewData instanceWith:@"2018-4-1" score:arc4random()%100],
                                          [ChartViewData instanceWith:@"2018-4-2" score:arc4random()%100],
                                          [ChartViewData instanceWith:@"2018-4-3" score:arc4random()%100],
                                          [ChartViewData instanceWith:@"2018-4-4" score:arc4random()%100],
                                          [ChartViewData instanceWith:@"2018-4-5" score:arc4random()%100],
                                          [ChartViewData instanceWith:@"2018-4-6" score:arc4random()%100],
                                          [ChartViewData instanceWith:@"2018-4-7" score:arc4random()%100],
                                          [ChartViewData instanceWith:@"2018-4-8" score:arc4random()%100],
                                          [ChartViewData instanceWith:@"2018-4-9" score:arc4random()%100],
                                          [ChartViewData instanceWith:@"2018-4-10" score:arc4random()%100],
                                          ]];
};


-(void)setDatas:(NSArray<ChartViewData*>*)data{
    datas = data;
    [self postMessage:@"drawChart" data:data];
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    [self onMessage:message];
    
}

- (void)addUserScript:(WKUserScript *)userScript{
    DLog(@"WKUserScript:%@",userScript);
    
}


-(void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{
    
    completionHandler();
}


-(void)onMessage:(WKScriptMessage * )message{
    if(!message.body)return;
    if([message.body isKindOfClass:[NSDictionary class]]){
        NSString * target = message.body[@"target"];
        id data = message.body[@"data"];
        if([target isEqualToString:@"onLoad"])[self onChartLoad];
    }
}


-(void)postMessage:(NSString * )target data:(id)data{
    
    NSString * jsStr = [NSString stringWithFormat:@"onMessage(\"%@\",%@)",target,[ChartViewData tramforArray:data]];
    
    [webView evaluateJavaScript:jsStr completionHandler:^(id _Nullable d, NSError * _Nullable error) {
        DLog(@"error:%@",error);
    }];
}


-(void)onChartLoad{
    if(datas){
        [self setDatas:datas];
        datas = nil;
    }
}








@end
