//
//  TomeToothMoreInfoView.h
//  Base
//
//  Created by WTFSSD on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToothScoreView.h"

#import "ToothBrushingData.h"
@interface HomeToothMoreInfoView : UIView


@property(nonatomic,strong,readonly)  ToothScoreView * score;
@property(nonatomic,strong,readonly)  ToothBrushingData * dataView;
+(instancetype)instance;
@end
