//
//  HomeToothInfoView.h
//  Base
//
//  Created by WTFSSD on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ToothStatusView.h"

#import "ToothStatusTimerView.h"

#import "BatteryView.h"
#import "ToothbrushStatusView.h"

#import "CustomJumpBtns.h"




@interface HomeToothInfoView : UIView



///牙齿状态
@property(nonatomic,strong,readonly)ToothStatusView * toothStatus;

///定时器
@property(nonatomic,strong,readonly)ToothStatusTimerView * timerView;

///电量
@property(nonatomic,strong,readonly)BatteryView * battery;

///牙刷
@property(nonatomic,strong,readonly)ToothbrushStatusView * brush;


///刷牙模式按钮
@property(nonatomic,strong,readonly)CustomJumpBtns* modes;


-(void)startTimerWith:(BrushTime)mode;

-(void)stopTimer;

+(instancetype)instance;
@end
