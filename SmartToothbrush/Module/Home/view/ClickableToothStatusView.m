//
//  ClickableToothStatusView.m
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/8/8.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import "ClickableToothStatusView.h"


@interface ClickableToothStatusView(){
   UIImageView * container;
   NSMutableArray * images;
}


@property(nonatomic,strong,readonly)NSArray * points;





@end

@implementation ClickableToothStatusView

+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        self.showBorder = YES;
        [self setUp];
        
    }
    return self;
}

-(void)setUp{
    images = [NSMutableArray arrayWithCapacity:33];
    
    for (int i = 0; i<32; i++) {
        images[i] = @(NO);
    }
    
    container = [[UIImageView alloc] initWithFrame:CGRectZero];
    
    container.userInteractionEnabled = YES;
    
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onContainerClikck:)];

    
    [container addGestureRecognizer:gesture];
    
    [self addSubview:container];
    [container autoPinEdgesToSuperviewEdges];
    
    [self setImage];
}


-(void)onContainerClikck:(UITapGestureRecognizer*)gesture{
    CGPoint p = [gesture locationInView:self];
    
    
    NSInteger clickIndex = [self getClickIndex:p];
    NSLog(@"点击下标:%ld",clickIndex);
    if(clickIndex != NSNotFound && clickIndex!=0){
        id obj = images[clickIndex-1];
    
        if([obj isKindOfClass:NSClassFromString(@"UIImage")]){
            images[clickIndex-1] = @(NO);
        }else{
            NSString * s = [NSString stringWithFormat:@"tooth_%ld",clickIndex];
            NSLog(@"点击图片:%@",s);
            if(ImageNamed(s)){
                images[clickIndex-1] =ImageNamed(s);
            }
        }
        
        [self setImage];
    }
    
}

-(CGRect)makeReactWith:(NSDictionary*)dict{
    
    CGFloat w = container.frame.size.width;
    CGFloat h = container.frame.size.height;
    return CGRectMake(
                      [dict[@"x"] floatValue] * w,
                      [dict[@"y"] floatValue] * h,
                      [dict[@"w"] floatValue]* w,
                      [dict[@"h"] floatValue] * h);
}


-(void)setImage{
    NSArray * imgs = [images bk_select:^BOOL(id obj) {
        return [obj isKindOfClass:NSClassFromString(@"UIImage")];
    }];
    container.image = [self mergedImageOnMainImage:ImageNamed(@"tooth0_all") WithImageArray:imgs];
    

}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    
    if(self.showBorder){
        [container.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj removeFromSuperview];
        }];
        
        [self.points enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            UIView * v = [[UIView alloc] initWithFrame:[self makeReactWith:obj]];
            [container addSubview:v];
            v.layer.borderColor = [UIColor blackColor].CGColor;
            v.layer.borderWidth = 0.5;
            
        }];
    }
   
}

-(NSInteger)getClickIndex:(CGPoint)p{
    __block NSInteger index = NSNotFound;
    CGRect clickReact = CGRectMake(p.x, p.y, 1, 1);
    [self.points enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CGRect react2 = [self makeReactWith:obj];
        if(CGRectContainsRect(react2, clickReact)){
            index = [obj[@"index"] integerValue];
            *stop = YES;
        }
    }];

    return index;
}

-(NSArray *)points{
    return @[
             @{@"index":@(2),@"x":@(0.49867021276595747),@"y":@(0.015625),@"w":@(0.1199468085106383),@"h":@(0.08003472222222223)},
             @{@"index":@(3),@"x":@(0.6079787234042554),@"y":@(0.041666666666666664),@"w":@(0.10664893617021277),@"h":@(0.08177083333333333)},
             @{@"index":@(4),@"x":@(0.6747340425531915),@"y":@(0.08350694444444448),@"w":@(0.13058510638297874),@"h":@(0.08003472222222223)},
             @{@"index":@(5),@"x":@(0.7093085106382978),@"y":@(0.14600694444444448),@"w":@(0.13856382978723406),@"h":@(0.07135416666666666)},
             @{@"index":@(6),@"x":@(0.7492021276595744),@"y":@(0.2034722222222223),@"w":@(0.14122340425531915),@"h":@(0.06961805555555556)},
             @{@"index":@(7),@"x":@(0.7787234042553192),@"y":@(0.26093749999999993),@"w":@(0.14654255319148937),@"h":@(0.08177083333333333)},
             @{@"index":@(8),@"x":@(0.8),@"y":@(0.33038194444444435),@"w":@(0.15212765957446808),@"h":@(0.08215277777777778)},
             @{@"index":@(9),@"x":@(0.8159574468085107),@"y":@(0.40173611111111107),@"w":@(0.14654255319148937),@"h":@(0.08350694444444445)},
             @{@"index":@(10),@"x":@(0.8265957446808511),@"y":@(0.5286458333333334),@"w":@(0.14654255319148937),@"h":@(0.08524305555555556)},
             @{@"index":@(11),@"x":@(0.8026595744680851),@"y":@(0.6034722222222221),@"w":@(0.1547872340425532),@"h":@(0.08350694444444445)},
             @{@"index":@(12),@"x":@(0.7813829787234042),@"y":@(0.6730902777777779),@"w":@(0.14388297872340425),@"h":@(0.09045138888888889)},
             @{@"index":@(13),@"x":@(0.7970744680851064),@"y":@(0.7494791666666667),@"w":@(0.14122340425531915),@"h":@(0.07309027777777778)},
             @{@"index":@(14),@"x":@(0.7066489361702127),@"y":@(0.8034722222222221),@"w":@(0.13590425531914893),@"h":@(0.07482638888888889)},
             @{@"index":@(15),@"x":@(0.6534574468085106),@"y":@(0.8557291666666669),@"w":@(0.13058510638297874),@"h":@(0.07482638888888889)},
             @{@"index":@(16),@"x":@(0.5973404255319149),@"y":@(0.8869791666666669),@"w":@(0.08537234042553192),@"h":@(0.07829861111111111)},
             @{@"index":@(17),@"x":@(0.5066489361702128),@"y":@(0.9043402777777779),@"w":@(0.09335106382978724),@"h":@(0.07829861111111111)},
             @{@"index":@(18),@"x":@(0.41063829787234035),@"y":@(0.9043402777777779),@"w":@(0.09601063829787235),@"h":@(0.07829861111111111)},
             @{@"index":@(19),@"x":@(0.32526595744680853),@"y":@(0.8869791666666669),@"w":@(0.09069148936170213),@"h":@(0.08177083333333333)},
             @{@"index":@(20),@"x":@(0.23723404255319147),@"y":@(0.8574652777777779),@"w":@(0.12260638297872341),@"h":@(0.07482638888888889)},
             @{@"index":@(21),@"x":@(0.1680851063829787),@"y":@(0.8069444444444444),@"w":@(0.13324468085106383),@"h":@(0.07309027777777778)},
             @{@"index":@(22),@"x":@(0.11728723404255317),@"y":@(0.754861111111111),@"w":@(0.14388297872340425),@"h":@(0.06961805555555556)},
             @{@"index":@(23),@"x":@(0.07473404255319148),@"y":@(0.6817708333333334),@"w":@(0.14654255319148937),@"h":@(0.08527777777777777)},
             @{@"index":@(24),@"x":@(0.0425531914893617),@"y":@(0.6138888888888887),@"w":@(0.15212765957446808),@"h":@(0.08177083333333333)},
             @{@"index":@(25),@"x":@(0.026595744680851064),@"y":@(0.5338541666666666),@"w":@(0.14388297872340425),@"h":@(0.08524305555555556)},
             @{@"index":@(26),@"x":@(0.034574468085106384),@"y":@(0.4086805555555555),@"w":@(0.14654255319148937),@"h":@(0.08177083333333333)},
             @{@"index":@(27),@"x":@(0.034574468085106384),@"y":@(0.33732638888888883),@"w":@(0.14654255319148937),@"h":@(0.0765625)},
             @{@"index":@(28),@"x":@(0.06143617021276594),@"y":@(0.26614583333333325),@"w":@(0.14920212765957447),@"h":@(0.08177083333333333)},
             @{@"index":@(29),@"x":@(0.09867021276595743),@"y":@(0.2052083333333334),@"w":@(0.13856382978723406),@"h":@(0.06614583333333333)},
             @{@"index":@(30),@"x":@(0.1305851063829787),@"y":@(0.14427083333333338),@"w":@(0.13590425531914893),@"h":@(0.08003472222222223)},
             @{@"index":@(31),@"x":@(0.19420212765957443),@"y":@(-0.26371527777777776),@"w":@(0.13058510638297874),@"h":@(0.07829861111111111)},
             @{@"index":@(21),@"x":@(0.28218085106382984),@"y":@(0.041666666666666664),@"w":@(0.10398936170212766),@"h":@(0.08003472222222223)},
             @{@"index":@(1),@"x":@(0.3813829787234042),@"y":@(0.015625),@"w":@(0.1199468085106383),@"h":@(0.08003472222222223)}
             ];
}

- (UIImage*) mergedImageOnMainImage:(UIImage *)mainImg WithImageArray:(NSArray *)imgArray
{
    if (!imgArray) {
        return mainImg;
    }
    
    if([imgArray count]<=0){
        return mainImg;
    }
    
    UIGraphicsBeginImageContext(self.frame.size);
    CGFloat w = self.frame.size.width;
    CGFloat h = self.frame.size.height;
    [mainImg drawInRect:CGRectMake(0, 0, w,h)];
    int i = 0;
    for (UIImage *img in imgArray) {
        if(i++ == 0) continue;
        [img drawInRect:self.bounds];
        i++;
    }
    
    CGImageRef NewMergeImg = CGImageCreateWithImageInRect(UIGraphicsGetImageFromCurrentImageContext().CGImage,self.bounds);
    
    UIGraphicsEndImageContext();
    if (NewMergeImg == nil) {
        return nil;
    }
    else {
        
        UIImage * img = [UIImage imageWithCGImage:NewMergeImg];
        UIGraphicsEndImageContext();
        return img;
    }
    
    return nil;
}
@end
