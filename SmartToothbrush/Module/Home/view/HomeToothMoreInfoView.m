//
//  TomeToothMoreInfoView.m
//  Base
//
//  Created by WTFSSD on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import "HomeToothMoreInfoView.h"


@interface HomeToothMoreInfoView(){

    UIView * chartContainer;
    

}


@property(nonatomic,strong,readwrite)    ToothScoreView * score;
@property(nonatomic,strong,readwrite)   ToothBrushingData * dataView;
@end

@implementation HomeToothMoreInfoView

+(instancetype)instance{
    return [[self alloc] initWithFrame:CGRectZero];
}


-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUp];
    
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}


-(void)setUp{
    chartContainer = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self setViewStyle:chartContainer];
    [self addSubview:chartContainer];

    [chartContainer autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:10];
    [chartContainer autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:12];
    [chartContainer autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self withOffset:-24];
    [chartContainer autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:chartContainer withMultiplier:215/351.f];
    
    
    _score = [ToothScoreView instance];
    [chartContainer addSubview:_score];
    
    _score.title = @"本周刷牙得分";
    
    [_score autoPinEdgesToSuperviewEdges];
    
    
    _dataView = [ToothBrushingData instance];
    [self setViewStyle:_dataView];
    [self addSubview:_dataView];
    
    [_dataView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:chartContainer withOffset:10];
    [_dataView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:12];
    [_dataView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self withOffset:-24];
    [_dataView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionWidth ofView:_dataView withMultiplier:327/351.f];

    _dataView.title = @"本次刷牙数据";

}


-(void)setViewStyle:(UIView*)v{
    v.backgroundColor = [UIColor whiteColor];
    v.layer.cornerRadius = 5;
    v.layer.masksToBounds = YES;
}
@end
