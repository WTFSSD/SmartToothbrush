//
//  ToothbrushStatusView.h
//  Base
//
//  Created by WTFSSD on 2018/4/30.
//  Copyright © 2018年 XHY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToothbrushStatusView : UIView

///更换时间
@property(nonatomic)CGFloat replaceDay;

+(instancetype)instance;
@end
