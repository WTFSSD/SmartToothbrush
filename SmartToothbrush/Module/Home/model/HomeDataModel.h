//
//  HomeDataModel.h
//  SmartToothbrush
//
//  Created by WTFSSD on 2018/6/21.
//  Copyright © 2018年 wtfssd. All rights reserved.
//

#import <Foundation/Foundation.h>


///刷牙压力数据模型
@interface PressModel:NSObject
///刷牙面
@property(nonatomic,strong)NSString *surface;
///力度
@property(nonatomic,strong)NSString *value;
@end

///刷牙得分数据模型
@interface ScoreModel:NSObject
@property(nonatomic,strong)NSString * date;
@property(nonatomic,strong)NSString * score;
@end

///首页数据模型
@interface HomeModel :NSObject
///电量
@property(nonatomic,strong)NSString *electric;
///刷头剩余天数
@property(nonatomic,strong)NSString *day;
///本周刷牙得分
@property(nonatomic,strong)NSArray<ScoreModel*>*week;
///压力
@property(nonatomic,strong)NSArray<PressModel*>* press;
///清洁度
@property(nonatomic,strong)NSArray<PressModel*>* clear;

@end


///首页接口数据返回
@interface HomeDataModel : NSObject
@property(nonatomic,strong)NSString* code;
@property(nonatomic,strong)NSString* msg;
@property(nonatomic,strong)HomeModel* data;



@end
